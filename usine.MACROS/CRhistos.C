/************************************************************************
  ROOT macro to get empty 1D histogram whose labels are CRs or Elements
   > root -l
   > gSystem->Load("../lib/libUsine.so");
   > .x CRhistos.C
*************************************************************************/
#include "../include/TUMisc.h"

void CRhistos() {
   TCanvas *c = new TCanvas("LabelsCRsList", "LabelsCRsList", 600, 600);
   c->SetLogx(kFALSE);
   c->SetLogy(kFALSE);
   c->Divide(1,2);

   string f_init = TUMisc::GetPath("$USINE/inputs/init.TEST.par");
   TUInitParList *init_pars = new TUInitParList(f_init, false, stdout);
   TUCRList *cr_list = new TUCRList(init_pars, false, stdout);

   // Draw upper histo
   TH1D *h_cr = cr_list->OrphanEmptyHistoOfCRs("h_cr","Labels from CR list","2H-BAR","12C");
   c->cd(1);
   h_cr->SetTitle("Empty 1D histo of CR names as labels");
   h_cr->Draw();

   // Draw lower histo
   TH1D *h_elem = cr_list->OrphanEmptyHistoOfElements("h_elem","Labels from elements in CR list");
   c->cd(2);
   h_elem->SetTitle("Empty 1D histo of element names as labels");
   h_elem->Draw();
}
