// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUInteractions.h"
#include "../include/TUMath.h"
#include "../include/TUModel2DKisoVc.h"
#include "../include/TUNumMethods.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUModel2DKisoVc                                                      //
//                                                                      //
// Thin disc/halo 2D model with constant wind + homogeneous diffusion.  //
//                                                                      //
// The 2DKisoVc model (isotropic diffusion, constant Galactic wind) is  //
// a 2D propagation model (steady-state, disc of size R with diffusive  //
// halo). It is semi-analytical because the solution without E gains or //
// losses) is algebraic, the general equation being 2nd order in energy //
// (solved using an explicit numerical method, as discussed, e.g., in   //
// TUNumMethods::SolveEq_1D2ndOrder_Explicit).                          //
//                                                                      //
// See the online documentation for more details and references.        //
//////////////////////////////////////////////////////////////////////////


ClassImp(TUModel2DKisoVc)

//______________________________________________________________________________
TUModel2DKisoVc::TUModel2DKisoVc() : TUModelBase(), TUBesselJ0()
{
   // ****** Default constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUModel2DKisoVc::~TUModel2DKisoVc()
{
   // ****** default destructor ******

   Initialise(true);
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::Ai(Int_t i_bess, Int_t j_cr, Int_t k_ekn, Double_t const &kdiff)
{
   //--- Returns Ai = 2.*h*gamma_inel + Vc + K*Si*coth(Si*L/2.)   [kpc/Myr]
   //    [Vc]=kpc/Myr, [kdiff]=kpc^2/Myr, [L]=kpc, [Si]=/kpc.
   //
   //  i_bess            Bessel index
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  kdiff             Diffusion coefficient for this CR and E [kpc^2/Myr]

   Double_t si = Si(i_bess, j_cr, k_ekn,  kdiff);
   return Ai(si, j_cr, k_ekn, kdiff);
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::Ai(Int_t i_bess, Int_t j_cr, Int_t k_ekn)
{
   //--- Returns Ai = 2.*h*gamma_inel + Vc + K*Si*coth(Si*L/2.)   [kpc/Myr]
   //
   //  i_bess            Bessel index
   //  j_cr              CR index
   //  k_ekn             Energy index

   return Ai(i_bess, j_cr, k_ekn, ValueK(j_cr, k_ekn));
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::Ai(Double_t const &si, Int_t j_cr, Int_t k_ekn, Double_t const &kdiff)
{
   //--- Returns Ai = 2.*h*gamma_inel + Vc + K*Si*coth(Si*L/2.)   [kpc/Myr}
   //    [Vc]=kpc/Myr, [kdiff]=kpc^2/Myr, [L]=kpc, [Si]=/kpc.
   //
   //  si                Si term
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  kdiff             Diffusion coefficient for this CR and E [kpc^2/Myr]


   // If inelastic interaction
   Double_t gamma_inel = 0.;
   if (TUPropagSwitches::IsDestruction())
      gamma_inel = TUXSections::Get_nvSigTotIA_ISM0D(j_cr, k_ekn);

   // The implementation TUMath::Coth (which includes an asymptotic expansion),
   // is preferred over coth (which diverges for large arguments)
   return 2.* GetParVal_h() * gamma_inel + ValueVc_kpcperMyr()
          + kdiff * si * TUMath::Coth(si * GetParVal_L() * 0.5);
}

//______________________________________________________________________________
Bool_t TUModel2DKisoVc::CalculateChargedCRs(Int_t jcr_start, Int_t jcr_stop, Bool_t is_verbose, FILE *f_log)
{
   //--- Computes differential density N0_i^j at z=0 (i=Fourier-Bessel coef index,
   //    j=(anti-)nuclei index) required to evaluate the propagated  differential
   //    density for any (r,z) in this 2D diffusion model.
   //    N.B.: Ni(z) is constructed from Ni(0) elsewhere.
   //  jcr_start         Index of CR for first to calculate
   //  jcr_stop          Index of CR for last to calculate
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE
   //
   //    For each i and j, the differential equation for N0_i^j with E losses (B^j),
   //    gains (C^j), and source term (S_i^j), see details in the class documentation,
   //    reads:
   //       A_i^j(Ekn)*N0_i^j(Ekn) + 2hd/dEtot( B^j*N0_i^j - C^j dN0_i^j/dEtot ) = S_i^j(Ekn)
   //    which is recast into the more-convenient form (we drop i and j for simplicity)
   //       N0 + (fCoefPrefactor * d/dLnEkn(fCoef1stOrder*N0 -  fCoef2ndOrderHalfBin * dN0/dLnEkn) = Sterm/A
   //    with
   //       fCoefPrefactor = 1/(A*Ek)
   //       fCoef1stOrder  = B
   //       fCoef2ndOrderHalfBin  = C/Ek
   //    N.B.: if neither losses nor energy gains (i.e., B^j=C^j=0), dNdEkn=Sterm;
   //          otherwise a numerical inversion is required.
   //    N.B.: if for some reason, some parameters of the models have been changed
   //          (e.g. K), do not forget to update FillCoefPreFactorAndAiTerm().
   //
   //    In the following, Sterm_i^j is the sum of various contributions (all taken
   //    at z=0) that includes
   //       (1)  Secondary contributions
   //       (2a) Primary standard contributions (in disc)
   //       (2b) Primary exotic contributions (in halo)
   //       (3)  BETA-radioactive contribution
   //       (4)  EC-radioactive contribution [TODO]
   //    The subtleties are that
   //      (i) the first (standard prim.) and the third (secondary) contributions occurs only for z=0 so that
   //          - they never depend on z
   //          - they are calculated as a composite function 2h*.../Ai
   //      (ii) the second (exotic), fourth and fifth (radioactive) contributions occur for any z
   //          - they are calculated for z=0 only in this routine (to calculate Ni(0))
   //          - there are dedicated functions Ni_PrimContrib... that directly calculate them.
   //   N.B.: We chose [Sterm^j]=[Ni] = (GeV/n)^{-1}.m^{-3}

   string indent = TUMessages::Indent(true);

   // Check indices to run
   if (jcr_start < 0)
      jcr_start = 0;
   if (jcr_stop > GetNCRs() - 1)
      jcr_stop = GetNCRs() - 1;
   if (jcr_start > jcr_stop)
      TUMessages::Warning(f_log, "TUModel2DKisoVc", "CalculateChargedCRs", "jcr_start>jcr_stop, nothing to do");

   Bool_t is_check = false;
   if (is_verbose && is_check)
      fprintf(f_log, "%sTUModel2DKisoVc::CalculateChargedCRs\n", indent.c_str());

   // Allocate source terms: [sterm] = [Ni] = (GeV/n)^{-1}.m^{-3}
   Double_t *sterm_tot = new Double_t[GetNE()];
   Double_t *sterm_prim = new Double_t[GetNE()];

   // Loop on CRs (heaviest to lightest)
   for (Int_t j_cr = jcr_stop; j_cr >= jcr_start; --j_cr) {
      if (is_verbose && is_check)
         fprintf(f_log, "%s   - j_cr=%d (%s)\n", indent.c_str(), j_cr, GetCREntry(j_cr).GetName().c_str());

      // If BETA rad parent for j_cr (and BETA decay mode
      // switched on), store its index for further usage
      Int_t j_radbeta_parent = -1;
      if (TUPropagSwitches::IsDecayFedBETA())
         j_radbeta_parent = TUCRList::IndexParentBETA(j_cr);

      // Loop on NBessel coefficients
      for (Int_t i_bess = GetNBessel() - 1; i_bess >= 0; --i_bess) {

         /*****************************/
         /* A. Calculate source terms */
         /*****************************/
         // Siterm = (2hQi_prim+2hQi_sec+Qi_rad) / Aiterm
         // [/(GeV/n m3)] = [kpc/(GeV/n m3 Myr)] / [kpc/Myr]
         // N.B.: in practice we calculate sterm, and multiply by 2h/Aiterm
         // after having added all contributions (in order to get Siterm)

         // Initialise [Sterm] = [Ni] = (GeV/n)^{-1}.m^{-3}
         // first used as [sterm] = (GeV/n)^{-1}.m^{-3} / Myr
         for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {
            sterm_tot[k_ekn] = 0.;
            sterm_prim[k_ekn] = 0.;
         }


         // A(1) Secondary contrib. (parent+targ->j_cr)
         //-------------------------------------------
         if (TUPropagSwitches::IsSecondaries()) {
            // N.B.: valid whether straight-ahead approx. or differential production
            //   sterm_sec  =  Q_sec
            // [/(GeV/n m3 Myr)] = [/(GeV/n m3 Myr)]

            // Loop on list of parents (for j_cr)
            for (Int_t k_in_lop = TUCRList::GetNParents(j_cr) - 1; k_in_lop >= 0; --k_in_lop) {
               // If only zeros in X-sec, skip it!
               if (TUModelBase::IsProdNull(k_in_lop, j_cr))
                  continue;

               Int_t k_in_lon = TUCRList::IndexInCRList_Parent(j_cr, k_in_lop);

               // Adds in sterm_tot secondary production from ni_proj. Two cases:
               //    1: secondary production from a BETA parent with local underdensity
               //    2: no underdensity (regular 2D model)
               if (TUPropagSwitches::IsDecayFedBETA() && TUCRList::GetCREntry(k_in_lon).IsUnstable(true)
                     && GetParVal_rh() > 1.e-5) {
                  // Local underdensity and secondary production from a radioactive parent
                  // N.B.: watch out, this is tricky!
                  // If rhole>0, a damping factor exp(-r_hole/sqrt(K/gamma_rad)) was
                  // applied to the flux of the radioactive species in the disc (z=0),
                  // see below. This damping does not apply outside the local bubble:
                  // as secondary production in the thin disc comes from a spatial region
                  // much larger than r_hole (Taillet and Maurin, 2003), the spallative
                  // contribution from a radioactive parent must consider the un-damped flux.
                  Double_t *ni_proj = new Double_t[GetNE()];
                  for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {
                     Double_t damping =  exp(-GetParVal_rh() / sqrt(ValueK(k_in_lon, k_ekn)
                                             / TUAxesCrE::GammaRadBETA_perMyr(k_in_lon, k_ekn)));
                     ni_proj[k_ekn] = GetNiTot(i_bess, k_in_lon, k_ekn) / damping;
                  }
                  TUXSections::SecondaryProduction(ni_proj, k_in_lop, j_cr, fISMEntry,
                                                   sterm_tot, GetEpsIntegr(), true /*is_ism0D*/, f_log);
                  delete[] ni_proj;
                  ni_proj = NULL;
               } else {
                  // Otherwise, standard calculation
                  Double_t *ni_proj = GetNiTot(i_bess, k_in_lon);
                  TUXSections::SecondaryProduction(ni_proj, k_in_lop, j_cr, fISMEntry,
                                                   sterm_tot, GetEpsIntegr(), true /*is_ism0D*/, f_log);
                  ni_proj = NULL;
               }
            }

            // If current nucleus BETA-radioactive and local bubble, then there is a damping
            // factor to account for (see Donato et al. 2003 or Putze et al., 2010).
            if (TUPropagSwitches::IsDecayBETA() && TUCRList::GetCREntry(j_cr).IsUnstable(true)
                  && GetParVal_rh() > 1.e-5) {
               for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {
                  Double_t damping = exp(-GetParVal_rh() / sqrt(ValueK(j_cr, k_ekn)
                                         / TUAxesCrE::GammaRadBETA_perMyr(j_cr, k_ekn)));
                  sterm_tot[k_ekn] *= damping;
               }
            }
         }

         // All other contributions (within an ekn loop)
         for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {

            // A(2a) Primary standard contrib.
            //--------------------------------
            if (TUPropagSwitches::IsPrimStandard() && !TUAxesCrE::IsPureSecondary(j_cr)) {
               Double_t sterm_std = 0.;
               // Loop on all available astrophysical sources
               // N.B.: normalisation for source spectrum must be in [/(GeV/n m3 Myr)]
               for (Int_t s_src = 0; s_src < TUModelBase::GetNSrcAstro(); ++s_src) {
                  // Skip if j_cr is not present in the s_src-th astro source
                  if (!TUModelBase::IsExistSrc(s_src, j_cr, true))
                     continue;
                  // Update energy coordinate and calculate spectrum
                  TUModelBase::UpdateCoordE(j_cr, k_ekn);
                  //cout << "j=" << j_cr << "=" << GetCREntry(j_cr).GetName() << " k=" << k_ekn << " qi=" << GetQiAstro(i_bess, TUModelBase::IndexCRInAstroSrc(s_src, j_cr), s_src)
                  //<< " Q(E)=" << TUModelBase::ValueSrcAstroSpectrum(s_src, j_cr, TUModelBase::GetCoordE()) << "  Ai=" << GetAiTerm(i_bess, j_cr, k_ekn) << endl;
                  sterm_std += GetQiAstro(i_bess, TUModelBase::GetSrcAstro(s_src)->IndexCRInSrcSpecies(j_cr), s_src)
                               * TUModelBase::ValueSrcAstroSpectrum(s_src, j_cr, TUModelBase::GetCoordE());
               }
               // Add in total (all contribs) and prim (primary contrib only)
               // N.B.: we divide by Aiterm after having added all contributions
               sterm_tot[k_ekn] += sterm_std;
               sterm_prim[k_ekn] += sterm_std;
            }
            // A(2b) Primary exotic contrib.
            //------------------------------
            if (TUPropagSwitches::IsPrimExotic() && !TUAxesCrE::IsPureSecondary(j_cr)) {
               // N.B.: normalisation for source spectrum must be in [/(GeV/n m3 Myr)]
               // Update energy coordinate and calculate spectrum
               Double_t sterm_exotic = Ni_PrimContribInHalo_z0(i_bess, j_cr, k_ekn);
               // N.B.: all contributions but this one  must be multiplied by mult,
               // so we divide here by mult to have the correct contribution below.
               Double_t mult = 2.*GetParVal_h() / GetAiTerm(i_bess, j_cr, k_ekn);
               sterm_exotic /= mult;
               // Add in total (all contribs) and prim (primary contrib only)
               sterm_tot[k_ekn] += sterm_exotic;
               sterm_prim[k_ekn] += sterm_exotic;
            }

            Double_t radinhalo_contrib = 0.;
            // A(3) BETA-rad. parent contrib.
            //-------------------------------
            //     Q_rad  =  Nz_WithParentBETA(z=0)
            //            = [/(GeV/n m3)]
            if (j_radbeta_parent != -1)
               radinhalo_contrib = Niz_WithParentBETA(j_radbeta_parent, i_bess, j_cr, k_ekn, 0.);

            // A(4) EC-rad. parent contrib.
            //-----------------------------
            // TO DO

            // All contributions are now added:
            //         Sterm =  2h  *  sterm  /  Aterm
            // [/(GeV/n m3)] = [kpc]*[/(GeV/n m3 Myr)]/[kpc/Myr]
            Double_t mult = 2.*GetParVal_h() / GetAiTerm(i_bess, j_cr, k_ekn);
            sterm_prim[k_ekn] *= mult;
            sterm_tot[k_ekn] = sterm_tot[k_ekn] * mult + radinhalo_contrib;
         }

         /*******************************/
         /* B. Solve Ni for source term */
         /*******************************/
         InvertForELossGain(j_cr, fISMEntry, GetCoefPreFactor(i_bess, j_cr), sterm_tot, sterm_prim, fInvertScheme, GetNiTot(i_bess, j_cr), GetNiPrim(i_bess, j_cr));

         /*********************************/
         /* C. Add tertiaries (iterative) */
         /*********************************/
         if (TUPropagSwitches::IsTertiaries() && TUXSections::IsTertiary(j_cr))
            IterateTertiariesNi(i_bess, j_cr, sterm_tot, sterm_prim, TUModelBase::GetEpsIterConv(), is_verbose, f_log);
      }
   }

   // Free memory
   delete[] sterm_tot;
   sterm_tot = NULL;
   delete[] sterm_prim;
   sterm_prim = NULL;
   if (is_verbose && is_check)
      fprintf(f_log, "%sTUModel1DKisoVc::CalculateChargedCRs <DONE>\n", indent.c_str());

   TUMessages::Indent(false);
   return true;
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::CoeffReac1stOrder(Int_t j_cr, Int_t k_ekn)
{
   //--- Returns first order term from reacceleration.
   //  j_cr              Index of CR
   //  k_ekn             Index of energy (here running from -1 to NE)

   if (k_ekn >= 0 && k_ekn < GetNE())
      return (1. + GetBeta(j_cr, k_ekn) * GetBeta(j_cr, k_ekn)) * ValueKpp(j_cr, k_ekn) / GetEtot(j_cr, k_ekn);
   else {
      Double_t ekn = TUAxesCrE::GetE(j_cr)->EvalOutOfRange(k_ekn);
      Double_t beta = TUPhysics::Beta_mAEkn(TUAxesCrE::GetCREntry(j_cr).GetmGeV(), TUAxesCrE::GetCREntry(j_cr).GetA(), ekn);
      Double_t etot = TUPhysics::Ekn_to_E(ekn, TUAxesCrE::GetCREntry(j_cr).GetA(), TUAxesCrE::GetCREntry(j_cr).GetmGeV());
      TUModelBase::UpdateCoordE(j_cr, ekn);
      return
         (1. + beta * beta) * TUModelBase::ValueKpp(TUModelBase::GetCoordE()) / etot;
   }
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::CoeffReac2ndOrderLowerHalfBin(Int_t j_cr, Int_t k_ekn)
{
   //--- Returns second order term from reacceleration: evaluated at lower half-bin.
   //  j_cr              Index of CR
   //  k_ekn             Index of energy (here running from 0 to NE)

   if (k_ekn != GetNE())  // Return at k_ekn-1/2
      return TUAxesCrE::GetBeta(j_cr, k_ekn, -1) * TUAxesCrE::GetBeta(j_cr, k_ekn, -1)
             * ValueKpp(j_cr, k_ekn, -1) / TUAxesCrE::GetEk(j_cr, k_ekn, -1);
   else  // Return at NE-1/2 = (NE-1) + 1/2
      return TUAxesCrE::GetBeta(j_cr, GetNE() - 1, 1) * TUAxesCrE::GetBeta(j_cr, GetNE() - 1, 1)
             * ValueKpp(j_cr, GetNE() - 1, 1) / TUAxesCrE::GetEk(j_cr, GetNE() - 1, 1);
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::EvalFlux(Int_t j_cr, Int_t k_ekn, Double_t r, Double_t z, Int_t tot0_prim1_sec2)
{
   //--- Returns for j_cr at position r,z (solution is even in z) total, primary,
   //    or secondary flux [#part/(m2.s.sr.GeV/n)].
   //       - flux = v/(4*pi)*dN/dE  [dNdE]=# part/(m^3.GeV/n), [c]=m/s;
   //       - if radioactive parent, more tricky (see, Putze et al., 2010).
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  z                 X coordinate = z (height in the halo)
   //  tot0_prim1_sec2   Switch to have total(0), primary(1) or secondary(2) contrib.

   // Solution is even
   z = fabs(z);

   // If outside geometry, flux is 0.
   if (z > GetParVal_L() - 1.e-3 || r > GetParVal_R() - 1.e-3)
      return 0.;

   // N.B.: if rhole>0 and radioactive nucleus, calculation applies only for z=0!
   if (z > 1.e-10 && TUPropagSwitches::IsDecayBETA()
         && GetParVal_rh() > 1.e-5
         && TUAxesCrE::GetCREntry(j_cr).GetBETAHalfLife() > 1.e-40) {
      string message = "Flux for a BETA-unstable " + TUAxesCrE::GetCREntry(j_cr).GetName()
                       + " for r_hole>0 is not valid for z>0!";
      TUMessages::Warning(stdout, "TUModel2DKisoVc", "EvalFlux", message);
      return 0.;
   }

   // Factor to convert differential density to flux = [v/(4*pi)]
   Double_t factor = TUAxesCrE::GetBeta(j_cr, k_ekn)
                     * TUPhysics::C_mpers() / (4.*TUMath::Pi());


   // All we have are the Ni(z=0): resummation is necessary to have dNdE(r,z), then flux(r,z)
   // N.B.: interpolation required to get z
   Double_t dnde = 0.;
   if (tot0_prim1_sec2 == 0) {
      // Find if j_cr has a radioactive parent in CR list (requires special case)
      Int_t j_beta = TUAxesCrE::TUCRList::IndexParentBETA(j_cr);
      for (Int_t i_bess = TUBesselJ0::GetNBessel() - 1; i_bess >= 0; --i_bess) {
         Double_t niz = 0.;
         // Ni(z): special case if BETA-rad contribution
         //   - if (parent BETA) -> Niz_WithParentBETA(z) directly gives Ni(z) with all contribs.
         //   - else -> Niz_WithoutParentBETA(z) accounts for standard and/or exotic contributions
         if (j_beta != -1) {
            if (z > 1.e-10)
               niz = Niz_WithParentBETA(j_beta, i_bess, j_cr, k_ekn, z);
            else // In this specific case (z=0), it is faster and yet correct
               // not to call 'Nz_WithParentBETA'
               niz = Niz_WithoutParentBETA(GetNiTot(i_bess, j_cr, k_ekn), i_bess, j_cr, k_ekn, z);
         } else
            niz = Niz_WithoutParentBETA(GetNiTot(i_bess, j_cr, k_ekn), i_bess, j_cr, k_ekn, z);

         // N(z) = sum_i Ni(z)*J0(zeta_i*r/R)
         dnde += niz * TUBesselJ0::J0Zetai_r2R(i_bess, r);
      }
      // The flux is finally obtained by the relation
      // Flux = v /(4 *pi) * dN/dE
      return dnde * factor;
   } else if (tot0_prim1_sec2 == 1) {
      for (Int_t i_bess = TUBesselJ0::GetNBessel() - 1; i_bess >= 0; --i_bess) {
         // N(z) = sum_i Ni(z)*J0(zeta_i*r/R)
         dnde += TUBesselJ0::J0Zetai_r2R(i_bess, r)
                 * Niz_WithoutParentBETA(GetNiPrim(i_bess, j_cr, k_ekn), i_bess, j_cr, k_ekn, z);
      }
      // The flux is finally obtained by the relation
      // Flux = v /(4 *pi) * dN/dE
      return dnde * factor;
   } else if (tot0_prim1_sec2 == 2)
      return EvalFlux(j_cr, k_ekn, r, z, 0) - EvalFlux(j_cr, k_ekn, r, z, 1);
   else
      TUMessages::Error(stdout, "TUModel2DKisoVc", "EvalFlux",
                        "Allowed values for tot0_prim1_sec2 are 0, 1, and 2");

   return 0.;
}

//______________________________________________________________________________
void TUModel2DKisoVc::FillCoefPreFactorAndAiTerm()
{
   //--- Fills fCoefPreFactor=2h/(Ai*Ek) [Myr/GeV] and AiTerm [kpc/Myr].

   // Allocate memory
   if (fCoefPreFactor) delete[] fCoefPreFactor;
   fCoefPreFactor = new Double_t[GetNBessel()*GetNE()*GetNCRs()];

   if (fAiTerm) delete[] fAiTerm;
   fAiTerm = new Double_t[GetNBessel()*GetNE()*GetNCRs()];

   // Loop on all indices to fill fCoefPreFactor anf fAiTerm
   for (Int_t i_bess = GetNBessel() - 1; i_bess >= 0; --i_bess) {
      for (Int_t j_cr = GetNCRs() - 1; j_cr >= 0; --j_cr) {
         for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {
            SetAiTerm(i_bess, j_cr, k_ekn, Ai(i_bess, j_cr, k_ekn));
            Double_t factor =  2. * GetParVal_h()
                               / (GetAiTerm(i_bess, j_cr, k_ekn) * TUAxesCrE::GetEk(j_cr, k_ekn));
            SetCoefPreFactor(i_bess, j_cr, k_ekn, factor);
         }
      }
   }
}

//______________________________________________________________________________
void TUModel2DKisoVc::Initialise(Bool_t is_delete)
{
   //--- Initialises members specific to this model.
   //    N.B.: fISMEntry never needs to be deleted, because it relies on
   //    TUModelBase::fISM which is created and deleted elsewhere.
   //
   //  is_delete         Whether to delete or just initialise

   InitialiseSrc(is_delete);

   if (is_delete) {
      if (fAiTerm) delete[] fAiTerm;
      if (fCoefPreFactor) delete[] fCoefPreFactor;
      if (fNiPrim) delete[] fNiPrim;
      if (fNiTot) delete[] fNiTot;
   }

   fAiTerm = NULL;
   fAxisRIntegr.Initialise();
   fAxisZIntegr.Initialise();
   fCoefPreFactor = NULL;
   fIndexPar_h = -1;
   fIndexPars_L = -1;
   fIndexPars_R = -1;
   fIndexPars_rh = -1;
   fInvertScheme = kTRID;
   fISMEntry = NULL;
   fNiPrim = NULL;
   fNiTot = NULL;
}

//______________________________________________________________________________
void TUModel2DKisoVc::InitialiseSrc(Bool_t is_delete)
{
   //--- Initialises fQiAstro and fQizDM members (sources).
   //  is_delete         Whether to delete or just initialise

   if (is_delete) {
      if (fQiAstro) {
         for (Int_t i = 0; i < TUModelBase::GetNSrcAstro(); ++i) {
            if (fQiAstro[i]) {
               delete[] fQiAstro[i];
               fQiAstro[i] = NULL;
            }
         }
         delete[] fQiAstro;
      }
      if (fQizDM) {
         for (Int_t i = 0; i < TUModelBase::GetNSrcDM(); ++i) {
            if (fQizDM[i]) {
               delete[] fQizDM[i];
               fQizDM[i] = NULL;
            }
         }
         delete[] fQizDM;
      }
   }
   fQiAstro = NULL;
   fQizDM = NULL;
}

//______________________________________________________________________________
void TUModel2DKisoVc::InitialiseForCalculation(Bool_t is_init_or_update, Bool_t is_force_update, Bool_t is_verbose, FILE *f_log)
{
   //--- Initialises and/or update (optimised or not) model members and values
   //    required for model calculation.
   //
   //  is_init_or_update Whether initialise class or update (look for changed parameters)
   //  is_force_update   If is_init_or_update=false (update), force to to it even if no parameter changed (useful in a few occasions)
   //  is_verbose        Verbose or not
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);

   // Model-specific quantities: only for initialisation
   if (is_init_or_update) {
      if (is_verbose)
         fprintf(f_log, "%s[TUModel2DKisoVc::InitialiseForCalculation]\n", indent.c_str());

      // Set model parameter indices
      if (is_verbose)
         fprintf(f_log, "%s   - Retrieve h, L, R, rhole indices (in TUFreeParList)\n", indent.c_str());
      fIndexPar_h = TUModelBase::TUFreeParList::IndexPar("h");
      fIndexPars_L = TUModelBase::TUFreeParList::IndexPar("L");
      fIndexPars_R = TUModelBase::TUFreeParList::IndexPar("R");
      fIndexPars_rh = TUModelBase::TUFreeParList::IndexPar("rhole");
      this->SetBessRSol(GetRSol(), GetParVal_R());

      // Allocate memory for Ni coeffs
      Int_t n_size = TUBesselJ0::GetNBessel() * GetNE() * GetNCRs();
      if (is_verbose)
         fprintf(f_log, "%s   - Allocate fNiTot[NBessel*NCRs*NE] and fNiPrim[NBessel*NCRs*NE]\n", indent.c_str());
      if (fNiTot)  delete[] fNiTot;
      fNiTot = new Double_t[n_size];
      if (fNiPrim) delete[] fNiPrim;
      fNiPrim = new Double_t[n_size];
      // And initialise
      for (Int_t i = 0; i < n_size; ++i) {
         fNiTot[i] = 0.;
         fNiPrim[i] = 0.;
      }
      // If initialisation, propagation is mandatory (set to not propagated)
      TUModelBase::SetPropagated(false);
   } else {
      // If update, check whether model is updated
      TUModelBase::TUFreeParList::UpdateParListStatusFromIndividualStatus();
      if (TUModelBase::TUFreeParList::GetParListStatus())
         TUModelBase::SetPropagated(false);
   }

   // Update par list status for all parameters
   TUModelBase::UpdateAllParListStatusFromIndividualParStatus();

   // ISM: initialisation, update (if ISM was modified), or force update
   if (is_init_or_update || TUModelBase::GetFreeParsStatusISM() || is_force_update) {

      // Set fISMEntry to TUMediumTXYZ entry
      if (is_verbose) {
         if (is_init_or_update)
            fprintf(f_log, "%s   - Set fISMEntry (thin disc) from TUMediumTXYZ\n", indent.c_str());
         else
            fprintf(f_log, "%s   - Update fISMEntry (thin disc)\n", indent.c_str());
      }

      TUModelBase::GetISM()->UpdateFromFreeParsAndResetStatus();
      TUCoordTXYZ *dummy_txyz = NULL;
      TUMediumTXYZ *medium = TUModelBase::GetISM();
      medium->FillMediumEntry(dummy_txyz);
      fISMEntry = medium->GetMediumEntry();
      medium = NULL;
      if (is_init_or_update && is_verbose)
         fISMEntry->Print(f_log);

      // Fill or update nuclear rates (because ISM was changed)
      if (is_verbose) {
         if (is_init_or_update)
            fprintf(f_log, "%s   - Fill nuclear rates (optimised for constant density ISM)\n", indent.c_str());
         else
            fprintf(f_log, "%s   - Update nuclear rates (optimised for constant density ISM)\n", indent.c_str());
      }
      TUXSections::FillInterRate_ISM0D(fISMEntry, true);
   }


   // XSection: initialisation, update (if XS were modified), or force update
   if (is_init_or_update || TUModelBase::GetFreeParsStatusXSections() || is_force_update) {

      // Set/Update XS from free parameters
      if (is_verbose) {
         if (!is_init_or_update)
            fprintf(f_log, "%s   - Update XSections and nuclear rates (optimised for constant density ISM)\n", indent.c_str());
      }
      TUModelBase::TUXSections::ParsXS_UpdateFromFreeParsAndResetStatus(fISMEntry);

      // We have to force update for the remaining parameters (coeff to calculate are related to rates)
      is_force_update = true;
   }


   // Transport: initialisation, update (if transport or half-lives were modified), or force update
   if (is_init_or_update || TUModelBase::GetFreeParsStatusTransport()
         || TUModelBase::GetFreeParsStatusHalfLives() || is_force_update) {

      if (is_verbose) {
         if (is_init_or_update)
            fprintf(f_log, "%s   - Fill pre-factor terms\n", indent.c_str());
         else
            fprintf(f_log, "%s   - Update pre-factor terms\n", indent.c_str());
      }
      TUModelBase::GetTransport()->UpdateFromFreeParsAndResetStatus();
      TUModelBase::TUAxesCrE::TUCRList::ParsHalfLife_UpdateFromFreeParsAndResetStatus();
      FillCoefPreFactorAndAiTerm();
   }

   // Geometry: initialisation or update (if axes were changed)
   if (is_init_or_update || TUModelBase::GetAxesTXYZ()->GetFreeParsStatusXAxis()
         || TUModelBase::GetAxesTXYZ()->GetFreeParsStatusYAxis() || is_force_update) {

      // If X-max changed (radial coordinate)
      Bool_t is_R_changed = TUModelBase::GetAxesTXYZ()->GetFreeParsStatusXAxis();
      Bool_t is_L_changed = TUModelBase::GetAxesTXYZ()->GetFreeParsStatusYAxis();
      if (is_R_changed) {
         fAxisRIntegr.SetClass(0., GetParVal_R(), GetNR(), "RIntegr", "kpc", kLIN);
         this->SetBessRSol(GetRSol(), GetParVal_R());
         if (is_verbose)
            fprintf(f_log, "%s   - Update radial grid (R value updated to %le)\n", indent.c_str(), GetParVal_R());
      }

      // If Y-max changed (z coordinate)
      if (is_L_changed) {
         fAxisZIntegr.SetClass(0., GetParVal_L(), GetNZ(), "ZIntegr", "kpc", kLIN);
         if (is_verbose)
            fprintf(f_log, "%s   - Update vertical grid (L value updated to %le)\n", indent.c_str(), GetParVal_L());
      }

      // Set TXYZ axes
      TUModelBase::GetAxesTXYZ()->UpdateFromFreePars();
      if (TUModelBase::GetAxesTXYZ()->GetFreePars())
         TUModelBase::GetAxesTXYZ()->GetFreePars()->ResetStatusParsAndParList();


      // Allocate and perform FB expansions for primary sources
      if (is_init_or_update) {
         if (is_verbose)
            fprintf(f_log, "%s   - Set Fourier-Bessel coefficients Qi and Qi(z)\n", indent.c_str());
         InitialiseSrc(true);
         // Astro (disc)
         if (TUModelBase::GetNSrcAstro() > 0) {
            fQiAstro = new TUBesselQiSrc*[TUModelBase::GetNSrcAstro()];
            for (Int_t s_src = 0; s_src < TUModelBase::GetNSrcAstro(); ++s_src) {
               TUSrcMultiCRs *src = TUModelBase::GetSrcAstro(s_src);
               if (is_verbose)
                  fprintf(f_log, "%s     * Calculate Qi for source in disc = %s\n",
                          indent.c_str(), src->GetSrcName().c_str());
               if (src->GetNSpecies() <= 0) {
                  string message = "This source contains no species, check your initialisation file!";
                  TUMessages::Error(f_log, "TUModel2DKisoVc", "InitialiseForCalculation", message);
               }
               fQiAstro[s_src] = new TUBesselQiSrc[src->GetNSpecies()];
               for (Int_t j_species = 0; j_species < src->GetNSpecies(); ++j_species) {
                  if (is_verbose)
                     fprintf(f_log, "%s        -> %s\n",
                             indent.c_str(), src->GetSpecies()->GetCREntry(j_species).GetName().c_str());
                  if (!src->GetSrcSpatialDist(j_species)) {
                     string message = "No spatial distribution found for this source and species!";
                     TUMessages::Error(f_log, "TUModel2DKisoVc", "InitialiseForCalculation", message);
                  }
                  //src->GetSrcSpatialDist(j)->PrintSummary();
                  fQiAstro[s_src][j_species].UseAxisr(&fAxisRIntegr);
                  fQiAstro[s_src][j_species].CalculateAndStoreQi(this, src->GetSrcSpatialDist(j_species), false, f_log);
               }
               src = NULL;
            }
         }
         // DM (halo)
         if (TUModelBase::GetNSrcDM() > 0) {
            fQizDM = new TUBesselQiSrc*[TUModelBase::GetNSrcDM()];
            for (Int_t s_src = 0; s_src < TUModelBase::GetNSrcDM(); ++s_src) {
               TUSrcMultiCRs *src = TUModelBase::GetSrcDM(s_src);
               if (is_verbose)
                  fprintf(f_log, "%s     * Calculate Qi(z) for source in halo = %s\n",
                          indent.c_str(), src->GetSrcName().c_str());
               fQizDM[s_src] = new TUBesselQiSrc[src->GetNSpecies()];
               for (Int_t j_species = 0; j_species < src->GetNSpecies(); ++j_species) {
                  if (is_verbose)
                     fprintf(f_log, "%s        -> %s\n", indent.c_str(),
                             src->GetSpecies()->GetCREntry(j_species).GetName().c_str());
                  if (!src->GetSrcSpatialDist(j_species)) {
                     string message = "No spatial distribution found for this source and species!";
                     TUMessages::Error(f_log, "TUModel2DKisoVc", "InitialiseForCalculation", message);
                  }
                  fQizDM[s_src][j_species].UseAxisr(&fAxisRIntegr);
                  fQizDM[s_src][j_species].UseAxisz(&fAxisZIntegr);
                  fQizDM[s_src][j_species].CalculateAndStoreQiz(this, src->GetSrcSpatialDist(j_species), false, f_log);
               }
               src = NULL;
            }
         }
      } else {
         if ((is_R_changed || is_L_changed) && is_verbose)
            fprintf(f_log, "%s   - Update Fourier-Bessel coefficients Qi and Qi(z)\n", indent.c_str());

         // Only if R (radial boundary) updated
         if (is_R_changed) {
            for (Int_t i = 0; i < TUModelBase::GetNSrcAstro(); ++i) {
               TUSrcMultiCRs *src = TUModelBase::GetSrcAstro(i);
               if (is_verbose)
                  fprintf(f_log, "%s     * Update Qi for source in disc = %s\n",
                          indent.c_str(), src->GetSrcName().c_str());
               for (Int_t j = 0; j < src->GetNSpecies(); ++j) {
                  fQiAstro[i]->UseAxisr(&fAxisRIntegr);
                  fQiAstro[i]->CalculateAndStoreQi(this, src->GetSrcSpatialDist(j), true, f_log);
               }
               src = NULL;
            }
         }
         // DM sources in the halo (R or Z updated)
         if (is_R_changed || is_L_changed) {
            for (Int_t i = 0; i < TUModelBase::GetNSrcDM(); ++i) {
               TUSrcMultiCRs *src = TUModelBase::GetSrcDM(i);
               if (is_verbose)
                  fprintf(f_log, "%s     * Update Qi(z) for source in halo = %s\n",
                          indent.c_str(), src->GetSrcName().c_str());
               for (Int_t j = 0; j < src->GetNSpecies(); ++j) {
                  fQizDM[i]->UseAxisr(&fAxisRIntegr);
                  fQizDM[i]->CalculateAndStoreQiz(this, src->GetSrcSpatialDist(j), true, f_log);
               }
               src = NULL;
            }
         }
      }

   }

   // Sources: only for update (if sources were modified)
   if (!is_init_or_update) {
      for (Int_t i = 0; i < GetNSrcAstro(); ++i)
         TUModelBase::GetSrcAstro(i)->UpdateFromFreeParsAndResetStatus();
      for (Int_t i = 0; i < GetNSrcDM(); ++i)
         TUModelBase::GetSrcDM(i)->UpdateFromFreeParsAndResetStatus();
   }


   // Propagation switches: just reset status
   TUPropagSwitches::SetStatus(false);
   // Update and reset model parameters
   TUModelBase::ResetStatusParsAndParList();
   if (is_init_or_update && is_verbose)
      fprintf(f_log, "%s[TUModel2DKisoVc::InitialiseForCalculation] <DONE>\n", indent.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUModel2DKisoVc::IterateTertiariesNi(Int_t i_bess, Int_t j_cr, Double_t *sterm_tot,
      Double_t *sterm_prim, Double_t const &eps, Bool_t is_verbose, FILE *f_log)
{
   //--- Solves iteratively E redistribution from inel. non-annihil. rescattering (INA).
   //    With INA iterations, the diffusion equation becomes an integro-differential
   //    equation (2nd order derivative in E, one integration for E redistribution).
   //    The integral term involves the equilibrium spectrum, which is calculated
   //    iteratively (convergence is reached after a couple of iterations in practice).
   //    The formula below is slightly changed in practise (see TUXSections::TertiaryProduction)
   //    to perform the calculation on a log-scale energy:
   //       - step 1: calculate tertiary contrib. (over ISM components) from current dN/dEkn:BEGIN_LATEX(fontsize=14, separator='=', align='lcl')
   //          Si_{ter} = #frac{Qi_{ter}}{Ai_{term}}, with
   //          Qi_{ter}(E) = #int_{E}^{#infty} n v' #frac{d#sigma^{INA}_{CR+ISM -> CR+X+ISM}}{dE} (E' -> E) Ni(E')dE' - nv#sigma^{INA}_{CR+ISM -> CR+X+ISM} (E) Ni(E)
   //END_LATEX
   //       - step 2: calculate new equilibrium spectra Ni (adding tertiary term)
   //       - step 3: go back to step 1 and iterate with new Ni until convergence reached
   //
   // INPUTS:
   //  i_bess            Bessel index
   //  j_cr              CR index
   //  sterm_tot         Source term (prim+sec contribs.) before tertiaries
   //  sterm_prim        Source term (prim. only) before tertiaries
   //  eps               Precision sought before stopping iterations
   //  is_verbose        Chatter on or off
   //  f_log             Log for USINE
   // OUTPUTS:
   //  sterm_tot         Source term (prim+sec contribs.) including tertiaries
   //  sterm_prim        Source term (prim. only) including tertiaries


   // If j_cr not a tertiary, nothing to do
   if (!TUXSections::IsTertiary(j_cr))
      return;

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   // Set maximum iteration number
   const Int_t n_iter_max = 100;

   // Allocate and initialise sterm (enable comparison before/after iteration)
   Double_t max_dev = 0.;
   Double_t *ni_ref = new Double_t[TUModelBase::GetNE()];
   Double_t *stermtot_ter = new Double_t[TUModelBase::GetNE()];
   Double_t *stermtot_ter_old = new Double_t[TUModelBase::GetNE()];

   // Update new/old Sterm for primary contrib. only (if required)
   Double_t *stermprim_ter = NULL;
   Double_t *stermprim_ter_old = NULL;
   if (!TUAxesCrE::IsPureSecondary(j_cr)) {
      stermprim_ter = new Double_t[TUModelBase::GetNE()];
      stermprim_ter_old = new Double_t[TUModelBase::GetNE()];
      for (Int_t k = TUModelBase::GetNE() - 1; k >= 0; --k) {
         stermtot_ter[k] = 0.;
         stermprim_ter[k] = 0.;
         ni_ref[k] = GetNiTot(i_bess, j_cr, k);
      }
   } else {
      for (Int_t k = TUModelBase::GetNE() - 1; k >= 0; --k) {
         stermtot_ter[k] = 0.;
         ni_ref[k] = GetNiTot(i_bess, j_cr, k);
      }
   }

   // Iterate until convergence for any energy:
   //    |(N_{iter i+1} - N_{iter i})| / N_{iter i} < eps
   Int_t counter = 0;
   Int_t k_converged = 0;
   do {
      /******************************/
      /* Step 1: calculate tertiary */
      /******************************/
      ++counter;
      Int_t k_converged_iter = TUModelBase::GetNE() - 1;

      // Max difference after/before iteration (over all CR energies)
      max_dev = 0.;

      // Check convergence on total calculated flux: limiting convergence
      // is expected for higher energy points (less points to integrate
      // on tertiary production)
      for (Int_t k = TUModelBase::GetNE() - 1 ; k >= 0; --k) {
         // sterm_ter = 2h*Qiterm/Aiterm   [sterm_ter]=[sterm]= [Ni]

         Bool_t is_converged = false;

         // Update new/old Sterm (primary and secondary contrib.)
         stermtot_ter_old[k] = stermtot_ter[k];
         stermtot_ter[k] = 2. * GetParVal_h() / GetAiTerm(i_bess, j_cr, k)
                           * TUXSections::TertiaryProduction(GetNiTot(i_bess, j_cr), j_cr, k, fISMEntry, eps, true, is_converged);

         Double_t delta = stermtot_ter[k] - stermtot_ter_old[k];
         Double_t rel_diff = 0.;
         if (sterm_tot[k] + delta < 0.)
            sterm_tot[k] = 0.;
         else {
            sterm_tot[k] += delta;
            // Check convergence of integration at this energy only if rel_dif>eps
            rel_diff = fabs((delta) / fabs(sterm_tot[k]));
         }
         // Check if converged (contrib to tertiary needs to be significant and flux non null)
         //cout << "         k=" << k << "   is_converged=" << is_converged
         //     << "   sterm_tot[k]=" << sterm_tot[k] << "   rel_diff=" << rel_diff << endl;
         if (!is_converged && sterm_tot[k] > 1.e-40 && rel_diff > eps)
            k_converged_iter = k;

         // Keep only maximum difference to check overall convergence of this iteration
         max_dev = TMath::Max(max_dev, rel_diff);

         // Update new/old Sterm for primary contrib. only (if required)
         if (!TUAxesCrE::IsPureSecondary(j_cr)) {
            stermprim_ter_old[k] = stermprim_ter[k];
            stermprim_ter[k] = 2. * GetParVal_h() / GetAiTerm(i_bess, j_cr, k)
                               * TUXSections::TertiaryProduction(GetNiPrim(i_bess, j_cr), j_cr, k, fISMEntry, eps, true, is_converged);
            sterm_prim[k] += stermprim_ter[k] - stermprim_ter_old[k];
         }
      }

      /****************************/
      /* Step 2: calculate new Ni */
      /****************************/
      InvertForELossGain(j_cr, fISMEntry, GetCoefPreFactor(i_bess, j_cr), sterm_tot, sterm_prim, fInvertScheme, GetNiTot(i_bess, j_cr), GetNiPrim(i_bess, j_cr));

      k_converged = max(k_converged, k_converged_iter);
      // cout << "****** iter=" << counter << "   k_converged_iter="
      //      << k_converged_iter << "   k_converged=" << k_converged << endl;

      // Stop if reached maximum allowed number of iterations
      if (counter > n_iter_max) break;

      // Stop if convergence reached
   } while (max_dev > eps);

   // Warning message if convergence of numerical integration in tertiary
   // contribution (that matters, i.e. for which the difference w/o tertiary
   // is larger than required precision) is not reached
   Int_t k_matters = TUModelBase::GetNE();
   for (Int_t k = TUModelBase::GetNE() - 1 ; k >= 0; --k) {
      if (ni_ref[k] > 0. && fabs(ni_ref[k] - GetNiTot(i_bess, j_cr, k)) / GetNiTot(i_bess, j_cr, k) > eps) {
         k_matters = k;
         break;
      }
   }

   if (k_converged < TUModelBase::GetNE() && k_matters > k_converged) {
      string message = Form("Precision eps=%f (for tertiary numerical integration)"
                            " not reached for %s for all Ekn in range [%.3le,%.3le] [GeV/n]",
                            eps, TUAxesCrE::GetCREntry(j_cr).GetName().c_str(), TUAxesCrE::GetEkn(j_cr, k_converged),
                            TUAxesCrE::GetEkn(j_cr, k_matters));
      TUMessages::Warning(f_log, "TUModel1DKisoVc", "IterateTertiariesNi", string(message));
      fprintf(f_log, "\n");
   }

   if (is_verbose) {
      string qty = TUCRList::GetCREntry(j_cr).GetName();

      if (counter <= n_iter_max)
         fprintf(f_log, "%s    %s: tertiary contribution convergence (%.2le) reached after %d iterations\n",
                 indent.c_str(), qty.c_str(), eps, counter);
      else {
         string message = Form("%s%s: WARNING -- tertiary contribution HAS NOT CONVERGED after %d iterations",
                               indent.c_str(), qty.c_str(), counter);
         TUMessages::Warning(f_log, "TUModel1DKisoVc", "IterateTertiariesNi", string(message));
      }
   }


   // Free memory
   delete[] stermtot_ter;
   stermtot_ter = NULL;
   delete[] stermtot_ter_old;
   stermtot_ter_old = NULL;

   delete[] ni_ref;
   ni_ref = NULL;

   if (stermprim_ter)
      delete[] stermprim_ter;
   stermprim_ter = NULL;
   if (stermprim_ter_old)
      delete[] stermprim_ter_old;
   stermprim_ter_old = NULL;
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::Ni_PrimContribInHalo_Yiz(Int_t i_bess, Int_t j_cr,
      Int_t k_ekn, Int_t i_z, Double_t const &kdiff, Double_t const &si)
{
   //--- Returns yi(z[i_z]) required to calculate primary contribution from species
   //    distributed in the diffusive halo, as given in App.A of Barrau et al.,
   //    A&A 388 (2002):BEGIN_LATEX(fontsize=14, separator='=', align='lcl')
   //          yi(z) = 2 #int_{0}^{z} exp#left(#frac{Vc(z-z')}{2K}#right) #times sinh(Si(z-z')/2) #times qi_{prim}(z') dz'
   //END_LATEX
   //    N.B.: source spectrum Q_prim(E) is hidden in qi_prim(z').
   //
   //  i_bess            Bessel index
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  i_z               Index of position (z) up to which to carry out integration
   //  kdiff             Spatial diffusion coefficient (j_cr, k_ekn) [kpc^2/Myr]
   //  si                Si(i_bess, j_cr, k_ekn)  [/kpc]


   // Update bin of E coordinate!
   TUModelBase::UpdateCoordE(j_cr, k_ekn);

   // Initialisation
   Double_t z = fAxisZIntegr.GetVal(i_z);
   Double_t yi_z = 0.;
   Int_t n_z = i_z;
   if (!n_z % 2) n_z += 1;
   vector<Double_t> integrand(n_z, 0.);

   // Loop on all DM sources producing j_cr
   for (Int_t s_src = 0; s_src < TUModelBase::GetNSrcDM(); ++s_src) {
      // If j_cr not in source, skip it
      Int_t j_species = TUModelBase::GetSrcDM(s_src)->IndexCRInSrcSpecies(j_cr);
      if (j_species < 0)
         continue;

      TUSrcMultiCRs *dm_src = TUModelBase::GetSrcDM(s_src);

      // Perform the integration on integrand exp(Vc(z-z')/(2K)) sinh(Si(z-z')/2) qi_{prim}(z')
      for (Int_t n = 0; n < n_z; n++) {
         Double_t z_prime = fAxisZIntegr.GetVal(n);
         integrand[n] += exp(ValueVc_kpcperMyr() * (z - z_prime) / (2.*kdiff))
                         * sinh(si * (z - z_prime) * 0.5) * fQizDM[s_src][j_species].GetQiz(i_bess, n);
         // Units: [vc]=kpc/Myr , [z]=kpc, [kdiff=kpc^2/Myr]
      }
      Double_t yi_z_src = 0.;
      TUNumMethods::IntegrationSimpsonLin(&fAxisZIntegr, &integrand[0], 0, n_z, yi_z_src);

      // Multiplication by the source spectrum and factor 2 (to take into account contribs from  [-z -> 0]
      yi_z += yi_z_src * 2.*dm_src->ValueSrcSpectrum(j_species, TUModelBase::GetCoordE());
      dm_src = NULL;
   }
   return yi_z;
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::Ni_PrimContribInHalo_Yiz(Int_t i_bess, Int_t j_cr,
      Int_t k_ekn, Double_t z, Double_t const &kdiff, Double_t const &si)
{
   //--- Returns yi(z) [interpolated if necessary from values calculated on z grid]
   //    required to calculate primary contribution from species distributed in the
   //    diffusive halo. See
   //
   //  i_bess            Bessel index
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  i_z               Index of position (z) up to which to carry out integration
   //  kdiff             Spatial diffusion coefficient (j_cr, k_ekn) [kpc^2/Myr]
   //  si                Si(i_bess, j_cr, k_ekn)  [/kpc]

   // The solution is even
   z = fabs(z);

   // If z close to node in z-grid
   Int_t iz_closest = fAxisZIntegr.IndexClosest(z);
   if (z < 1.e-3 || fabs(fAxisZIntegr.GetVal(iz_closest) - z) / z < 1.e-3 || iz_closest == fAxisZIntegr.GetN() - 1)
      return Ni_PrimContribInHalo_Yiz(i_bess, j_cr, k_ekn, iz_closest, kdiff, si);

   // If interpolation required
   Double_t yi_z_lo = Ni_PrimContribInHalo_Yiz(i_bess, j_cr, k_ekn, iz_closest, kdiff, si);
   Double_t yi_z_hi = Ni_PrimContribInHalo_Yiz(i_bess, j_cr, k_ekn, iz_closest + 1, kdiff, si);

   return TUMath::Interpolate(z, fAxisZIntegr.GetVal(iz_closest),
                              fAxisZIntegr.GetVal(iz_closest + 1), yi_z_lo, yi_z_hi, kLOGLOG);
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::Ni_PrimContribInHalo_z0(Int_t i_bess, Int_t j_cr, Int_t k_ekn)
{
   //--- Returns contribution Ni(z=0) from primary CR j_cr in the diffusive
   //    halo @ Ekn (i-th bessel coef) in [(GeV/n)^{-1} m^{-3}]. See formulae,
   //    e.g., in  Barrau et al., A&A 388 (2002):BEGIN_LATEX(fontsize=14, separator='=', align='lcl')
   //          Ni_{prim}(0) = exp#left(#frac{-V_{c}L}{2K}#right) #times #frac{yi(L)}{Ai#times sinh(SiL/2)}
   //          yi(z) = 2 #int_{0}^{z} exp#left(#frac{Vc(z-z')}{2K}#right) #times sinh(Si(z-z')/2) #times qi_{prim}(z') dz'
   //END_LATEX
   //    N.B.: source spectrum Q_prim(E) is hidden in qi_prim(z').
   //
   //  i_bess            Bessel index
   //  j_cr              CR index
   //  k_ekn             Energy index

   Double_t vc = ValueVc_kpcperMyr();
   Double_t kdiff = ValueK(j_cr, k_ekn);
   Double_t si = Si(i_bess, j_cr, k_ekn, kdiff);

   Double_t yi_L = Ni_PrimContribInHalo_Yiz(i_bess, j_cr, k_ekn, GetParVal_L(), kdiff, si);
   return exp(-0.5 * vc * GetParVal_L() / kdiff)
          * yi_L / (GetAiTerm(i_bess, j_cr, k_ekn) * sinh(si * GetParVal_L() * 0.5));
   // [Ni_prim_z0] = (GeV/n)^{-1} m^{-3}
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::Niz_WithoutParentBETA(Double_t const &ni0, Int_t i_bess,
      Int_t j_cr, Int_t k_ekn, Double_t z)
{
   //--- Returns Ni(z) from Ni(0) when CR has no BETA-unstable parent [(GeV/n)^{-1} m^{-3}]
   //    (see Niz_WithParentBETA otherwise). The formula depends on whether the
   //    CR source is from the galactic disc (e.g., Putze et al. 2010) or from
   //    the whole halo (e.g., Barrau et al., 2002). Note that there is a misprint
   //    in their Eq.(A5)..., where KAiSi should be replaced by KSi], but anyway,
   //    we use below another and more compact form. The formulae for the two terms
   //    apply to the Bessel coefficients of index i: BEGIN_LATEX(fontsize=14, separator='=', align='lcl')
   //      N_{i}^{disc}(z)=N_{i}(0) #times exp#left(#frac{V_{c}z}{2K}#right) #times #frac{sinh(S_{i}[L-z]/2)}{sinh(S_{i} L/2)}
   //      N_{i}^{halo}(z)=N_{i}^{halo}(0) #times exp#left(#frac{V_{c}z}{2K}#right) #times #frac{sinh(S_{i}[L-z]/2)}{sinh(S_{i} L/2)}
   //       + exp#left(-#frac{V_{c}(L-z)}{2K}#right) #times #frac{y_{i}(L)}{KS_{i}}#frac{sinh(S_{i}z/2)}{sinh(S_{i} L/2)}
   //       - #frac{y_{i}(z)}{KS_{i}}
   //    END_LATEX
   //    where Ni(0) is the solution at z=0 with E gains and losses (standard species
   //    in the disc, or exotic species in the halo), and where the quantity BEGIN_LATEX(fontsize=14, separator='=', align='lcl')
   //      y_{i}(z) = 2 #int_{0}^{z} #left[ exp#left(#frac(Vc(z-z')}{2K}#right) #times sinh(S_{i}(z-z')/2) #times q_{i}^{prim}(z') dz' #right]
   //    END_LATEX
   //
   //  ni0               Differential density Ni(z=0)
   //  i_bess            Bessel index
   //  j_cr              CR index
   //  k_ekn             Ekn index
   //  z                 Vertical position in halo [kpc]


   // The solution is even
   z = fabs(z);
   if (z < 1.e-5)
      return ni0;

   // Contribution from standard source (in the disc)
   Double_t kdiff = ValueK(j_cr, k_ekn);
   Double_t si = Si(i_bess, j_cr, k_ekn, kdiff);
   Double_t ni_z = ni0 * exp(0.5 * ValueVc_kpcperMyr() * z / kdiff)
                   * TUMath::SinhRatio(si * GetParVal_L() * 0.5, si * z * 0.5);
   Double_t vc = ValueVc_kpcperMyr();

   // Contributions from DM sources (in the halo)
   if (TUPropagSwitches::IsPrimExotic()) {
      // Loop on all DM sources producing j_cr
      for (Int_t i_src = 0; i_src < TUModelBase::GetNSrcDM(); ++i_src) {
         // If j_cr not in source, skip it
         Int_t j_species = TUModelBase::GetSrcDM(i_src)->IndexCRInSrcSpecies(j_cr);
         if (j_species < 0)
            continue;

         Double_t yi_z = Ni_PrimContribInHalo_Yiz(i_bess, j_cr, k_ekn, z, kdiff, si);
         Double_t yi_L = Ni_PrimContribInHalo_Yiz(i_bess, j_cr, k_ekn, GetParVal_L(), kdiff, si);
         ni_z +=   exp(-0.5 * vc * (GetParVal_L() - z) / kdiff)
                   * yi_L / (si * kdiff) * TUMath::InvSinh(si * GetParVal_L() * 0.5) / TUMath::InvSinh(si * z * 0.5)
                   - yi_z / (kdiff * si);
      }
   }
   return ni_z;
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::Niz_WithParentBETA(Int_t j_parent, Int_t i_bess, Int_t j_cr, Int_t k_ekn, Double_t z)
{
   //--- Returns Ni(z) if CR has a BETA-unstable parent  [(GeV/n)^{-1} m^{-3}]
   //    (see Niz_WithoutParentBETA otherwise).
   //    N.B: the formulae are tricky, especially when a local bubble underdensity
   //    is enabled (i.e., rh>0). Details of the formulae are given in Putze et
   //    al. (2010).
   //
   //  j_parent          Index of the radioactive parent (in list of CRs)
   //  i_bess            Bessel index
   //  j_cr              Index of CR in list
   //  k_ekn             Index of kinetic energy per nucleon
   //  z                 Vertical position in halo [kpc]

   // The solution is even
   z = fabs(z);

   // Base variables required for calculation
   Double_t vc = ValueVc_kpcperMyr();
   Double_t kdiff_daughter = ValueK(j_cr, k_ekn);
   Double_t kdiff_parent = ValueK(j_parent, k_ekn);
   Double_t si_parent = Si(i_bess, j_parent, k_ekn, kdiff_parent);
   Double_t si_daughter = Si(i_bess, j_cr, k_ekn, kdiff_daughter);
   Double_t gamma_inel_daughter = 0.;
   if (TUPropagSwitches::IsDestruction())
      gamma_inel_daughter = TUXSections::Get_nvSigTotIA_ISM0D(j_cr, k_ekn);
   Double_t gamma_rad_daughter = 0.;
   Double_t gamma_rad_parent = 0.;
   if (TUAxesCrE::GetCREntry(j_cr).GetBETAHalfLife() > 1.e-40)
      gamma_rad_daughter = TUAxesCrE::GammaRadBETA_perMyr(j_cr, k_ekn);
   if (TUAxesCrE::GetCREntry(j_parent).GetBETAHalfLife() > 1.e-40)
      gamma_rad_parent = TUAxesCrE::GammaRadBETA_perMyr(j_parent, k_ekn);
   Double_t invsh_parent = TUMath::InvSinh(si_parent * GetParVal_L() * 0.5);
   Double_t ch_daughter = cosh(si_daughter * GetParVal_L() * 0.5);
   Double_t coth_parent = TUMath::Coth(si_parent * GetParVal_L() * 0.5);


   // N(0)^parent if rh\neq0 (local underdensity)
   // -------------------------------------------
   // By construction in the code, the flux Nr of a BETA-unstable CR includes
   // a damping factor, see Donato, Maurin, Taillet, A&A 381, 539 (2002),
   //     damping = exp(-r_hole/sqrt(Kdiff*gamma*tau))
   //             = exp(-r_hole/sqrt(Kdiff/Gamma_rad))
   // to take into account the effect of the local bubble. By construction,
   // the factor is applied to all positions whereas it should apply only
   // in the solar neighbourhood. The daughter of such a nucleus is stable
   // and thus the effective diffusive zone where it contributes to the flux
   // (see, e.g., Taillet and Maurin, 2003) does not feel this damping factor.
   // We thus correct from this 'inappropriate' correction below:
   Double_t damping = 1.;
   if (GetParVal_rh() > 1.e-5) {
      damping = exp(-GetParVal_rh() / sqrt(kdiff_parent / gamma_rad_parent));
      //cout  << GetCREntry(j_cr).GetName() << " " << k_ekn << " damping=" << damping << endl;
   }
   Double_t ni_parent = GetNiTot(i_bess, j_parent, k_ekn) / damping;


   // Some useful variables (k=index for BETA-rad parent, j=index for CR daughter)
   /*-------------------------------------------------------------------|
   |  Qty                 Expression                     |     Unit     |
   |--------------------------------------------------------------------|
   | bob                  1/K^k-1/K^j                    | kpc^{-2} Myr |
   | alpha            exp[ VcL/2*bob) ]                  |      1       |
   | a     Vc^2/(2K^k)*bob+gamma_r^k/K^k-gamma_r^j/K^j   |   kpc^{-2}   |
   | ai            S1D^kVc/2 * (1/K^k-1/K^j)             |   kpc^{-2}   |
   | theta   -gamma_r^k*N^k/[sinh(SiL/2)*K^j*(a^2-ai^2)] |   kpc^2*[N]  |
   | theta_shparent    theta/sinh(SiL/2)                 |   kpc^2*[N]  |
   | fact1           Vc + 2*h*gamma_inel^j               | kpc Myr^{-1} |
   | fact2      Vc*(2-K^j/K^k) + 2*h*gamma_inel^j        | kpc Myr^{-1} |
   |-------------------------------------------------------------------*/
   Double_t bob = (1. / kdiff_parent - 1. / kdiff_daughter);
   Double_t alpha = exp((vc * GetParVal_L() * 0.5) * bob);
   Double_t a = (0.5 * TUMath::Sqr(vc) / kdiff_parent) * bob
                + gamma_rad_parent / kdiff_parent
                - gamma_rad_daughter / kdiff_daughter;
   Double_t ai = 0.5 * vc * si_parent * bob;
   Double_t theta = -gamma_rad_parent * ni_parent * invsh_parent
                    / (kdiff_daughter * (a * a - ai * ai));
   Double_t theta_shparent = -gamma_rad_parent * ni_parent
                             / (kdiff_daughter * (a * a - ai * ai));
   Double_t fact1 = vc + 2.* GetParVal_h() * gamma_inel_daughter;
   Double_t fact2 = vc * (2. - kdiff_daughter / kdiff_parent)
                    + 2.* GetParVal_h() * gamma_inel_daughter;



   // From these variables, we define xi and wi (see Putze et al., 2010):
   //    xi = theta*(-alpha*ai/ch_daughter + a*sh_parent + ai*ch_parent);
   //    wi = theta*(  alpha*ai/ch_daughter*fact1
   //            -  sh_parent*(a*fact2 + ai*kdiff_daughter*s1d_parent)
   //           -  ch_parent*(ai*fact2 + a*kdiff_daughter*s1d_parent)  );
   // which are actually rewritten using theta_shparent instead of theta
   // (to avoid numerical issues in xi and wi for large sinh arguments:
   //    xi = theta_shparent*(-alpha*ai/ch_daughter/sh_parent + a + ai*coth_parent);
   //    wi = theta_shparent*(  alpha*ai/ch_daughter/sh_parent*fact1
   //             -              (a*fact2 + ai*kdiff_daughter*s1d_parent)
   //             -  coth_parent*(ai*fact2 + a*kdiff_daughter*s1d_parent)  );
   Double_t xi = theta_shparent
                 * (-alpha * ai / ch_daughter * invsh_parent + a + ai * coth_parent);
   Double_t wi = theta_shparent
                 * (alpha * ai / ch_daughter * invsh_parent * fact1
                    - (a * fact2 + ai * kdiff_daughter * si_parent)
                    - coth_parent * (ai * fact2 + a * kdiff_daughter * si_parent));


   // Solution
   //---------
   if (z < 1.e-3) {  // Case z=0
      // What we are looking for in this case is the specific contribution to
      // add to Sterm (where the primary Qi and the secondary 2hGamma^{k->j}N^k
      // contributions have already been taken into account; see in
      // CalculateChargedCRs...). The dimension [Sterm_rad] should be the same
      // as [Sterm].
      //
      // The solution when radioactive losses exist is expressed as
      //    N(0) = Ci + xi = [ (Qi+2hGamma^{k->j}N^k(0))/Ai + wi/Ai] + xi
      // The relevance of using a separate term xi does probably not appear clearly
      // here, but it cannot be avoided (see Putze et al., 2010). Note also that we
      // have isolated the explicit contribution from the radioactive term).
      // Actually, in CalculateChargedCRs, the standard contributions (standard
      // primary + secondary) have already been taken into account... the remaining
      // quantity to return is thus
      //    Sterm_rad = wi/Ai + xi

      Double_t Ni_rad = (wi / GetAiTerm(i_bess, j_cr, k_ekn) + xi);
      // [Ni_rad]= (GeV/n)^{-1} m^{-3} = [dNdE]
      // N.B.: in z=0, only returns the radioactive contribution
      //(not the primary and secondary one)
      return Ni_rad;
   } else {  // Case z!=0
      // In this case, it is assumed that the Ni^j(0) have been calculated in a
      // previous step, so it really contains all the contributions (primary,
      // secondary, radioactive...) when GetNiTot(i_bess, j_cr,k_ekn) is called.
      //
      // For the z-dependent formula,
      //    Ni^j(z)= exp(Vc*z/2K^j)*
      //            (   [Ni^j(0)-xi] * [sinh(Si^j(L-z)/2)/sinh(Si^jL/2)]
      //               - alpha*theta*ai * cosh(Si^j z/2.)/cosh(Si^j L/2.)
      //            )
      //        + theta*exp(Vc*z/2K^k)* ( a*sinh(Si^k(L-z)/2.) + ai*cosh(Si^k(L-z)/2.) )
      //
      // However, if Vc=0 and gamma_rad_daughter=0, then s1d_daughter=0,
      // and to avoid a numerical problem, we have to make the subsitution
      //   TUMath::SinhRatio(si_daughter*L*0.5,si_daughter*z*0.5) => (L-z)/L
      Double_t z_trick = 0.;
      if (fabs(si_daughter) < 1.e-40)
         z_trick = (GetParVal_L() - z) / GetParVal_L();
      else
         z_trick = TUMath::SinhRatio(si_daughter * GetParVal_L() * 0.5, si_daughter * z * 0.5);

      Double_t Ni_rad = exp(vc * z * 0.5 / kdiff_daughter) *
                        ((GetNiTot(i_bess, j_cr, k_ekn) - xi) * z_trick
                         - alpha * theta * ai * cosh(si_daughter * z * 0.5) / cosh(si_daughter * GetParVal_L() * 0.5))
                        + theta_shparent * exp(vc * z * 0.5 / kdiff_parent)
                        * (a * TUMath::SinhRatio(si_parent * GetParVal_L() * 0.5, si_parent * z * 0.5)
                           + ai * TUMath::CoshToSinh(si_parent * GetParVal_L() * 0.5, si_parent * z * 0.5));
      // [Ni_rad]= (GeV/n)^{-1} m^{-3} = [dNdE]
      return Ni_rad;
   }
}


//______________________________________________________________________________
Bool_t TUModel2DKisoVc::SetClass_ModelSpecific(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Sets model members not set in the base class (not generic).
   //  init_pars         USINE initialisation parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose) {
      fprintf(f_log, "%s[TUModel2DKisoVc::SetClass_ModelSpecific]\n", indent.c_str());
      // Set Bessel
      fprintf(f_log, "%s### Set NBessel used for calculation\n", indent.c_str());
   }

   const Int_t gg = 3;
   string gr_sub_name[gg] = {"Model2DKisoVc", "Base", "NBessel"};
   Int_t n_bessel =  atoi(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   if (is_verbose) {
      fprintf(f_log, "%s   => NBessel = %d\n", indent.c_str(), n_bessel);
      fprintf(f_log, "%s### Set BESSEL functions J0(zeta_i) and J1(zeta_i)\n", indent.c_str());
   }
   this->SetZerosAndJ1Zeros(n_bessel);

   if (is_verbose)
      fprintf(f_log, "%s### Set NSteps used for integrations (r and z)\n", indent.c_str());

   gr_sub_name[2] = "NIntegrR";
   Int_t nr_integr =  atoi(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   gr_sub_name[2] = "NIntegrZ";
   Int_t nz_integr =  atoi(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   if (is_verbose)
      fprintf(f_log, "%s   => NIntegrR=%d, NIntegrZ=%d\n", indent.c_str(), nr_integr, nz_integr);

   fAxisRIntegr.SetClass(0., TUModelBase::GetAxesTXYZ()->GetMax(1), nr_integr, "RIntegr", "kpc", kLIN);
   fAxisZIntegr.SetClass(0., TUModelBase::GetAxesTXYZ()->GetMax(2), nz_integr, "ZIntegr", "kpc", kLIN);

   if (is_verbose)
      fprintf(f_log, "%s[TUModel2DKisoVc::SetClass_ModelSpecific] <DONE>\n", indent.c_str());
   TUMessages::Indent(false);

   return true;
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::Si(Int_t i_bess, Int_t j_cr, Int_t k_ekn, Double_t const &kdiff)
{
   //--- Returns Si=sqrt( (Vc/K)^2 + (2*zero_i/R)^2 + 4*gamma_rad/K )    [/kpc]
   //    with [Vc]=kpc/Myr and [kdiff]=kpc^2/Myr
   //
   //  i_bess            Bessel index
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  kdiff             Diffusion coefficient for this CR and E [kpc^2/Myr]

   // Is is a BETA-radioactive CR? Is is taken into account for propagation?
   Double_t rad_beta = 0.;
   if (GetCREntry(j_cr).GetBETAHalfLife() > 1.e-40 && TUPropagSwitches::IsDecayBETA())
      rad_beta =  4.* TUAxesCrE::GammaRadBETA_perMyr(j_cr, k_ekn) / kdiff;

   return sqrt(TUMath::Sqr(ValueVc_kpcperMyr() / kdiff) + rad_beta
               + TUMath::Sqr(2.*GetZero(i_bess) / GetParVal_R()));
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::Si(Int_t i_bess, Int_t j_cr, Int_t k_ekn)
{
   //--- Returns Si=sqrt( (Vc/K)^2 + (2*zero_i/R)^2 + 4*gamma_rad/K )    [/kpc]
   //
   //  i_bess            Bessel index
   //  j_cr              CR index
   //  k_ekn             Energy index

   return Si(i_bess, j_cr, k_ekn, ValueK(j_cr, k_ekn));
}

//______________________________________________________________________________
void TUModel2DKisoVc::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message = "Load 1DKisoVc Model and test.";
   TUMessages::Test(f, "TUModel1DKisoVc", message);

   // Set class and test methods
   fprintf(f, " * Load class\n");
   fprintf(f, "   > TUModelBase::SetClass(init_pars=\"%s\", Model, is_verbose=false, f_log=f);\n", init_pars->GetFileNames().c_str());
   TUModelBase::SetClass(init_pars, "Model2DKisoVc", false, f);

   fprintf(f, " * Basic prints\n");
   fprintf(f, "   > PrintSummaryBase(f)\n");
   PrintSummaryBase(f);
   fprintf(f, "   > PrintPars(f)\n");
   PrintPars(f);
   fprintf(f, "   > PrintSummaryModel(f)\n");
   PrintSummaryModel(f);
   fprintf(f, "   > PrintSummaryTransport(f)\n");
   PrintSummaryTransport(f);
   fprintf(f, "   > PrintSources(f, true)\n");
   PrintSources(f, true);
   fprintf(f, "\n");

   fprintf(f, "   > PrintSummary(f)\n");
   PrintSummary(f);
   fprintf(f, "\n");

   //TUCoordTXYZ coords;
   //fprintf(f, "   > TUCoordTXYZ coords; TUI_SetTXYZ(coords);\n");
   //TUI_SetTXYZ(coords);
   //fprintf(f, "   > coords.PrintVals();\n");
   //coords.PrintVals();
   //fprintf(f, "\n");

   fprintf(f, "N.B.: run ./bin/usine to see the model in action!");
   fprintf(f, "\n");
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::ValueK(Int_t j_cr, Int_t k_ekn, Int_t halfbin_status)
{
   //--- Returns K(E) for a given CR and energy bin [kpc^2/Myr].
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  halfbin_status    Evaluated at bine (0), bine-1/2 (-1), or bine+1/2 (+1)
   // Update E coordinate
   TUModelBase::UpdateCoordE(j_cr, k_ekn, halfbin_status);

   return TUModelBase::ValueK(TUModelBase::GetCoordE());
}

//______________________________________________________________________________
Double_t TUModel2DKisoVc::ValueKpp(Int_t j_cr, Int_t k_ekn, Int_t halfbin_status)
{
   //--- Returns Kpp(E) for a given CR and energy bin [GeV^2/Myr].
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  halfbin_status    Evaluated at bine (0), bine-1/2 (-1), or bine+1/2 (+1)
   // Update E coordinate
   TUModelBase::UpdateCoordE(j_cr, k_ekn, halfbin_status);

   return TUModelBase::ValueKpp(TUModelBase::GetCoordE());
}