// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUValsTXYZEVirtual.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUValsTXYZEVirtual                                                   //
//                                                                      //
// Pure virtual (abstract) class for T,X,Y,Z,E dependent values.        //
//                                                                      //
// The abstract class TUValsTXYZEVirtual provides a virtual class       //
// to be used for two possible generic objects that depend on           //
// generic space-time (T,X,Y,Z) and energy (E) coordinates:             //
//    - TUValsTXYZEFormula: formula-based TXYZE dependence;
//    - TUValsTXYZEGrid: values given on a TXYZE grid.
//                                                                      //
// This abstract class allows to use a 'generic' object (this class,    //
// from which TUValsTXYZEFormula and TUValsTXYZEGrid derive) to         //
// describe in the most general way, CR transport (in TUTransport), ISM //
// (in TUMediumTXYZ), sources (in TUSrcPointLike, TUSrcSteadyState),    //
// etc.                                                                 //
//                                                                      //
// The most important virtual methods of the class are (we remind that  //
// models deriving from this class must have them at least):            //
//   - SetClass() initialise the class (formula or values on a grid);
//   - GetFreePars() returns the free parameters (for formula);
//   - ValueTXYZ() restricts to TXYZ-dependent values (no E) and
//     returns the value in the bin/position;
//   - Value() is similar, but applies to the full TXYZE dependence.
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUValsTXYZEVirtual)

//______________________________________________________________________________
TUValsTXYZEVirtual::TUValsTXYZEVirtual()
{

}

//______________________________________________________________________________
TUValsTXYZEVirtual::~TUValsTXYZEVirtual()
{

}
