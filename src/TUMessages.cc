// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <cstdlib>
// ROOT include
// USINE include
#include "../include/TUMessages.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUMessages                                                           //
//                                                                      //
// Encapsulate trivial USINE messages (Warning, Error, Processing...).  //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

NamespaceImp(TUMessages)

//______________________________________________________________________________
void TUMessages::Error(FILE *f, string const &class_name, string const &method, string const &message)
{
   //--- Error message: prints "class_name::method message -> message" and aborts.
   //  f                 File in which to print
   //  class_name        Class from which this function is called
   //  method            Method from which this function is called
   //  message           Error message

   TUMessages::Indent(true);
   string indent = TUMessages::Indent(false);
   fprintf(f, "\n");
   fprintf(f, "%s----------------- ERROR -----------------\n", indent.c_str());
   fprintf(f, "%s  in %s::%s\n", indent.c_str(), class_name.c_str(), method.c_str());
   fprintf(f, "%s    -> %s\n", indent.c_str(), message.c_str());
   fprintf(f, "%s----------------- ABORT -----------------\n", indent.c_str());

   // To have on screen the message, always.
   if (f != stdout) {
      fprintf(stdout, "\n");
      fprintf(stdout, "%s----------------- ERROR -----------------\n", indent.c_str());
      fprintf(stdout, "%s  in %s::%s\n", indent.c_str(), class_name.c_str(), method.c_str());
      fprintf(stdout, "%s    -> %s\n", indent.c_str(), message.c_str());
      fprintf(stdout, "%s----------------- ABORT -----------------\n", indent.c_str());
   }

   abort();
}

//______________________________________________________________________________
string TUMessages::Indent(Bool_t is_add_or_subtract)
{
   //--- Indents 'space_indent' by a given number of spaces (add or subtract).
   //  is_add_or_subtract  Whether to add or cut spaces

   static string space_indent = "";

   if (is_add_or_subtract)
      space_indent = "   " + space_indent;
   else
      space_indent = space_indent.substr(3);
   return space_indent;
}

//______________________________________________________________________________
void TUMessages::Processing(FILE *f, string const &class_name, string const &method, string const &message)
{
   //--- Processing message: prints "class_name::method message".
   //  f                 File in which to print
   //  class_name        Class from which this function is called
   //  method            Method from which this function is called
   //  message           Error message

   string indent = TUMessages::Indent(true);
   fprintf(f, "%s ........... processing ..........\n", indent.c_str());
   fprintf(f, "%s%s::%s  %s\n", indent.c_str(), class_name.c_str(), method.c_str(), message.c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUMessages::Separator(FILE *f, string const &title, Int_t choice)
{
   //--- Prints centred-separator-enclosed title (because it is nicer this way!).
   //  f                 File in which to print
   //  title             Title message to print
   //  choice            Separator choice (0=default, 1 for USINE version, 2 for TUI)

   Int_t center = 22;
   char sep = '-';
   if (choice == 1) {
      sep = '_';
   } else if (choice == 2) {
      sep = '*';
   }


   Int_t j = 0;
   while (title[j] != '\0') {
      j++;
   }
   fprintf(f, "\n");

   for (Int_t i = 0; i < center - j / 2; i++)
      fprintf(f, " ");

   for (Int_t i = 0; i < j + 4; i++)
      fprintf(f, "%c", sep);
   fprintf(f, "\n");
   if (choice == 1)
      fprintf(f, "\n");

   for (Int_t i = 0; i < center - j / 2; i++)
      fprintf(f, " ");
   fprintf(f, "  %s\n", title.c_str());

   for (Int_t i = 0; i < center - j / 2; i++)
      fprintf(f, " ");

   for (Int_t i = 0; i < j + 4; i++)
      fprintf(f, "%c", sep);
   fprintf(f, "\n\n");
}

//______________________________________________________________________________
void TUMessages::Test(FILE *f, string const &class_name, string const &message)
{
   //--- Test message.
   //  f                 File in which to print
   //  class_name        Class from which this function is called
   //  message           Check message

   TUMessages::Separator(f, string(class_name + "::TEST"));
   fprintf(f, "- Class content: %s\n\n", message.c_str());
}

//______________________________________________________________________________
void TUMessages::Warning(FILE *f, string const &class_name, string const &method, string const &message)
{
   //--- Warning message: prints "class_name::method message -> message".
   //  f                 File in which to print
   //  class_name        Class from which this function is called
   //  method            Method from which this function is called
   //  message           Warning message

   fprintf(f, "\n");
   string indent = TUMessages::Indent(true);
   fprintf(f, "%s------- WARNING in %s::%s -------\n", indent.c_str(),
           class_name.c_str(), method.c_str());
   fprintf(f, "%s    Message: %s\n", indent.c_str(), message.c_str());
   fprintf(f, "%s------- WARNING in %s::%s -------\n", indent.c_str(),
           class_name.c_str(), method.c_str());
   TUMessages::Indent(false);
}

