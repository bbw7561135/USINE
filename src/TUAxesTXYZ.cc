// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUAxesTXYZ.h"
#include "../include/TUMessages.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUAxesTXYZ                                                           //
//                                                                      //
// T,X,Y,Z coordinates, time and spatial (<=3D) grids.                  //
//                                                                      //
// The class TUAxesTXYZ provides grids (based on TUAxis) to handle      //
// time dependence (optional) and spatial dependence (from 0D to 3D     //
// coordinates). The first spatial coordinate is always denoted X,      //
// the second Y, and the third Z:                                       //
//    - 1D model => X axis is used;
//    - 2D model => X and Y axes are used;
//    - 3D model => X, Y, and Z axes are used.
// Note that {X,Y,Z} can be assigned to be, for instance,               //
// {"R","theta","Z"} for a 3D cylindrical geometry.                     //
//                                                                      //
// The min and max spatial coordinates are allowed to change at runtime //
// (e.g., using minimisation routines). This is enabled by the use of:  //
//    - fFreePars that monitors any change of the its parameters;
//    - the private methods UpdateTXYZFromFreePars()
//      and UpdateAxesFromFreePars() that define a correspondence
//      between fFreePars and the class parameters;
//    - the public methods GetFreeParListStatus(), UpdateClassFromFreePars()
//      and ResetStatusParsAndParList(), that must be called by the user
//      to i) watch whether the free parameters of the class were
//      changed, ii) to apply this change to the class parameters, and
//      to reset to 'unchanged' the class status.
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUAxesTXYZ)

//______________________________________________________________________________
TUAxesTXYZ::TUAxesTXYZ()
{
   // ****** Default constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUAxesTXYZ::TUAxesTXYZ(TUAxesTXYZ const &axes_txyz)
{
   // ****** Copy constructor ******
   //  axes_txyz         Object to copy

   Initialise(false);
   Copy(axes_txyz);
}

//______________________________________________________________________________
TUAxesTXYZ::~TUAxesTXYZ()
{
   // ****** Default destructor ******

   Initialise(true);
}

//______________________________________________________________________________
void TUAxesTXYZ::Copy(TUAxesTXYZ const &axes_txyz)
{
   //--- Copies axes_txyz in current class.
   //  axes_txyz         Axes to copy from

   // Copy free pars
   if (fFreePars) delete fFreePars;
   if (axes_txyz.IsFreePars()) {
      fFreePars = new TUFreeParList();
      TUFreeParList *tmp = axes_txyz.GetFreePars();
      fFreePars->Copy(*tmp);
      tmp = NULL;
   }

   // Copy fAxes (N.B.: fAxes allocates and set to NULL in Initialise())
   for (Int_t i = 0; i < axes_txyz.GetNAxes(); ++i) {
      if (fAxes[i])
         delete fAxes[i];
      if (axes_txyz.IsTXYZ(i)) {
         fAxes[i] = new TUAxis();
         TUAxis *tmp = axes_txyz.GetAxis(i);
         fAxes[i]->Copy(*tmp);
         tmp = NULL;
      }
   }

   // Update number of space-time dimensions and geometry name
   UpdateNDimAndGeomName();
}

//______________________________________________________________________________
Bool_t TUAxesTXYZ::GetFreeParsStatusAxesTXYZ(Int_t i_axis) const
{
   //--- Returns whether one of the T, X, Y, or Z axis parameter has been changed.
   //  i_axis            Axis to check (T, X, Y, or Z)

   // Different actions depending on the mapping of fFreePars
   // with current class members. We remind that it was decided
   // in SetClass() on the following choice:
   //    0 -> Tmin,  1 -> Tmax
   //    2 -> Xmin,  3 -> Xmax
   //    4 -> Ymin,  5 -> Ymax
   //    6 -> Zmin,  7 -> Zmax
   //
   // => We check whether val min and max have changed
   Int_t i_free_min = fFreePars->IndexInExtClassToIndexPar(2 * i_axis);
   Int_t i_free_max = fFreePars->IndexInExtClassToIndexPar(2 * i_axis + 1);
   if (i_free_min >= 0 && fFreePars->GetParEntry(i_free_min)->GetStatus())
      return kTRUE;
   if (i_free_max >= 0 && fFreePars->GetParEntry(i_free_max)->GetStatus())
      return kTRUE;

   return kFALSE;
}

//______________________________________________________________________________
void TUAxesTXYZ::Initialise(Bool_t is_delete)
{
   //--- Initialises class members (and delete if required).
   //  is_delete         Whether to delete or just initialise

   if (is_delete) {
      if (fAxes) {
         for (Int_t i = 0; i < fNAxes; ++i) {
            if (fAxes[i])
               delete fAxes[i];
            fAxes[i] = NULL;
         }
         delete[] fAxes;
      }
      fAxes = NULL;

      if (fFreePars)
         delete fFreePars;
   } else {
      // Allocates for 4 grids (0->T, 1->X, 2->Y, 3->Z)
      fNAxes = 4;
      fAxes = new TUAxis*[fNAxes];
      for (Int_t i = 0; i < fNAxes; ++i)
         fAxes[i] = NULL;
      fNDim = 0;
   }
   fFreePars = NULL;
}

//______________________________________________________________________________
void TUAxesTXYZ::PrintSummary(FILE *f) const
{
   //--- Prints summary in file f (axes summary and free pars).
   //  f                 File in which to print

   string indent = TUMessages::Indent(true);
   fprintf(f, "%s   Geometry: %s\n", indent.c_str(), fGeomName.c_str());
   for (Int_t i_axis = 0; i_axis < 4; ++i_axis) {
      if (fAxes[i_axis])
         fAxes[i_axis]->PrintSummary(f);
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUAxesTXYZ::SetAxis(Int_t i, Double_t val_min, Double_t val_max, Int_t n,
                         string const &name, string const &unit, gENUM_AXISTYPE axis_type)
{
   //--- Sets grid values for 'i-th' axis.
   //  i                 Axis considered (0=T, 1=X, 2=Y, 3=Z)
   //  val_min           Lower boundary
   //  val_max           Upper boundary
   //  n                 Number of points
   //  name              Coordinate name
   //  unit              Unit name
   //  axis_type         Axis type (kLOG or kLIN, see TUEnum.h)

   // If Axis already exists, just update values
   if (fAxes[i]) {
      fAxes[i]->SetClass(val_min, val_max, n, name, unit, axis_type);
      // Otherwise, create and fill Axis
   } else {
      fAxes[i] = new TUAxis();
      fAxes[i]->SetClass(val_min, val_max, n, name, unit, axis_type);
      UpdateNDimAndGeomName();
   }
}

//______________________________________________________________________________
void TUAxesTXYZ::SetClass(TUInitParList *init_pars, string const &group, string const &subgroup, Bool_t is_verbose, FILE *f_log)
{
   //--- Updates class formula/file from parameter file (belonging to Group#Subgroup).
   //    N.B.: params are ParNames, ParUnits, ParVals, TAxis, XAxis, YAxis, and ZAxis.
   //  init_pars         Initialisation parameters
   //  group             Group name
   //  subgroup          Subroup name
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   const Int_t gg = 3;
   string gr_sub_name[gg] = {group, subgroup, ""};

   string indent = TUMessages::Indent(true);
   if (is_verbose) {
      fprintf(f_log, "%s[TUAxesTXYZ::SetClass]\n", indent.c_str());
      fprintf(f_log, "%s### Set free parameters for spacetime axes [%s#%s]\n",
              indent.c_str(), gr_sub_name[0].c_str(), gr_sub_name[1].c_str());
   }

   // Delete previously existing fFreePars and add free geometry parameters!
   // N.B.: if no free parameters, delete anew fFreePars
   if (fFreePars)
      delete fFreePars;

   fFreePars = new TUFreeParList();
   fFreePars->AddPars(init_pars, group, subgroup, is_verbose, f_log);
   if (is_verbose)
      fFreePars->PrintPars(f_log);
   if (fFreePars->GetNPars() == 0) {
      delete fFreePars;
      fFreePars = NULL;
   }

   // Loop on axes to set values for geometry parameters
   // N.B.: format is "Propag#Model#Geometry_T" (X, Y, or Z)
   const Int_t n_axes = 4;
   string par_geom[n_axes] = {"TAxis", "XAxis", "YAxis", "ZAxis"};
   for (Int_t a = 0; a < n_axes; ++a) {
      // Get parameter value for axis 'a'
      Int_t i_par = init_pars->IndexPar(group, subgroup, par_geom[a]);
      if (i_par < 0) {
         string message = "Parameter " + par_geom[a] + " is missing for model "
                          + subgroup + " in " + init_pars->GetFileNames();
         TUMessages::Error(f_log, "TUAxesTXYZ", "SetClass", message);
      }

      // If axis val ="-", means that it is unused (nothing to do)
      string par_str = init_pars->GetParEntry(i_par).GetVal();
      if (par_str == "-")
         continue;


      // If not empty, must have the correct format, i.e. 'name:[min,max],n,X' (X=LIN or LOG))
      //   => if correct format, fill axis
      vector<string> par_axis;
      TUMisc::String2List(par_str, ":", par_axis);
      if (par_axis.size() != 2) {
         string message = "Badly formatted string=\'" + par_str + "\', must be \'name:[min,max],n,X\' (X=LIN or LOG)";
         TUMessages::Error(f_log, "TUAxesTXYZ", "SetClass", message);
      }
      string name = par_axis[0];

      string par_tmp = par_axis[1];
      par_axis.clear();
      TUMisc::String2List(par_tmp, "[],", par_axis);
      // Check format
      if (par_axis.size() != 4) {
         string message = "Badly formatted string=\'" + par_tmp + "\', must be \'[min,max],n,X\' (X=LIN or LOG)";
         TUMessages::Error(f_log, "TUAxesTXYZ", "SetClass", message);
      }
      string min_str = par_axis[0];
      string max_str = par_axis[1];
      Double_t min = 0.;
      Double_t max = 0.;
      Int_t n = atoi(par_axis[2].c_str());
      gENUM_AXISTYPE axis_type = kLOG;
      TUEnum::Name2Enum(par_axis[3], axis_type);

      // Check if min or max is among on of the free parameters (i.e. same name)
      //  => if yes, uses the given value of the free par., otherwise min (or max)
      // Set also the mapping of fFreePars with current class members, the latter
      // being associated to an index as defined below:
      //    0 -> Tmin,  1 -> Tmax
      //    2 -> Xmin,  3 -> Xmax
      //    4 -> Ymin,  5 -> Ymax
      //    6 -> Zmin,  7 -> Zmax
      Int_t ifree_for_min = -1;
      Int_t ifree_for_max = -1;
      for (Int_t i = 0; i < fFreePars->GetNPars(); ++i) {
         if (min_str == fFreePars->GetParEntry(i)->GetName()) {
            ifree_for_min = i;
            fFreePars->GetParEntry(i)->SetIndexInExtClass(2 * a);
         } else if (max_str == fFreePars->GetParEntry(i)->GetName()) {
            ifree_for_max = i;
            fFreePars->GetParEntry(i)->SetIndexInExtClass(2 * a + 1);
         }
      }

      if (ifree_for_min < 0) min = atof(min_str.c_str());
      else min = fFreePars->GetParEntry(ifree_for_min)->GetVal(false);

      if (ifree_for_max < 0) max = atof(max_str.c_str());
      else max = fFreePars->GetParEntry(ifree_for_max)->GetVal(false);

      // Now that all is checked, sets axis!
      SetAxis(a, min, max, n, name, "kpc", axis_type);
   }
   UpdateNDimAndGeomName();
   if (is_verbose) {
      PrintSummary(f_log);
      fprintf(f_log, "%s[TUAxesTXYZ::SetClass] <DONE>\n\n", indent.c_str());
   }
   TUMessages::Indent(false);
}


//______________________________________________________________________________
void TUAxesTXYZ::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message = "Definition of the geometry (0->3D w/wo T) and axes";
   TUMessages::Test(f, "TUAxesTXYZ", message);

   // Set class and test methods
   fprintf(f, " * Set class and basic methods\n");
   fprintf(f, "   > SetClass(init_pars=\"%s\", group=Model0DLeakyBox, subgroup=Geometry, true, f_log = f);\n",
           init_pars->GetFileNames().c_str());
   SetClass(init_pars, "Model0DLeakyBox", "Geometry", true, f);
   fprintf(f, "   > PrintSummary(f);\n");
   PrintSummary(f);
   fprintf(f, "\n");
   fprintf(f, "   > SetClass(init_pars, group=1DKisoVc, subgroup=Geometry, false);\n");
   SetClass(init_pars, "Model1DKisoVc", "Geometry", false, f);
   fprintf(f, "   > PrintSummary(f);\n");
   PrintSummary(f);
   fprintf(f, "   > GetFreeParsStatusTAxis();   =>  %d\n", GetFreeParsStatusTAxis());
   fprintf(f, "   > GetFreeParsStatusXAxis();   =>  %d\n", GetFreeParsStatusXAxis());
   fprintf(f, "   > GetFreePars()->ResetStatusParsAndParList();\n");
   GetFreePars()->ResetStatusParsAndParList();
   fprintf(f, "   > GetFreeParsStatusXAxis();   =>  %d\n", GetFreeParsStatusXAxis());
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, " * Change free parameters (of axis), propagate through class parameters, and check params status\n");
   Int_t i = GetFreePars()->IndexPar("L");
   fprintf(f, "   > GetFreePars()->IndexPar(\"L\");   =>  i=%d\n", i);
   fprintf(f, "   > GetFreePars()->SetParVal(i, 30., is_par_log=false;\n");
   GetFreePars()->SetParVal(i, 30., false);
   fprintf(f, "   > PrintSummary(f);     [grid as before: not yet updated!]\n");
   PrintSummary(f);
   fprintf(f, "   > GetFreeParsStatusXAxis();   =>  %d\n", GetFreeParsStatusXAxis());
   fprintf(f, "   > UpdateFromFreePars();\n");
   UpdateFromFreePars();
   fprintf(f, "   > PrintSummary(f);     [grid now updated]\n");
   PrintSummary(f);
   fprintf(f, "   > GetFreeParsStatusXAxis();   =>  %d\n", GetFreeParsStatusXAxis());
   fprintf(f, "   > GetFreePars()->ResetStatusParsAndParList()\n");
   GetFreePars()->ResetStatusParsAndParList();
   fprintf(f, "   > GetFreeParsStatusXAxis();   =>  %d\n", GetFreeParsStatusXAxis());
   fprintf(f, "\n");
   fprintf(f, "\n");

   // Copy
   fprintf(f, " * Test Copy method:\n");
   fprintf(f, "   > PrintSummary(f);\n");
   PrintSummary(f);
   fprintf(f, "\n");
   fprintf(f, "   > TUAxesTXYZ axes_txyz; axes_txyz.Copy(*this);\n");
   TUAxesTXYZ axes_txyz;
   axes_txyz.Copy(*this);
   fprintf(f, "   > axes_txyz.PrintSummary(f);\n");
   axes_txyz.PrintSummary(f);
   fprintf(f, "\n");

}

//______________________________________________________________________________
void TUAxesTXYZ::UpdateFromFreePars()
{
   //--- If one of the free parameters (fFreePars) has been updated, update
   //    class parameters.

   if (!fFreePars)
      return;

   // Loop on all free parameters
   for (Int_t i = 0; i < fFreePars->GetNPars(); ++i) {
      // Watch if one of the parameters has been updated
      if (fFreePars->GetParEntry(i)->GetStatus()) {
         // Different actions depending on the mapping of fFreePars
         // with current class members. For this class, it was decided
         // in SetClass() on the following choice:
         //    0 -> Tmin,  1 -> Tmax
         //    2 -> Xmin,  3 -> Xmax
         //    4 -> Ymin,  5 -> Ymax
         //    6 -> Zmin,  7 -> Zmax
         switch (fFreePars->GetParEntry(i)->IndexInExtClass()) {
            case 0 :
               UpdateFromFreePars(0, 1, fFreePars->GetParEntry(i)->GetVal(false));
               break;
            case 1 :
               UpdateFromFreePars(0, 0, fFreePars->GetParEntry(i)->GetVal(false));
               break;
            case 2 :
               UpdateFromFreePars(1, 1, fFreePars->GetParEntry(i)->GetVal(false));
               break;
            case 3 :
               UpdateFromFreePars(1, 0, fFreePars->GetParEntry(i)->GetVal(false));
               break;
            case 4 :
               UpdateFromFreePars(2, 1, fFreePars->GetParEntry(i)->GetVal(false));
               break;
            case 5 :
               UpdateFromFreePars(2, 0, fFreePars->GetParEntry(i)->GetVal(false));
               break;
            case 6 :
               UpdateFromFreePars(3, 1, fFreePars->GetParEntry(i)->GetVal(false));
               break;
            case 7 :
               UpdateFromFreePars(3, 0, fFreePars->GetParEntry(i)->GetVal(false));
               break;
         }
      }
   }
}

//______________________________________________________________________________
void TUAxesTXYZ::UpdateFromFreePars(Int_t i_axis, Bool_t is_min_or_max, Double_t const &val)
{
   //--- Updates axes parameter (min or max) from values given in fFreePars.
   //  i_axis            Axis considered (T, X, Y, or Z)
   //  is_min_or_max     Whether to update min or max grid value
   //  val               New value to use

   if (is_min_or_max)
      SetAxis(i_axis, val, GetAxis(i_axis)->GetMax(), GetAxis(i_axis)->GetN(),
              GetAxis(i_axis)->GetName(), GetAxis(i_axis)->GetUnit(false),
              GetAxis(i_axis)->GetAxisType());
   else
      SetAxis(i_axis, GetAxis(i_axis)->GetMin(), val, GetAxis(i_axis)->GetN(),
              GetAxis(i_axis)->GetName(), GetAxis(i_axis)->GetUnit(false),
              GetAxis(i_axis)->GetAxisType());
}

//______________________________________________________________________________
void TUAxesTXYZ::UpdateNDimAndGeomName()
{
   //--- Updates the number of spatial coordinates (and geometry name).

   fNDim = 0;
   for (Int_t i = 1; i < 4; ++i)
      if (fAxes[i]) {
         ++fNDim;
      }
   fGeomName = Form("%dD", fNDim);

   string coord = "";
   string coord_real = "";
   if (fNDim == 1) {
      coord = "X";
      coord_real = fAxes[1]->GetName();
   } else if (fNDim == 2) {
      coord = "XY";
      coord_real = fAxes[1]->GetName() + fAxes[2]->GetName();
   } else if (fNDim == 3) {
      coord = "XYZ";
      coord_real = fAxes[1]->GetName() + fAxes[2]->GetName() + fAxes[3]->GetName();
   }

   if (IsT()) {
      fGeomName += " (Time-Dependent)";
      coord = "T" + coord;
   } else fGeomName += " (Steady-State)";

   if (fNDim == 1)
      fGeomName = fGeomName + " => " + coord + " axis (=" + coord_real + ")";
   else if (fNDim > 1)
      fGeomName = fGeomName + " => " + coord + " axes (=" + coord_real + ")";

   // Check whether coordinates are rightly set
   // (i.e. fNDim=1->X, fNDim=12->XY, fNDim=3->XYZ)
   if (fNDim == 1 && !fAxes[1])
      TUMessages::Error(stdout, "TUAxesTXYZ", "UpdateNDimAndGeomName",
                        "For 1D, X 'axis' must be used!");
   else if (fNDim == 2 && fAxes[3])
      TUMessages::Error(stdout, "TUAxesTXYZ", "UpdateNDimAndGeomName",
                        "For 2D, XY 'axes' must be used!");
}

