// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <cmath>
#include <cstdlib>
#include <fstream>
// ROOT include
// USINE include
#include "../include/TUAtomProperties.h"
#include "../include/TUCREntry.h"
#include "../include/TUMessages.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUCREntry                                                            //
//                                                                      //
// Description (charts) of a cosmic-ray (CR) entry.                     //
//                                                                      //
// The class TUCREntry provides a description of a CR entry (a CR       //
// being a (anti-)nucleus, a lepton, a gamma-ray, or a neutrino):       //
//    - A, Z, m, r_rms;
//    - BETA and EC decay time, daughter and parents names;
//    - list/branching ratio of 'ghost' nuclei (not used in propagation).
//
// Most of the data members are meaningful for CR nuclei only (they are //
// left empty for neutrino and gamma-rays obviously). A TUCREntry then  //
// constitutes an elementary brick to build lists of CRs, providing     //
// charts to be used for propagation.                                   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUCREntry)

//______________________________________________________________________________
TUCREntry::TUCREntry()
{
   // ****** Default constructor ******
   Initialise();
}

//______________________________________________________________________________
TUCREntry::TUCREntry(TUCREntry const &cr)
{
   // ****** Copy constructor ******
   //  cr                Object to copy

   Initialise();
   Copy(cr);
}

//______________________________________________________________________________
TUCREntry::~TUCREntry()
{
   // ****** Default destructor ******
}

//______________________________________________________________________________
void TUCREntry::Copy(TUCREntry const &cr)
{
   //--- Copies cr into the data members (cr is left unchanged).
   //  cr                 Object to copy from

   fA = cr.GetA();
   fBETAHalfLife = cr.GetBETAHalfLife();
   fBETAHalfLifeErr = cr.GetBETAHalfLifeErr();
   fBETACRFriend = cr.GetBETACRFriend();
   fECHalfLife = cr.GetECHalfLife();
   fECHalfLifeErr = cr.GetECHalfLifeErr();
   fECCRFriend = cr.GetECCRFriend();
   fFamily = cr.GetFamily();
   fGhostBr = cr.GetGhostBr();
   fIsEmpty = cr.IsEmpty();
   fIsUnstable = cr.IsUnstable();
   fm = cr.Getmamu();
   fName = cr.GetName();
   fRrms = cr.GetRrms();
   fSSIsotopeAbundance = cr.GetSSIsotopeAbundance();
   fSSIsotopicFraction = cr.GetSSIsotopicFraction();
   fType = cr.GetType();
   fZ = cr.GetZ();

   // And copy ghosts for this CR!
   fGhosts.clear();
   for (Int_t i = 0; i < cr.GetNGhosts(); ++i)
      fGhosts.push_back(cr.GetGhost(i));
}

//______________________________________________________________________________
Bool_t TUCREntry::IsChargedCR() const
{
   //--- Checks that CR family for bincr is not a neutral particle.

   if (fFamily == kNUC || fFamily == kANTINUC || fFamily == kLEPTON)
      return true;
   else
      return false;
}

//______________________________________________________________________________
Bool_t TUCREntry::IsSame(TUCREntry const &cr) const
{
   //--- Returns whether cr name is the same as the class CR entry.
   //  cr                 CR entry to compare to class content name

   if (cr.GetName() == fName)
      return true;
   else
      return false;
}

//______________________________________________________________________________
Bool_t TUCREntry::IsUnstable(Bool_t is_beta_or_ec) const
{
   //--- Returns whether cr is stable or unstable (BETA or EC decay).
   //    N.B.: half-lives are set -1 when 'stable'.
   //  is_beta_or_ec      Select if for BETA or EC decay

   if (is_beta_or_ec) {
      if (fBETAHalfLife < 0)
         return false;
      else
         return true;
   } else {
      if (fECHalfLife < 0)
         return false;
      else
         return true;
   }
}

//______________________________________________________________________________
void TUCREntry::Initialise()
{
   //--- Initialises the data members values to default values.

   fA = -1;
   fBETAHalfLife = -1.;
   fBETAHalfLifeErr = 0.;
   fBETACRFriend = "";
   fECHalfLife = -1.;
   fECHalfLifeErr = 0.;
   fECCRFriend = "";
   fFamily = kNUC;
   fGhostBr = 0.;
   fIsEmpty = true;
   fIsUnstable = false;
   fm = -1.;
   fName = "";
   fRrms = 0.;
   fSSIsotopeAbundance = 0.;
   fSSIsotopicFraction = 0.;
   fType = "";
   fZ = 0;
   fGhosts.clear();
}

//______________________________________________________________________________
void TUCREntry::Print(FILE *f, Bool_t is_ghost) const
{
   //--- Prints in file f charts for the CR entry.
   //  f                  Output file
   //  is_ghost           Skip write ghost the entry is itself a ghost
   //  daughter           Name of the daughter for an unstable nucleus

   if (fIsEmpty) return;

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);


   fprintf(f, "%s%4d  %3d  %9s   %.3le GeV   %4.3f fm | %5s ",
           indent.c_str(), fZ, fA, fName.c_str(), GetmGeV(), fRrms, fType.c_str());
   string type = fType;
   TUMisc::UpperCase(type);

   if (type == "BETA-FED")
      fprintf(f, "-> %5s", fBETACRFriend.c_str());
   if (type == "EC-FED")
      fprintf(f, "-> %5s", fECCRFriend.c_str());
   else if (type == "MIX-FED") {
      fprintf(f, "-> %5s (BETA)", fBETACRFriend.c_str());
      fprintf(f, " and %5s (EC)", fECCRFriend.c_str());
   } else if (type == "BETA")
      fprintf(f, " t1/2=%.3e Myr (+/-%.2e)", fBETAHalfLife, fBETAHalfLifeErr);
   else if (type == "EC")
      fprintf(f, " t1/2=%.3e Myr (+/-%.2e)", fECHalfLife, fECHalfLifeErr);
   else if (type == "MIX")
      fprintf(f, " t1/2(BETA)=%.3e Myr (+/-%.2e)  and  t1/2(EC)=%.3e Myr (+/-%.2e)",
              fBETAHalfLife, fBETAHalfLifeErr, fECHalfLife, fECHalfLifeErr);
   if (is_ghost) PrintGhosts(f, true);
   else fprintf(f, "\n");
}

//______________________________________________________________________________
void TUCREntry::PrintGhosts(FILE *f, Bool_t is_only_ghosts) const
{
   //--- Prints in file f all 'ghost' nuclei for this CR entry.
   //  f                  Output file
   //  is_only_ghosts     Skip name of CR if only ghosts to write

   if (!is_only_ghosts)
      fprintf(f, "     %-6s <= ", GetName().c_str());
   else
      fprintf(f, "  ||  ");

   if (GetNGhosts() == 0) {
      printf(" [no ghosts]\n");
      return;
   }

   for (Int_t g = 0; g < GetNGhosts(); g ++)
      fprintf(f, " %-4s(%2.1f%%)", fGhosts[g].GetName().c_str(), fGhosts[g].GetGhostBr());
   fprintf(f, "\n");
}

//______________________________________________________________________________
void TUCREntry::SetEntry(Int_t a, Int_t z, Double_t const &m_gev, string const &name)
{
   //--- Fills CR entry from mass, A, and Z.
   //  a                  Atomic number
   //  z                  Charge
   //  m_amu              Mass [GeV]
   //  name               Name (e.g. 12C)

   fA = a;
   fZ = z;
   fm = m_gev / TUPhysics::Convert_amu_to_GeV();

   // Set name
   if (name != "")
      fName = name;
   else {
      TUAtomElements atom;
      string tmp = atom.ZToElementName(z);
      fName = Form("%d%s", fA, tmp.c_str());
   }

   // Set type and family
   SetType("STABLE");
   if (fA > 0) {
      if (fZ >= 0)
         fFamily = kNUC;
      else
         fFamily = kANTINUC;
   }
   fIsEmpty = false;
}

//______________________________________________________________________________
void TUCREntry::SetEntry(string const &crentry_line, TUAtomElements const &atom, Bool_t is_set_ghost)
{
   //--- Fills class (entry) from a chart-formatted line (see, e.g.,
   //    in $USINE/inputs/crcharts_Zmax30_ghost97.dat). Ghosts
   //    nuclei (used only for X-sections calculations) are optional.
   // INPUT:
   //  crentry_line       Chart-formatted line
   //  atom               TUAtomElements used to get Z from ghost names
   //  is_set_ghosts          Are 'ghosts' properties loaded?
   // OUTPUT:
   //  entry              CR entry file from line

   // Remove blank spaces from line start/stop
   if (crentry_line.find_first_not_of(" ") == string::npos) return;
   string line = TUMisc::RemoveBlancksFromStartStop(crentry_line);

   // Check line format
   vector<string> charts;
   TUMisc::String2List(line, " \t", charts);
   if (charts.size() < 19) {
      string message = "Ill-defined format for CR chart = \"" + line + "\". See format"
                       " in $USINE/inputs/crcharts_Zmax30_ghost97.dat";
      TUMessages::Error(stdout, "TUCREntry", "SetEntry", message);
   }

   fm = atof(charts[0].c_str());
   fZ = atoi(charts[1].c_str());
   fA = atoi(charts[2].c_str());
   fName = charts[3];
   TUMisc::UpperCase(fName);
   SetType(charts[4]);
   fRrms = atof(charts[5].c_str());
   // skip || = charts[6]
   // BETA-decay-related
   Double_t beta_hl = atof(charts[7].c_str());
   Double_t beta_hl_err = atof(charts[8].c_str());
   string beta_hl_unit = charts[9];
   if (beta_hl_unit != "XXX") {
      fBETAHalfLife = beta_hl * TUPhysics::Convert_to_Myr(beta_hl_unit);
      fBETAHalfLifeErr = beta_hl_err * TUPhysics::Convert_to_Myr(beta_hl_unit);
   }
   TUMisc::UpperCase(charts[10]);
   fBETACRFriend = charts[10];
   // skip | = charts[11]
   // EC-decay-related
   Double_t ec_hl = atof(charts[12].c_str());
   Double_t ec_hl_err = atof(charts[13].c_str());
   string ec_hl_unit = charts[14];
   if (ec_hl_unit != "XXX") {
      fECHalfLife = ec_hl * TUPhysics::Convert_to_Myr(ec_hl_unit);
      fECHalfLifeErr = ec_hl_err * TUPhysics::Convert_to_Myr(ec_hl_unit);
   }
   TUMisc::UpperCase(charts[15]);
   fECCRFriend = charts[15];
   // skip || = charts[16]

   fIsEmpty = false;
   SetFamily();

   // If fill ghost, read them!
   if (is_set_ghost) {
      fGhosts.clear();
      Int_t n_ghosts = atoi(charts[17].c_str());
      for (Int_t i = 0; i < n_ghosts; ++i) {
         Int_t k = 18 + 4 * i;
         // skip | = charts[k]
         Int_t a = atoi(charts[k + 2].c_str());
         Int_t z = atoi(charts[k + 1].c_str());
         string name = "";
         if (z == 0 && a == 1)
            name = "1n";
         else {
            name = Form("%d%s", a, atom.ZToElementName(z).c_str());
            TUMisc::UpperCase(name);
         }
         TUCREntry ghost;
         ghost.SetGhost(a, z, (Double_t)a, atof(charts[k + 3].c_str()), name);
         AddGhost(ghost);
      }
   }
}

//______________________________________________________________________________
void TUCREntry::SetEntry(string const &cr, string const &f_charts, Bool_t is_set_ghost)
{
   //--- Sets charts for 'cr' from the file f_charts.
   //  cr                 Name (case insensitive) of the CR
   //  f_charts           File from which to read the charts
   //  is_set_ghost       Are 'ghosts' properties loaded?

   // CR names are case insensitive
   string cr_up = cr;
   TUMisc::UpperCase(cr_up);

   // If input file exist, read it (else abort)
   ifstream f_read(f_charts.c_str());
   TUMisc::IsFileExist(f_read, f_charts);

   Initialise();


   // Read chart-formatted line
   string line;
   TUAtomProperties atom;
   Bool_t is_found = false;
   while (getline(f_read, line)) {
      // Skip comments (start with '#') and empty lines
      string chart_line = TUMisc::RemoveBlancksFromStartStop(line);
      if (chart_line[0] == '#' || chart_line.size() == 0) continue;
      SetEntry(chart_line, atom, is_set_ghost);
      if (cr_up == fName) {
         is_found = true;
         break;
      }
   }

   // If CR no found
   if (!is_found) {
      Initialise();
      string message = "Could not find any nucleus with name " + cr;
      TUMessages::Warning(stdout, "TUCREntry", "SetEntry", message);
   }
}

//______________________________________________________________________________
void TUCREntry::SetFamily()
{
   //--- Finds and sets cosmic-ray family from its properties.

   if (fIsEmpty) {
      string message = "CR entry has not been filled yet";
      TUMessages::Error(stdout, "TUCREntry", "SetFamily", message);
   }

   if (fZ > 0 && fA > 0)
      fFamily = kNUC;
   else if (fZ < 0 && fA > 0)
      fFamily = kANTINUC;
   else if (fA == 0 && fabs(fZ) == 1)
      fFamily = kLEPTON;
}

//______________________________________________________________________________
void TUCREntry::SetType(string const &type)
{
   //--- Set type and also fills fIsUnstable. Note that is type=STABLE, the
   //    members fBETACRFriend, fBETAHalLife, etc. are set to '-1'.
   //  type               Type to set

   fType = type;
   TUMisc::UpperCase(fType);
   if (fType != "STABLE" && fType != "BETA" && fType != "EC" && fType != "MIX"
         && fType != "BETA-FED" && fType != "EC-FED" && fType != "MIX-FED") {
      string message = "Type \'" + fType + "\' does not exist: choose among"
                       + " STABLE, BETA, EC, MIX, BETA-FED, EC-FED, MIX-FED";
      TUMessages::Error(stdout, "TUCREntry", "SetType", message);
   }

   if (fType == "BETA" || fType == "EC" || fType == "MIX")
      fIsUnstable = true;
   else {
      fIsUnstable = false;
      fBETAHalfLife = -1.;
      fBETAHalfLifeErr =  0.;
      fECHalfLife = -1.;
      fECHalfLifeErr =  0.;
      if (fType == "STABLE") {
         fBETACRFriend = "";
         fECCRFriend = "";
      }
   }
}

//______________________________________________________________________________
void TUCREntry::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "Simple list of properties useful to describe the CR\n"
                    "propagation (A, Z, name, m, BETA decay, EC decay,"
                    " list of ghosts...).";
   TUMessages::Test(f, "TUCREntry", message);

   // Fill entry from file of charts
   string f_charts = TUMisc::GetPath("$USINE/inputs/crcharts_Zmax30_ghost97.dat");
   fprintf(f, "   > SetEntry(CR_name=10Be, file_name=$USINE/inputs/crchartsZmax30_ghost97.dat, is_set_ghost=true);\n");
   SetEntry("10Be", f_charts, true);
   fprintf(f, "   > GetNGhosts();  => %d\n", GetNGhosts());
   fprintf(f, "   > PrintGhosts(f);\n");
   PrintGhosts(f);
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "   > IsChargedCR();  => %d\n", IsChargedCR());
   fprintf(f, "\n");
   fprintf(f, "   > SetEntry(CR_name=4He, file_name=$USINE/inputs/crchartsZmax30_ghost97.dat, is_set_ghost=true);\n");
   SetEntry("4He", f_charts, true);
   fprintf(f, "   > GetNGhosts();  => %d\n", GetNGhosts());
   fprintf(f, "   > Print(f, is_ghost=true);\n");
   Print(f, true);
   fprintf(f, "\n");
   fprintf(f, "   > IsChargedCR();  => %d\n", IsChargedCR());

   // Copy
   fprintf(f, "\n");
   fprintf(f, " * Create 'cr' filled with 9Be, then copy it to class:\n");
   fprintf(f, "   > TUCREntry cr; cr.SetEntry(9Be, file_name, is_ghost=true);\n");
   TUCREntry cr;
   cr.SetEntry("9Be", f_charts, true);
   fprintf(f, "   > cr.Print(f, true);\n");
   cr.Print(f, true);
   fprintf(f, "   > Copy(cr);\n");
   Copy(cr);
   fprintf(f, "   > Print(f, true);\n");
   Print(f, true);
}
