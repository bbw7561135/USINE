// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUSrcPointLike.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUSrcPointLike                                                       //
//                                                                      //
// Point-like source: time-dependent spectrum/position for a single CR. //
//                                                                      //
// The class TUSrcPointLike provides the description of a generic       //
// point-like source, whose position can depend on time (the class      //
// derives from the abstract class TUSrcVirtual). It corresponds to a   //
// single species, for which the spectrum is a generic function of      //
// time and energy (based on TUValsTXYZE object). The key parameters    //
// are:                                                                 //
//    - source name, position, and dates;
//    - spectrum description (and its parameters).
//                                                                      //
// Free parameters and formulae:                                        //
// -+-+-+-+-+-+-+-+-+-+-+-+-+-
// Some class members can be described by formulae (which depend on free//
// parameter values, see TUValsTXYZVirtual). Whenever a parameter value //
// is changed, it has to be propagated to the formula. There are two    //
// ways to achieve that:
//    A. you can decide not to care about it: the class TUValsTXYZEFormula
//      will do the job automatically for you, whenever any method ValueXXX()
//      of this class is called. Indeed, the latter will invariably end up
//      calling TUValsTXYZEFormula::EvalFormula() or the like of it, and
//      then TUValsTXYZEFormula::UpdateFromFreeParsAndResetStatus()
//     to do the job.
//    B. call UpdateFromFreeParsAndResetStatus() to update all formula found
//      in this class (and reset TUFreeParList status to 'updated'.
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUSrcPointLike)

//______________________________________________________________________________
TUSrcPointLike::TUSrcPointLike() : TUSrcVirtual()
{
   // ****** Default constructor ******

   Initialise(false);

   // Allocate for fTDepPosition
   const Int_t n = 3;
   fTDepPosition = new TUValsTXYZEFormula*[n];
   fIsDeleteTDepPosition = new Bool_t[n];
   for (Int_t i = 0; i < n; ++i) {
      fTDepPosition[i] = NULL;
      fIsDeleteTDepPosition[i] = true;
   }
}

//______________________________________________________________________________
TUSrcPointLike::TUSrcPointLike(TUSrcPointLike const &src_pointlike) : TUSrcVirtual()
{
   // ****** Copy constructor ******
   //  src_pointlike     Pointer used to fill class

   Initialise(false);

   // Allocate for fTDepPosition
   const Int_t n = 3;
   fTDepPosition = new TUValsTXYZEFormula*[n];
   fIsDeleteTDepPosition = new Bool_t[n];
   for (Int_t i = 0; i < n; ++i) {
      fTDepPosition[i] = NULL;
      fIsDeleteTDepPosition[i] = true;
   }

   Copy(src_pointlike);
}

//______________________________________________________________________________
TUSrcPointLike::~TUSrcPointLike()
{
   // ****** Default destructor ******

   Initialise(true);
   delete[] fTDepPosition;
   delete[] fIsDeleteTDepPosition;
}

//______________________________________________________________________________
void TUSrcPointLike::CopySpectrumOrSpatDist(TUValsTXYZEVirtual *spect_or_spatdist, Bool_t is_spect_or_spatdist)
{
   //--- Copies 'spect_or_spatdist' to describe spectrum or spatial distribution
   //    of this class.
   //  spect_or_spatdist      Object is the spectrum (or spatial distribution to use)
   //  is_spect_or_spatdist   Spectrum or spatial distribution?

   if (!spect_or_spatdist)
      return;

   if (is_spect_or_spatdist) {
      if (fIsDeleteSrcSpectrum && fSrcSpectrum)
         delete fSrcSpectrum;
      fSrcSpectrum = NULL;
      fIsDeleteSrcSpectrum = true;
      if (fIsDeleteSrcSpectrum)
         fSrcSpectrum = spect_or_spatdist->Clone();
   } else {
      string message = "You cannot call this method to fill the class spatial distribution,"
                       " because a point-like source has none";
      TUMessages::Error(stdout, "TUSrcPointLike", "Copy", message);
   }
}

//______________________________________________________________________________
void TUSrcPointLike::Copy(TUSrcPointLike const &src_pointlike)
{
   //--- Copies object in class.
   //  src_pointlike     Object to copy from

   Initialise(true);

   fSrcName = src_pointlike.GetSrcName();
   fTStart = src_pointlike.GetTStart();
   fTStop = src_pointlike.GetTStop();


   fIsDeleteSrcSpectrum = src_pointlike.IsDeleteSrcSpectrum();
   if (fIsDeleteSrcSpectrum) {
      TUValsTXYZEVirtual *src = src_pointlike.GetSrcSpectrum();
      if (src)
         fSrcSpectrum = src->Clone();
      src = NULL;
   } else
      fSrcSpectrum = src_pointlike.GetSrcSpectrum();

   const Int_t n = 3;
   for (Int_t i = 0; i < n; ++i) {
      fIsDeleteTDepPosition[i] = src_pointlike.IsDeleteTDepPosition(i);
      if (fIsDeleteTDepPosition[i]) {
         TUValsTXYZEFormula *tmp = src_pointlike.GetTDepPosition(i);
         if (tmp)
            fTDepPosition[i] = tmp->Clone();
         tmp = NULL;
      } else
         fTDepPosition[i] = src_pointlike.GetTDepPosition(i);
   }
}

//______________________________________________________________________________
void TUSrcPointLike::FillSpectrumFromTemplate(TUSrcTemplates *templ, string const &templ_name, FILE *f_log)
{
   //---Fills fSpectrum from template matching 'templ_name'.
   //  templ             List of spectra and spatial distrib. templates
   //  templ_name        Source spectrum template to search for
   //  f_log             Log for USINE

   if (!templ)
      return;

   string indent = TUMessages::Indent(true);
   fprintf(f_log, "%s### Create point-like source spectrum from template %s\n",
           indent.c_str(), templ_name.c_str());

   // Search for index i template corresponding to templ_name
   Int_t i_templ = templ->IndexSrcTemplate(templ_name, true);
   if (i_templ < 0)
      TUMessages::Error(f_log, "TUSrcPointLike", "FillSpectrumFromTemplate", string("Template"
                        + templ_name + " not found!"));

   // Delete (if required) spectrum
   if (fSrcSpectrum && fIsDeleteSrcSpectrum)
      delete fSrcSpectrum;

   fSrcSpectrum = templ->GetSrc(i_templ)->Clone();
   fIsDeleteSrcSpectrum = true;

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUSrcPointLike::FillSpectrumOrSpatialDist(string const &formula, Bool_t is_verbose, FILE *f_log, string const &commasep_vars,
      TUFreeParList *free_pars, Bool_t is_use_or_copy_freepars, Bool_t is_spect_or_spatdist)
{
   //--- Allocate and updates fSrcSpectrum for formula.
   //  formula                  Formula to implement (see http://warp.povusers.org/FunctionParser/fparser.html)
   //  is_verbose               Verbose on or off
   //  f_log                    Log for USINE
   //  commasep_vars            List of comma-separated variables, e.g., "t,x,y,z" (default is no var)
   //  free_pars                List of free parameters (used only if formula and free_pars!=NULL)
   //  is_use_or_copy_freepars  Whether the parameters passed must be added (copied) or just used (pointed at)
   //  is_spect_or_spatdist   Spectrum or spatial distribution?

   // No source spatial distribution for Point-like source
   if (!is_spect_or_spatdist)
      return;

   if (fIsDeleteSrcSpectrum && fSrcSpectrum)
      delete fSrcSpectrum;

   fSrcSpectrum = new TUValsTXYZEFormula();
   fIsDeleteSrcSpectrum = true;
   fSrcSpectrum->SetClass(formula, is_verbose, f_log, commasep_vars, free_pars, is_use_or_copy_freepars);
}

//______________________________________________________________________________
void TUSrcPointLike::FillSpectrumOrSpatialDistFromTemplate(TUSrcTemplates *templ, string const &templ_name,
      Bool_t is_spect_or_spatdist, FILE *f_log)
{
   //--- Fills spectrum and spat.dist. ((none for point-like src) from template template.
   //  templ                  List of templates to use
   //  templ_name             Name of template to pick
   //  is_spect_or_spatdist   Whether we fill spectrum or spatial dist.
   //  f_log             Log for USINE

   if (!templ)
      return;

   if (is_spect_or_spatdist)
      FillSpectrumFromTemplate(templ, templ_name, f_log);

}

//______________________________________________________________________________
void TUSrcPointLike::Initialise(Bool_t is_delete)
{
   //---Initialises class members (and delete if required).
   //  is_delete         Whether to delete or just initialise

   const Int_t n = 3;

   if (is_delete) {
      if (fIsDeleteSrcSpectrum && fSrcSpectrum) delete fSrcSpectrum;
      for (Int_t i = 0; i < n; ++i) {
         if (fTDepPosition[i] && fIsDeleteTDepPosition)
            delete fTDepPosition[i];
         fTDepPosition[i] = NULL;
      }
   }

   fIsDeleteSrcSpectrum = true;
   fSrcSpectrum = NULL;
   fTStart = 1.e40;
   fTStop = -1.e40;
}

//______________________________________________________________________________
void TUSrcPointLike::PrintSrc(FILE *f, Bool_t is_header) const
{
   //--- Prints in f, the (time-dependent) position of the source.
   //  f                 File in which to print
   //  header            Whether to write source info

   string indent = TUMessages::Indent(true);

   if (is_header)
      fprintf(f, "%s Point-like source = %s\n", indent.c_str(), fSrcName.c_str());

   const Int_t n = 3;
   string tmp[n] = {"X(t)", "Y(t)", "Z(t)"};
   for (Int_t i = 0; i < 3; ++i) {
      if (fTDepPosition[i])
         fTDepPosition[i]->PrintSummary(f, tmp[i]);
   }

   if (fSrcSpectrum)
      fSrcSpectrum->PrintSummary(f, "Spectrum");

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUSrcPointLike::SetTDepPosition(Int_t i_coord, string const &fparser_formula)
{
   //--- Sets time-dependent formula for fTDepPosition[i_coord] (function parser formatted).
   //  i_coord           Select coordinate (X, Y, or Z)
   //  fparser_formula   Formula depending on t

   if (fTDepPosition[i_coord] && fIsDeleteTDepPosition[i_coord])
      delete fTDepPosition[i_coord];

   fTDepPosition[i_coord] = new TUValsTXYZEFormula();
   fTDepPosition[i_coord]->SetFormula(fparser_formula, /*commasep_vars*/"t");
}

//______________________________________________________________________________
void TUSrcPointLike::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message = "Generic point-like source (position and source spectrum) for a single CR species";
   TUMessages::Test(f, "TUSrcPointLike", message);


   // Set class and test methods
   fprintf(f, " * Base class:\n");
   fprintf(f, "   > SetSrcName(src_pointlike);\n");
   SetSrcName("src_test");
   fprintf(f, "   > SetTDepPosition(0, formula_t=3*t);\n");
   SetTDepPosition(0, "3*t");
   fprintf(f, "   > SetTDepPosition(1, formula_t=2*t);\n");
   SetTDepPosition(1, "2*t");
   fprintf(f, "   > SetTDepPosition(2, formula_t=t*t*t);\n");
   SetTDepPosition(2, "t*t*t");
   fprintf(f, "   > ValueSrcPosition(0, 0.);   =>   %le\n", ValueSrcPosition(0, 0.));
   fprintf(f, "   > ValueSrcPosition(0, 1.);   =>   %le\n", ValueSrcPosition(0, 1.));
   fprintf(f, "   > ValueSrcPosition(0, 2.);   =>   %le\n", ValueSrcPosition(0, 2.));
   fprintf(f, "   > ValueSrcPosition(1, 2.);   =>   %le\n", ValueSrcPosition(1, 2.));
   fprintf(f, "   > ValueSrcPosition(2, 2.);   =>   %le\n", ValueSrcPosition(2, 2.));
   fprintf(f, "   > PrintSrc(f);\n");
   PrintSrc(f);
   fprintf(f, "\n");

   fprintf(f, " * Load source spectrum from available src templates:\n");
   fprintf(f, "   > TUSrcTemplates templ;\n");
   TUSrcTemplates templ;
   fprintf(f, "   > templ.SetClass(init_pars=\"%s\", is_verbose=false, f_log=f);\n", init_pars->GetFileNames().c_str());
   templ.SetClass(init_pars, false, f);
   fprintf(f, "\n");
   fprintf(f, "   > FillSpectrumFromTemplate(&templ, POWERLAW, f_log=f);\n");
   FillSpectrumFromTemplate(&templ, "POWERLAW", f);
   fprintf(f, "   > PrintSrc(f);\n");
   PrintSrc(f);
   fprintf(f, "\n");

   fprintf(f, " * Copy class:\n");
   fprintf(f, "   > TUSrcPointLike src_copy;\n");
   TUSrcPointLike src_copy;
   fprintf(f, "   > src_copy.Copy(*this);\n");
   src_copy.Copy(*this);
   fprintf(f, "   > src_copy.PrintSrc(f);\n");
   src_copy.PrintSrc(f);
   fprintf(f, "\n");

   fprintf(f, " * Modify parameters:\n");
   fprintf(f, "   > ValueSrcPosition(0, 2.);   =>   %le\n", ValueSrcPosition(0, 2.));
   fprintf(f, "   > ValueSrcPosition(1, 2.);   =>   %le\n", ValueSrcPosition(1, 2.));
   fprintf(f, "   > ValueSrcPosition(2, 2.);   =>   %le\n", ValueSrcPosition(2, 2.));
   fprintf(f, "   > TUI_ModifyFreeParameters();  [uncomment in TEST() to enable it]\n");
   //TUI_ModifyFreeParameters();
   fprintf(f, "   > ValueSrcPosition(0, 2.);   =>   %le\n", ValueSrcPosition(0, 2.));
   fprintf(f, "   > ValueSrcPosition(1, 2.);   =>   %le\n", ValueSrcPosition(1, 2.));
   fprintf(f, "   > ValueSrcPosition(2, 2.);   =>   %le\n", ValueSrcPosition(2, 2.));
   fprintf(f, "\n");
}

//______________________________________________________________________________
Bool_t TUSrcPointLike::TUI_ModifyFreeParameters()
{
   //--- Text User Interface to modufy free parameters of this source.

   // Create a temporary TUFreeParList to handle all source parameters
   TUFreeParList tmp_pars;

   // Get free pars for spectrum
   if (fSrcSpectrum)
      tmp_pars.AddPars(fSrcSpectrum->GetFreePars(), true);

   // Get pars for spectrum
   const Int_t n = 3;
   for (Int_t k = 0; k < n; ++k) {
      if (fTDepPosition[k])
         tmp_pars.AddPars(fTDepPosition[k]->GetFreePars(), true);
   }

   PrintSrc();
   tmp_pars.TUI_Modify();

   if (tmp_pars.GetParListStatus()) {
      UpdateFromFreeParsAndResetStatus();
      return true;
   } else
      return false;
}

//______________________________________________________________________________
void TUSrcPointLike::UpdateFromFreeParsAndResetStatus()
{
   //--- Updates all formulae from updated fFreePars values and reset fFreePars status.

   // Update spectrum
   if (fSrcSpectrum)
      fSrcSpectrum->UpdateFromFreeParsAndResetStatus(true);

   // Update Src position
   if (fTDepPosition) {
      const Int_t n = 3;
      for (Int_t i = 0; i < n; ++i) {
         if (fTDepPosition[i])
            fTDepPosition[i]->UpdateFromFreeParsAndResetStatus(true);
      }
   }
}

//______________________________________________________________________________
void TUSrcPointLike::UseSpectrumOrSpatDist(TUValsTXYZEVirtual *spect_or_spatdist, Bool_t is_spect_or_spatdist)
{
   //--- Uses 'spect_or_spatdist' to describe spectrum or spatial distribution
   //    of this class.
   //  spect_or_spatdist      Object is the spectrum (or spatial distribution to use)
   //  is_spect_or_spatdist   Spectrum or spatial distribution?

   if (is_spect_or_spatdist) {
      if (fIsDeleteSrcSpectrum && fSrcSpectrum)
         delete fSrcSpectrum;
      fSrcSpectrum = NULL;
      fIsDeleteSrcSpectrum = false;
      if (spect_or_spatdist)
         fSrcSpectrum = spect_or_spatdist;
   } else {
      string message = "You cannot call this method to fill the class spatial distribution,"
                       " because a point-like source has none";
      TUMessages::Error(stdout, "TUSrcPointLike", "Use", message);
   }
}
//______________________________________________________________________________
void TUSrcPointLike::UseTDepPosition(Int_t i_coord, TUValsTXYZEFormula *pos)
{
   //--- Uses 'pos' to describe positions of this class.
   //  i_coord           Select coordinate (X, Y, or Z)
   //  pos               Formula position on t

   if (fTDepPosition[i_coord] && fIsDeleteTDepPosition[i_coord])
      delete fTDepPosition[i_coord];

   fTDepPosition[i_coord] = pos;
   fIsDeleteTDepPosition[i_coord] = false;
}
