// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <algorithm>
#include <string>
#include <vector>
using namespace std;
// ROOT include
// DAVID ROOT
#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TMatrixD.h>
#include <TMatrixDBase.h>
#include <TMatrixDSym.h>
#include <TMatrixDSymEigen.h>
#include <TVectorD.h>
#include <TRandom.h>
#include <TStyle.h>
// USINE include
#include "../include/TUMath.h"
#include "../include/TUMessages.h"
#include "../include/TUMisc.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUMath                                                               //
//                                                                      //
// Encapsulate mathematical functions.                                  //
//                                                                      //
// The namespace TUMath encapsulates:                                   //
//    - Interpolation (lin-lin, lin-log, log-lin, and log-log);
//    - Derivative;
//    - Expansion for a few combination of sinh and cosh-like formulae;
//    - Bessel formulae from various formulae;
// BEGIN_HTML
// <b>... also plenty from </b> <a href="http://root.cern.ch/root/html/TMath.html">TMath ROOT CERN namespace</a>.
// END_HTML
//////////////////////////////////////////////////////////////////////////

NamespaceImp(TUMath)

//______________________________________________________________________________
void TUMath::Bias(vector<Double_t> const &x, vector<Double_t> &y, vector<Double_t> const &x_bias, vector<Double_t> const &y_bias, gENUM_INTERPTYPE interp_type)
{
   //--- Multiplicative bias y(x) based on interpolated values of y_bias(x_bias) on x.
   // INPUTS:
   //  x                     [N] x-axis of quantities to bias
   //  y                     [N] y-axis quantities to bias
   //  x_bias                [NBias] Position x at which bias is calculated
   //  y_bias                [NBias] Bias value to apply
   //  interp_type       Enumerators for enabled interpolations (see TUEnum)
   // OUTPUTS:
   //  y                     [N] y-axis biased quantities

   Int_t n_bias = x_bias.size();
   Int_t n = x.size();
   // Loop on model energies and interpolate
   for (Int_t i = 0; i < n; ++i) {
      Double_t bias = 1.;
      if (x[i] < x_bias[0] + 1.e-10)
         bias = y_bias[0];
      else if (x[i] > x_bias[n_bias - 1] - 1.e-10)
         bias = y_bias[n_bias - 1];
      else {
         Int_t k = TUMath::BinarySearch(n_bias, &x_bias[0], x[i]);
         bias = TUMath::Interpolate(x[i], x_bias[k], x_bias[k + 1], y_bias[k], y_bias[k + 1], interp_type);
      }
      //cout << "  Model bias (model): " << i << " " << x[i] << " " << y[i] << " " << y[i]*bias << endl;

      // Apply bias
      y[i] *= bias;
   }
}

//______________________________________________________________________________
Int_t TUMath::BinarySearch(Int_t n, Double_t const *array, Double_t const &value)
{
   //--- Returns value closest to 'value' in 'array[n]'.
   //  n                  Number of entries n array
   //  array              [n] Array in which to search for
   //  value              Value to search in array

   const Double_t *p_ind;
   p_ind = std::lower_bound(array, array + n, value);
   if ((p_ind != array + n) && (*p_ind == value))
      return (p_ind - array);
   else
      return (p_ind - array - 1);
}

//______________________________________________________________________________
Int_t TUMath::BinarySearch(Int_t n, Int_t const *array, Int_t const &value)
{
   //--- Returns value closest to 'value' in 'array[n]'.
   //  n                  Number of entries n array
   //  array              [n] Array in which to search for
   //  value              Value to search in array

   const Int_t *p_ind;
   p_ind = std::lower_bound(array, array + n, value);
   if ((p_ind != array + n) && (*p_ind == value))
      return (p_ind - array);
   else
      return (p_ind - array - 1);
}

//______________________________________________________________________________
void TUMath::DrawFromMultiGauss(const TVectorD &pars_mean, const TMatrixDSym &cov_matrix, TVectorD &pars_gen)
{
   //--- Generate parameters from mean value of parameters and associated covariance matrix.
   // INPUTS:
   //  pars_mean         [npars] Vector of mean values
   //  cov_matrix        [npars*npars] Covariance matrix associated to parameters
   // OUTPUT;
   //  pars_gen          [npars] Output generated parameters

   Int_t n_pars = pars_mean.GetNrows();

   // Check sizes
   if (n_pars <= 0)
      TUMessages::Error(stdout, "TUMath", "DrawFromMultiGauss", "n_par must be >0");
   if (cov_matrix.GetNrows() != n_pars) {
      string message = Form("pars_mean.GetNrows()=%d is different from cov_matrix.GetNrows()=%d... cannot proceed",
                            pars_mean.GetNrows(), cov_matrix.GetNrows());
      TUMessages::Error(stdout, "TUMath", "DrawFromMultiGauss", message);
   }
   // Check that cov_matrix is symmetric
   for (Int_t i_row = 0; i_row < n_pars; i_row++) {
      for (Int_t i_col = i_row; i_col < n_pars; i_col++) {
         if (cov_matrix(i_row, i_col) != cov_matrix(i_col, i_row)) {
            string message = Form("Cov. matrix not symmetric, malformed at i_row=%d, i_col=%d... cannot proceed", i_row, i_col);
            TUMessages::Error(stdout, "TUMath", "DrawFromMultiGauss", message);
         }
      }
   }


   // Initialise size of vector
   pars_gen.ResizeTo(n_pars);

   // Rotate mean parameters (to draw Gaussian distributed parameters in diagonalised base)
   TVectorD eigen_values;
   TMatrixD eigen_vectors = cov_matrix.EigenVectors(eigen_values);
   TMatrixD eigen_vectors_invert = eigen_vectors;
   eigen_vectors_invert.Invert();

   TVectorD rot_pars_mean = eigen_vectors_invert * pars_mean;
   for (Int_t i_par = 0; i_par < n_pars; i_par++) {
      Double_t variance = eigen_values[i_par];
      // Check for positive-definiteness of cov_matrix
      if (variance < 0) {
         string message = Form("Negative eigenvariance=%f on i_par=%d... cannot proceed", variance, i_par);
         TUMessages::Error(stdout, "TUMath", "DrawFromMultiGauss", message);
      }
      // Draw Gaussian distributed
      pars_gen[i_par] = gRandom->Gaus(rot_pars_mean[i_par], sqrt(variance));
   }
   // Rotate back to parameter space
   pars_gen = eigen_vectors * pars_gen;
}

//______________________________________________________________________________
void TUMath::DrawFromMultiGaussAndPlot(TApplication *my_app, vector<string> par_names_units,
                                       const TVectorD &pars_mean, const TMatrixDSym &cov_matrix,
                                       Int_t n_samples, Double_t const *bound_min, Double_t const *bound_max)
{
   //--- Plots distribution of parameters drawn from covariance matrix.
   //  amy_app           ROOT application where to plot selection
   //  par_names_units   [npars] List of parameter names and unit
   //  pars_mean         [npars] Vector of mean values
   //  cov_matrix        [npars*npars] Covariance matrix associated to parameters
   //  n_samples         Number of samples to draw
   //  bound_min         [npars] Upper boundary used to exclude drawn points (only used if non-null)
   //  bound_max         [npars] Lower boundary used to exclude drawn points (only used if non-null)
   // Check consistency of dimensions
   Int_t n_pars = par_names_units.size();

   // Check sizes
   if (n_pars == 0)
      TUMessages::Error(stdout, "TUMath", "DrawFromMultiGaussAndPlot", "No parameters provided in vector<string> par_names_units");
   if (pars_mean.GetNrows() != n_pars) {
      string message = Form("pars_mean.GetNrows()=%d is different from the number of parameters=%d... cannot proceed",
                            pars_mean.GetNrows(), n_pars);
      TUMessages::Error(stdout, "TUMath", "DrawFromMultiGauss", message);
   }


   // Draw and fill vector
   vector<vector<Double_t> > drawn_vals(n_pars, vector<Double_t>(n_samples, 0.));
   vector<Double_t> drawn_min(n_pars, 1.e40);
   vector<Double_t> drawn_max(n_pars, -1.e40);
   Int_t n_good = 0;
   do {
      TVectorD pars_gen;
      TUMath::DrawFromMultiGauss(pars_mean, cov_matrix, pars_gen);

      // Check if in boundaries
      if (bound_min) {
         Bool_t is_pass = true;
         for (Int_t i_par = 0; i_par < n_pars; ++i_par) {
            if (pars_gen[i_par] < bound_min[i_par]) {
               is_pass = false;
               break;
            }
         }
         if (!is_pass)
            continue;
      }
      if (bound_max) {
         Bool_t is_pass = true;
         for (Int_t i_par = 0; i_par < n_pars; ++i_par) {
            if (pars_gen[i_par] > bound_max[i_par]) {
               is_pass = false;
               break;
            }
         }
         if (!is_pass)
            continue;
      }

      // Add in list and update min and max
      for (Int_t i_par = 0; i_par < n_pars; ++i_par) {
         drawn_vals[i_par][n_good] = pars_gen[i_par];
         drawn_min[i_par] = min(pars_gen[i_par], drawn_min[i_par]);
         drawn_max[i_par] = max(pars_gen[i_par], drawn_max[i_par]);
      }
      ++n_good;
   } while (n_good < n_samples);

   // Create, fill, and plot canvas and 1D/2D histograms
   TUMisc::RootSetStylePlots();
   TText *usine_txt = TUMisc::OrphanUSINEAdOnPlots();
   gStyle->SetOptStat(1110);

   TCanvas *cvs = new TCanvas("multigauss", "Drawn from covariance matrix", 700, 700);
   cvs->SetLogx(0);
   cvs->SetLogy(0);
   cvs->Divide(n_pars, n_pars, /*x_margin*/0.001, /*y_margin*/0.001, /*colour*/0);

   TH1D **h1d = new TH1D*[n_pars];
   TH2D **h2d = new TH2D*[n_pars * (n_pars - 1) / 2];

   Int_t n_bins = max(10, n_samples / 50);
   for (Int_t i_par = 0; i_par < n_pars; ++i_par) {
      // Set name 1D histo and fill
      string h_name = par_names_units[i_par];
      TUMisc::RemoveSpecialChars(h_name);
      h1d[i_par] = new TH1D(h_name.c_str(), "PDF", n_bins, drawn_min[i_par], drawn_max[i_par]);
      h1d[i_par]->SetXTitle(h_name.c_str());
      for (Int_t i_sample = 0; i_sample < n_samples; ++i_sample)
         h1d[i_par]->Fill(drawn_vals[i_par][i_sample]);

      // Plot in appropriate pad
      Int_t i_cd_diag = i_par * (n_pars + 1) + 1;
      cvs->cd(i_cd_diag);
      gPad->SetLogx(0);
      gPad->SetLogy(0);
      h1d[i_par]->Draw();
      usine_txt->Draw();
   }

   // Loop on rows and columns to plot 2D distributions
   Int_t i_histo = -1;
   for (Int_t i_row = 0; i_row < n_pars; ++i_row) {
      for (Int_t i_col = 0; i_col < i_row; ++i_col) {
         ++ i_histo;

         // Set name 1D histo and fill
         string x_title = par_names_units[i_col];
         string y_title = par_names_units[i_row];
         string h_name = x_title  + "_vs_" + y_title;
         TUMisc::RemoveSpecialChars(h_name);
         h2d[i_histo] = new TH2D(h_name.c_str(), "2D PDF",
                                 n_bins, drawn_min[i_col], drawn_max[i_col],
                                 n_bins, drawn_min[i_row], drawn_max[i_row]);
         h2d[i_histo]->SetXTitle(x_title.c_str());
         h2d[i_histo]->SetYTitle(y_title.c_str());
         for (Int_t i_sample = 0; i_sample < n_samples; ++i_sample)
            h2d[i_histo]->Fill(drawn_vals[i_col][i_sample], drawn_vals[i_row][i_sample]);

         // Plot in appropriate pad
         Int_t i_cd = i_row * n_pars + 1 + i_col;
         cvs->cd(i_cd);
         gPad->SetLogx(0);
         gPad->SetLogy(0);
         h2d[i_histo]->Draw("CONT");
         usine_txt->Draw();
      }
   }
   cvs->Modified();
   my_app->Run(kTRUE);

   // Free memory
   for (Int_t i = 0; i < n_pars; ++i) {
      delete h1d[i];
      h1d[i] = NULL;
   }
   delete[] h1d;
   for (Int_t i = 0; i < n_pars * (n_pars - 1) / 2; ++i) {
      delete h2d[i];
      h2d[i] = NULL;
   }
   delete[] h2d;
   delete cvs;
   delete usine_txt;
}

//______________________________________________________________________________
Double_t TUMath::Interpolate(Double_t const &x, Double_t const &x1,  Double_t const &x2,
                             Double_t const &y1, Double_t const &y2, gENUM_INTERPTYPE interp_type)
{
   //--- Interpolate (or extrapolate) according to 'interp_type'.
   //  x                 Position to inter/extra-polate
   //  x1                Know position 1
   //  x2                Known position 2
   //  y1                value at x1
   //  y2                Value at x2
   //  interp_type       Enumerators for enabled interpolations (see TUEnum)

   switch (interp_type) {
      case kLOGLOG:
         return ((y1 <= 0. || y2 <= 0.) ? 0. : (y1 * pow(x / x1, log(y2 / y1) / log(x2 / x1))));
      case kLOGLIN:
      case kLOGLIN_INFNULL_SUPCST:
         return  y1 + log(x / x1) * (y2 - y1) / log(x2 / x1);
      case kLINLIN:
         return y1 + (x - x1) * (y2 - y1) / (x2 - x1);
      case kLINLOG:
         return y1 * pow(y2 / y1, (x - x1) / (x2 - x1));
      default:
         return 0.;
   }
   return 0.;
}

//______________________________________________________________________________
Bool_t TUMath::Interpolate(Double_t const *x, Int_t n, Double_t const *y, Double_t const *x_new, Int_t n_new, Double_t *y_interp,
                           gENUM_INTERPTYPE interp_type, Bool_t is_verbose, FILE *f_log)
{
   //--- Fills the n_new interpolated values y_interp[n_new] at x_new[n_new]
   //    and return false if any x_new[i] falls out of the range [x[0],x[n-1]]
   //    (i.e. means that some extrapolation took place).
   // INPUTS:
   //  x[n]              Vector of positions for which y(x) is known
   //  n                 Number of positions for which y(x) is known
   //  y[n]              Vector of y(x[n]) values
   //  x_new[n_new]      Vector of positions at which to interpolate
   //  n_new             Number of values to interpolate
   //  interp_type       Enumerators for enabled interpolations (see TUEnum)
   //  is_verbose        Verbose or not
   //  f_log             Log for USINE
   // OUTPUT:
   //  y_interp[n_new]   Vector of interpolated values

   Bool_t is_interp = true;

   // Loop over all x_new[k]
   for (Int_t k = 0; k < n_new; ++k) {
      if (x_new[k] < x[0]) {
         if (interp_type == kLOGLIN_INFNULL_SUPCST)
            y_interp[k] = 0.;
         else {
            y_interp[k] = Interpolate(x_new[k], x[0], x[1], y[0], y[1], interp_type);
            is_interp = false;
         }
         if (is_verbose) {
            string message = Form("Extrapolation required for k=%i", k);
            TUMessages::Warning(f_log, "TUMath", "Interpolate", message);
            fprintf(f_log, "x_new[%d]=%le   x[0]=%le   x[n-1]=%le\n", k, x_new[k], x[0], x[n - 1]);
         }
      } else if (x_new[k] > x[n - 1]) {
         if (interp_type == kLOGLIN_INFNULL_SUPCST)
            y_interp[k] = y[n - 1];
         else {
            y_interp[k] = Interpolate(x_new[k], x[n - 2], x[n - 1], y[n - 2], y[n - 1], interp_type);
            is_interp = false;
         }
         if (is_verbose) {
            string message = Form("Extrapolation required for k=%i", k);
            TUMessages::Warning(f_log, "TUMath", "Interpolate", message);
            fprintf(f_log, "x_new[%d]=%le   x[0]=%le   x[n-1]=%le\n", k, x_new[k], x[0], x[n - 1]);
         }
      } else {
         // Search in x[] the closest value to x_new[k] and interpolate
         Int_t kk = TUMath::BinarySearch(n, x, x_new[k]);
         y_interp[k] = Interpolate(x_new[k], x[kk], x[kk + 1], y[kk], y[kk + 1], interp_type);
      }
   }
   return is_interp;
}

//______________________________________________________________________________
Double_t TUMath::GammaFunction_PosHalfInteger(Int_t i)
{
   //--- Returns Gamma(i+1/2) from Gradshteyn & Ryzhik (8.339-2).
   //  i                 Index at which to evaluate Gamma(i+1/2)

   Double_t tmp = 1.;
   for (Int_t n = 1; n <= i; ++n) {
      tmp *= (Double_t)(2 * n - 1) / 2.;
   }
   return sqrt(Pi()) * tmp;
}

//______________________________________________________________________________
Double_t TUMath::GammaFunction_NegHalfInteger(Int_t i)
{
   //--- Returns Gamma(1/2-i) from Gradshteyn & Ryzhik (8.339-3).
   //  i                 Index at which to evaluate Gamma(1/2-i)

   Double_t tmp = 1.;
   for (Int_t n = 1; n <= i; ++n) {
      tmp *= (-1.) * 2. / (Double_t)(2 * n - 1);
   }
   return sqrt(Pi()) * tmp;
}

//______________________________________________________________________________
Double_t TUMath::J0(Double_t const &x)
{
   //--- Returns J0(x), Bessel function of order 0 (not accurate for x>>100.).
   //   N.B.: prefer JpOptimized for a better accuracy (but much slower)!
   //  x                 Argument of the function

   Double_t ax = (x < 0) ? -x : x;

   if (ax < 8.0) {
      Double_t y = x * x;
      return (57568490574.e0 + y * (-13362590354.e0 + y * (651619640.7e0
                                    + y * (-11214424.18e0 + y * (77392.33017e0 + y * -184.9052456e0)))))
             / (57568490411.e0 + y * (1029532985.e0 + y * (9494680.718e0 +
                                      y * (59272.64853e0 + y * (267.8532712e0 + y * 1.e0)))));
   } else {
      Double_t z  = 8. / ax;
      Double_t y  = z * z;
      Double_t xx = ax - .785398164;
      return sqrt(.636619772 / ax)
             * (cos(xx)
                * (1.e0 + y * (-.1098628627e-2 + y * (.2734510407e-4
                               + y * (-.2073370639e-5 + y * .2093887211e-6))))
                - z * sin(xx)
                * (-.1562499995e-1 + y * (.1430488765e-3 + y * (-.6911147651e-5
                                          + y * (.7621095161e-6 + y * -.934945152e-7)))));
   }
}

//______________________________________________________________________________
Double_t TUMath::J1(Double_t const &x)
{
   //--- Returns J1(x), Bessel function of order 1 (not accurate for x>>100.).
   //    N.B.: prefer JpOptimized for a better accuracy!
   //  x                 Argument of the function

   Double_t ax = (x < 0.) ? -x : x;

   if (ax < 8.0) {
      Double_t y = x * x;
      return x * (72362614232.e0 + y * (-7895059235.e0 + y * (242396853.1e0
                                        + y * (-2972611.439e0 + y * (15704.48260e0
                                              + y * -30.16036606e0)))))
             / (144725228442.e0 + y * (2300535178.e0 + y * (18583304.74e0 +
                                       y * (99447.43394e0 + y * (376.9991397e0
                                             + y * 1.e0)))));
   } else {
      Double_t z  = 8. / ax;
      Double_t y  = z * z;
      Double_t xx = ax - 2.356194491;
      Double_t sign = (x < 0) ? -1. : 1.;
      return sign * sqrt(.636619772 / ax)
             * (cos(xx)
                * (1.e0 + y * (.183105e-2 + y * (-.3516396496e-4
                               + y * (.2457520174e-5 + y * -.240337019e-6))))
                - z * sin(xx)
                * (.04687499995e0 + y * (-.2002690873e-3 + y * (.8449199096e-5
                                         + y * (.8449199096e-5 + y * .8449199096e-5)))));
   }
}

//______________________________________________________________________________
Double_t TUMath::J1_ZeroJ0(Int_t i, Double_t const &zeroJ0_i)
{
   //--- Returns J1(ZeroJ0(i)) for high order zeros of J0 (trade-off between
   //    precision/time):
   //       - if x<x_asympt => use "J1(x)"  [numerical recipices];
   //       - if x>x_asympt => use "JpGamma(p,x)" [Gradshteyn & Ryzhik];
   //    where we set x_asympt = 10.1.
   //  i                 Index for which to calculate J1(ZeroJ0(i))
   //  zeroJ0_i          Value of the i-th zero of J0 (to speed-up calculation)

   const Double_t x_asympt = 10.1;

   if (zeroJ0_i < x_asympt)
      return J1(zeroJ0_i);
   else {
      // zeroJ0_i can be taken modulo 2pi, because J1(zeroJ0_i) is defined
      // as a combination of cos() and sin().
      const Int_t p = 1;          // Bessel order (here J1, so that p)1)
      const Int_t n_cutoff = 10;  // maximum steps to evaluate the corrective
      // coefficients (in practical, do not go
      // beyond 12, otherwise divergences arise
      // (even if using long Double_t)...

      ++i;
      Double_t zeroi_mod2pi = 0.;

      Double_t _zeroJ0_i = Pi() / 4.*(Double_t)(4 * i - 1)
                           + 1. / (2. * Pi() * (Double_t)(4 * i - 1))
                           - 31. / (6. * pow(Pi() * (Double_t)(4 * i - 1), 3.))
                           + 3779. / (15. * pow(Pi() * (Double_t)(4 * i - 1), 5.));

      if (i % 2 == 0)
         zeroi_mod2pi = -Pi() / 4.
                        + 1. / (2. * Pi() * (Double_t)(4 * i - 1))
                        - 31. / (6. * pow(Pi() * (Double_t)(4 * i - 1), 3.))
                        + 3779. / (15. * pow(Pi() * (Double_t)(4 * i - 1), 5.));

      else if (i % 2 == 1)
         zeroi_mod2pi = 3.*Pi() / 4.
                        + 1. / (2. * Pi() * (Double_t)(4 * i - 1))
                        - 31. / (6. * pow(Pi() * (Double_t)(4 * i - 1), 3.))
                        + 3779. / (15. * pow(Pi() * (Double_t)(4 * i - 1), 5.));


      Double_t c1 = 1.;
      Double_t c2 = 1. / (2.*_zeroJ0_i) * (Double_t)(2 * p + 1)
                    / 2.*(Double_t)(2 * p - 1) / 2.;

      for (Int_t k = 1; k < n_cutoff; ++k) {

         Double_t fact_2k = 1.;

         for (Int_t l = 1; l <= (2 * k); ++l)
            fact_2k *= (Double_t)(l);

         Double_t fact_2k_plus1 = fact_2k * (Double_t)(2 * k + 1);

         Double_t c1k = RatioGammaFunction_HalfInteger(p, 2 * k)
                        * pow(-1., (Double_t)(k))
                        / pow(2.*_zeroJ0_i, 2.*(Double_t)(k))
                        / (Double_t)(fact_2k);
         Double_t c2k = RatioGammaFunction_HalfInteger(p, 2 * k)
                        * pow(-1., (Double_t)(k))
                        / pow(2.*_zeroJ0_i, (Double_t)(2 * k + 1))
                        / (Double_t)(fact_2k_plus1)
                        * (Double_t)(2 * (p + 2 * k) + 1)
                        / 2.*(Double_t)(2 * (p - 2 * k) - 1) / 2.;

         c1 += c1k;
         c2 += c2k;
      }

      return sqrt(2. / (Pi() * _zeroJ0_i)) *
             (cos(zeroi_mod2pi - Pi() / 2.*(Double_t)(p) - Pi() / 4.) * c1
              - sin(zeroi_mod2pi - Pi() / 2.*(Double_t)(p) - Pi() / 4.) * c2);
   }
}

//______________________________________________________________________________
Double_t TUMath::J1Series(Double_t const &x)
{
   //--- Returns J1(x) Bessel function of order 1 (only for small x).
   //    [series representation (Gradshteyn & Ryzhik 8.441-2)].
   //  x                 Argument of the function

   const Int_t k_max = 1000;
   if (x > 0.5) {
      TUMessages::Warning(stdout, "TUMath", "J1Series", "Out of range (argument too large)");
      return 0.;
   }

   Double_t result = 1.;
   for (Int_t k = 1; k <= k_max; ++k) {

      Double_t fact_k = 1.;

      for (Int_t l = 1; l <= k; ++l)
         fact_k *= (Double_t)l;

      Double_t fact_k_plus_1 = fact_k * (Double_t)(k + 1);

      result += (-1.) * pow(x / 2., (Double_t)(2 * k))
                / (fact_k * fact_k_plus_1);
   }
   result *= x / 2.;
   return (result);
}

//______________________________________________________________________________
Double_t TUMath::JpGamma(Int_t p, Double_t const &x)
{
   //--- Returns Jp(x) Bessel function of order p: Gamma representation is valid
   //    only for x>>1 and x>>p (Gradshteyn & Ryzhik, 8.451-1). The following
   //    identities are used:
   //      Gamma(n+3/2) = (2n+1)/2 * Gamma(n+1/2);
   //      Gamma(n-1/2) = 2/(2n-1) * Gamma(n+1/2).
   //  p                 Order of the Bessel function to consider
   //  x                 Argument of the function

   if (x < 3. || x < 3 * p) {
      TUMessages::Warning(stdout, "TUMath", "JpGamma", "Out of range (argument too small)");
      return (0.);
   }

   const Int_t n_cutoff = 10; // maximum number of steps to evaluate
   // the corrective coefficients (in practice,
   // do not go beyond 12, otherwise divergences
   // arise (even when you use long Double_t)...
   Double_t c1 = 1.;
   Double_t c2 = 1. / (2.*x) * (Double_t)(2 * p + 1) / 2.*(Double_t)(2 * p - 1) / 2.;

   for (Int_t k = 1; k < n_cutoff; ++k) {
      Double_t fact_2k = 1.;

      for (Int_t l = 1; l <= (2 * k); ++l)
         fact_2k *= (Double_t)(l);

      Double_t fact_2k_plus1 = fact_2k * (Double_t)(2 * k + 1);

      Double_t c1k = RatioGammaFunction_HalfInteger(p, 2 * k)
                     * pow(-1., (Double_t)(k))
                     / (pow(2.*x, 2.*(Double_t)(k)) * (Double_t)(fact_2k));
      Double_t c2k = RatioGammaFunction_HalfInteger(p, 2 * k)
                     * pow(-1., (Double_t)(k))
                     / (pow(2.*x, (Double_t)(2 * k + 1)) * (Double_t)(fact_2k_plus1))
                     * (Double_t)(2 * (p + 2 * k) + 1)
                     / 2.*(Double_t)(2 * (p - 2 * k) - 1) / 2.;
      c1 += c1k;
      c2 += c2k;
   }

   Double_t result = sqrt(2. / (Pi() * x))
                     * c1 * (cos(x - Pi() / 2.*(Double_t)(p) - Pi() / 4.)
                             - c2 * sin(x - Pi() / 2.*(Double_t)(p) - Pi() / 4.));
   return result;
}

//______________________________________________________________________________
Double_t TUMath::JpOptimized(Int_t p, Double_t const &x)
{
   //--- Returns Jp(x) Bessel function of order p (best trade-off between precision/time):
   //      - if x<x_asympt => use J0/1(x)  [numerical recipices];
   //      - if x>x_asympt => use JpGamma(p,x) [Gradshteyn & Ryzhik].
   //    N.B.: for p=0 and p=1, much slower than direct use of J0 and J1.
   //  p                 Order of the Bessel function to consider
   //  x                 Argument of the function

   const Double_t x_asympt = 10.1;

   if (x > x_asympt) return JpGamma(p, x);
   else if ((p == 0) && (x < x_asympt)) return J0(x);
   else if ((p == 1) && (x < x_asympt)) return J1(x);
   else return 0.;
}

//______________________________________________________________________________
Double_t TUMath::JpIntegral(Int_t p, Double_t const &x)
{
   //--- Returns Jp(x) Bessel function of order p [integral representation]
   //   (Gradshteyn & Ryzhik 8.411-1).
   //  x                 Argument of the function

   Double_t int1, int2, result = 0., step = 0.001;
   // Finer step for large values of x
   if (fabs(x) > 1000.) step = fabs(1 / (500 * x));

   // Simpson integration
   for (Double_t j = step; j < 1.; j += (2.*step)) {
      int1 = 4.*cos((Double_t)(p) * Pi() * j
                    - x * sin(Pi() * j)) / 3.;
      int2 = 2.*cos((Double_t)(p) * Pi() * (j + step)
                    - x * sin(Pi() * (j + step))) / 3.;
      result += int1 + int2;
   }
   int1 = 1. / 3.;
   int2 = -cos((Double_t)(p) * Pi()) / 3.;
   result += int1 + int2;
   result *= step;

   return result;
}

//______________________________________________________________________________
Double_t TUMath::ZeroJ0(Int_t i)
{
   //--- Returns zeta_i (i-th zero of the bessel function J0, i.e. J0(zeta_i)=0).
   //    For the first 20 zeros, uses high accuracy values (required in some cases).
   //    For higher order, a series expansion suffices.
   //  i                 Index for which to find zeta_i

   if (i < 1) printf("index is not valid\n");
   if (i == 1) return 2.4048255576957727686;
   if (i == 2) return 5.5200781102863106496;
   if (i == 3) return 8.6537279129110122170;
   if (i == 4) return 11.791534439014281614;
   if (i == 5) return 14.930917708487785948;
   if (i == 6) return 18.071063967910922543;
   if (i == 7) return 21.211636629879258959;
   if (i == 8) return 24.352471530749302737;
   if (i == 9) return 27.493479132040254796;
   if (i == 10) return 30.634606468431975118;
   if (i == 11) return 33.7758202135735686842385463467;
   if (i == 12) return 36.9170983536640439797694930633;
   if (i == 13) return 40.0584257646282392947993073740;
   if (i == 14) return 43.1997917131767303575240727287;
   if (i == 15) return 46.3411883716618140186857888791;
   if (i == 16) return 49.4826098973978171736027615332;
   if (i == 17) return 52.6240518411149960292512853804;
   if (i == 18) return 55.7655107550199793116834927735;
   if (i == 19) return 58.9069839260809421328344066346;
   if (i == 20) return 62.0484691902271698828525002647;
   else return Pi() / 4.* (Double_t)(4 * i - 1)
                  + 1. / (2. * Pi() * (Double_t)(4 * i - 1))
                  - 31. / (6. * pow(Pi() * (Double_t)(4 * i - 1), 3.))
                  + 3779. / (15. * pow(Pi() * (Double_t)(4 * i - 1), 5.));
}

//______________________________________________________________________________
void TUMath::Quantiles(vector<Double_t> const &values, vector<Double_t> const &quantiles, vector<Double_t> &v_q)
{
   //--- Calculates vector of percentiles from the second-variant method with C=1 (see wikipedia).
   // Given N ordered values v_1,v_2...v_N and the percentile sought p
   //   1) Calculate the rank x of the p-th (e.g. 40) percentile
   //        x = p/100*(N-1)+1
   //   2) Calculate integer of rank xint = x/1 and rest xrest = x%1
   //   3) The value of the p-th percentile is
   //        v(p) = v_{xint} + xrest*(v_{xint+1}-v_{xint})
   //
   // INPUTS:
   //  value             Values from which to extract quantiles
   //  quantiles         Vector of quantiles to search for
   // OUTPUT:
   //  v_q               Vector of values for the quantiles sought

   const Int_t n_p = quantiles.size();
   const Int_t n_vals = values.size();
   v_q.clear();

   // Sort value (do on copy in case ordering used outside the function)
   vector<Double_t> sorted_vals = values;
   sort(sorted_vals.begin(), sorted_vals.end());
   for (Int_t i_p = 0; i_p < n_p; ++i_p) {
      Double_t x = quantiles[i_p] / 100. * Double_t(n_vals - 1) + 1.;
      Int_t xint = Int_t(x);
      Double_t xrest = x - (Double_t)xint;
      Double_t val_p = sorted_vals[xint - 1] + xrest * (sorted_vals[xint] - sorted_vals[xint - 1]);
      v_q.push_back(val_p);
   }
}

//______________________________________________________________________________
Double_t TUMath::RatioGammaFunction_HalfInteger(Int_t p, Int_t i)
{
   //--- Returns Gamma(p+i+1/2)/Gamma(p-i+1/2) from Gradshteyn & Ryzhik (8.339-4).
   //  p                 Index at which to evaluate the function
   //  i                 Index at which to evaluate the function

   Double_t tmp = 1.;
   for (Int_t n = 1; n <= i; ++n) {
      tmp *= (4.*(Double_t)(p * p)
              - (long)((2 * n - 1) * (2 * n - 1))) / 4.;
   }
   return tmp;
}

//______________________________________________________________________________
void TUMath::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "The namespace TUMath contains some\n"
                    "useful mathematical functions. Only a few of them are\n"
                    "tested below.";
   TUMessages::Test(f, "TUMath", message);

   /***********************/
   /*   Binary searches   */
   /***********************/
   // Search for the index of an element in a sorted list
   fprintf(f, " * Binary search (examples using TMath from ROOT):\n");
   fprintf(f, "   Consider the list[n=6]={-3, 2, 4, 10, 20, 25}.\n");
   fprintf(f, "   The algorithm TUMath::BinarySearch returns the index\n"
           "   i for which val<list[i] (and val>list[i-1]).\n");
   const Int_t n_l = 6;
   Double_t list[n_l] = { -3., 2., 4., 10., 20., 25.};
   const Int_t m = 5;
   Double_t sought[m] = {3, 15, -1, -7, 40};
   for (Int_t i = 0; i < m; ++i)
      fprintf(f, "   > TUMath::BinarySearch(n=%d, list, sought=%f);  =>  index=%d\n",
              n_l, sought[i], (Int_t)TUMath::BinarySearch(n_l, list, sought[i]));


   /*********************/
   /*  Interpolations   */
   /*********************/
   // Interpolate, but returns 0 if below x_min, and y_max if above x_max
   fprintf(f, "\n");
   fprintf(f, " * Interpolation:\n");
   fprintf(f, "   Consider (x,y)[4] = { (1.,1.), (10.,2.), (100.,3.), (1000.,3.) }\n");
   fprintf(f, "   > Interpolate(x, n, y, x_new, n_new, y_interp, kLOGLIN_INFNULL_SUPCST, is_verbose=true, f_out=f);\n");
   const Int_t n = 4;
   Double_t x[n] = {1., 10., 100., 1000.};
   Double_t y[n] = {1., 2., 3., 4.};
   const Int_t n_new = n + 1;
   Double_t x_new[n_new] = {0.1, 3.162277, 9.9, 101., 2000.};
   Double_t y_interp[n_new];
   Interpolate(x, n, y, x_new, n_new, y_interp, kLOGLIN_INFNULL_SUPCST, true, f);
   for (Int_t i = 0; i < n + 1; ++i)
      fprintf(f, "     -> for x = %le, y_interp = %le\n", x_new[i], y_interp[i]);
   fprintf(f, "   > Interpolate(5, x[0], x[1], y[0], y[1], kLINLIN);  => %le\n",
           Interpolate(5, x[0], x[1], y[0], y[1], kLINLIN));
   fprintf(f, "   > Interpolate(5, x[0], x[1], y[0], y[1], kLINLOG);  => %le\n",
           Interpolate(5, x[0], x[1], y[0], y[1], kLINLOG));
   fprintf(f, "   > Interpolate(5, x[0], x[1], y[0], y[1], kLOGLIN);  => %le\n",
           Interpolate(5, x[0], x[1], y[0], y[1], kLOGLIN));
   fprintf(f, "   > Interpolate(5, x[0], x[1], y[0], y[1], kLOGLOG);  => %le\n",
           Interpolate(5, x[0], x[1], y[0], y[1], kLOGLOG));


   /***************/
   /*  Quantiles  */
   /***************/
   fprintf(f, "\n");
   fprintf(f, " * Test quantiles (10,50,75) from 100 Gaussian distributed values (mean=3., sigma=1.)\n");

   const Int_t n_vals = 100;
   Double_t gaus_sample[n_vals] = {
      4.17984279,  3.7138822,   3.6082898,   2.98955573,  4.96335972,  4.04339669,
      2.69840948,  3.39145013,  2.74865951,  4.42580974,  3.56494666,  3.84972236,
      2.51426031,  2.22409243,  2.34833954,  2.55104482,  2.42504663,  3.18873432,
      5.17310188,  3.72505711,  5.10660462,  2.57630392,  3.21795203,  2.64933454,
      4.06048811,  3.09262033,  3.29794091,  3.68382207,  3.77919287,  3.01829577,
      1.4689743,   3.7873083,   2.83688681,  4.53735155,  3.64473351,  2.70033235,
      4.04729735,  4.09085703,  3.81646037,  4.35057489,  5.22943517,  3.83256091,
      1.88601148,  4.249697,    4.00395828,  2.22081754,  3.07318779,  5.45486683,
      1.56502435,  2.49759847,  1.90525126,  3.12266353,  2.61695119,  3.16929977,
      1.79324773,  3.23356075,  3.28453739,  1.00956538,  3.41080527,  1.5395,
      2.30408182,  2.70827103,  4.13862891,  2.94133514,  2.64415058,  2.59399071,
      2.34883784,  2.97396942,  3.37247254,  2.22698347,  2.82751315,  3.75247109,
      3.3775053,   3.43953688,  2.38384656,  3.902866,    3.81814827,  1.13021473,
      2.62520498,  4.86853113,  3.06169028,  4.00284074,  3.51237991,  5.19906551,
      3.53796612,  4.00880672,  1.37219497,  2.23169886,  3.19006961,  2.6910782,
      2.10075712,  4.71145861,  4.29226561,  4.00647467,  4.21937372,  2.45375381,
      3.37488892,  3.59440699,  3.26666039,  2.43156503
   };

   vector<Double_t> values(n_vals, 0.);
   for (Int_t i = 0; i < n_vals; ++i)
      values[i] = gaus_sample[i];

   const Int_t n_q = 3;
   vector<Double_t> quantiles(n_q, 0.);
   quantiles[0] = 10.;
   quantiles[1] = 50.;
   quantiles[2] = 75.;
   vector<Double_t> v_q(n_q, 0.);
   fprintf(f, "  > Quantiles(values, quantiles, v_q);\n");
   Quantiles(values, quantiles, v_q);
   for (Int_t q = 0; q < n_q; ++q)
      fprintf(f, "    - quantile=%lf  =>  val_quantile=%le\n", quantiles[q], v_q[q]);
   fprintf(f, "\n");


   // ROOT CALCULATION
   /* fprintf(f, "  > ROOT calculation (with TH1)\n");
   Double_t val_min = *min_element(values.begin(), values.end());
   Double_t val_max = *max_element(values.begin(), values.end());
   cout << val_min << " " << val_max << endl;
   Int_t n_bins = 200;
   TH1D histo("", "", n_bins, val_min, val_max);
   for (Int_t i=0; i< n_vals; ++i)
      histo.Fill(values[i]);
   for (Int_t q=0; q<n_q; ++q)
      quantiles[q]= quantiles[q]/100.;
   for (Int_t q=0; q<n_q; ++q)
      v_q[q] = 0.;
   histo.GetQuantiles(n_q, &v_q[0], &quantiles[0]);
   for (Int_t q=0; q<n_q; ++q)
      fprintf(f, "    - quantile=%lf  =>  val_quantile=%le\n", quantiles[q], v_q[q]);
   */


   /**********************************/
   /*  Draw from covariance matrix   */
   /**********************************/
   fprintf(f, "\n");
   const Int_t n_par = 5;
   Int_t n_draw = 10000;
   fprintf(f, " * Draw from covariance matrix (and mean values):\n");
   fprintf(f, "   Consider %d parameters with mean=1 and diagonal of cov_ii = 2*(i+1)\n", n_par);
   fprintf(f, "      - draw %d realizations using DrawFromMultiGauss()\n", n_draw);
   fprintf(f, "      - calculate the sample mean and variance\n");
   fprintf(f, "     => check that equal to input values\n");
   fprintf(f, "\n");

   TVectorD pars_mean(n_par);
   TMatrixDSym cov_matrix(n_par);
   TVectorD pars_gen;
   // Set mean to 1 for all elements, and diagonal
   // of covariance matrix (sigma^2) to 2^2, 4^2, 6^2...
   for (Int_t i = 0; i < n_par; ++i) {
      pars_mean(i) = 1.;
      cov_matrix(i, i) = pow(Double_t(2 * (i + 1)), 2);
   }
   // Draw 1000 realisation and calculate mean and sigma
   vector <Double_t> gen[n_par];
   for (Int_t i = 0; i < n_draw; ++i) {
      DrawFromMultiGauss(pars_mean, cov_matrix, pars_gen);
      for (Int_t i = 0; i < n_par; ++i)
         gen[i].push_back(pars_gen(i));
   }

   for (Int_t i = 0; i < n_par; ++i) {
      Double_t sum = std::accumulate(gen[i].begin(), gen[i].end(), 0.0);
      Double_t mean = sum / gen[i].size();
      Double_t sq_sum = std::inner_product(gen[i].begin(), gen[i].end(), gen[i].begin(), 0.0);
      Double_t stdev = std::sqrt(sq_sum / gen[i].size() - mean * mean);
      fprintf(f, "    - i=%d: mu_i=%.0f, sqrt(cov_ii)=%.0f  =>  <mu_i>=%le, <sigma_i>=%le (%d realisation)\n",
              i, pars_mean(i), sqrt(cov_matrix(i, i)), mean, stdev, n_draw);
   }


   /**************************/
   //--- Bessel functions ---//
   /**************************/
   fprintf(f, "\n");
   fprintf(f, " * Bessel functions (several representations):\n");

   Double_t jp = JpOptimized(0, 3.); // arg = p, x and return Jp(x)
   Double_t zi = ZeroJ0(1); // arg = i-th zero of J0
   Double_t j0 = J0(3.);  // return J0(x) with the more simple formulae
   Double_t j1 = J1(3.);  // return J1(x) with the more simple formulae
   fprintf(f, "   => 1st-zero of J0=%e, J0(3.)=%e/%e, J1(3.)=%e\n", zi, jp, j0, j1);

   fprintf(f, "\n");
   fprintf(f, " * Comparison of bessel calculations using various approximations\n"
           "   (from numerical recipes and Gradshteyn & Ryzhik).\n");
   // Check the zeroth bessel function J0(x)
   fprintf(f, "                   --- J0(x) ---\n\n"
           "        x                 Num.Rec.          Integral repr.        Gamma repr.\n"
           "          small x  =>      (OK)                   (OK)              (fails)\n"
           "          for x>>1 =>      (OK)        (time-consuming, fails) (the most accurate)\n");
   for (Double_t val = 0.1; val < 10000.; val *= 2.5) {
      if (val > 3)
         fprintf(f, "        x=%7.3e %19.10e %19.10e %19.10e\n", val, J0(val),
                 JpIntegral(0, val), JpGamma(0, val));
      else
         fprintf(f, "        x=%7.3e %19.10e %19.10e          -\n", val, J0(val), JpIntegral(0, val));
   }

   // Check the firt order bessel function J1(val)
   fprintf(f, "\n\n                   --- J1(x) ---\n\n"
           "        x                 Num.Rec.          Integral repr.        Gamma repr.        Series repr.\n"
           "          small x  =>      (OK)                   (OK)              (fails)        (only for x<<1)\n"
           "          for x>>1 =>      (OK)        (time-consuming, fails) (the most accurate)     (fails)\n");

   for (Double_t val = 0.001; val < 10000.; val *= 2.5) {
      if (val < 0.5)
         fprintf(f, "        x=%7.3e %19.10e %19.10e          -          %19.10e\n", val, J1(val),
                 JpIntegral(1, val), J1Series(val));
      else if (val < 5)
         fprintf(f, "        x=%7.3e %19.10e %19.10e          -                   -\n",
                 val, J1(val), JpIntegral(1, val));
      else
         fprintf(f, "        x=%7.3e %19.10e %19.10e %19.10e          -\n", val, J1(val),
                 JpIntegral(1, val), JpGamma(1, val));
   }

   // Check the eval. J1(zeroJ0_i) by standard vs optimized method (for large arguments) */
   fprintf(f, "\n                   --- J1(zeroJ0_i) ---\n\n"
           "          i       zeroJ0_i          J1_zeroJ0 (for x>>1)  Num.Rec.\n");

   for (Int_t i = 0; i < 100000; i = (i + 1) * 2) {
      Double_t zero_i = ZeroJ0(i + 1);
      fprintf(f, "        %5d  %12.8e %19.10e %19.10e\n", i, zero_i,
              J1(zero_i), J1_ZeroJ0(i, zero_i));
   }
}
