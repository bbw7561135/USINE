// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUAtomElements.h"
#include "../include/TUMediumEntry.h"
#include "../include/TUMessages.h"
#include "../include/TUPhysics.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUMediumEntry                                                        //
//                                                                      //
// Densities and temperature of the medium (single space-time entry).   //
//                                                                      //
// The class TUMediumEntry provides a description of the medium         //
// properties (valid at a given position, or for a uniform medium).     //
// In this version, the enabled properties are:                         //
//    - number density [cm^-3] for a list of user-defined elements;
//    - temperature of the plasma [K].
//                                                                      //
// Note that if H is present in the medium, providing the density of HI //
// (neutral atomic), HII (ionised atomic hydrogen), and H2 (molecular   //
// hydrogen) becomes compulsory: as a result, the density n_H is        //
// automatically set to n_H = n_HI + n_HII + 2*N_H2.                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUMediumEntry)

//______________________________________________________________________________
TUMediumEntry::TUMediumEntry()
{
   // ****** Default constructor ******

   Initialise();
}

//______________________________________________________________________________
TUMediumEntry::TUMediumEntry(TUMediumEntry const &medium)
{
   // ****** Copy constructor ******
   //  medium            Object to copy

   Initialise();
   Copy(medium);
}

//______________________________________________________________________________
TUMediumEntry::TUMediumEntry(string const &commasep_elements)
{
   // ****** Default constructor ******
   //   => Allocates number of elements and Z from a comma-spearated list of names.
   //  commasep_elements Comma-separated list of elements (e.g., "H,He,C")

   SetElements(commasep_elements);
}

//______________________________________________________________________________
TUMediumEntry::~TUMediumEntry()
{
   // ****** Default destructor ******
   //
}

//______________________________________________________________________________
void TUMediumEntry::Copy(TUMediumEntry const &medium)
{
   //--- Copies 'medium' in class.
   //  medium            Object to copy from

   Initialise();

   for (Int_t i = 0; i < medium.GetNMediumElements(); ++i) {
      fDensities.push_back(medium.GetDensity(i));
      fElementNames.push_back(medium.GetNameMediumElement(i));
      fElementZ.push_back(medium.GetZElement(i));
   }

   fDensityHI = medium.GetDensityHI();
   fDensityHII = medium.GetDensityHII();
   fDensityH2 = medium.GetDensityH2();
   fPlasmaT = medium.GetPlasmaT();
}

//______________________________________________________________________________
void TUMediumEntry::Initialise()
{
   //--- Initialises and clear vectors.

   fDensities.clear();
   fDensityHI = 0.;
   fDensityHII = 0.;
   fElementNames.clear();
   fElementZ.clear();
   fDensityH2 = 0.;
   fPlasmaT = 0.;
}

//______________________________________________________________________________
Double_t TUMediumEntry::MeanMassDensity() const
{
   //--- Returns mean mass density of the medium [g/cm3], with 1g = NAvogadro amu
   //       <nm>=Sum_{i in ISM} (n_i m_i) / NAvogadro.

   Double_t mean_m = 0.;
   for (Int_t i = 0; i < GetNMediumElements(); ++i) {
      if (GetNameMediumElement(i) == "H")
         mean_m += GetDensity(i) *  TUPhysics::mH_amu();
      else if (GetNameMediumElement(i) == "HE")
         mean_m += GetDensity(i) * TUPhysics::mHe_amu();
      else
         mean_m += GetDensity(i) * Double_t(GetZElement(i) * 2);
   }
   mean_m /= TUPhysics::NAvogadro();
   return mean_m;
}

//______________________________________________________________________________
void TUMediumEntry::Print(FILE *f) const
{
   //--- Prints ISM parameters in file f.
   //  f                 File in which to print

   string indent = TUMessages::Indent(true);

   for (Int_t i = 0; i < GetNMediumElements(); ++i) {
      if (GetZElement(i) == 1) {
         fprintf(f, "%s   HI                 = %.3le [cm^-3]\n",
                 indent.c_str(), GetDensityHI());
         fprintf(f, "%s   HII                = %.3le [cm^-3]\n",
                 indent.c_str(), GetDensityHII());
         fprintf(f, "%s   H2                 = %.3le [cm^-3]\n",
                 indent.c_str(), GetDensityH2());
      }
      fprintf(f,    "%s   %-3s                = %.3le [cm^-3]\n",
              indent.c_str(), GetNameMediumElement(i).c_str(), GetDensity(i));
   }
   fprintf(f, "%s   Plasma Density     = %.3le [cm^-3]\n",
           indent.c_str(), GetPlasmaDensity());
   fprintf(f, "%s   Plasma Temperature = %.3le [K]\n",
           indent.c_str(), fPlasmaT);

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUMediumEntry::SetElements(string const &commasep_elements)
{
   //--- Allocates number of elements and Z from a comma-separated list of names.
   //  commasep_elements  Comma-separated list of elements (e.g., "H,He,C")

   Initialise();

   // Use a TAtomicElements class to check that names in list exist
   // (i.e., that it is an element)
   TUAtomElements elements;
   TUMisc::String2List(commasep_elements, ",", fElementNames);
   for (Int_t i = 0; i < GetNMediumElements(); ++i) {
      TUMisc::UpperCase(fElementNames[i]);
      for (Int_t j = i + 1; j < GetNMediumElements(); ++j) {
         if (i == 0) TUMisc::UpperCase(fElementNames[j]);
         if (fElementNames[j] == fElementNames[i]) {
            string message = "the same element appears several time in "
                             + commasep_elements;
            TUMessages::Error(stdout, "TUMediumEntry", "SetElements", message);
         }
      }
      fElementZ.push_back(elements.ElementNameToZ(fElementNames[i]));
      fDensities.push_back(0.);
   }
}

//______________________________________________________________________________
void TUMediumEntry::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message =
      "Medium properties at a given location (or for uniform medium):\n"
      "   - Number density [/cm3] for a list of user-defined elements;\n"
      "   - Temperature [k]\n"
      "   -...\n"
      "N.B.: The medium is defined by a comma-separated list of elements.\n"
      "If H is in the medium, HI, HII, and H2 can be specified (and then\n"
      " n_H = n_HI + n_HII + 2 n_H2).";
   TUMessages::Test(f, "TUMediumEntry", message);

   // Set class and test methods
   fprintf(f, " * Fill class\n");
   fprintf(f, "   > SetElements(He,C,N);\n");
   SetElements("He,C,N");
   fprintf(f, "   > SetDensity(i_el=0, dens=1.);\n");
   SetDensity(0, 1.);
   fprintf(f, "   > SetDensity(i_el=1, dens=2.);\n");
   SetDensity(1, 2.);
   fprintf(f, "   > SetDensity(i_el=2, dens=3.);\n");
   SetDensity(2, 3.);
   fprintf(f, "   > SetPlamsaT(T=1.e4);\n");
   SetPlasmaT(1.e4);
   fprintf(f, "   > GetNameMediumElement(1);   =>  %s\n",
           GetNameMediumElement(1).c_str());
   fprintf(f, "   > Print(f)\n");
   Print(f);

   fprintf(f, "\n");
   fprintf(f, " * Test when H is in medium (HI, HII, and H2 can be filled then)\n");
   fprintf(f, "   > SetElements(H,He);\n");
   SetElements("H,He");
   fprintf(f, "   > SetDensity(i_el=1, dens=2.);\n");
   SetDensity(1, 2.);
   fprintf(f, "   > SetPlamsaT(T=1.e5);\n");
   SetPlasmaT(1.e5);
   fprintf(f, "   > SetDensityH(HI=0.1, HII=0.5, H2=5);\n");
   SetDensityH(0.1, 0.5, 5.);
   fprintf(f, "   > GetDensity(0);             =>  %le\n", GetDensity(0));
   fprintf(f, "   > GetDensityNeutral(0);      =>  %le\n", GetDensityNeutral(0));
   fprintf(f, "   > GetDensity(1);             =>  %le\n", GetDensity(1));
   fprintf(f, "   > GetDensityNeutral(1);      =>  %le\n", GetDensityNeutral(1));
   fprintf(f, "   > IndexElementInMedium(Z=1); =>  %d\n", IndexElementInMedium(1));
   fprintf(f, "   > Print(f)\n");
   Print(f);

   fprintf(f, "\n");
   fprintf(f, " * Test copy\n");
   fprintf(f, "   > TUMediumEntry medium;\n");
   TUMediumEntry medium;
   fprintf(f, "   > medium.Copy(*this);\n");
   medium.Copy(*this);
   fprintf(f, "   > medium.Print(f)\n");
   medium.Print(f);
}
