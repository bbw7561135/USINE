// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <fstream>
// ROOT include
// USINE include
#include "../include/TUInitParList.h"
#include "../include/TUMessages.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUInitParList                                                        //
//                                                                      //
// List of initialisation parameters (based on TUInitParEntry).         //
//                                                                      //
// The class TUInitParList provides a list of free parameters           //
// (vector of TUInitParEntry). The purpose of this class is to          //
// initialise all parameters (ingredients needed for a run, data,       //
// propagation model parameters). These parameters are read from an     //
// initialisation file (see below) to fill this class. Hence, any       //
// parameter used in USINE should be handled by this class. The GUI     //
// for USINE is also based on the content of this class.                //
//                                                                      //
// The class is based on three vectors:                                 //
//    - fPars is a vector of TUInitParEntry (note that
//      a parameter can be multivalued);
//    - fNameGroups is a list of groups (e.g., "Base", "LeakyBox"), and
//      their description/definition (used in the GUI);
//    - fNameSubGroups is a list of subgroups (for each group), and their
//      description/definition (used in the GUI).
// Beside SetClass() and Print(), the methods of the class mostly       //
// consist in finding the parameter indices from                        //
// group/subgroup/parameter names.                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUInitParList)

//______________________________________________________________________________
TUInitParList::TUInitParList()
{
   // ****** Default constructor ******
   //
}

//______________________________________________________________________________
TUInitParList::TUInitParList(TUInitParList const &par_list)
{
   // ****** Copy constructor ******
   //  par_list          Object to copy

   Copy(par_list);
}

//______________________________________________________________________________
TUInitParList::TUInitParList(string const &f_parfile, Bool_t is_verbose, FILE *f_log)
{
   // ****** Standard constructor ******
   //  f_parfile         Name of USINE parameter file to load
   //  is_verbose        Chatter is on or off
   //  f_log             Log for USINE

   SetClass(f_parfile, is_verbose, f_log);
}

//______________________________________________________________________________
TUInitParList::~TUInitParList()
{
   // ****** Default destructor ******
   //
}

//______________________________________________________________________________
void TUInitParList::AddGroup(string const &group, string const &info_group)
{
   //--- Adds group in class.
   //  group             Group name
   //  group_info        Group information

   // Check whether this group is copied yet in fNameGroups
   if (IndexGroup(group) < 0) {
      fInfoGroups.push_back(info_group);
      fNameGroups.push_back(group);

      // And add entry in fNameSubGroups (fill at later stage)
      fInfoSubGroups.push_back(vector<string>());
      fNameSubGroups.push_back(vector<string>());
   }
}

//______________________________________________________________________________
void TUInitParList::AddSubGroup(string const &group, string const &subgroup, string const &info_subgroup)
{
   //--- Adds group in class.
   //  group             Group name
   //  subgroup          SubGroup name
   //  subgroup_info     SubGroup information

   // Check whether this subgroup is copied yet in fNameSubGroups
   Int_t i_group = IndexGroup(group);
   if (i_group >= 0 && IndexSubGroup(group, subgroup) < 0) {
      fInfoSubGroups[i_group].push_back(info_subgroup);
      fNameSubGroups[i_group].push_back(subgroup);
   }
}

//______________________________________________________________________________
void TUInitParList::Copy(TUInitParList const &par_list)
{
   //--- Copies entry in class.
   //  par_list          Object to copy from

   Initialise();

   TUMisc::String2List(par_list.GetFileNames(), ",", fFileNames);

   for (Int_t i = 0; i < par_list.GetNGroups(); ++i) {
      fInfoGroups.push_back(par_list.GetInfoGroup(i));
      fNameGroups.push_back(par_list.GetNameGroup(i));

      vector<string> tmp_name, tmp_info;
      for (Int_t j = 0; j < par_list.GetNSubGroups(i); ++j) {
         tmp_info.push_back(par_list.GetInfoSubGroup(i, j));
         tmp_name.push_back(par_list.GetNameSubGroup(i, j));
      }
      fInfoSubGroups.push_back(tmp_info);
      fNameSubGroups.push_back(tmp_name);
   }

   fPars.clear();
   for (Int_t i = 0; i < par_list.GetNPars(); ++i)
      fPars.push_back(par_list.GetParEntry(i));
}

//______________________________________________________________________________
gENUM_FCNTYPE TUInitParList::ExtractFormat(Int_t i_par, Int_t j_multi, string &extracted, Bool_t is_verbose, FILE *f_log) const
{
   //--- Returns FCNTYPE of the j_multi-th value of (multi-valued) initialisation
   //    parameter i_par. For instance, kFORMULA,kSPLINE,kGRID (see in TUEnum).
   // INPUTS:
   //  i_par             Index of the parameter in 'init_pars'
   //  j_multi           j-th entry for multi-valued parameter i_par
   //  is_verbose        Verbose on or off
   //  f_log             Log for USINE
   // OUTPUT:
   //  extracted         extracted=X, where X is extracted from the parameter value formatted as "string|X" or "string:string|X"
   //                    where 'string|X' determines whose format TUValsTXYZEVirtual will be:
   //                       "FORMULA|X" -> formula with X=fparser formulae (will use TUValsTXYZEFormula)
   //                       "GRID|X"    -> grid of values with X=filename (will use TUValsTXYZGrid)
   //                       "SPLINE|X"  -> spline with X=USINE spline format (will use TUValsTXYZESpline)

   // Single- or multi-valued parameters?
   Bool_t is_multi = fPars[i_par].IsMultiVal();

   // Check index of multi-valued entry sought exists!
   if (is_multi && (j_multi < 0 || j_multi >= fPars[i_par].GetNVals())) {
      string message = " parameter " + (string)Form("%d", j_multi)  + " for "
                       + fPars[i_par].GetGroup() + "@" + fPars[i_par].GetSubGroup()
                       + "@" + fPars[i_par].GetName()
                       + " not found (only " + (string)Form("%d", fPars[i_par].GetNVals()) + "available!";
      TUMessages::Error(f_log, "TUInitParList", "IsFormula", message);
   }

   string indent = TUMessages::Indent(true);


   // Cast parameter whose format should be "string|X" or "string:string|X"
   vector<string> cast;
   vector<string> multival;
   TUMisc::String2List(fPars[i_par].GetVal(j_multi), "|", cast);
   // Find if we dealt with "string|X" or "string:string|X",
   // i.e. whether cast[0] is "string" or "string:string"
   TUMisc::String2List(cast[0], ":", multival);
   if (cast.size() != 2 || (is_multi && multival.size() != 2) || (!is_multi && multival.size() > 1)) {
      string message = string("bad format: should be 'string|X' (if M=0) or 'string:string|X' (i M=1) for ")
                       + fPars[i_par].GetVal(j_multi) + string("  in initialisation file(s) ") + GetFileNames();
      TUMessages::Error(f_log, "TUInitParList", "IsFormula", message);
   }


   // 'string' (=FCNTYPE) searched for is in multival[0] + extracted
   gENUM_FCNTYPE fcntype = kFORMULA;
   if (!is_multi) {
      TUEnum::Name2Enum(cast[0], fcntype);
      extracted = cast[1];
   } else if (multival.size() == 2) {
      TUEnum::Name2Enum(multival[1], fcntype);
      extracted = cast[1];
      if (is_verbose)
         fprintf(f_log, "\n%s  * %s", indent.c_str(), multival[0].c_str());
   }

   TUMessages::Indent(false);
   return fcntype;
}

//______________________________________________________________________________
Int_t TUInitParList::GetNPars(string const &group, string const &subgroup) const
{
   //--- Returns number of parameters belonging to 'group@subgroup' (in fPars[]).
   //  group             Name of group ("ALL" for all groups)
   //  subgroup          Name of subgroup in group ("ALL" for all subgroups)

   Int_t n = 0;

   // For all groups and subgroups
   if (group == "ALL" && subgroup == "ALL")
      return GetNPars();

   // for a subgroups of a given group
   else if (subgroup == "ALL") {
      for (Int_t i = 0; i < GetNPars(); ++i) {
         if (group == fPars[i].GetGroup())
            ++n;
      }
      return n;
   }

   // else for a given group/subgroup
   for (Int_t i = 0; i < GetNPars(); ++i) {
      if (fPars[i].GetGroup() == group && fPars[i].GetSubGroup() == subgroup)
         ++n;
   }
   return n;
}

//______________________________________________________________________________
Int_t TUInitParList::IndexGroup(string const &group) const
{
   //--- Returns index of 'group' in fNameGroups[] (-1 if not found).
   //  group             Name of group to search for

   // N.B.: fNameGroups format is GROUP@info
   for (Int_t i = 0; i < GetNGroups(); ++i) {
      if (group == fNameGroups[i])
         return i;
   }
   return -1;
}

//______________________________________________________________________________
Int_t TUInitParList::IndexPar(string const &group, string const &subgroup,
                              string const &param, Bool_t is_verbose) const
{
   //--- Returns index of 'group@subgroup@name' in fPars[] (-1 if not found).
   //  group             Name of group
   //  subgroup          Name of subgroup (in group)
   //  param             Name of parameter (in group@subgroup)
   //  is_verbose        Chatter on or off

   for (Int_t i = 0; i < GetNPars(); ++i) {
      if (fPars[i].IsSame(group, subgroup, param))
         return i;
   }

   if (is_verbose) {
      string message = "Did not find " + group + "@" + subgroup + "@" + param;
      TUMessages::Warning(stdout, "TUInitParList", "IndexPar(group,subgroup,param)", message);
   }
   return -1;
}

//______________________________________________________________________________
Int_t TUInitParList::IndexSubGroup(string const &group, string const &subgroup) const
{
   //--- Returns index of 'subgroup' in fNameSubGroups (-1 if not found).
   //  group             Name of group to search for
   //  subgroup          Name of subgroup (in 'group')

   Int_t i_group = IndexGroup(group);
   if (i_group < 0)
      return -1;

   for (Int_t j = 0; j < GetNSubGroups(i_group); ++j) {
      if (subgroup == GetNameSubGroup(i_group, j))
         return j;
   }
   return -1;
}

//______________________________________________________________________________
vector<Int_t> TUInitParList::IndicesPars(string const &group, string const &subgroup) const
{
   //--- Returns vector of parameter indices belonging to 'group@subgroup' in fPars[].
   //  group             Name of group ("ALL" for all groups)
   //  subgroup          Name of subgroup in group ("ALL" for all subgroups)

   vector<Int_t> indices;
   for (Int_t i = 0; i < GetNPars(); ++i) {
      if (group == "ALL" && subgroup == "ALL")
         indices.push_back(i);
      else if (subgroup == "ALL") {
         if (fPars[i].GetGroup() == group)
            indices.push_back(i);
      } else {
         if (fPars[i].GetGroup() == group && fPars[i].GetSubGroup() == subgroup)
            indices.push_back(i);
      }
   }
   return indices;
}

//______________________________________________________________________________
void TUInitParList::Initialise()
{
   //--- Clears all vectors of the class.

   fFileNames.clear();
   fNameGroups.clear();
   fInfoGroups.clear();
   fInfoSubGroups.clear();
   fPars.clear();
   fNameSubGroups.clear();
}

//______________________________________________________________________________
void TUInitParList::Print(FILE *f, string const &group, string const &subgroup) const
{
   //--- Prints in FILE *f parameters belonging to 'group@subgroup'.
   //  f                 Output file
   //  group             Name of group ("ALL " for all groups)
   //  subgroup          Name of subgroup in group ("ALL" for all subgroups)

   // N.B.: whole parfile is written only if group=subgroup="ALL"

   for (Int_t i = 0; i < GetNGroups(); ++i) {
      if (group != "ALL" && group != fNameGroups[i])
         continue;
      // Loop on all subgroups for this group
      for (Int_t j = 0; j < GetNSubGroups(i); ++j) {
         if (subgroup != "ALL" && subgroup != fNameSubGroups[i][j])
            continue;
         // Loop on all parameters for this group@subgroup
         vector<Int_t> l_indices = IndicesPars(fNameGroups[i], fNameSubGroups[i][j]);
         for (Int_t k = 0; k < (Int_t)l_indices.size(); ++k)
            fPars[l_indices[k]].Print(f);
         fprintf(f, "\n");
      }
   }
}

//______________________________________________________________________________
void TUInitParList::PrintListGroups(FILE *f) const
{
   //--- Prints in file f all groups in the class.
   //  f                 Output file

   for (Int_t i = 0; i < GetNGroups(); ++i)
      fprintf(f, "@%s\n", fNameGroups[i].c_str());
}

//______________________________________________________________________________
void TUInitParList::PrintListSubGroups(FILE *f, string const &group) const
{
   //--- Prints in file f all subgroups belonging to group.
   //  f                 Output file
   //  group             Name of group ("ALL" for all groups)

   Bool_t is_all = false;
   if (group == "ALL")
      is_all = true;

   for (Int_t i_group = 0; i_group < GetNGroups(); ++i_group) {
      if (!is_all && fNameGroups[i_group] != group)
         continue;

      for (Int_t i = 0; i < GetNSubGroups(i_group); ++i)
         fprintf(f, "@%s@%s\n", fNameGroups[i_group].c_str(), fNameSubGroups[i_group][i].c_str());
   }
}

//______________________________________________________________________________
void TUInitParList::SetClass(string const &f_parfile, Bool_t is_verbose, FILE *f_log)
{
   //--- Adds to the class all parameters found in f_parfile. If parameter
   //    already exists (and not declared to be a multi-valued parameter)
   //    overwrite current value with new one. Otherwise adds parameter or
   //    new value (if multi-valued).
   //  f_parfile         Name of USINE parameter file to load
   //  is_verbose        Whether to print some stuff or not
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);

   if (is_verbose)
      fprintf(f_log, "%s[TUInitParList::SetClass]\n", indent.c_str());

   // Check if exists
   ifstream f_read(f_parfile.c_str());
   if (!(TUMisc::IsFileExist(f_read, f_parfile, false))) {
      string message = "file " + f_parfile + " not found";
      TUMessages::Error(f_log, "TUInitParList", "SetClass", message);
      TUMessages::Indent(false);
      return;
   }

   // Find if $USINE, if yes, we replace it in file
   string file = f_parfile;
   TUMisc::SubstituteEnvInPath(file, "$USINE");
   fFileNames.push_back(file);

   string tmp_line;
   string current_group = "";
   string current_subgroup = "";

   Int_t line_id = 0;
   while (getline(f_read, tmp_line)) {
      ++line_id;

      //--- if it is a comment (start with //) or an empty line, skip it
      // Remove blank spaces from line start/stop
      if (tmp_line.find_first_not_of(" \t") == string::npos) continue;
      string current_line = TUMisc::RemoveBlancksFromStartStop(tmp_line);
      if (current_line[0] == '#')
         continue;
      //--- If not a comment, processes the line
      //  => it may be a group, a subgroup or a parameter!
      if (current_line.substr(0, 5) == "GROUP") {
         SetGroup(current_line, current_group);
      } else if (current_line.substr(0, 8) == "SUBGROUP") {
         SetSubGroup(current_line, current_group, current_subgroup);
      } else {
         // This is a parameter!
         TUInitParEntry par;
         string line_info = Form("line %d from %s", line_id, f_parfile.c_str());
         par.SetEntry(current_line, line_info);
         //fprintf(f_log, "%s------ Line read is:\n", indent.c_str());
         //par.Print(f_log);
         //fprintf(f_log, "%s------\n", indent.c_str());

         // Check if parameter already exists:
         //   - if not, create it
         //   - if exists and M=1 (fMultiVal=true), value added to vector of fVal if not duplicated
         //   - if exists and M=0 (fMultiVal=false), update value of parameter
         Int_t index = IndexPar(&par, false);
         if (index < 0) {
            // Check if group exist: if not, add it!
            AddGroup(par.GetGroup(), "");
            AddSubGroup(par.GetGroup(), par.GetSubGroup(), "");
            fPars.push_back(par);
         } else {
            if (fPars[index].IsMultiVal()) {
               Int_t i_val = TUMisc::IndexInList(fPars[index].GetVals(), par.GetVal(), false);
               if (i_val < 0)
                  fPars[index].AddVal(par.GetVal());
               // No warning below, generate too much chatter!
               //else {
               //   string message = "Line " + current_line + " already loaded, skip duplicates!";
               //   TUMessages::Warning(f_log, "TUInitParList", "SetClass", message);
               //}
            } else {
               if (fPars[index].GetVal() != par.GetVal()) {
                  // Update value of the parameter!
                  string tmp_str = par.GetGroup() + "@" + par.GetSubGroup()
                                   + "@" + par.GetName();
                  if (is_verbose) {
                     fprintf(f_log, "%s   + %s is updated to %s [old value=%s]\n",
                             indent.c_str(), tmp_str.c_str(),
                             par.GetVal().c_str(), fPars[index].GetVal().c_str());
                  }
                  fPars[index].SetVal(par.GetVal());
               }
            }
         }
      }
   }

   if (is_verbose)
      fprintf(f_log, "%s[TUInitParList::SetClass] <DONE>\n\n", indent.c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUInitParList::SetGroup(string &line_group, string &current_group)
{
   //--- Extracts group name and group info (added to fNameGroups) from a
   //    'line_group' (see syntax, e.g., in $USINE/inputs/init.TEST.par).
   // INPUT:
   //  line_group        Line to read and cast
   // OUTPUT:
   //  current_group     Group name found in line

   // Get group name and info from line
   string info_group;
   vector<string> l_str;
   TUMisc::String2List(line_group, "@", l_str);
   for (Int_t j = 0; j < (Int_t)l_str.size(); j++) {
      if (j == 1) current_group = l_str[j];
      else if (j == 2) info_group = l_str[j];
   }
   AddGroup(current_group, info_group);
}

//______________________________________________________________________________
void TUInitParList::SetSubGroup(string &line_subgroup, string &current_group,
                                string &current_subgroup)
{
   //--- Extracts subgroup name and subgroup info (added to fNameSubGroups)
   //    from a 'line_subgroup' and 'current_group' (see syntax, e.g., in
   //    $USINE/inputs/init.TEST.par).
   // INPUT:
   //  line_subgroup     Line to read and cast
   //  current_group     Group name found in line
   // OUTPUT:
   //  current_subgroup  SubGroup name found in line

   // Check if a current_group exists
   if (current_group == "") {
      string message = "No GROUP found before line\n" + line_subgroup;
      TUMessages::Error(stdout, "TUInitParList", "SetSubGroup", message);
   }
   // Get subgroup name and info from line
   string info_subgroup;
   vector<string> l_str;
   TUMisc::String2List(line_subgroup, "@", l_str);
   for (Int_t j = 0; j < (Int_t)l_str.size(); j++) {
      if (j == 1) current_subgroup = l_str[j];
      else if (j == 2) info_subgroup = l_str[j];
   }

   AddSubGroup(current_group, current_subgroup, info_subgroup);
}

//______________________________________________________________________________
void TUInitParList::TEST(string const &f_parfile, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f_parfile         Name of USINE parameter file to load
   //  f                 Output file for test

   string message = "Input parameters for USINE.";
   TUMessages::Test(f, "TUInitParList", message);

   // Set class and test methods
   fprintf(f, " * Fill class:\n");
   fprintf(f, "   > SetClass(f_parfile=\"%s\", is_verbose=false, f_log=f)\n", f_parfile.c_str());
   SetClass(TUMisc::GetPath(f_parfile), false, f);
   fprintf(f, "   > SetClass(f_parfile=\"%s\", true, f_log=f)\n", f_parfile.c_str());
   SetClass(TUMisc::GetPath(f_parfile), true, f);
   fprintf(f, "   > GetNGroups(); => %d\n", GetNGroups());
   fprintf(f, "   > GetNSubGroups(1); => %d\n", GetNSubGroups(1));
   fprintf(f, "   > GetNSubGroups(IndexGroup(group=base)); => %d\n",
           GetNSubGroups(IndexGroup("Base")));
   fprintf(f, "   > GetNPars(); => %d\n", GetNPars());
   fprintf(f, "   > GetNPars(group=Base); =>%d\n", GetNPars("Base"));
   fprintf(f, "   > GetNPars(group=Base, subgroup=EnergyGrid); => %d\n",
           GetNPars("Base", "EnergyGrid"));
   fprintf(f, "   > GetNPars(group=UsineFit, subgroup=Config); => %d\n",
           GetNPars("UsineFit", "Config"));
   fprintf(f, "   > GetNPars(group=UsineFit, subgroup=TOAData); => %d\n",
           GetNPars("UsineFit", "TOAData"));
   fprintf(f, "   > GetNPars(group=UsineFit, subgroup=FreePars); => %d\n",
           GetNPars("UsineFit", "FreePars"));
   fprintf(f, "   > GetNPars(group=UsineFit, subgroup=Outputs); => %d\n",
           GetNPars("UsineFit", "Outputs"));
   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, "   > PrintListGroups(f)\n");
   PrintListGroups(f);
   fprintf(f, "   > IndexGroup(GetNameGroup(3)); => %d\n", IndexGroup(GetNameGroup(3)));
   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, "   > PrintListSubGroups(f, group=[default=ALL])\n");
   PrintListSubGroups(f, "ALL");
   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, "   > PrintListSubGroups(f, group=SolMod0DFF)\n");
   PrintListSubGroups(f, "SolMod0DFF");
   fprintf(f, "   > IndexSubGroup(\"SolMod0DFF\", \"Base\"); => %d\n",
           IndexSubGroup("SolMod0DFF", "Base"));
   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, "   > Print(f, group=SolMod0DFF, subgroup=Base)\n");
   Print(f, "SolMod0DFF", "Base");
   fprintf(f, "\n");

   // Create copy, and check comparison methods
   fprintf(f, " * Check copy method:\n");
   fprintf(f, "   > TUInitParList par_ref;\n");
   TUInitParList par_ref;
   fprintf(f, "   > par_ref.Copy(*this);\n");
   par_ref.Copy(*this);
   fprintf(f, "   > GetParEntry(17).Print(f)\n");
   GetParEntry(17).Print(f);
   fprintf(f, "   > par_ref.GetParEntry(17).Print(f)\n");
   par_ref.GetParEntry(17).Print(f);
   fprintf(f, "   > GetParEntry(42).Print(f)\n");
   GetParEntry(42).Print(f);
   fprintf(f, "   > par_ref.GetParEntry(42).Print(f)\n");
   par_ref.GetParEntry(42).Print(f);

   fprintf(f, "\n\n");
   fprintf(f, " * Read second init file (update parameters)\n");
   fprintf(f, "   > Print(f, group=Base, subgroup=EnergyGrid)\n");
   Print(f, "Base", "EnergyGrid");
   string pfile = "$USINE/inputs/init.TEST2.par";
   fprintf(f, "   > SetClass(pfile=%s);\n", pfile.c_str());
   SetClass(TUMisc::GetPath(pfile), false, f);
   fprintf(f, "   > Print(f, group=Base, subgroup=EnergyGrid)\n");
   Print(f, "Base", "EnergyGrid");
}
