// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUAtomElements.h"
#include "../include/TUMessages.h"
#include "../include/TUMisc.h"
#include "../include/TUNormEntry.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUNormEntry                                                          //
//                                                                      //
// User-selected data point on which to normalise a calculated flux.    //
//                                                                      //
// The class TUNormEntry is used whenever the CR source normalisation   //
// is not fitted to the data. In that case, to match the data, one is   //
// left with only two options:                                          //
//    - set directly the proper normalisation of the source term;
//    - provide a data point and force the normalisation to
//      match this data point: this is done at runtime.
//                                                                      //
// For that purpose, one needs to provide the:                          //
//    - name of the quantity whose source needs to be renormalised;
//    - experiment, whose measurement is used for renormalisation;
//    - energy for which the normalisation will be ensured.
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUNormEntry)

//______________________________________________________________________________
TUNormEntry::TUNormEntry()
{
   // ****** Default constructor ******

   fE = 0.;
   fEType = kEKN;
   fExpList = "";
   fName = "";
   fZ = 0;
}

//______________________________________________________________________________
TUNormEntry::TUNormEntry(TUNormEntry const &entry)
{
   // ****** Copy constructor ******
   //  entry             Pointer used to fill class

   Copy(entry);
}

//______________________________________________________________________________
TUNormEntry::~TUNormEntry()
{
   // ****** Default destructor ******
}

//______________________________________________________________________________
void TUNormEntry::Copy(TUNormEntry const &entry)
{
   //--- Copies "entry" in the class.
   //  entry             Object to copy from

   fE = entry.GetE();
   fEType = entry.GetEType();
   fExpList = entry.GetExpList();
   fIsElement = entry.IsElement();
   fName = entry.GetName();
   fZ = entry.GetZ();
}

//______________________________________________________________________________
void TUNormEntry::Print(FILE *f) const
{
   //--- Prints in file f the queried entry.
   //  f                 File in which to print

   string indent = TUMessages::Indent(true);
   fprintf(f, "%s   %-8s %-12s    %le [%s]\n", indent.c_str(), fName.c_str(),
           fExpList.c_str(), fE, TUEnum::Enum2Unit(fEType, false).c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUNormEntry::SetQty(string const &name)
{
   //--- Set fName, fIsElement, and fZ from qty.
   //  name             Species or element whose calculated flux is to match exp. data (case insensitive)

   fName = name;
   TUMisc::UpperCase(fName);

   // Set fIsElement
   TUAtomElements atom;
   if (!atom.IsElement(fName)) {
      fIsElement = false;
      fZ = atom.CRNameToZ(fName);
   } else {
      fIsElement = true;
      fZ = atom.ElementNameToZ(fName);
   }
}

//______________________________________________________________________________
void TUNormEntry::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "Normalisation entry (to match the\n"
                    "calculated and measured flux at a given energy).";
   TUMessages::Test(f, "TUNormEntry", message);

   // Set class and summary content
   SetQty("10B");
   fprintf(f, "   > SetQty(10B);\n");
   fprintf(f, "   > IsElement()  =>  %d\n", IsElement());
   fprintf(f, "   > GetZ()       =>  %d\n", GetZ());
   SetQty("B");
   fprintf(f, "   > SetQty(B);\n");
   fprintf(f, "   > IsElement()  =>  %d\n", IsElement());
   fprintf(f, "   > GetZ()       =>  %d\n", GetZ());
   fprintf(f, "\n");

   fprintf(f, "   > SetExpList(AMS,Voyag);\n");
   SetExpList("AMS,Voyag");
   fprintf(f, "   > SetE(E=10);\n");
   SetE(10);
   fprintf(f, "   > SetEType(kEkn);\n");
   SetEType(kEKN);
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "   > TUNormEntry entry; entry.Copy(*this); entry.Print(f);\n");
   TUNormEntry entry;
   entry.Copy(*this);
   entry.Print(f);
}
