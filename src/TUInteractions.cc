// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <cmath>
// ROOT include
// DAVID ROOT: all display
#include <TApplication.h>
#include <TAxis.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TMultiGraph.h>
// USINE include
#include "../include/TUAtomProperties.h"
#include "../include/TUCREntry.h"
#include "../include/TUMath.h"
#include "../include/TUMediumEntry.h"
#include "../include/TUMessages.h"
#include "../include/TUInteractions.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUInteractions                                                   //
//                                                                      //
// Encapsulate functions related to physics processes.                  //
//                                                                      //
// The namespace TUInteractions encapsulates                        //
//    - Mean energy loss per unit time dE/dt in GeV Myr^{-1} for:
//        - Colombian interaction;
//        - Ionisation.
//    - Electron capture (EC) and stripping (ES) on nuclei [TO DO]:
//        - REC (Radiative EC);
//        - NREC (Non-Radiative);
//        - ECPP.
//                                                                      //
// Note that for most nuclear interactions (fragmentation and inelastic //
// cross-section) consist of complicated formulae/codes not given here. //
// (see, e.g., Webber et al. 1988-2003, Silberberg et al. 1973-1999).   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

NamespaceImp(TUInteractions)

//______________________________________________________________________________
Double_t TUInteractions::DEdtCoulomb(TUCREntry const &cr, TUMediumEntry *medium,
                                     Double_t const &ekn)
{
   //--- Returns dE/dt for Coulomb losses in ionised plasma [GeV/Myr].
   //    N.B.: collisions in a complete ionized plasma are dominated by scattering
   //    off the thermal electrons (temperature "T_e" and density "n_e").
   //    Refs.: Mannheim & Schlickeiser, A&A 286, 983 (1994);
   //           Strong & Moskalenko, ApJ 509, 212 (1998).
   //
   //  cr                CR entry
   //  medium            TUMediumEntry object of matter properties
   //  ekn               Kinetic energy per nucleon of cr [GeV/n]


   // Return 0 if no plasma (otherwise, ln_lambda returns inf)
   if (medium->GetPlasmaDensity() < 1.e-40)
      return 0.;


   Double_t p = TUPhysics::Ekn_to_p(ekn, cr.GetA(), cr.GetmGeV());
   Double_t E = TUPhysics::Ekn_to_E(ekn, cr.GetA(), cr.GetmGeV());
   Double_t beta = TUPhysics::Beta_pE(p, E);
   Double_t gamma = TUPhysics::Gamma_Em(E, cr.GetmGeV());

   Double_t x_m = pow(3.*sqrt(TUMath::Pi()) / 4., 1. / 3.)
                  * sqrt(2.* TUPhysics::kB_GeVperK() * medium->GetPlasmaT()
                         / TUPhysics::me_GeV());

   // Coulomb logarithm
   Double_t ln_lambda = 0.5 * log(
                           (TUMath::Sqr(TUPhysics::me_GeV()) * cr.GetmGeV()
                            * TUMath::Sqr(gamma * TUMath::Sqr(beta))) /
                           (TUMath::Pi() * TUPhysics::ElectronRadius_cm()
                            * TUPhysics::hbar_GeVs() * TUPhysics::hbar_GeVs()
                            * TUMath::Sqr(TUPhysics::C_cmpers()) * medium->GetPlasmaDensity()
                            * (cr.GetmGeV() + 2.*gamma * TUPhysics::me_GeV())));

   // Losses: mind the negative sign...
   return  -4. * TUMath::Pi() * TUPhysics::C_cmperMyr() * TUPhysics::me_GeV() *
           (TUMath::Sqr(TUPhysics::ElectronRadius_cm()) * TUMath::Sqr(cr.GetZ())
            * medium->GetPlasmaDensity() * ln_lambda * TUMath::Sqr(beta) /
            (TUMath::Cub(x_m) + TUMath::Cub(beta)));
}

//______________________________________________________________________________
Double_t TUInteractions::DEdtIonisation(TUCREntry const &cr, TUMediumEntry *medium,
                                        TUAtomProperties *atom, Double_t const &ekn)
{
   //--- Returns dE/dt for Ionisation losses on neutral gas [GeV/Myr].
   //    Refs.: Mannheim & Schlickeiser, A&A 286, 983 (1994);
   //           Strong & Moskalenko, ApJ 509, 212 (1998).
   //
   //  cr                CR entry
   //  medium            TUMediumEntry object of matter properties
   //  atom              Atomic properties for all elements
   //  ekn               Kinetic energy per nucleon of cr [GeV/n]

   Double_t p = TUPhysics::Ekn_to_p(ekn, cr.GetA(), cr.GetmGeV());
   Double_t E = TUPhysics::Ekn_to_E(ekn, cr.GetA(), cr.GetmGeV());
   Double_t beta = TUPhysics::Beta_pE(p, E);
   Double_t gamma = TUPhysics::Gamma_Em(E, cr.GetmGeV());
   Double_t tmp = 2. * TUPhysics::me_GeV() * TUMath::Sqr((beta * gamma));

   // Maximum transfered energy in GeV
   Double_t Q_max = tmp / (1. + 2.*gamma * TUPhysics::me_GeV() / cr.GetmGeV());
   // Geometric mean of all ionization and excitation potential for all ISM elements [GeV]
   Double_t mean = 0;
   for (Int_t i = 0; i < medium->GetNMediumElements(); ++i) {
      Double_t Belem = log(tmp * Q_max
                           / (1.e-18 * atom->GetIMean(medium->GetZElement(i))
                              * atom->GetIMean(medium->GetZElement(i))))
                       - 2.*TUMath::Sqr(beta);
      mean += medium->GetDensityNeutral(i) * Belem;
   }
   return -2.*TUMath::Pi() * TUPhysics::C_cmperMyr() * TUPhysics::me_GeV()
          * mean / beta * TUMath::Sqr(TUPhysics::ElectronRadius_cm())
          * TUMath::Sqr(cr.GetZ());
}

//______________________________________________________________________________
Double_t TUInteractions::DEdtIonCoulomb(TUCREntry const &cr, TUMediumEntry *medium,
                                        TUAtomProperties *atom, Double_t const &ekn)
{
   //--- Returns dE/dt for combined ionisation + Coulomb losses [GeV/Myr].
   //    Refs.: Mannheim & Schlickeiser, A&A 286, 983 (1994);
   //           Strong & Moskalenko, ApJ 509, 212 (1998).
   //
   //  cr                CR entry
   //  medium            TUMediumEntry object of matter properties
   //  atom              Atomic properties for all elements
   //  ekn               Kinetic energy per nucleon of cr [GeV/n]

   return DEdtCoulomb(cr, medium, ekn) + DEdtIonisation(cr, medium, atom, ekn);
}

//______________________________________________________________________________
Double_t TUInteractions::DEdtIonCoulomb(TUCREntry const &cr, TUMediumEntry *medium,
                                        TUAtomProperties *atom, Double_t const &beta, Double_t const &gamma)
{
   //--- Returns dE/dt for combined ionisation + Coulomb losses [GeV/Myr].
   //    Refs.: Mannheim & Schlickeiser, A&A 286, 983 (1994);
   //           Strong & Moskalenko, ApJ 509, 212 (1998).
   //
   //  cr                CR entry
   //  medium            TUMediumEntry object of matter properties
   //  atom              Atomic properties for all elements
   //  beta              v/c of the CR
   //  gamma             Lorentz factor of the CR

   Double_t beta2 = TUMath::Sqr(beta);
   Double_t gamma2 = TUMath::Sqr(gamma);

   //--- Coulomb losses...
   Double_t dedt_coulomb = 0.;
   if (medium->GetPlasmaDensity() > 1.e-40) {
      Double_t x_m = pow(3.*sqrt(TUMath::Pi()) / 4., 1. / 3.) * sqrt(2.*TUPhysics::kB_GeVperK()
                     * medium->GetPlasmaT() / TUPhysics::me_GeV());
      // Coulomb logarithm
      Double_t ln_lambda = 0.5 * log(TUMath::Sqr(TUPhysics::me_GeV()) * cr.GetmGeV()
                                     * (gamma2 * TUMath::Sqr(beta2))
                                     / (TUMath::Sqr(TUPhysics::C_cmpers()) * TUMath::Pi() * TUPhysics::ElectronRadius_cm()
                                        * TUMath::Sqr(TUPhysics::hbar_GeVs()) *
                                        medium->GetPlasmaDensity() * (cr.GetmGeV() + 2.*gamma * TUPhysics::me_GeV())));
      dedt_coulomb = -4.*TUMath::Pi() * TUPhysics::C_cmperMyr() * TUPhysics::me_GeV()
                     * TUMath::Sqr(TUPhysics::ElectronRadius_cm()) * TUMath::Sqr(cr.GetZ())
                     * medium->GetPlasmaDensity() * ln_lambda * beta2
                     / (TUMath::Cub(x_m) + TUMath::Cub(beta));
   }

   //--- Ionisation losses...
   Double_t tmp = 2. * TUPhysics::me_GeV() * beta2 * gamma2;
   // Maximum transfered energy [GeV]
   Double_t Q_max = tmp / (1. + 2.*gamma * TUPhysics::me_GeV() / cr.GetmGeV());
   // Geometric mean of all ionization and excitation potentials for H and He (in GeV) */
   Double_t mean = 0;
   for (Int_t i = 0; i < medium->GetNMediumElements(); ++i) {
      Double_t Belem = log(tmp * Q_max / (1.e-18
                                          * atom->GetIMean(medium->GetZElement(i))
                                          * atom->GetIMean(medium->GetZElement(i))))
                       - 2.*TUMath::Sqr(beta);
      mean += medium->GetDensityNeutral(i) * Belem;
   }
   Double_t dedt_ion = -2.*TUMath::Pi() * TUPhysics::C_cmperMyr()
                       * TUPhysics::me_GeV() * (TUPhysics::ElectronRadius_cm()
                             * TUPhysics::ElectronRadius_cm() * TUMath::Sqr(cr.GetZ()))
                       / beta * mean;

   return dedt_coulomb + dedt_ion;
}

//______________________________________________________________________________
TGraph *TUInteractions::OrphanGetGraphELoss(TUCREntry const &cr, TUMediumEntry *medium,
      TUAtomProperties *atom, TUAxis *ekn, Bool_t is_ion_or_coulomb, Bool_t is_rate_or_time)
{
   //--- Returns ROOT TGraph for rate or characteristic time for E losses (ion or coulomb).
   //
   //  cr                CR entry
   //  medium            TUMediumEntry object of matter properties
   //  atom              Atomic properties for all elements
   //  ekn               Kinetic energy per nucleon of cr [GeV/n]
   //  is_ion_or_coulomb Returns for ionisation looses (true) or coulomb (false)
   //  is_rate_or_time   Returns loss rate [/Myr] or characteristic time [Myr]


   TGraph *gr = new TGraph(ekn->GetN());
   for (Int_t k = 0; k < ekn->GetN(); ++k) {
      Double_t dedt = 0.;
      if (is_ion_or_coulomb)
         dedt = DEdtIonisation(cr, medium, atom, ekn->GetVal(k));
      else
         dedt = DEdtCoulomb(cr, medium, ekn->GetVal(k));

      if (is_rate_or_time)
         gr->SetPoint(k, ekn->GetVal(k), -dedt / (cr.GetA()*ekn->GetVal(k)));
      else
         gr->SetPoint(k, ekn->GetVal(k), -cr.GetA()*ekn->GetVal(k) / dedt);
   }

   const Int_t n = 2;
   string loss[n] = {"coulomb", "ion"};
   string qty[n] = {"time", "rate"};
   string gr_name = loss[(Int_t)is_ion_or_coulomb] + "_" + qty[(Int_t)is_rate_or_time] + "_" + cr.GetName();
   TUMisc::RemoveSpecialChars(gr_name);
   gr->SetName(gr_name.c_str());
   gr->SetTitle("");
   gr->GetXaxis()->SetTitle((ekn->GetNameAndUnit()).c_str());
   if (is_rate_or_time)
      gr->GetYaxis()->SetTitle("Loss rate [Myr^{-1}]");
   else
      gr->GetYaxis()->SetTitle("Characteristic time [Myr]");

   return gr;
}

//______________________________________________________________________________
void TUInteractions::PlotEC(TApplication *my_app, Bool_t is_test, FILE *f_test)
{
   //--- Plots electronic capture and stripping as in Letaw et al. ApJSS 56, 396 (1984),
   //    i.e. Fig. 3, 4 and 5.
   //  my_app             ROOT application where to plot selection
   //  is_test            Whether called as test  (print graph content if test)
   //  f_test             File in which to print

   const Int_t n_fig = 3;
   Double_t ekn[8] = { .05, 0.1, 0.5, 1., 5., 10.};
   Int_t n_z = 100;
   Int_t n_ekn[n_fig] = {4, 6, 6};
   string pre_name = "ec_Letawetal_ApJSS56_396_1984";
   string pre_title = "Letaw et al., ApJSS 56, 396 (1984)";
   string name[n_fig] = {"fig3", "fig4", "fig5"};
   string y_title[n_fig] = {"log_{10} (#lambda_{s}) (g/cm^{2})",
                            "log_{10} (#tau_{a}) (Myr)",
                            "-log_{10} [#lambda_{s}/(#lambda_{a}+#lambda_{s}) ]"
                           };
   string y_name[n_fig] = {"lambdas", "taua", "lambdas_to_lambdaa_lambdas"};
   TCanvas *c_fig[n_fig] = {NULL, NULL, NULL};
   TMultiGraph *mg[n_fig] = {NULL, NULL, NULL};
   TText *usine_txt = TUMisc::OrphanUSINEAdOnPlots();
   TLegend *leg[n_fig] = {NULL, NULL, NULL};
   Int_t Ztarget = 1;

   for (Int_t c = 0; c < n_fig; ++c) {
      string c_name = pre_name + "_" + name[c];
      TUMisc::RemoveSpecialChars(c_name);
      string c_title = pre_title + " - " + name[c];
      c_fig[c] = new TCanvas(c_name.c_str(), c_title.c_str(), c * 455, 0, 450, 300);
      //c_fig[c]->SetGridx();
      //c_fig[c]->SetGridy();
      c_fig[c]->SetLogx(0);
      c_fig[c]->SetLogy(0);

      mg[c] = new TMultiGraph();
      mg[c]->SetName(c_name.c_str());
      string x_title = "Z";
      string mg_titles = ";" + x_title + ";" + y_title[c];
      mg[c]->SetTitle(mg_titles.c_str());
      leg[c] = new TLegend(0.13, 0.5, 0.6, 0.93, NULL, "brNDC");

      for (Int_t k = n_ekn[c] - 1; k >= 0; --k) {
         TGraph *gr = new TGraph(n_z);
         string ekn_name = Form("Ekn = %.2f [GeV/n]", ekn[k]);
         string gr_name = Form("%s_%.2fgevn_%s", y_name[c].c_str(), ekn[k], name[c].c_str());
         gr->SetName(gr_name.c_str());
         gr->SetTitle("");

         TUCREntry cr;
         for (Int_t z = 1; z <= n_z; ++z) {
            cr.SetEntry(2 * z, z, Double_t(2 * z));
            Double_t y = 0.;
            if (c == 0) {
               // Fig.3: lambda_s = 10^27/Avogadro * A_H/sigStripping_Kshell_Letaw(proj, Zt, eknuc)
               y = log10(1.e27 / TUPhysics::NAvogadro() * 1. / SigES_Letaw(cr, Ztarget, ekn[k]));
            } else if (c == 1) {
               // Fig. 4: tau_a [Myr} = 732.65/ (beta* nH(=0.3)* sigCapture_Kshell_Letaw(proj, Zt, eknuc))
               Double_t beta =   TUPhysics::Beta_mAEkn(cr.GetmGeV(), cr.GetA(), ekn[k]);
               y = log10(1. / (.3 * beta * (TUPhysics::C_cmperMyr() * TUPhysics::Convert_mb_to_cm2()) * SigEC_Letaw(cr, Ztarget, ekn[k])));
            } else if (c == 2) {
               // Fig. 5: -log10(lambda_s/(lambda_a+lambda_s))
               y = -log10((1. / SigES_Letaw(cr, Ztarget, ekn[k])) /
                          (1. / SigES_Letaw(cr, Ztarget, ekn[k]) + 1. / SigEC_Letaw(cr, Ztarget, ekn[k])));
            }
            gr->SetPoint(z - 1, z, y);
         }
         gr->SetLineColor(TUMisc::RootColor(k));
         gr->SetLineStyle(k + 1);
         gr->SetLineWidth(2);
         mg[c]->Add(gr, "L");
         TLegendEntry *entry = leg[c]->AddEntry(gr, ekn_name.c_str(), "L");
         entry->SetTextColor(TUMisc::RootColor(k));
         entry = NULL;
         gr = NULL;
      }

      if (c == 0) {
         mg[c]->SetMinimum(-4);
         mg[c]->SetMaximum(0);
      } else if (c == 1) {
         mg[c]->SetMinimum(-2.5);
         mg[c]->SetMaximum(8.5);
      } else if (c == 2) {
         mg[c]->SetMinimum(0);
         mg[c]->SetMaximum(15);
      }
      mg[c]->Draw("AL");
      usine_txt->Draw();
      // If print
      if (is_test)
         TUMisc::Print(f_test, mg[c], 0);
      leg[c]->Draw();
      c_fig[c]->Modified();
      c_fig[c]->Update();
   }

   if (!is_test)
      my_app->Run(kTRUE);

   for (Int_t c = 0; c < n_fig; ++c) {
      delete mg[c];
      delete leg[c];
      delete c_fig[c];
   }
   delete usine_txt;
}

//______________________________________________________________________________
void TUInteractions::PlotELosses(vector<TUCREntry> &crs, TUMediumEntry *medium,
                                 TUAtomProperties *atom, TUAxis *ekn, TApplication *my_app,
                                 Bool_t is_test, FILE *f_test)
{
   //--- Plots or save plots (root and jpeg) for user-selected entries.
   //  crs                List of crs for which to plot E-losses
   //  medium             ISM
   //  atom               Atomic properties
   //  ekn                Ekn axis on which to plot
   //  my_app             ROOT application where to plot selection
   //  is_test            Whether called as test (print graph content if test)
   //  f_test             File in which to print test

   if (crs.size() == 0)
      TUMessages::Error(stdout, "TUInteractions", "PlotELosses", "No CR selected");

   // Title
   vector <string> densities;
   for (Int_t i = 0; i < medium->GetNMediumElements(); ++i) {
      string tmp = Form("n%s=%.2f", medium->GetNameMediumElement(i).c_str(), medium->GetDensity(i));
      densities.push_back(tmp);
   }
   string str_n = TUMisc::List2String(densities, ',');
   vector<string> cr_names;
   for (Int_t i = 0; i < (Int_t)crs.size(); ++i)
      cr_names.push_back(crs[i].GetName());
   string names = TUMisc::List2String(cr_names, ',');
   string title = Form("Elosses: ISM (%s | ne=%.3f /cm3, Te=%.2e K)", str_n.c_str(),
                       medium->GetPlasmaDensity(), medium->GetPlasmaT());

   // Get Production plots (if required)
   TText *usine_txt = TUMisc::OrphanUSINEAdOnPlots();
   const Int_t n_loss = 2;
   const Int_t n_qty = 2;
   const Int_t n_mg = 4;
   string loss[n_loss] = {"coulomb", "ion"};
   string qty[n_qty] = {"time", "rate"};
   TLegend **leg = new TLegend*[n_qty];
   TMultiGraph **mg = new TMultiGraph*[n_qty];
   TCanvas **c = new TCanvas*[n_qty];
   for (Int_t i = 0; i < n_qty; ++i) {
      c[i] = new TCanvas((qty[i] + "_eloss").c_str(), title.c_str(), i * 610, 0, 600, 400);
      c[i]->SetLogx(1);
      c[i]->SetLogy(1);
      leg[i] = new TLegend(0.13, 0.5, 0.6, 0.93, NULL, "brNDC");
      mg[i] = new TMultiGraph();
      string mg_name = qty[i] + "_" + names;
      TUMisc::RemoveSpecialChars(mg_name);
      mg[i]->SetName(mg_name.c_str());
      string x_title = ekn->GetNameAndUnit();
      string y_title = "Characteristic time [Myr]";
      if (i == 1)
         y_title = "Loss rate [Myr^{-1}]";
      for (Int_t j = 0; j < n_loss; ++j) {
         for (Int_t j_cr = 0; j_cr < (Int_t)crs.size(); ++j_cr) {
            Bool_t is_ion_or_coulomb = (Bool_t)j;
            Bool_t is_rate_or_time = Bool_t(i);
            TGraph *gr = OrphanGetGraphELoss(crs[j_cr], medium, atom, ekn, is_ion_or_coulomb, is_rate_or_time);
            gr->SetLineColor(TUMisc::RootColor(j_cr));
            gr->SetLineStyle(j + 1);
            gr->SetLineWidth(2);
            gr->SetDrawOption("L");
            mg[i]->Add(gr, "L");
            string text = TUMisc::FormatCRQty(crs[j_cr].GetName(), 2) + " (" + loss[j] + ")";
            TLegendEntry *entry = leg[i]->AddEntry(gr, text.c_str(), "L");
            entry->SetTextColor(TUMisc::RootColor(j_cr));
            entry = NULL;
         }
      }
      string mg_titles = ";" + x_title + ";" + y_title;
      mg[i]->SetTitle(mg_titles.c_str());

      mg[i]->Draw("A");
      usine_txt->Draw();
      leg[i]->Draw();
      // Print
      if (is_test)
         TUMisc::Print(f_test, mg[i], 0);
      c[i]->Modified();
      c[i]->Update();
   }
   if (!is_test)
      my_app->Run(kTRUE);

   // Free memory
   for (Int_t i = 0; i < n_qty; ++i) {
      delete mg[i];
      mg[i] = NULL;
      delete leg[i];
      leg[i] = NULL;
      delete c[i];
      c[i] = NULL;
   }
   delete[] mg;
   delete[] leg;
   delete[] c;
   delete usine_txt;
}

//______________________________________________________________________________
Double_t TUInteractions::Sigpp(Double_t const &etot, Int_t el0_inel1_tot2)
{
   //--- pp cross sections (fits) with one p at rest.
   //    Ref: Martin et al., PRD 92, 114021 (2015)
   //  etot    Total energy of incoming proton
   //  switch  Which XS to return

   Double_t nu2m = etot / TUPhysics::mp_GeV();

   // Parameters from            (Tab.I)  (Tab.II)
   Double_t c0      = 26.76;       //  23.54     26.76
   Double_t c1      = -0.049;      //  0.2043    -0.049
   Double_t c2      = 0.2425;      //  0.2328    0.2425
   Double_t beta    = 43.49;       //  45.05     43.49
   Double_t mu      = 0.6486;      //  0.6593    0.6486
   Double_t alpha   = 0.4068;      //  0.4069    0.4068
   Double_t delta   = -29.05;      //  -29.05    -29.05
   Double_t b0      = 7.565;       //  7.436     7.565
   Double_t b1      = -1.022;      //  -1.036    -1.022
   Double_t b2      = 0.1213;      //  0.1230    0.1213
   Double_t beta_e  = 14.00;       //  14.51     14.00
   Double_t delta_e = -5.897;      //  -5.897    -5.897

   Double_t log_nu2m = log(nu2m);

   // Equation (1)
   Double_t sig0 = c0 + c1 * log_nu2m + c2 * log_nu2m * log_nu2m + beta * pow(nu2m, mu - 1.); // [mb]
   // Equation (2)
   Double_t sigpp_tot = sig0 + delta * pow(nu2m, alpha - 1.);
   // Equation (3)
   Double_t sigpp_elas = b0 + b1 * log_nu2m + b2 * log_nu2m * log_nu2m + beta_e * pow(nu2m, mu - 1.) + delta_e * pow(nu2m, alpha - 1.);
   Double_t sigpp_inel = sigpp_tot - sigpp_elas;

   switch (el0_inel1_tot2) {
      case 0:
         return sigpp_elas;
      case 1:
         return sigpp_inel;
      case 2:
         return sigpp_tot;
      default:
         return 0.;
   }
}

//______________________________________________________________________________
Double_t TUInteractions::SigEC_Letaw(TUCREntry const &cr, Int_t zt, Double_t const &ekn)
{
   //--- Returns Electronic Capture X-section [mb]:
   //    Ref: Letaw et al.
   //  cr      CR entry
   //  zt      Charge of the target
   //  ekn     Kinetic energy per nucleon of cr [GeV/n]

   return (SigREC_Letaw(cr, zt, ekn) + SigNREC_Letaw(cr, zt, ekn));
}

//______________________________________________________________________________
Double_t TUInteractions::SigEC_OBK(TUCREntry const &cr, Int_t zt, Double_t const &ekn)
{
   //--- Returns Electronic Capture X-section [mb]: OBK first order Born approx.
   //    Ref: Dennis et al., Phys. Rev. A 50, 3991 (1994)
   //  cr      CR entry
   //  zt      Charge of the target
   //  ekn     Kinetic energy per nucleon of cr [GeV/n]

   Int_t Zp = cr.GetZ();
   Int_t Ap = cr.GetA();

   Double_t zteff = zt;
   //  zteff = zt - Delta Z(n) where Delta Z(n) is evaluated according
   // to the Slater rule. The effective charge is the amount of positive charge
   // on the nucleus perceived by an electron. Electrons intervening between
   // the nucleus and an outer electron are said to shield or screen the outer
   // electron from the nucleus so that the outer electron does not experience
   // the full nuclear charge (the larger the number of e-, the smaller the
   // effective charge zteff - browse the web for further explanations and numbers).
   //
   // For CRs, we are only interested in K-shell capture, so only 2 configurations
   // can arise :
   //  - Zp + H  or  Zp + He+ : 1s orbital, single e-, no screening zteff = zt
   //  - Zp + He : two electrons on the 1s orbital, K-shell screening zteff = zt - 0.3
   if (zt == 2) zteff -= 0.3;

   Double_t beta = TUPhysics::Beta_mAEkn(cr.GetmGeV(), Ap, ekn);
   Double_t sigma = pow(2., 18) / 5.*TUMath::Pi()
                    * (TUMath::Sqr(TUPhysics::BohrRadius_cm()) / TUPhysics::Convert_mb_to_cm2())
                    * pow(Zp * zteff, 5) * pow(TUPhysics::AlphaQED() / beta, 12);

   // If Zp + He, there are 2 electrons in the K-shell, the EC probability is twice larger...
   if (zt == 2) sigma *= 2.; // mb
   if (zt == 1) sigma *= 1.; // mb

   return (sigma); // mb
}

//______________________________________________________________________________
Double_t TUInteractions::SigEC_OBK2(TUCREntry const &cr, Int_t zt, Double_t const &ekn)
{
   //--- Returns Electronic Capture X-section [mb]: OBK second+third order Born approx.
   //    Ref: Dennis et al., Phys. Rev. A 50, 3991 (1994)
   //  cr      CR entry
   //  zt      Charge of the target
   //  ekn     Kinetic energy per nucleon of cr [GeV/n]

   Int_t Zp = cr.GetZ();
   Int_t Ap = cr.GetA();

   Double_t zteff = zt;
   //  zteff = zt - Delta Z(n) where Delta Z(n) is evaluated according
   // to the Slater rule. The effective charge is the amount of positive charge
   // on the nucleus perceived by an electron. Electrons intervening between
   // the nucleus and an outer electron are said to shield or screen the outer
   // electron from the nucleus so that the outer electron does not experience
   // the full nuclear charge (the larger the number of e-, the smaller the
   // effective charge zteff - browse the web for further explanations and numbers).
   //
   // For CRs, we are only interested in K-shell capture, so only 2 configurations
   // can arise :
   //  - Zp + H  or  Zp + He+ : 1s orbital, single e-, no screening zteff = zt
   //  - Zp + He : two electrons on the 1s orbital, K-shell screening zteff = zt - 0.3
   if (zt == 2) zteff -= 0.3;

   Double_t beta = TUPhysics::Beta_mAEkn(cr.GetmGeV(), Ap, ekn);
   Double_t sigma_OBK = pow(2., 18) / 5.*TUMath::Pi()
                        * (TUMath::Sqr(TUPhysics::BohrRadius_cm()) / TUPhysics::Convert_mb_to_cm2())
                        * pow((Double_t)(Zp * zt), 5.) * pow(TUPhysics::AlphaQED() / beta, 12);
   Double_t sigma = sigma_OBK * (0.295 + 5.*TUMath::Pi() / pow(2., 11)
                                 / (Zp + zteff) * TUPhysics::AlphaQED() / beta);

   // If Zp + He, there are 2 electrons in the K-shell, the EC probability is twice larger...
   if (zt == 2) sigma *= 2.; // mb
   if (zt == 1) sigma *= 1.; // mb

   return (sigma); // mb

}

//______________________________________________________________________________
Double_t TUInteractions::SigNREC_Letaw(TUCREntry const &cr, Int_t zt, Double_t const &ekn)
{
   //--- Returns Non-Radiative Electronic Capture X-section [mb]:
   //    Ref.: Letaw et al.
   //  cr      CR entry
   //  zt      Charge of the target
   //  ekn     Kinetic energy per nucleon of cr [GeV/n]

   Int_t Zp = cr.GetZ();
   Int_t Ap = cr.GetA();
   Double_t mp = cr.GetmGeV();

   Double_t zteff = zt;
   //  zteff = zt - Delta Z(n) where Delta Z(n) is evaluated according
   // to the Slater rule. The effective charge is the amount of positive charge
   // on the nucleus perceived by an electron. Electrons intervening between
   // the nucleus and an outer electron are said to shield or screen the outer
   // electron from the nucleus so that the outer electron does not experience
   // the full nuclear charge (the larger the number of e-, the smaller the
   // effective charge zteff - browse the web for further explanations and numbers).
   //
   // For CRs, we are only interested in K-shell capture, so only 2 configurations
   // can arise :
   //  - Zp + H  or  Zp + He+ : 1s orbital, single e-, no screening zteff = zt
   //  - Zp + He : two electrons on the 1s orbital, K-shell screening zteff = zt - 0.3
   if (zt == 2) zteff -= 0.3;

   Double_t beta = TUPhysics::Beta_mAEkn(mp, Ap, ekn);
   Double_t gamma = TUPhysics::Gamma_mAEkn(mp, Ap, ekn);
   Double_t S = beta * gamma / TUPhysics::AlphaQED();

   Double_t sigma = pow(2., 18) * (TUMath::Pi() / 5.)
                    * (TUMath::Sqr(TUPhysics::BohrRadius_cm()) / TUPhysics::Convert_mb_to_cm2())
                    * TUMath::Sqr(gamma) * pow((Double_t)(Zp * zteff), 5) * pow(S, 8.)
                    //* pow(TUPhysics::AlphaQED()/beta,12.);  // From Wilson, ICRC 15, Vol. 2, p. 274 (1978)
                    * pow(S * S + TUMath::Sqr((Double_t)(Zp + zteff)), -5)
                    * pow(S * S + TUMath::Sqr((Double_t)(Zp - zteff)), -5); // From Letaw et al, 1986

   // If Zp + He, there are 2 electrons in the K-shell, the EC probability is twice larger...
   if (zt == 2) sigma *= 2.; // mb
   if (zt == 1) sigma *= 1.; // mb
   return sigma; // mb
}

//______________________________________________________________________________
Double_t TUInteractions::SigNREC_Eichler(TUCREntry const &cr, Int_t zt, Double_t const &ekn)
{
   //--- Returns Non-Radiative Electronic Capture X-section [mb]: eikonal approx.
   //    Ref.: Eichler 85.
   //  cr      CR entry
   //  zt      Charge of the target
   //  ekn     Kinetic energy per nucleon of cr [GeV/n]

   Int_t Zp = cr.GetZ();
   Int_t Ap = cr.GetA();

   Double_t zteff = zt;
   //  zteff = zt - Delta Z(n) where Delta Z(n) is evaluated according
   // to the Slater rule. The effective charge is the amount of positive charge
   // on the nucleus perceived by an electron. Electrons intervening between
   // the nucleus and an outer electron are said to shield or screen the outer
   // electron from the nucleus so that the outer electron does not experience
   // the full nuclear charge (the larger the number of e-, the smaller the
   // effective charge zteff - browse the web for further explanations and numbers).
   //
   // For CRs, we are only interested in K-shell capture, so only 2 configurations
   // can arise :
   //  - Zp + H  or  Zp + He+ : 1s orbital, single e-, no screening zteff = zt
   //  - Zp + He : two electrons on the 1s orbital, K-shell screening zteff = zt - 0.3
   if (zt == 2) zteff -= 0.3;

   Double_t beta = TUPhysics::Beta_mAEkn(cr.GetmGeV(), Ap, ekn);
   Double_t gamma = TUPhysics::Gamma_mAEkn(cr.GetmGeV(), Ap, ekn);

   Double_t delt = sqrt((gamma - 1.) / (gamma + 1.));
   Double_t v = beta / TUPhysics::AlphaQED();  // !!!! Velocity in Atomic Units
   //  -> corresponds to the Bohr velocity
   Double_t eta = 1. / v;
   Double_t p = delt / TUPhysics::AlphaQED();

   Double_t Seik = 1. + 5.*eta * p / 4.
                   + 5.*TUMath::Sqr(eta * p) / 12.
                   + TUMath::Sqr(eta * zteff) / 6.;

   Double_t Smagn = -TUMath::Sqr(delt) + 5.*pow(delt, 4) / 16. + 5
                    * TUMath::Sqr(delt) * gamma / (8.*(gamma + 1.))
                    + TUMath::Sqr(delt * eta * zteff) / 4. + 5.*pow(delt, 4)
                    * TUMath::Sqr(eta * zteff) / 48.;

   Double_t Sorb = 5.*TUMath::Pi() * delt * TUPhysics::AlphaQED() * (Zp + zteff) / 18.
                   - 5.*TUMath::Pi() * TUMath::Cub(delt) * TUPhysics::AlphaQED()
                   * (zteff + Zp) / 36.
                   - 5.*TUPhysics::AlphaQED() * delt * eta * TUMath::Sqr(zteff)
                   * (1. - TUMath::Sqr(delt) / 2.) / 8.
                   - 5.*TUMath::Pi() * delt * gamma * TUPhysics::AlphaQED()
                   * Zp / (18.*(gamma + 1.))
                   + 5.*TUMath::Pi() * delt * TUMath::Sqr(gamma) * TUPhysics::AlphaQED()
                   * Zp / (28.*TUMath::Sqr(gamma + 1.))
                   - 5.*TUMath::Pi() * delt * gamma * TUPhysics::AlphaQED()
                   * (Zp + zteff - TUMath::Sqr(delt) * Zp) / (28.*(gamma + 1.));

   // Check
   //cout << "Seik=" << Seik << " Smagn=" <<Smagn << " Sorb=" <<Sorb << endl;

   Double_t sigma = (pow(2., 8) * TUMath::Pi() * pow((Double_t)(Zp * zteff), 5)
                     * (gamma + 1.) * (TUMath::Pi() * eta * zteff)
                     / (5.*v * v * pow(((Double_t)(zteff * zteff) + p * p), 5)
                        * 2.*TUMath::Sqr(gamma)))
                    * (exp(-2.*eta * zteff * atan(-p / zteff)) / sinh(TUMath::Pi()
                          * eta * zteff)) * (Seik + Smagn + Sorb);

   // sigma = TUMath::Sqr(TUPhysics::BohrRadius_cm())/TUPhysics::Convert_mb_to_cm2()*sigma;

   // If Zp + He, there are 2 electrons in the K-shell, the EC probability is twice larger...
   if (zt == 2) sigma *= 2.; // mb
   if (zt == 1) sigma *= 1.; // mb

   return (sigma); // mb
}

//______________________________________________________________________________
Double_t TUInteractions::SigREC_Letaw(TUCREntry const &cr, Int_t zt, Double_t const &ekn)
{
   //--- Returns Radiative Electronic Capture X-section [mb].
   //    Refs.: Wilson (1978) as given by Letaw et al., APJSS 56, 369 (1984)
   //  cr      CR entry
   //  zt      Charge of the target
   //  ekn     Kinetic energy per nucleon of cr [GeV/n]

   Int_t Zp = cr.GetZ();
   Int_t Ap = cr.GetA();
   Double_t mp = cr.GetmGeV();

   Double_t zteff = zt;
   //  zteff = zt - Delta Z(n) where Delta Z(n) is evaluated according
   // to the Slater rule. The effective charge is the amount of positive charge
   // on the nucleus perceived by an electron. Electrons intervening between
   // the nucleus and an outer electron are said to shield or screen the outer
   // electron from the nucleus so that the outer electron does not experience
   // the full nuclear charge (the larger the number of e-, the smaller the
   // effective charge zteff - browse the web for further explanations and numbers).
   //
   // For CRs, we are only interested in K-shell capture, so only 2 configurations
   // can arise :
   //  - Zp + H  or  Zp + He+ : 1s orbital, single e-, no screening zteff = zt
   //  - Zp + He : two electrons on the 1s orbital, K-shell screening zteff = zt - 0.3
   if (zt == 2) zteff -= 0.3;

   Double_t a = TUPhysics::AlphaQED() * Zp;
   Double_t zeta = sqrt(1 - a * a) - 1;
   Double_t beta = TUPhysics::Beta_mAEkn(mp, Ap, ekn);
   Double_t gamma = TUPhysics::Gamma_mAEkn(mp, Ap, ekn);
   Double_t log_beta = log((1. + beta) / (1. - beta));
   Double_t M = 4. / 3. + (gamma * (gamma - 2.) / (gamma + 1.))
                * (1. - (log_beta / (2.*TUMath::Sqr(gamma) * beta)));

   Double_t N = (-4.*gamma + 34. - 63. / gamma + 25. / (TUMath::Sqr(gamma))
                 + 8. / (TUMath::Cub(gamma))) / (15.*TUMath::Cub(beta))
                - (gamma - 2.) * log_beta / (2.*TUMath::Sqr(beta) * gamma * (gamma + 1.));

   Double_t R = -exp(-8.4 * a * a + 14.*a - 8.28);

   Double_t sigma = 1.5 * Zp * zteff * (TUPhysics::SigmaThomson_barn() * 1.e3)
                    * beta * gamma / TUMath::Cub(gamma - 1.)
                    * pow(a, 4. + 2.*zeta) * exp(-2.*a / (beta * cos(a)))
                    * (M * (1. + R) + TUMath::Pi() * a * N);

   // If Zp + He, there are 2 electrons in the K-shell, the EC probability is twice larger...
   if (zt == 2) sigma *= 2.; // in mb
   if (zt == 1) sigma *= 1.;    // in mb

   return (sigma);
}

//______________________________________________________________________________
Double_t TUInteractions::SigREC_Anholt(TUCREntry const &cr, Int_t zt, Double_t const &ekn,
                                       TUAtomProperties *atom)
{
   //--- Returns Radiative Electronic Capture X-section [mb].
   //    Ref: Anholt, Phys. Rev. A 31, 3579 (1985).
   //    N.B.: difference from SigREC_Letaw => N and R terms slightly different.
   //  cr      CR entry
   //  zt      Charge of the target
   //  ekn     Kinetic energy per nucleon of cr [GeV/n]

   Int_t Zp = cr.GetZ();
   Int_t Ap = cr.GetA();
   Double_t mp = cr.GetmGeV();

   Double_t zteff = zt;
   //  zteff = zt - Delta Z(n) where Delta Z(n) is evaluated according
   // to the Slater rule. The effective charge is the amount of positive charge
   // on the nucleus perceived by an electron. Electrons intervening between
   // the nucleus and an outer electron are said to shield or screen the outer
   // electron from the nucleus so that the outer electron does not experience
   // the full nuclear charge (the larger the number of e-, the smaller the
   // effective charge zteff - browse the web for further explanations and numbers).
   //
   // For CRs, we are only interested in K-shell capture, so only 2 configurations
   // can arise :
   //  - Zp + H  or  Zp + He+ : 1s orbital, single e-, no screening zteff = zt
   //  - Zp + He : two electrons on the 1s orbital, K-shell screening zteff = zt - 0.3
   if (zt == 2) zteff -= 0.3;

   Double_t a = TUPhysics::AlphaQED() * Zp;
   Double_t zeta = sqrt(1. - a * a) - 1.;
   Double_t beta = TUPhysics::Beta_mAEkn(mp, Ap, ekn);
   Double_t gamma = TUPhysics::Gamma_mAEkn(mp, Ap, ekn);
   Double_t log_beta = log((1. + beta) / (1. - beta));

   Double_t M = 4. / 3. + (gamma * (gamma - 2.) / (gamma + 1.))
                * (1. - (log_beta / (2.*TUMath::Sqr(gamma) * beta)));

   Double_t N = (-4.*gamma + 34. - 63. / gamma + 25. / (TUMath::Sqr(gamma))
                 + 8. / (TUMath::Cub(gamma))) / (15.*TUMath::Cub(beta))
                - (TUMath::Sqr(gamma) - 3.*gamma + 2.) * log_beta
                / (2.*TUMath::Cub(gamma) * TUMath::Sqr(beta) * TUMath::Sqr(beta));

   Double_t R = pow(a, 2.*zeta) * exp(-2.*a / (beta * cos(a))) * (1 + TUMath::Pi() * a * N / M);

   Double_t l = (gamma - 1.) + (atom->GetEionKshell(Zp) * 1.e-6)
                / (Ap * TUPhysics::Convert_amu_to_GeV());
   Double_t sigmaPE = 1.5 * 0.665 * pow(a, 5) * TUMath::Cub(beta * gamma)
                      * R * M / (TUPhysics::AlphaQED() * pow(l, 5)); // in barn

   Double_t sigma = zt * sigmaPE * 1.e3 * TUMath::Sqr((l / (gamma * beta))); // in mb

   // If Zp + He, there are 2 electrons in the K-shell, the EC probability is twice larger...
   if (zt == 2) sigma *= 2.; // in mb
   if (zt == 1) sigma *= 1.; // in mb

   return (sigma); // in mb
}

//______________________________________________________________________________
Double_t TUInteractions::SigES_Letaw(TUCREntry const &cr, Int_t zt, Double_t const &ekn)
{
   //--- Returns Electron Stripping X-section [mb].
   //    Ref.: Letaw et al.
   //  cr      CR entry
   //  zt      Charge of the target
   //  ekn     Kinetic energy per nucleon of cr [GeV/n]

   // N.B.: ionization formulae are not screened, since at the high
   // velocities of interest, screening effects are less than 10% even
   // for the M shell (Meyerhof et et al., Phys. Rev. A 35, 1055 (1987),
   // Appendix)
   Double_t C1 = 0.285;
   Double_t C2 = 0.048;

   Int_t Zp = cr.GetZ();
   Int_t Ap = cr.GetA();

   Double_t beta = TUPhysics::Beta_mAEkn(cr.GetmGeV(), Ap, ekn);
   Double_t gamma = TUPhysics::Gamma_mAEkn(cr.GetmGeV(), Ap, ekn);

   Double_t sigma = 4.*TUMath::Pi() * TUMath::Sqr(TUPhysics::BohrRadius_cm())
                    * TUMath::Sqr(TUPhysics::AlphaQED() / (Zp * beta))
                    * (TUMath::Sqr(zt) + zt) * C1 * (log(4.*TUMath::Sqr(beta) * TUMath::Sqr(gamma)
                          / (C2 * TUMath::Sqr(TUPhysics::AlphaQED() * Zp))) - TUMath::Sqr(beta))
                    / TUPhysics::Convert_mb_to_cm2();

   return sigma; // mb
}

//______________________________________________________________________________
Double_t TUInteractions::SigES_Bohr(TUCREntry const &cr, Int_t zt, Double_t const &ekn)
{
   //--- Returns Electron Stripping X-section [mb]: Bohr formulation.
   //    Ref.: Dennis et al., Phys. Rev. A 50, 3991 (1994).
   //  cr      CR entry
   //  zt      Charge of the target
   //  ekn     Kinetic energy per nucleon of cr [GeV/n]

   Int_t Zp = cr.GetZ();
   Int_t Ap = cr.GetA();

   Double_t beta = TUPhysics::Beta_mAEkn(cr.GetmGeV(), Ap, ekn);
   Double_t I_B = Double_t(zt * (zt + 1)) / (2 * TUMath::Sqr(Zp));
   Double_t sigma = 8.*TUMath::Pi() * (TUMath::Sqr(TUPhysics::BohrRadius_cm())
                                       / TUPhysics::Convert_mb_to_cm2()) * I_B * TUMath::Sqr(TUPhysics::AlphaQED() / beta);

   return sigma; // mb
}

//______________________________________________________________________________
Double_t TUInteractions::SigES_Gillespie(TUCREntry const &cr, Int_t zt, Double_t const &ekn)
{
   //--- Returns Electron Stripping X-section [mb]: Gillespie formulation
   //    Ref.: Dennis et al., Phys. Rev. A 50, 3991 (1994).
   //  cr      CR entry
   //  zt      Charge of the target
   //  ekn     Kinetic energy per nucleon of cr [GeV/n]

   Int_t Zp = cr.GetZ();
   Int_t Ap = cr.GetA();

   Double_t beta = TUPhysics::Beta_mAEkn(cr.GetmGeV(), Ap, ekn);
   Double_t I_G = 1.24 * zt / (TUMath::Sqr(Zp))
                  * (1. + 0.105 * zt - 5.4e-4 * TUMath::Sqr(zt));
   Double_t sigma = 8.*TUMath::Pi() * (TUMath::Sqr(TUPhysics::BohrRadius_cm())
                                       / TUPhysics::Convert_mb_to_cm2()) * I_G
                    * TUMath::Sqr(TUPhysics::AlphaQED() / beta);

   return sigma; // mb
}

//______________________________________________________________________________
Double_t TUInteractions::SigES_Baltz(TUCREntry const &cr, Int_t zt, Double_t const &ekn)
{
   //--- Returns Electron Stripping X-section [mb]:  ultrarelativistic regime.
   //    Ref.: Baltz 2001.
   //  cr      CR entry
   //  zt      Charge of the target
   //  ekn     Kinetic energy per nucleon of cr [GeV/n]

   Int_t Zp = cr.GetZ();
   Int_t Ap = cr.GetA();
   //Double_t zteff = 0.;
   //if (zt == 2) zteff = zt - 0.3;
   //else if (zt == 1) zteff = zt;

   Double_t gamma_lorentz = TUPhysics::Gamma_mAEkn(cr.GetmGeV(), Ap, ekn);
   Double_t gammap = sqrt(1. - TUMath::Sqr(TUPhysics::AlphaQED() * Zp));
   Double_t gammat = sqrt(1. - TUMath::Sqr(TUPhysics::AlphaQED() * zt));

   Double_t A = 1792.*zt * zt * (1. + 3.*gammap + 2.*TUMath::Sqr(gammap)) / (Zp * Zp); // barns
   Double_t B = A * pow(gammat, 0.1) * pow(1. - TUMath::Sqr(TUPhysics::AlphaQED())
                                           * zt * Zp, 0.25) * log(2.37 * TUPhysics::AlphaQED() * Zp / (1. - gammap));

   Double_t sigma = (A * log(gamma_lorentz) + B) * 1e3; // en mbarns
   return sigma;
}

//______________________________________________________________________________
Double_t TUInteractions::SigES_Meyerhof(TUCREntry const &cr, Int_t zt, Double_t const &ekn)
{
   //--- Returns Electron Stripping X-section [mb].
   //    Ref.: Meyerhof et al., Phys. Rev. A 35, 1055 (1987), Appendix.
   //  cr      CR entry
   //  zt      Charge of the target
   //  ekn     Kinetic energy per nucleon of cr [GeV/n]

   Int_t Zp = cr.GetZ();
   Int_t Ap = cr.GetA();
   Double_t beta = TUPhysics::Beta_mAEkn(cr.GetmGeV(), Ap, ekn);

   Double_t eta = TUMath::Sqr((beta / (Zp * TUPhysics::AlphaQED())));
   printf("%le %d\n", eta, zt);
   //static TUAtomProperties AtomProp;
   //// AtomProp.getEionKshell(Zp) = energy ionization of the K-shell of nucleus Z in keV
   //Double_t theta = (AtomProp.getEionKshell(Zp)*1.e3/Rydberg)/TUMath::Sqr(Zp);
   return 0.;
}

//______________________________________________________________________________
void TUInteractions::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "Contains the description of many physics processes\n"
                    "(energy losses, attachment and stripping X-sec,...)";
   TUMessages::Test(f, "TUInteractions", message);


   fprintf(f, " * Check consistency ion/coulomb\n");
   // CR entry
   string f_charts = TUMisc::GetPath("$USINE/inputs/crcharts_Zmax30_ghost84.dat");
   fprintf(f, "   > TUCREntry cr; cr.SetEntry(\"12C\", f_charts);\n");
   TUCREntry cr;
   cr.SetEntry("12C", f_charts);
   fprintf(f, "   > cr.Print(f);\n");
   cr.Print(f);
   fprintf(f, "\n");

   // ISM entry
   fprintf(f, "   > TUMediumEntry ism; ism.SetElements(\"H,He\"); ism.SetPlasmaT(1.e4);\n");
   fprintf(f, "   > ism.SetDensity(i_elem=1,0.1 [/cm3]); ism.DensityH(HI=0.867, HII=0.033, H2=0.);\n");
   TUMediumEntry ism;
   ism.SetElements("H,He");
   ism.SetPlasmaT(1.e4);
   ism.SetDensity(1, 0.1);
   ism.SetDensityH(0.867, 0.033, 0.);
   fprintf(f, "   > ism.Print();\n");
   ism.Print(f);
   fprintf(f, "\n");

   // Atom properties
   string f_atom = TUMisc::GetPath("$USINE/inputs/atomic_properties.dat");
   fprintf(f, "   > TUAtomProperties atom_prop; atom_prop.SetClass(f_atom);\n");
   TUAtomProperties atom_prop;
   atom_prop.SetClass(f_atom, f);
   fprintf(f, "\n");

   string tmp_cr[2] = {"12C", "16O"};
   for (Int_t i = 0; i < 2; ++i) {
      cr.SetEntry(tmp_cr[i], f_charts);
      fprintf(f, "%5s: A=%3d  Z=%3d  m(amu)=%.3f\n", cr.GetName().c_str(), cr.GetA(), cr.GetZ(), cr.Getmamu());
      fprintf(f, "   ekn(GeV)     ion      coul      tot1       tot2    (GeV/Myr)\n");
      for (Double_t ekn = 0.1; ekn < 1.1e3; ekn *= 2.) {
         Double_t ion = DEdtIonisation(cr, &ism, &atom_prop, ekn);
         Double_t coulomb = DEdtCoulomb(cr, &ism, ekn);
         Double_t tot1 = DEdtIonCoulomb(cr, &ism, &atom_prop, ekn);
         Double_t beta = TUPhysics::Beta_mAEkn(cr.GetmGeV(), cr.GetA(), ekn);
         Double_t gamma = TUPhysics::Gamma_mAEkn(cr.GetmGeV(), cr.GetA(), ekn);
         Double_t tot2 = DEdtIonCoulomb(cr, &ism, &atom_prop, beta, gamma);
         fprintf(f, "   %.2e  %.2e  %.2e  %.2e  %.2e\n", ekn, ion, coulomb, tot1, tot2);
      }
      fprintf(f, "\n");
   }
   fprintf(f, "EC: TO DO!\n");
}
