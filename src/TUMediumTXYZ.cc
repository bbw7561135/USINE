// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include "iostream"
// ROOT include
// USINE include
#include "../include/TUAtomElements.h"
#include "../include/TUMediumTXYZ.h"
#include "../include/TUValsTXYZEFormula.h"
#include "../include/TUValsTXYZEGrid.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUMediumTXYZ                                                         //
//                                                                      //
// Density and temperature of the medium (up to 3D+1 description).      //
//                                                                      //
// The class TUMediumTXYZ provides a time- and space-dependent          //
// description of the medium (generalisation of TUMediumEntry),         //
// whose properties are:                                                //
//    - number density [cm^-3] for a list of user-defined elements (stored in TUMediumEntry);
//    - temperature of the plasma [K].
// Each of this quantity is described by a TUValsTXYZEVirtual object,   //
// enabling an array- or formula-based (with free parameters)           //
// description.                                                         //
//                                                                      //
// As for TUMediumEntry, if H is present in the medium, the density     //
// of HI (neutral atomic), HII (ionised atomic hydrogen), and H2        //
// (molecular hydrogen) must be specified: as a result, the density     //
//  n_H is automatically set to n_H = n_HI + n_HII + 2*N_H2.            //
//                                                                      //
// N.B.: (t,x,y,z) space-time variable (resp. bin) is represented by a  //
// 4-vector (resp. integer) also used for any dimensionality (1,2,3).   //
// Only the relevant first components are then used.                    //
//                                                                      //
// Free parameters and formulae:                                        //
// -+-+-+-+-+-+-+-+-+-+-+-+-+-
// Some class members may be described by formulae (which depend on free//
// parameter values, see TUValsTXYZVirtual). Whenever a parameter value //
// is changed, it has to be propagated to the formula. There are two    //
// ways to achieve that:
//    A. you can decide not to care about it: the class TUValsTXYZEFormula
//      will do the job automatically for you, whenever any method ValueXXX()
//      of this class is called. Indeed, the latter will invariably end up
//      calling TUValsTXYZEFormula::EvalFormula() or the like of it, and
//      then TUValsTXYZEFormula::UpdateFromFreeParsAndResetStatus() to
//     do the job.
//    B. call UpdateFromFreeParsAndResetStatus() to update all formula
//     found in this class (and reset TUFreeParList status to 'updated'.
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUMediumTXYZ)

//______________________________________________________________________________
TUMediumTXYZ::TUMediumTXYZ()
{
   // ****** Default constructor ******

   fDensities = NULL;
   fDensityH2 = NULL;
   fDensityHI = NULL;
   fDensityHII = NULL;
   fFreePars = NULL;
   fMediumEntry = new TUMediumEntry();
   fPlasmaT = NULL;
}

//______________________________________________________________________________
TUMediumTXYZ::TUMediumTXYZ(TUMediumTXYZ const &medium)
{
   // ****** Copy constructor ******
   //  medium            Object to copy from

   fDensities = NULL;
   fDensityH2 = NULL;
   fDensityHI = NULL;
   fDensityHII = NULL;
   fFreePars = NULL;
   fMediumEntry = new TUMediumEntry();
   fPlasmaT = NULL;
   Copy(medium);
}

//______________________________________________________________________________
TUMediumTXYZ::~TUMediumTXYZ()
{
   // ****** Default destructor ******
   //

   Initialise(true);
}

//______________________________________________________________________________
void TUMediumTXYZ::Copy(TUMediumTXYZ const &medium)
{
   //--- Copies medium in current class.
   //  medium          Object to copy from

   Initialise(true);

   TUMediumEntry *tmp_entry = medium.GetMediumEntry();
   if (tmp_entry)
      fMediumEntry = tmp_entry->Clone();
   else
      fMediumEntry = NULL;

   TUValsTXYZEVirtual *dummy;
   dummy = medium.GetDensityH2();
   if (dummy)
      fDensityH2 = dummy->Clone();
   else
      fDensityH2 = NULL;

   dummy = medium.GetDensityHI();
   if (dummy)
      fDensityHI = dummy->Clone();
   else
      fDensityHI = NULL;

   dummy = medium.GetDensityHII();
   if (dummy)
      fDensityHII = dummy->Clone();
   else
      fDensityHII = NULL;

   dummy = medium.GetPlasmaT();
   if (dummy)
      fPlasmaT = dummy->Clone();
   else
      fPlasmaT = NULL;


   TUFreeParList *tmp = medium.GetFreePars();
   if (tmp)
      fFreePars = tmp->Clone();
   else
      fFreePars = NULL;
   tmp = NULL;

   fDensities = new TUValsTXYZEVirtual*[medium.GetNMediumElements()];
   for (Int_t i = 0; i < medium.GetNMediumElements(); ++i) {
      dummy = medium.GetDensity(i);
      if (dummy)
         fDensities[i] = dummy->Clone();
      else
         fDensities[i] = NULL;
   }
   dummy = NULL;
}

//______________________________________________________________________________
void TUMediumTXYZ::FillMediumEntry(TUCoordTXYZ *coord_txyz)
{
   //--- Fills inherited TUMediumEntry from composition at a given position.
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z

   // Set density of elements
   if (fDensities) {
      for (Int_t i = 0; i < GetNMediumElements(); ++i) {
         if (fDensities[i]) {
            fMediumEntry->SetDensity(i, ValueDensity(i, coord_txyz));
         }
      }
   }

   // Set density of HI, HII and H2
   Double_t hi = 0, hii = 0., h2 = 0.;
   if (fDensityHI)  hi = ValueDensityHI(coord_txyz);
   if (fDensityHII) hii = ValueDensityHII(coord_txyz);
   if (fDensityH2)  h2 = ValueDensityH2(coord_txyz);
   fMediumEntry->SetDensityH(hi, hii, h2);

   // Set plasma temperature
   if (fPlasmaT)
      fMediumEntry->SetPlasmaT(ValuePlasmaT(coord_txyz));
}

//______________________________________________________________________________
void TUMediumTXYZ::Initialise(Bool_t is_delete)
{
   //--- Initialises (delete and set to NULL).
   //  is_delete         Whether to delete or just initialise

   // N.B.: fMediumEntry must always be allocated, unless destructor calls Initialise
   Int_t n_elem = 0;
   if (is_delete) {
      if (fMediumEntry) {
         n_elem = GetNMediumElements();
         delete fMediumEntry;
      }
      fMediumEntry = NULL;
   }

   if (fFreePars) delete fFreePars;
   fFreePars = NULL;
   if (fDensities) {
      for (Int_t i = 0; i < n_elem; ++i) {
         if (fDensities[i]) delete fDensities[i];
         fDensities[i] = NULL;
      }
      delete[] fDensities;
   }
   fDensities = NULL;

   if (fDensityHI)  delete fDensityHI;
   fDensityHI = NULL;
   if (fDensityHII) delete fDensityHII;
   fDensityHII = NULL;
   if (fDensityH2)  delete fDensityH2;
   fDensityH2 = NULL;
   if (fPlasmaT)    delete fPlasmaT;
   fPlasmaT = NULL;
}

//______________________________________________________________________________
void TUMediumTXYZ::PrintSummary(FILE *f) const
{
   //--- Prints medium summary.
   //  f                 File in which to print

   if (fDensities) {
      for (Int_t i = 0; i < GetNMediumElements(); ++i) {
         if (fDensities[i])
            fDensities[i]->PrintSummary(f, GetNameMediumElement(i));
      }
   }
   if (fDensityHI) fDensityHI->PrintSummary(f, "HI");
   if (fDensityHII) fDensityHII->PrintSummary(f, "HII");
   if (fDensityH2) fDensityH2->PrintSummary(f, "H2");
   if (fPlasmaT) fPlasmaT->PrintSummary(f, "Plasma T");

}

//______________________________________________________________________________
void TUMediumTXYZ::SetClass(TUInitParList *init_pars, string const &group, string const &subgroup, Bool_t is_verbose, FILE *f_log)
{
   //--- Updates class formula/file from base file (belonging to Group#Subgroup).
   //  init_pars         Initialisation parameters
   //  group             Group name
   //  subgroup          Subroup name
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUMediumTXYZ::SetClass]\n", indent.c_str());

   // Clear class
   Initialise(true);
   fMediumEntry = new TUMediumEntry();


   // Allocate and add free medium parameters (delete if no free param. found)
   fFreePars = new TUFreeParList();
   fFreePars->AddPars(init_pars, group, subgroup, is_verbose, f_log);
   if (is_verbose)
      fFreePars->PrintPars(f_log);
   if (fFreePars->GetNPars() == 0) {
      delete fFreePars;
      fFreePars = NULL;
   }

   const Int_t gg = 3;
   string gr_sub_name[gg] = {"Base", "MediumCompo", "Targets"};

   // Set medium targets
   if (is_verbose)
      fprintf(f_log, "%s### Set medium targets [%s#%s#%s]\n",
              indent.c_str(), gr_sub_name[0].c_str(), gr_sub_name[1].c_str(), gr_sub_name[2].c_str());
   Int_t i_targ = init_pars->IndexPar(gr_sub_name);
   if (i_targ < 0)
      TUMessages::Error(f_log, "TUMediumTXYZ", "SetClass",
                        "Requires in the initialisation parameters of"
                        " group#subgroup#param = Base#MediumCompo#string#Targets");

   string targ = init_pars->GetParEntry(i_targ).GetVal();
   SetElements(targ);
   if (is_verbose)
      fprintf(f_log, "%s  => %s\n", indent.c_str(), targ.c_str());

   // Check whether the list of elements in the medium is not empty: if not, allocate
   if (GetNMediumElements() == 0)
      TUMessages::Error(f_log, "TUMediumTXYZ", "SetClass", "No element found!");
   else {
      fDensities = new TUValsTXYZEVirtual*[GetNMediumElements()];
      for (Int_t i = 0; i < GetNMediumElements(); ++i)
         fDensities[i] = NULL;
   }

   // Look whether H is part of the medium elements
   // (H needs a special case with HI, HII, and H2)
   Int_t index_h = IndexElementInMedium(1);
   Bool_t is_h_in_medium = false;
   if (index_h >= 0)
      is_h_in_medium = true;
   Bool_t is_h = false;
   Bool_t is_hi = false;
   Bool_t is_hii = false;
   Bool_t is_h2 = false;

   // TAtomicElements class is required to check whether element
   // names in Medium list exist
   TUAtomElements elements;
   if (is_verbose)
      fprintf(f_log, "%s### Set medium distributions\n", indent.c_str());

   const Int_t n_medium = 2;
   string par[n_medium] = {"Density", "Te"};
   string commasep_vars = "t,x,y,z";

   gr_sub_name[0] = group;
   gr_sub_name[1] = subgroup;
   // Loop on medium components to fill
   for (Int_t i = 0; i < n_medium; ++i) {
      // Find parameter index and number of entries (for this par)
      gr_sub_name[2] = par[i];
      Int_t i_par = init_pars->IndexPar(gr_sub_name);
      if (i_par < 0) continue;
      else if (is_verbose)
         fprintf(f_log, "%s  - %s %s", indent.c_str(), par[i].c_str(), ("["
                 + gr_sub_name[0] + "@" + gr_sub_name[1] + "@" + gr_sub_name[2] + "]").c_str());

      Int_t n_multi = init_pars->GetParEntry(i_par).GetNVals();
      if (n_multi == 1 && init_pars->GetParEntry(i_par).GetVal() == "-") continue;

      // If parameter found, allocate and fill relevant class member
      // Loop on multiple values of the parameter
      for (Int_t j_multi = 0; j_multi < n_multi; ++j_multi) {
         string val = init_pars->GetParEntry(i_par).GetVal(j_multi);
         string qty = val.substr(0, val.find_first_of(":"));
         Int_t index_qty = -1;
         //cout << " param=" << par[i] << "  val[" << j_multi << "]="
         //     << val << "  => qty=" << qty << endl;

         // Allocate and fill medium description: to do so, check whether
         // formula or read file (and retrieve formula or file name)
         string formula_or_file = "";
         gENUM_FCNTYPE format = init_pars->ExtractFormat(i_par, j_multi, formula_or_file, is_verbose, f_log);

         if (is_h_in_medium && (qty == "HI" || qty == "HII" || qty == "H2")) {
            TUValsTXYZEVirtual *density = NULL;
            if (format == kFORMULA) {
               density = new TUValsTXYZEFormula();
               density->SetClass(formula_or_file, is_verbose, f_log, commasep_vars, fFreePars);
            } else if (format == kGRID) {
               density = new TUValsTXYZEGrid();
               density->SetClass(formula_or_file, is_verbose, f_log);
            } else if (format == kSPLINE) {
               TUMessages::Error(f_log, "TUMediumTXYZ", "SetClass", "Spline not implemented for TXYZ coordinates");
            }
            if (qty == "HI") {
               is_hi = true;
               fDensityHI = density;
            } else if (qty == "HII") {
               is_hii = true;
               fDensityHII = density;;
            } else if (qty == "H2") {
               is_h2 = true;
               fDensityH2 = density;
            }
            density = NULL;
         } else if (par[i] == "Te") {
            if (format == kFORMULA) {
               fPlasmaT = new TUValsTXYZEFormula();
               fPlasmaT->SetClass(formula_or_file, is_verbose, f_log, commasep_vars, fFreePars);
            } else if (format == kGRID) {
               fPlasmaT = new TUValsTXYZEGrid();
               fPlasmaT->SetClass(formula_or_file, is_verbose, f_log);
            } else if (format == kSPLINE) {
               TUMessages::Error(f_log, "TUMediumTXYZ", "SetClass", "Spline not implemented yet for this function");
            }
         } else {
            // Look whether qty is an element in list
            Int_t z_qty = elements.ElementNameToZ(qty);
            if (z_qty < 0) {
               string message = "quantity in string " + init_pars->GetParEntry(i_par).GetVal(j_multi)
                                + " is not an element or a valid Medium entry (e.g. Te)";
               TUMessages::Error(f_log, "TUMediumTXYZ", "SetClass", message);
            } else if (z_qty == 1)
               is_h = true;

            // If qty is an element, check whether it is in the list of Medium elements
            index_qty = IndexElementInMedium(z_qty);
            if (index_qty < 0) {
               string message = "element " + qty + " from " + init_pars->GetParEntry(i_par).GetVal(j_multi)
                                + " is not in the list of medium elements => discard it";
               TUMessages::Warning(f_log, "TUMediumTXYZ", "SetClass", message);
            } else {
               // Check if this element has already been filled (if already allocated,
               // means that the same quantity appears twice in the initialisation file).
               if (fDensities[index_qty]) {
                  string message = "element " + qty + " from " + init_pars->GetParEntry(i_par).GetVal(j_multi)
                                   + " appears twice: check your initialisation file!";
                  TUMessages::Error(f_log, "TUMediumTXYZ", "SetClass", message);
               }

               // Set to appropriate pointer
               if (format == kFORMULA) {
                  fDensities[index_qty] = new TUValsTXYZEFormula();
                  fDensities[index_qty]->SetClass(formula_or_file, is_verbose, f_log, commasep_vars, fFreePars);
               } else if (format == kGRID) {
                  fDensities[index_qty] = new TUValsTXYZEGrid();
                  fDensities[index_qty]->SetClass(formula_or_file, is_verbose, f_log);
               } else if (format == kSPLINE) {
                  TUMessages::Error(f_log, "TUMediumTXYZ", "SetClass", "Spline not implemented yet for this function");
               }
            }
         }
         // Check that we have either H or (HI and/or HII and/or H2), not both (not allowed)
         if (is_h && (is_hi || is_hii || is_h2))
            TUMessages::Error(f_log, "TUMediumTXYZ", "SetClass",
                              "Having components H and (HI and/or HII and/or H2) is not allowed: "
                              "it's either H only or the separate components without H!");

      }

   }

   if (is_verbose)
      fprintf(f_log, "%s[TUMediumTXYZ::SetClass] <DONE>\n\n", indent.c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUMediumTXYZ::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message = "Medium properties description. This extends \n"
                    "the class TUMediumEntry to 3D+1, where either formula- or\n"
                    "array based-description are possible:\n"
                    "   - Number density [/cm3] for a list of user-defined elements;\n"
                    "   - Temperature [k]\n"
                    "   -...\n"
                    "N.B.: The medium is defined by a comma-separated list of elements.\n"
                    "If H is in the medium, HI, HII, and H2 can be specified (and then\n"
                    " n_H = n_HI + n_HII + 2 n_H2).";
   TUMessages::Test(f, "TUMediumTXYZ", message);

   /******************************/
   /*   Array-based description  */
   /******************************/

   /*****************************/
   /* Formula-based description */
   /*****************************/
   // Set class and test methods
   fprintf(f, " * Check formula-based description (for Leaky-Box model: formula = cst)\n");
   fprintf(f, "   > SetClass(init_pars=\"%s\", group=Model0DLeakyBox, subgroup=ISM, is_verbose=false, f_log=f);\n",
           init_pars->GetFileNames().c_str());
   SetClass(init_pars, "Model0DLeakyBox", "ISM", false, f);

   fprintf(f, "   > PrintSummary(f)\n");
   PrintSummary(f);
   fprintf(f, "   > TUCoordTXYZ coord_txyz;  coord_txyz.SetValTXYZ(0, 1, 0, 2);\n");
   TUCoordTXYZ coord_txyz;
   coord_txyz.SetValTXYZ(0, 1, 0, 2);
   fprintf(f, "   > FillMediumEntry(&coord_txyz);\n");
   FillMediumEntry(&coord_txyz);
   fprintf(f, "   > PrintEntry(f)\n");
   PrintEntry(f);

   fprintf(f, "\n");
   fprintf(f, " * Check more complex formula-based description depending on free pars (for ModelTEST model)\n");
   fprintf(f, "   > SetClass(init_pars, group=ModelTEST, subgroup=ISM, false, f);\n");
   SetClass(init_pars, "ModelTEST", "ISM", false, f);
   fprintf(f, "   > PrintSummary(f)\n");
   PrintSummary(f);
   fprintf(f, "   > GetFreePars()->PrintPars(f)\n");
   GetFreePars()->PrintPars(f);
   fprintf(f, "   > coord_txyz.SetValTXYZ(0, 2, 0, 1);\n");
   coord_txyz.SetValTXYZ(0, 2, 0, 1);
   fprintf(f, "   > ValueDensityHI(&coord_txyz);   => %le\n",
           ValueDensityHI(&coord_txyz));
   fprintf(f, "   > ValueDensityHII(&coord_txyz);  => %le\n",
           ValueDensityHII(&coord_txyz));
   fprintf(f, "   > ValueDensityH2(&coord_txyz);   => %le\n",
           ValueDensityH2(&coord_txyz));
   fprintf(f, "   > FillMediumEntry(&coord_txyz);\n");
   FillMediumEntry(&coord_txyz);
   fprintf(f, "   > PrintEntry(f);\n");
   PrintEntry(f);
   fprintf(f, "\n");

   fprintf(f, " * N.B.: every time ValueXXX() or UpdateFromFreeParsAndResetStatus() is called, update all class members.\n");
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > for (Int_t i_elem=0; i_elem<GetNMediumElements(); ++i_elem)\n");
   for (Int_t i_elem = 0; i_elem < GetNMediumElements(); ++i_elem)
      fprintf(f, "        ValueDensity(%d, &coord_txyz);  => %le  [GetNameMediumElement(%d)=%s]\n",
              i_elem, ValueDensity(i_elem, &coord_txyz), i_elem, GetNameMediumElement(i_elem).c_str());
   fprintf(f, "   ---\n");
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > GetFreePars()->SetParVal(/*n0*/0, 0.1)\n");
   GetFreePars()->SetParVal(/*n0*/0, 0.1);
   fprintf(f, "   > GetFreePars()->SetParVal(/*n1*/0, 0.01)\n");
   GetFreePars()->SetParVal(/*n1*/1, 0.01);
   fprintf(f, "   > GetFreePars()->PrintPars(f)\n");
   GetFreePars()->PrintPars(f);
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > for (Int_t i_elem=0; i_elem<GetNMediumElements(); ++i_elem)   [all class updated whenever ValueXXX() called]\n");
   for (Int_t i_elem = 0; i_elem < GetNMediumElements(); ++i_elem)
      fprintf(f, "        ValueDensity(%d, &coord_txyz);  => %le  [GetNameMediumElement(%d)=%s]\n",
              i_elem, ValueDensity(i_elem, &coord_txyz), i_elem, GetNameMediumElement(i_elem).c_str());
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   ---\n");
   fprintf(f, "   > GetFreePars()->SetParVal(/*n0*/0, 1.)\n");
   GetFreePars()->SetParVal(/*n0*/0, 1.);
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > UpdateFromFreeParsAndResetStatus();\n");
   UpdateFromFreeParsAndResetStatus();
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > for (Int_t i_elem=0; i_elem<GetNMediumElements(); ++i_elem)   [all class updated calling UpdateFromFreeParsAndResetStatus()]\n");
   for (Int_t i_elem = 0; i_elem < GetNMediumElements(); ++i_elem)
      fprintf(f, "        ValueDensity(%d, &coord_txyz);  => %le  [GetNameMediumElement(%d)=%s]\n",
              i_elem, ValueDensity(i_elem, &coord_txyz), i_elem, GetNameMediumElement(i_elem).c_str());
   fprintf(f, "\n");


   // Copy
   fprintf(f, " * Test Copy method:\n");
   fprintf(f, "   > FillMediumEntry(&coord_txyz);\n");
   FillMediumEntry(&coord_txyz);
   fprintf(f, "   > PrintEntry(f);\n");
   PrintEntry(f);
   fprintf(f, "\n");
   fprintf(f, "   > TUMediumTXYZ medium; medium.Copy(*this);\n");
   TUMediumTXYZ medium;
   medium.Copy(*this);
   fprintf(f, "   > PrintEntry(f);\n");
   PrintEntry(f);
   fprintf(f, "   > medium.PrintSummary(f);\n");
   medium.PrintSummary(f);
   fprintf(f, "   > medium.ValueDensityHI(&coord_txyz);   => %le\n",
           medium.ValueDensityHI(&coord_txyz));
   fprintf(f, "   > medium.ValueDensityHII(&coord_txyz);  => %le\n",
           medium.ValueDensityHII(&coord_txyz));
   fprintf(f, "   > medium.ValueDensityH2(&coord_txyz);   => %le\n",
           medium.ValueDensityH2(&coord_txyz));
   fprintf(f, "   > medium.FillMediumEntry(&coord_txyz);\n");
   fprintf(f, "   > medium.PrintEntry(f);\n");
   medium.PrintEntry(f);
   fprintf(f, "\n");
}

//______________________________________________________________________________
void TUMediumTXYZ::UpdateFromFreeParsAndResetStatus()
{
   //--- Updates all formulae from updated fFreePars values and reset fFreePars status.

   // If status of parameters has changed
   if (fFreePars && fFreePars->GetParListStatus()) {
      // Update fDensities (for all elements)
      if (fDensities) {
         for (Int_t i = 0; i < GetNMediumElements(); ++i) {
            if (fDensities[i]) fDensities[i]->UpdateFromFreeParsAndResetStatus(true);
         }
      }

      // Update HI, HII and H2
      if (fDensityHI)
         fDensityHI->UpdateFromFreeParsAndResetStatus(true);

      if (fDensityHII)
         fDensityHII->UpdateFromFreeParsAndResetStatus(true);

      if (fDensityH2)
         fDensityH2->UpdateFromFreeParsAndResetStatus(true);

      // Update other properties (plasma...)
      if (fPlasmaT)
         fPlasmaT->UpdateFromFreeParsAndResetStatus(true);

      fFreePars->ResetStatusParsAndParList();
   }
}
