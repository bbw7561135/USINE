// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUBesselJ0.h"
#include "../include/TUMessages.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUBesselJ0                                                           //
//                                                                      //
// Simple class for expansion/resummation on J0 Bessel functions.       //
//                                                                      //
// The class TUBesselJ0 provides simple methods and members             //
// to Bessel expand/resum a function. The data members are:             //
//     - fNBessel: order up to which the expansion/resummation is done;
//     - fZero: zeros[fNMax] of J0 (i.e. for which J0(fZero[i])=0);
//     - fJ1Zero: J1(fZero[fNMax]), J1 evaluated for fZero[fNBessel].
//
// We remind that the Bessel functions allow to solve the triangle      //
// operator: the second order derivative of the Bessel function is      //
// proportional to the Bessel function. For instance, for the 2D        //
// diffusion model with cylindrical symmetry, the zeros of J0, and      //
// J1(zeros) are repeatedly used, so they are stored in this class.     //
//                                                                      //
// BEGIN_HTML
// <b>Useful relationship</b> (Laplacian in cylindrical coordinates)
// END_HTML
//BEGIN_LATEX(fontsize=14, separator='=', align=rcl)
//    J_{0}(#zeta_{i}) = 0   #Rightarrow    #zeta_{i} are said to be zeros of J_{0} (they correspond to the data member fZero)
//    -#nabla^{2}_{r} J_{0}(#zeta_{i}#frac{r}{R}) = #frac{#zeta_{i}}{R^{2}} J_{0}(#zeta_{i}#frac{r}{R})
//    f(r)=#sum_{i#equiv 1}^{#infty} N_{i} #times J_{0} #left( #zeta_{i} #frac{r}{R} #right)
//    N_{i} = #frac{2}{R^{2} J_{1}^{2}(#zeta_{i})} #times #int_{0}^{R} r f(r) J_{0}#left(#zeta_{i}#frac{r}{R}#right) dr
//END_LATEX
//////////////////////////////////////////////////////////////////////////

ClassImp(TUBesselJ0)

//______________________________________________________________________________
TUBesselJ0::TUBesselJ0()
{
   // ****** Default constructor ******

   fNBessel = 0;
   fZero.clear();
   fJ1Zero.clear();
   fRSol = -1.;
   fRmax = -1.;
}
//______________________________________________________________________________
TUBesselJ0::~TUBesselJ0()
{
   // ****** Default destructor ******

   fZero.clear();
   fJ1Zero.clear();
}

//______________________________________________________________________________
Double_t TUBesselJ0::BringmannCoeff(Int_t i_bess, Int_t n_bess)
{
   //--- Returns Torsten Bringman's coefficients (Bringmann, PhD thesis 2015)
   //    to speed up convergence.
   //  i_bess            Bessel coefficient (i_bess=0 is for first bessel coefficient)
   //  n_bess            Number of bessel coefficients used for resummation

   // Only applies for enough Bessel coeffs (M. Boudaud)
   if (n_bess < 20)
      return 1.;

   if (i_bess == (n_bess - 1) - 15) return (624. / 625.);
   else if (i_bess == (n_bess - 1) - 14) return (620. / 625.);
   else if (i_bess == (n_bess - 1) - 13) return (610. / 625.);
   else if (i_bess == (n_bess - 1) - 12) return (590. / 625.);
   else if (i_bess == (n_bess - 1) - 11) return (555. / 625.);
   else if (i_bess == (n_bess - 1) - 10) return (503. / 625.);
   else if (i_bess == (n_bess - 1) - 9)  return (435. / 625.);
   else if (i_bess == (n_bess - 1) - 8)  return (355. / 625.);
   else if (i_bess == (n_bess - 1) - 7)  return (270. / 625.);
   else if (i_bess == (n_bess - 1) - 6)  return (190. / 625.);
   else if (i_bess == (n_bess - 1) - 5)  return (122. / 625.);
   else if (i_bess == (n_bess - 1) - 4)  return (70. / 625.);
   else if (i_bess == (n_bess - 1) - 3)  return (35. / 625.);
   else if (i_bess == (n_bess - 1) - 2)  return (15. / 625.);
   else if (i_bess == (n_bess - 1) - 1)  return (5. / 625.);
   else if (i_bess == (n_bess - 1))    return (1. / 625.);
   else return 1.;
}

//______________________________________________________________________________
Double_t TUBesselJ0::Coeffs2Func(Double_t const &rho, Double_t *ni, Int_t n, Bool_t is_bringmann_weight)
{
   //--- Returns N(rho) reconstructed from its Fourier-Bessel coefficients ni.
   //  rho                 Value at which N(rho=r/R) needs to be evaluated
   //  ni[n]               Vector of Bessel-Fourier coefficients for N(rho)
   //  n                   Number of Bessel-Fourier coefficients used
   //  is_bringmann_weight Bessel coeff weighted for faster resummation (Bringmann, PhD thesis 2005)
   //BEGIN_LATEX(fontsize=14)
   // N#left(#rho=#frac{r}{R}#right) = #sum_{i=1}^{n} N_{i} J_{0} (#zeta_{i} #rho)
   //END_LATEX

   if (fZero.size() == 0) SetZerosAndJ1Zeros(n);
   if (n > fNBessel) {
      string message = Form("Resummation is only done with the first %i coeff.", fNBessel);
      TUMessages::Warning(stdout, "TUBesselJ0", "Coeffs2Func", message);
      n = fNBessel;
   }

   Double_t res = 0.;
   if (fBessRSol.size() != 0 && fabs((rho * fRmax) - fRSol) / fRSol < 1.e-3) {
      if (is_bringmann_weight) {
         for (Int_t i = n - 1; i >= 0; --i)
            res += ni[i] * BringmannCoeff(i, n) * fBessRSol[i];
      } else {
         for (Int_t i = n - 1; i >= 0; --i)
            res += ni[i] * fBessRSol[i];
      }
   } else {
      if (is_bringmann_weight) {
         for (Int_t i = n - 1; i >= 0; --i) {
            //res += (ni[i] * BringmannCoeff(i_bess) * TUMath::JpOptimized(0, (fZero[i] * rho)));
            res += ni[i] * BringmannCoeff(i, n) * TUMath::J0(fZero[i] * rho);
         }
      } else {
         for (Int_t i = n - 1; i >= 0; --i) {
            //res += (ni[i] * TUMath::JpOptimized(0, (fZero[i] * rho)));
            res += ni[i] * TUMath::J0(fZero[i] * rho);
         }
      }
   }
   return res;
}

//______________________________________________________________________________
Double_t TUBesselJ0::Coeffs2Func(Double_t const *coef_j0i_rho, Double_t *ni, Int_t n, Bool_t is_bringmann_weight)
{
   //--- Returns N(rho) reconstructed from its Fourier-Bessel coefficients ni.
   //    Same as above, but with J0(zeta_i*rho) coeffs passed directly to the
   //    function (calculated elsewhere only once to speed-up the calculation).
   //  coef_j0i_rho[n]     Vector of J0(zeta_i*rho) coefficients
   //  ni[n]               Vector of Bessel-Fourier coefficients for N(rho)
   //  n                   Number of Bessel-Fourier coefficients used
   //  is_bringmann_weight Bessel coeff weighted for faster resummation (Bringmann, PhD thesis 2005)

   if (fZero.size() == 0) SetZerosAndJ1Zeros(n);
   if (n > fNBessel) {
      string message = Form("Resummation is only done with the first %i coeff.", fNBessel);
      TUMessages::Warning(stdout, "TUBesselJ0", "Coeffs2Func", message);
      n = fNBessel;
   }

   Double_t res = 0.;
   if (is_bringmann_weight) {
      for (Int_t i = n - 1; i >= 0; --i)
         res += (ni[i] * BringmannCoeff(is_bringmann_weight, n) * coef_j0i_rho[i]);
   } else {
      for (Int_t i = n - 1; i >= 0; --i)
         res += (ni[i] * coef_j0i_rho[i]);
   }
   return res;
}

//______________________________________________________________________________
void TUBesselJ0::Print(FILE *f, Int_t n_terms) const
{
   //--- Prints in f the first n_terms values of fZero[i] and fJ1Zero[i].
   //  f                 File
   //  n_terms           Order up to which print terms

   if (fZero.size() == 0 || fJ1Zero.size() == 0)
      TUMessages::Warning(stdout, "TUBesselJ0", "Print",
                          "Call first \"SetZerosAndJ1_ZeroJ0\"!!!");
   else {
      for (Int_t i = 0; i < min(n_terms, fNBessel); ++i)
         fprintf(f, "i=%d  zero_i(%d)=%e  J1(zero_i)=%e\n", i, i + 1,
                 fZero[i], fJ1Zero[i]);
   }
}

//______________________________________________________________________________
void TUBesselJ0::SetBessRSol(Double_t const &rsol, Double_t const &rmax)
{
   //--- Fills values for J0(zero_i*Rsol/R), which is very often used.
   //  rsol              Distance Galactic centre - Sun [kpc]
   //  rmax              Radial boundary on which F-B expansion is performed

   if (fNBessel == 0)
      TUMessages::Error(stdout, "TUBesselJ0", "SetBessRSol", "SetZerosAndJ1Zeros must be called first");

   fRSol = rsol;
   fRmax = rmax;

   for (Int_t i = 0; i < fNBessel; ++i)
      fBessRSol.push_back(TUMath::J0(fZero[i]*rsol / rmax));
}

//______________________________________________________________________________
void TUBesselJ0::SetZerosAndJ1Zeros(Int_t nmax)
{
   //--- Calculates and stores fZero[nmax] (zeroes of J0, i.e. J0(fZero[i])=0)
   //    and fJ1Zero[nmax] (values of J1(fZero[i]).
   //  nmax              Order up to which perform the calculation

   if (nmax <= 0) {
      string message = "fNBessel=0, nothing to do";
      TUMessages::Warning(stdout, "TUBesselJ0", "SefZeroAndJ1_ZeroJ0", message);
   } else this->SetNBessel(nmax);
   fZero.clear();
   fJ1Zero.clear();

   for (Int_t i = 0; i < fNBessel; ++i) {
      fZero.push_back(TUMath::ZeroJ0(i + 1));
      fJ1Zero.push_back(TUMath::J1_ZeroJ0(i, fZero[i]));
   }
}

//______________________________________________________________________________
void TUBesselJ0::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "Expansion and resummation over Bessel J0 functions.";
   TUMessages::Test(f, "TUBesselJ0", message);

   fprintf(f, "   > SetZerosAndJ1Zeros(20); => store the first 20 zeros of J0 and J1(zeros)\n");
   SetZerosAndJ1Zeros(20);
   fprintf(f, "   > Print(f, 9);               => print the first 9 values\n");
   Print(f, 9);
   fprintf(f, "\n");
}
