// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <numeric>
// ROOT include
// USINE include
#include "../include/TUInteractions.h"
#include "../include/TUMath.h"
#include "../include/TUModelBase.h"
#include "../include/TUNumMethods.h"
#include "../include/TUSolMod0DFF.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUModelBase                                                          //
//                                                                      //
// Base ingredients for all propagation models (CR data, X-sections...).//
//                                                                      //
// The class TUModelBase is the centrepiece of all propagation models.  //
// It contains model-independent ingredients (inherited) and class      //
// members ingredients. The first category corresponds to quantities    //
// that are somehow independent of the propagation model selected       //
// (such as CR data, X-sections,...). The second category encompasses   //
// quantities that define the propagation model (geometry, transport,   //
// sources...). We detail below (I. and II.) the content of these two   //
// categories. We present separately (III.) the classes TUFreeParList   //
// (inherited) and TUSolModVirtual (class member), because of their     //
// specific role in this class (and for propagation models in general). //
// To conclude, we provide a brief 'How to' (IV.) on how to write a     //
// class for a propagation model deriving from this class.              //
//                                                                      //
// All calculated quantities are IS fluxes!                             //
//                                                                      //
// I. Model-independent ingredients (inherited)                         //
//                                                                      //
//   - TUAxesCrE: list of propagation CRs (+parents) and energies;
//   - TUPropagSwitches: enable/disable propagation options;
//   - TUDataSet: CR data available for propagation analysis;
//   - TUNormList[optional]: reference CR data to normalise calculated model fluxes;
//   - TUSrcTemplates: templates for source description (formula, files...);
//   - TUXSections: all necessary X-sec calculated on TUAxesCrE axes;
//   - TUFreeParList: Model enabled free parameters.
//                                                                      //
//                                                                      //
// II. Model-dependent ingredients (class members)                      //
//                                                                      //
//  This category can be further cast in two types, related to the      //
//  role they play:                                                     //
//                                                                      //
//   1. Geometry and TXYZE coordinates
//      - TUAxesTXYZ *fAxesTXYZ: geometry of the propagation model;
//      - TUCoordE and TUCoordTXYZ: E and TXYZ coordinate;
//                                                                      //
//   2. Quantities depending on TXYZE (and CR) coordinates
//      - TUMediumTXYZ => ISM for the model (depends on TXYZ);
//      - TUTransport  => Diffusion, convection, etc (depends on TXYZECr);
//      - TUSrcMultiCRs[NDM+NAstro] => list of DM and astro sources.
//                                                                      //
//   Note that a dark matter (DM) or astrophysical (ASTRO) source is    //
//   steady-state or point-like (TUSrcPointLike or TUSrcSteadyState),   //
//   and applies to a list of species (TUSrcMultiSpecies). USINE        //
//   enables any source (whose spatial distribution and spectra are     //
//   taken among TUSrcTemplates).                                       //
//                                                                      //
//                                                                      //
// III. Initialisation, free parameters, solar modulation               //
//                                                                      //
// For any propagation model calculation, we have to go through:        //
//                                                                      //
//   1. Initialisation stage
//   All the above classes are allocated and filled thanks to a USINE
//   initialisation file. This file (specific USINE format) is read by
//   a TUInitParList object ('passive' reading of the file, in order to
//   get the initialisation parameter names, values, help on parameter,
//   ...). In each of the above class, the method SetClass(TUInitParList)
//   sort the content of TUInitParList to fill the model ingredients.
//   Note that the inherited class of this class (model-independent
//   ingredients, see I) are filled regardless of the propagation model
//   chosen, whereas model-dependent ingredients (also filled here, see II)
//   depend on the model name (or gENUM_PROPAG_MODEL). In other words, all
//   class members are filled here, though model-dependent ones depend on
//   a keyword/model which is used to extract the desired initialisation
//   parameters in fInitParList. For that matter, the present number of
//   initialisation parameters fully define any propagation model: look,
//   e.g., for GROUP name 0DLeakyBox in BEGIN_HTML <a href="../inputs/init.TEST.par">$USINE/inputs/init.TEST.par</a>.END_HTML
//                                                                      //
//   2. Free parameters
//   The quantity TUFreeParList is very important as it is the entry
//   point to access the model free parameters, which are required
//   whenever a minimisation analysis is performed. Indeed, TUFreeParList
//   gathers (points to) all free parameters appearing in the class
//   members (of this class) in this single parameter. Then, each time
//   the Calculate() class method is called, the code checks:
//      A. Whether some of the free parameters status is at 'updated';
//      B. If (A) true, update quantities depending on the updated parameters;
//      C. Do propagation anew and reset free parameter status.
//                                                                      //
//   3. Solar modulation
//   Solar modulation is a separate step from propagation. It is mandatory
//   to compare interstellar (IS) fluxes calculated from the model to
//   top-of-atmosphere (TOA) data. Once the user selected a modulation
//   model in the initialisation file (among those available in the USINE
//   package), it is used whenever a TOA flux is calculated. Note that once
//   the propagation stage is done, it is always possible to change the Solar
//   modulation model, in order to compare two TOA fluxes calculated from
//   the same IS flux but from different modulation models.
//                                                                      //
//                                                                      //
// IV. How to build a new model deriving from this class?               //
//                                                                      //
// The USINE package was developed having in mind the possibility to    //
// easily implement new propagation model solutions (new class). We     //
// give below the main steps required to do that (as was done for       //
// instance to implement the Leaky-Box model in TUModel0D_LB), the      //
// 1D cylindrical model in TUModel1DKisoVc, etc.):                     //
//                                                                      //
//   1. Choose a new ID + name, and proceeds as follows in TUEnum.h:
//        - Increment number of models (gN_PROPAG_MODEL) by 1;
//        - Add the model ID in list (from gENUM_PROPAG_MODEL);
//                                                                      //
//   2. Define your model geometry/ingredients/parameters in the
//      initialisation file (follow the USINE format and help yourself
//      from existing models, e.g., from TUModel0D_LB).
//      N.B.: as long as you keep the USINE syntax (and provide as many
//      subgroup names as an existing class, you do not have to worry about
//      anything else). You can however add as many source type/names
//      as you wish: again, if it meets the USINE syntax (as exemplified
//      for sources for other models), everything will go smoothly and
//      automatically (no need to rewrite more pieces of code!).
//       => Ensure that the 'group' name you choose (in the initialisation
//          file) matches the model name in gENUM_PROPAG_MODEL_NAMES (see 1).
//                                                                      //
//   3. Create a new class, the only requirements to make it work with
//      TUModelBase class (this class) being:
//         - it must inherits TUModelBase;
//         - it requires the empty constructor (and destructor);
//         - Some TUModelBase virtual methods must exist in the new class:
//             *) EvalFluxPrim(): CR flux@(Ekn,txyz) => primary contrib. only
//             *) EvalFluxTot(): CR flux@(Ekn,txyz) => all contributions
//             *) InitialiseForCalculation(): initialise class members of new class (or only update)
//         - Some TUModelBase virtual methods are optional in the new class:
//              *) CalculateChargedCRs() => model solution for nuclei
//            N.B.: if not filled for a species, not propagated (e.g., some
//            models are not meant be valid for all species).
//                                                                      //
// N.B.: although full 3D+1 and formula-based or grid value description //
// are enabled, not all methods and checks are validated yet. So if you //
// use specific cases not yet enabled, just be careful!                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUModelBase)

//______________________________________________________________________________
TUModelBase::TUModelBase() : TUAxesCrE(), TUPropagSwitches(), TUDataSet(), TUNormList(),
   TUSrcTemplates(), TUXSections(), TUFreeParList()
{
   // ****** Default constructor ******
   Initialise(false);
}

//______________________________________________________________________________
TUModelBase::~TUModelBase()
{
   // ****** Default destructor ******
   Initialise(true);
}

//______________________________________________________________________________
Bool_t TUModelBase::CalculateChargedCRs(Int_t jcr_start, Int_t jcr_stop, Bool_t is_verbose, FILE *f_log)
{
   //--- Calculates nuclei and antinuclei for model.
   //  jcr_start         Index of CR for first to calculate
   //  jcr_stop          Index of CR for last to calculate
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   if (fModel == kBASE)
      return false;
   else {
      string message = "You must override this virtual method in class for " + GetModelName();
      TUMessages::Error(f_log, "TUModelBase", "CalculateChargedCRs", message);
      return is_verbose;
   }
   return false;
}

//______________________________________________________________________________
void TUModelBase::CalculateChargedCRs_NormalisedToData(Int_t jcr_start, Int_t jcr_stop, Bool_t is_verbose, FILE *f_log)
{
   //--- Calculates flux for (anti)nuclei and iterates calculation in order to
   //    match the user-selected measured fluxes at a given energy (listed in
   //    TUNormList). In practice, the primary source abundance (normalisation)
   //    is changed so that the relative difference between the model and the
   //    data point is smaller than the user 'eps'.
   //    N.B.: we use the Force-Field approximation to modulate IS fluxes to
   //    compare to data, because it is simple, fast, and good enough as long
   //    as the data points are not taken at too low energy (to be as Solar
   //    modulation/model insensitive as possible).
   //  jcr_start         Index of CR for first to calculate
   //  jcr_stop          Index of CR for last to calculate
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   // Not possible if more than one astro source
   if (GetNSrcAstro() != 1) {
      string message = "Source spectrum re-normalisation is possible only if "
                       "there is one and only one astro. source loaded";
      TUMessages::Error(f_log, "TUModelBase", "Calculate_NucAntinuc_NormalisedToData", message);
   }

   string indent = TUMessages::Indent(true);

   // If no data to normalise to, only propagate
   if (TUNormList::GetNNorm() == 0) {
      if (is_verbose)
         fprintf(f_log, "%s CalculateChargedCRs_NormalisedToData: No data to normalise to (only propagate)\n", indent.c_str());
      CalculateChargedCRs(jcr_start, jcr_stop, is_verbose, f_log);
      TUMessages::Indent(false);
      return;
   }

   if (is_verbose)
      fprintf(f_log, "%s [START NORMALISATION TO CR DATA]\n", indent.c_str());

   Double_t error = 0.;
   Int_t attempt = 0;
   Int_t attempt_max = 20;
   do {
      ++attempt;
      // Calculate IS fluxes from the current abundance sources
      CalculateChargedCRs(jcr_start, jcr_stop, is_verbose, f_log);
      error = NormaliseSrcAbundToCRDataUsingNormList(attempt, is_verbose, f_log);

      if (fabs(error - 15.) < 1.e-3) {
         // Means that all the fluxes were negative (could not renormalised).
         // May be an indication that the propagation parameters lead to
         // numerical instabilities in the calculation. We output the
         // propagation parameters in that case
         if (is_verbose)
            PrintPars(f_log);
      } else if (is_verbose) {
         if (error >= 0. && attempt > 0) {
            if (attempt == 1)
               fprintf(f_log, "%s    [norm] for 1st attempt: worst difference with data in normalization Max(Delta q)/q= %le (sought eps=%.2le)\n", indent.c_str(), error, fEpsNormData);
            else if (attempt == 2)
               fprintf(f_log, "%s    [norm] for 2nd attempt: worst difference with data in normalization Max(Delta q)/q= %le (sought eps=%.2le)\n", indent.c_str(), error, fEpsNormData);
            else if (attempt == 3)
               fprintf(f_log, "%s    [norm] for 3rd attempt: worst difference with data in normalization Max(Delta q)/q= %le (sought eps=%.2le)\n", indent.c_str(), error, fEpsNormData);
            else
               fprintf(f_log, "%s    [norm] for %dth attempt: worst difference with data in normalization Max(Delta q)/q= %le (sought eps=%.2le)\n", indent.c_str(), attempt, error, fEpsNormData);
         }
      }
   } while ((error > fEpsNormData) && (attempt < attempt_max));

   if (is_verbose) {
      if (attempt >= attempt_max) {
         string message = Form(" NORMALISATION TO CR DATA: convergence not reached after %d attempts!", attempt_max);
         TUMessages::Warning(f_log, "TUModelBase", "Calculate_NucAntinuc_NormalisedToData", message);
      } else
         fprintf(f_log, "%s [NORMALISATION DONE]\n", indent.c_str());
   }

   TUMessages::Indent(false);
}

//______________________________________________________________________________
Double_t TUModelBase::CalculateScaleFactor(Int_t i_norm, Bool_t is_first_attempt, Bool_t is_verbose, FILE *f_log) const
{
   //--- Calculate scale factor to apply to primary source term normalisation in
   //    order for model to match the user-selected list of normalisation data
   //    in TUNormList.
   //    N.B.: we use the Force-Field approximation to modulate IS fluxes to
   //    compare to data, because it is simple, fast, and good enough as long as
   //    the data points are not taken at too low energy (they are then "Solar
   //    modulation model" insensitive as possible).
   //
   //  i_norm            Index of species to normalise in inherited class TUNormList
   //  is_first_attempt  Whether calculate scale factor for first iteration or not
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   // Quantity to normalise is either an element or a CR
   TUDataEntry norm_data = TUNormList::GetNormData(i_norm);
   string qty = TUNormList::GetNormName(i_norm);
   gENUM_ETYPE e_type = norm_data.GetEType();
   TUAxis *e_grid =  TUAxesCrE::OrphanGetECombo(qty, e_type, f_log);

   //data.Print(stdout, false);

   // Create Force-Field Solar modulation model
   TUSolMod0DFF ff;
   ff.SetRig0(0.2);
   ff.Setphi(norm_data.GetExpphi());

   // Use primary and total flux (calculated from model) to calculate the scale
   // factor, i.e., the quantity to apply to the source spectrum normalisation
   // to match the model (from user-selected CR data a.k.a. quantity from a
   // given experiment at a given energy).

   // Get closest index, but cannot be last bin (otherwise interpolation crashes)
   Int_t k_data = e_grid->IndexClosest(norm_data.GetEMean());
   if (k_data < 0)
      k_data = 0;
   else if (k_data > e_grid->GetN() - 2)
      k_data = e_grid->GetN() - 2;
   TUCoordTXYZ *dummy = NULL;
   //fSolarSystemFluxesPrim->PrintVals(stdout, qty, e_grid, e_type, 0., dummy, &ff);

   Double_t *flux_toa_prim = fSolarSystemFluxesPrim->OrphanVals(qty, e_grid, e_type, 0., dummy, &ff);
   Double_t f_prim = TUMath::Interpolate(norm_data.GetEMean(), e_grid->GetVal(k_data),
                                         e_grid->GetVal(k_data + 1), flux_toa_prim[k_data],
                                         flux_toa_prim[k_data + 1], kLOGLOG);
   delete[] flux_toa_prim;
   flux_toa_prim = NULL;
   Double_t *flux_toa_tot = fSolarSystemFluxesTot->OrphanVals(qty, e_grid, e_type, 0., dummy, &ff);
   Double_t f_tot = TUMath::Interpolate(norm_data.GetEMean(), e_grid->GetVal(k_data),
                                        e_grid->GetVal(k_data + 1), flux_toa_tot[k_data],
                                        flux_toa_tot[k_data + 1], kLOGLOG);
   delete[] flux_toa_tot;
   flux_toa_tot = NULL;
   delete e_grid;
   e_grid = NULL;

   // Calculate scale factor (slightly different factor
   // whether this is first or second iter)
   Double_t scale_factor = 1.;
   Double_t residue = norm_data.GetY() - (f_tot - f_prim);

   if (!is_first_attempt) {
      // If fluxes are nan, do not rescale (otherwise minimisation
      // routines can stay stuck with Nan abundances)
      if (std::isnan(f_prim) || std::isnan(f_tot))
         return -1.;

      // If secondary contrib > measured value (overproduction) or f_prim<=0,
      // nothing can be done to adjust this CR flux. We force/set primary
      // contribution so that the primary contrib do not lead to more than
      // 1.e-3 that of the secondary contrib. (i.e., we set f_prim/f_sec=1.e-1).
      if (residue <= 0. || f_prim <= 0.) {
         if (is_verbose)
            fprintf(f_log, "%s      *** %5s -> too much secondary production (~%.3le), "
                    "cannot adjust data [change transport parameters]!\n",
                    indent.c_str(), qty.c_str(), (f_tot - f_prim) / norm_data.GetY());
         Double_t force = 1.e-2;
         Double_t tmp = f_prim / (f_tot - f_prim);
         if (tmp > force)
            scale_factor = force * (f_tot - f_prim) / f_prim;
         else
            return -1.;
      } else
         // Normal situation
         scale_factor = residue / f_prim;
   } else {
      // If primary flux <=0: nothing to do (detect that at least one flux
      // is non-negative, so that it will be able to provide a scale-factor).
      // If fluxes are nan, do not rescale (otherwise minimisation can stay
      // stuck with Nan abundances)
      if (f_prim <= 0. || std::isnan(f_prim) || std::isnan(f_tot))
         return -1.;

      // For primary species, the ratio measured/calculated gives directly
      // the scale factor to adjust abundances. However, for mixed species,
      // the secondary contribution may be dominant and the scale factor is
      // given by (measured-secondary_contrib)/primary_contrib
      scale_factor = residue / f_prim;

      // If secondary contrib. is larger than measurement (and f_prim is
      // non-null!): to avoid numerical convergence to 0 (which occurs when
      // rescaling down repeatedly the corresponding primary flux of this
      // nucleus, as done, e.g. in a minimisation run where source spectrum
      // normalisation is not a free parameter), we simply scale the primary
      // contribution to the quantity required to match the data.
      if (scale_factor <= 0. && f_prim > 0.)
         scale_factor = norm_data.GetY() / f_prim;
   }

   return scale_factor;
}

//______________________________________________________________________________
Double_t TUModelBase::EvalFlux(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz,
                               Int_t tot0_prim1_sec2)
{
   //--- Returns flux [{m2 sr s (GeV/n)}^-1] for CR j_cr (for total, or primary
   //    or secondary contrib only.). It is calculated at position coord_txyz.
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  tot0_prim1_sec2   Switch to get total(0), primary(1) or secondary(2) contrib.

   // Switch whether this is total, primary, or secondary flux:
   // for each choice, this is local flux or flux @ (t,x,y,z)
   if (tot0_prim1_sec2 == 0)
      return EvalFluxTot(j_cr, k_ekn, coord_txyz);
   else if (tot0_prim1_sec2 == 1)
      return EvalFluxPrim(j_cr, k_ekn, coord_txyz);
   else if (tot0_prim1_sec2 == 2)
      return EvalFlux(j_cr, k_ekn, coord_txyz, 0) - EvalFlux(j_cr, k_ekn, coord_txyz, 1);
   else
      TUMessages::Error(stdout, "TUModelBase", "EvalFlux",
                        "Allowed values for tot0_prim1_sec2 are 0, 1, and 2");

   return 0.;
}

//______________________________________________________________________________
Double_t TUModelBase::EvalFluxCombo(string const &combo, Int_t k_ekn, TUCoordTXYZ *coord_txyz,
                                    Int_t tot0_prim1_sec2, Bool_t is_verbose)
{
   //--- Returns flux [{m2 sr s (GeV/n)}^-1] for CR combo (for total, or primary
   //    or secondary contrib only.). It is calculated at position coord_txyz.
   //  combo             Combination of CRs (e.g. B/C, or 10Be+9Be/Be...)
   //  k_ekn             Energy index
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  tot0_prim1_sec2   Switch to get total(0), primary(1) or secondary(2) contrib.
   //  is_verbose        Print info or not about content of the combo

   // Extract indices in combo
   vector<Int_t> cr_indices;
   Int_t n_denom = 0;
   TUAxesCrE::ComboToCRIndices(combo, cr_indices, n_denom, is_verbose);

   // Calculate flux (=num/denom) - loop on all indices
   Double_t flux = 0.;
   Double_t denom = 0.;

   // Loop over all nuclei in combo
   Int_t n_crs = cr_indices.size();
   for (Int_t nn = 0 ; nn < n_crs; ++nn) {
      // Get flux for this CR
      Double_t dnde = EvalFlux(cr_indices[nn], k_ekn, coord_txyz, tot0_prim1_sec2);
      // Sort num and denom contributions
      if (nn < (n_crs - n_denom))
         flux += dnde;
      else
         denom += dnde;
   }

   // Form flux=num/denom
   if (n_denom > 0)
      flux /= denom;

   return flux;
}

//______________________________________________________________________________
Double_t TUModelBase::EvalFluxPrim(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz)
{
   //--- Calculate primary flux for model.

   if (fModel == kBASE)
      return 0.;
   else {
      string message = "You must override this virtual method in class for " + GetModelName();
      TUMessages::Error(stdout, "TUModelBase", "EvalFluxPrim", message);
   }
   return -1.;
}

//______________________________________________________________________________
Double_t TUModelBase::EvalFluxTot(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz)
{
   //--- Calculate total flux for model.

   if (fModel == kBASE)
      return 0.;
   else {
      string message = "You must override this virtual method in class for " + GetModelName();
      TUMessages::Error(stdout, "TUModelBase", "EvalFluxTot", message);
   }
   return -1.;
}

//______________________________________________________________________________
Double_t TUModelBase::EvalFraction(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz,
                                   Int_t isotopic0_prim1_sec2)
{
   //--- Returns fraction [%] for the flux of CR j_cr (isotopic fraction, primary
   //    or secondary content). The following definitions are used:
   //       * Isotopic fraction = flux(CR)/flux(corresponding element)
   //       * Primary content = Flux(prim. contrib.)/flux(all. contribs.)
   //       * Secondary content = 100. - Primary content
   //  j_cr                 CR index
   //  k_ekn                Energy index
   //  coord_txyz          Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  isotopic0_prim1_sec2 Switch to calculate isotopic(0), primary(1) or secondary(2) fraction

   if (isotopic0_prim1_sec2 == 0) {
      Double_t num = EvalFlux(j_cr, k_ekn, coord_txyz, 0);
      Double_t denom = EvalFluxCombo(CRToElementName(j_cr), k_ekn, coord_txyz, 0, false);
      if (fabs(denom) > 1.e-40 && fabs(num) > 1.e-40)
         return num * 100. / denom;
      else
         return 0.;
   } else if (isotopic0_prim1_sec2 == 1) {
      Double_t num = EvalFlux(j_cr, k_ekn, coord_txyz, 1);
      Double_t denom = EvalFlux(j_cr, k_ekn, coord_txyz, 0);
      if (fabs(denom) > 1.e-40 && fabs(num) > 1.e-40)
         return num * 100. / denom;
      else
         return 0.;
   } else if (isotopic0_prim1_sec2 == 2)
      return 100. - EvalFraction(j_cr, k_ekn, coord_txyz, 1);
   else
      TUMessages::Error(stdout, "TUModelBase", "EvalFraction",
                        "Allowed values for isotopic0_prim1_sec2 are 0, 1, and 2");

   return 0.;
}

//______________________________________________________________________________
Double_t TUModelBase::EvalFractionElement(Int_t z_element, Int_t k_ekn, TUCoordTXYZ *coord_txyz,
      Int_t prim1_sec2)
{
   //--- Returns fraction [%] for the elemental flux of z_element (primary or
   //    secondary content). The following definitions are used:
   //       * Primary content = Flux(prim. contrib.)/flux(all. contribs.)
   //       * Secondary content = 100. - Primary content
   //  z_element         Charge of the CR element
   //  k_ekn             Energy index
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  prim1_sec2        Switch to calculate primary(1) or secondary(2) content

   if (prim1_sec2 == 1) {
      Double_t num = EvalFluxCombo(ZToElementName(z_element), k_ekn, coord_txyz, 1, false);
      Double_t denom = EvalFluxCombo(ZToElementName(z_element), k_ekn, coord_txyz, 0, false);
      if (fabs(denom) > 1.e-40 && fabs(num) > 1.e-40)
         return num * 100. / denom;
      else return 0.;
   } else if (prim1_sec2 == 2)
      return 100. - EvalFractionElement(z_element, k_ekn, coord_txyz, 1);
   else
      TUMessages::Error(stdout, "TUModelBase", "EvalFractionElement",
                        "Allowed values for prim1_sec2 are 1, and 2");

   return 0.;
}

//______________________________________________________________________________
void TUModelBase::FillEFirstSecondOrderTerms(Int_t j_cr, TUMediumEntry *ism_entry, vector<Double_t> &b_startm1, vector<Double_t> &c_startmhalf, TUCoordTXYZ *coord_txyz)
{
   //--- Fills generic terms of second order diffusion equations in energy (with log step):
   //        n + A_ln * d/dLnEkn() B_ln * n - C_ln * dn/dLnEkn) = Src_ln
   //    with
   //       b_ln = <dE/dt>_{ion, coul.} + <dE/dt>_adiab + (1+beta^2)/E * K_pp  [GeV/Myr]
   //       c_ln = beta^2 K_pp / Ek            [GeV2/Myr]
   //    Note that in practice, to implement some boundary conditions, we use extended arrays
   //
   // INPUTS:
   //  j_cr              CR index
   //  ism_entry         ISM content for which to calculate (should correspond to ISM at coord_txyz)
   //  coord_txyz        Corrdinate at which to calculate
   // OUTPUTS:
   //  b_startm1         b_ln[NE+2] goes from {-1,0,...,NE-1,NE}, i.e. b_startm1[0] is for -1
   //  c_startmhalf      c_ln[NE+1] are evaluated at half-bin, from {-1/2,1/2,...NE-1/2}, i.e. c_startmhalf[0] is for -1/2

   // Reste to zero
   b_startm1.assign(GetNE() + 2, 0.);
   c_startmhalf.assign(GetNE() + 1, 0.);

   // Loop on Ekn
   for (Int_t k_e = GetNE(); k_e >= -1; --k_e) {
      Double_t first_order = 0., second_order = 0.;
      Double_t ekn = 0.;
      Bool_t is_in_grid = true;
      // Whether values to calculate are on E grid or not
      if (k_e == -1 || k_e == GetNE())
         is_in_grid = false;

      // Standard calculation if in grid, specific calculation otherwise
      if (is_in_grid)
         ekn = TUAxesCrE::GetEkn(j_cr, k_e);
      else
         // Calculate for k_e=-1 and k_e=NE
         ekn = GetE(j_cr)->EvalOutOfRange(k_e);


      // Ion and Coulomb losses
      if (IsELossCoulomb() && IsELossIon()) {
         if (is_in_grid)
            first_order += TUInteractions::DEdtIonCoulomb(GetCREntry(j_cr), ism_entry, this, GetBeta(j_cr, k_e), GetGamma(j_cr, k_e));
         else {
            Double_t beta = TUPhysics::Beta_mAEkn(TUAxesCrE::GetCREntry(j_cr).GetmGeV(), TUAxesCrE::GetCREntry(j_cr).GetA(), ekn);
            Double_t gamma = TUPhysics::Gamma_mAEkn(TUAxesCrE::GetCREntry(j_cr).GetmGeV(), TUAxesCrE::GetCREntry(j_cr).GetA(), ekn);
            first_order += TUInteractions::DEdtIonCoulomb(GetCREntry(j_cr), ism_entry, this, beta, gamma);
         }
      } else if (IsELossCoulomb())
         first_order += TUInteractions::DEdtCoulomb(GetCREntry(j_cr), ism_entry, ekn);
      else if (IsELossIon())
         first_order += TUInteractions::DEdtIonisation(GetCREntry(j_cr), ism_entry, this, ekn);


      // Adiabatic losses (in fCoef1stOrder only)
      if (IsELossAdiabatic() && GetModel() != kMODEL0DLEAKYBOX) {
         if (is_in_grid)
            first_order += AdiabaticLosses(j_cr, GetEk(j_cr, k_e), coord_txyz);
         else {
            Double_t ek = ekn * (Double_t)TUAxesCrE::GetCREntry(j_cr).GetA();
            first_order += AdiabaticLosses(j_cr, ek, coord_txyz);
         }
      }


      // Reacceleration (first and second order)
      if (IsEReacceleration()) {
         // B term is evaluated @ bin
         first_order += CoeffReac1stOrder(j_cr, k_e);
         // C term is evaluated @ lower half-bin from -1/2 to (NE-1)-1/2, and (NE-1)+1/2
         if (k_e >= 0)
            second_order += CoeffReac2ndOrderLowerHalfBin(j_cr, k_e);
      }

      // Copy in vector:
      //   b_startm1[0...NE+1] calculated for k_e={-1,...,NE}
      //   c_startmhalf[0,NE] calculated for k_e={-1/2,...NE-1/2}
      b_startm1[k_e + 1] = first_order;
      if (k_e >= 0)
         c_startmhalf[k_e] = second_order;
   }
}

//______________________________________________________________________________
void TUModelBase::FillModelFreePars(Bool_t is_verbose, FILE *f_log)
{
   //--- Updates (collect) all model free parameter in inherited class TUFreeParList.
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUModelBase::FillModelFreePars]\n", indent.c_str());

   // Set all Free parameters of the model
   // N.B.: check that allocated before adding them
   if (is_verbose)
      fprintf(f_log, "%s### Gather in one place all parameters\n", indent.c_str());

   if (fAxesTXYZ) {
      if (is_verbose)
         fprintf(f_log, "%s   - Geometry parameters\n", indent.c_str());
      TUFreeParList::UseParEntries(fAxesTXYZ->GetFreePars());
   }

   if (fISM) {
      if (is_verbose)
         fprintf(f_log, "%s   - ISM parameters\n", indent.c_str());
      TUFreeParList::UseParEntries(fISM->GetFreePars());
   }

   if (fTransport) {
      if (is_verbose)
         fprintf(f_log, "%s   - Transport parameters\n", indent.c_str());
      TUFreeParList::UseParEntries(fTransport->GetFreePars());
   }

   if (GetNSrcAstro() > 0) {
      if (is_verbose)
         fprintf(f_log, "%s   - Astrophysical (ASTRO) sources parameter\n", indent.c_str());
      for (Int_t i = 0; i < GetNSrcAstro(); ++i) {
         TUFreeParList::UseParEntries(fSrcAstro[i]->GetFreePars());
      }
   }

   if (GetNSrcDM() > 0) {
      if (is_verbose)
         fprintf(f_log, "%s   - Dark matter (DM) sources  parameters\n", indent.c_str());
      for (Int_t i = 0; i < GetNSrcDM(); ++i) {
         TUFreeParList::UseParEntries(fSrcDM[i]->GetFreePars());
      }
   }

   if (is_verbose)
      PrintPars(f_log);

   if (is_verbose)
      fprintf(f_log, "%s[TUModelBase::FillModelFreePars] <DONE>\n\n", indent.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUModelBase::FillFlux(vector<Double_t> &flux, Int_t j_cr, TUCoordTXYZ *coord_txyz, Int_t tot0_prim1_sec2)
{
   //--- Returns flux (total, from primary or secondary contrib. only) for all E
   //    for CR j_cr [{m2 sr s (GeV/n)}^-1]. It is calculated at position coord_txyz.
   // INPUTS:
   //  j_cr              CR index
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  tot0_prim1_sec2   Switch to get total(0), primary(1) or secondary(2) contrib.
   // OUTPUTS:
   //  flux             [NE] Vector of results

   flux.clear();
   flux.resize(GetNE());
   for (Int_t k = GetNE() - 1; k >= 0; --k)
      flux[k] = EvalFlux(j_cr, k, coord_txyz, tot0_prim1_sec2);
}

//______________________________________________________________________________
void TUModelBase::FillFluxCombo(vector<Double_t> &flux, string const &combo, TUCoordTXYZ *coord_txyz,
                                Int_t tot0_prim1_sec2, Bool_t is_verbose)
{
   //--- Returns flux (total, from primary or secondary contrib. only) for all E
   //    for CR combo [{m2 sr s (GeV/n)}^-1]. It is calculated at position coord_txyz.
   // INPUTS:
   //  combo             Combination of CRs (e.g. B/C, or 10Be+9Be/Be...)
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  tot0_prim1_sec2   Switch to get total(0), primary(1) or secondary(2) contrib.
   //  is_verbose        Print info or not about content of the combo
   // OUTPUTS:
   //  flux             [NE] Vector of results

   flux.clear();
   flux.resize(GetNE());
   for (Int_t k = GetNE() - 1; k >= 0; --k)  {
      if (k == 0)
         flux[k] = EvalFluxCombo(combo, k, coord_txyz, tot0_prim1_sec2, is_verbose);
      else
         flux[k] = EvalFluxCombo(combo, k, coord_txyz, tot0_prim1_sec2, false);
   }
}

//______________________________________________________________________________
void TUModelBase::FillFraction(vector<Double_t> &fraction, Int_t j_cr, TUCoordTXYZ *coord_txyz,
                               Int_t isotopic0_prim1_sec2)
{
   //--- Returns isotopic, primary or secondary fraction [%] in CR flux j_cr
   //    for all energies. It is calculated at position coord_txyz.
   // INPUTS:
   //  j_cr                  CR index
   //  coord_txyz            Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  isotopic0_prim1_sec2  Switch to get isotopic(0), primary(1) or secondary(2) fraction
   // OUTPUTS:
   //  fraction              [NE] Vector of results

   fraction.clear();
   fraction.resize(GetNE());
   for (Int_t k = GetNE() - 1; k >= 0; --k)
      fraction[k] = EvalFraction(j_cr, k, coord_txyz, isotopic0_prim1_sec2);
}

//______________________________________________________________________________
void TUModelBase::FillFractionElement(vector<Double_t> &fraction, Int_t z_element, TUCoordTXYZ *coord_txyz,
                                      Int_t prim1_sec2)
{
   //--- Returns primary or secondary fraction [%] in flux for element Z
   //    (for all energies). It is calculated at position coord_txyz.
   // INPUTS:
   //  z_element         Charge of the CR element
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  prim1_sec2        Switch to get isotopic(0), primary(1) or secondary(2) fraction
   // OUTPUTS:
   //  fraction              [NE] Vector of results

   fraction.clear();
   fraction.resize(GetNE());
   for (Int_t k = GetNE() - 1; k >= 0; --k)
      fraction[k] = EvalFractionElement(z_element, k, coord_txyz, prim1_sec2);
}

//______________________________________________________________________________
void TUModelBase::FillModelParameters(TURunOutputs &outputs) const
{
   //--- Retrieve model parameters in outputs (to be later plotted).
   //  outputs               Storage for retrieved model parameters

   vector<Int_t> i_start;
   vector<string> titles;
   vector<string> names;
   vector<Double_t> values;
   vector<string> units;

   // 4 families of parameters: ISM, Transport, AstroSrc, DMSrc
   Int_t n_pars = 0;
   if (fISM && fISM->GetFreePars()) {
      titles.push_back("ISM");
      i_start.push_back(n_pars);
      for (Int_t p = 0; p < fISM->GetFreePars()->GetNPars(); ++p) {
         TUFreeParEntry *par = fISM->GetFreePars()->GetParEntry(p);
         names.push_back(par->GetName());
         values.push_back(par->GetVal(false));
         units.push_back(par->GetUnit(true));
         par = NULL;
      }
      n_pars += fISM->GetFreePars()->GetNPars();
   }

   if (fTransport && fTransport->GetFreePars()) {
      titles.push_back("Transport");
      i_start.push_back(n_pars);
      for (Int_t p = 0; p < fTransport->GetFreePars()->GetNPars(); ++p) {
         TUFreeParEntry *par = fTransport->GetFreePars()->GetParEntry(p);
         names.push_back(par->GetName());
         values.push_back(par->GetVal(false));
         units.push_back(par->GetUnit(true));
         par = NULL;
      }
      n_pars += fTransport->GetFreePars()->GetNPars();
   }

   for (Int_t i = 0; i < fNSrcAstro; ++i) {
      if (fSrcAstro[i]->GetFreePars()) {
         titles.push_back("Source (astro): " + fSrcAstro[i]->GetSrcName());
         i_start.push_back(n_pars);
         for (Int_t p = 0; p < fSrcAstro[i]->GetFreePars()->GetNPars(); ++p) {
            TUFreeParEntry *par = fSrcAstro[i]->GetFreePars()->GetParEntry(p);
            names.push_back(par->GetName());
            values.push_back(par->GetVal(false));
            units.push_back(par->GetUnit(true));
            par = NULL;
         }
         n_pars += fSrcAstro[i]->GetFreePars()->GetNPars();
      }
   }

   for (Int_t i = 0; i < fNSrcDM; ++i) {
      if (fSrcDM[i]->GetFreePars()) {
         titles.push_back("Source (astro): " + fSrcDM[i]->GetSrcName());
         i_start.push_back(n_pars);
         for (Int_t p = 0; p < fSrcDM[i]->GetFreePars()->GetNPars(); ++p) {
            TUFreeParEntry *par = fSrcDM[i]->GetFreePars()->GetParEntry(p);
            names.push_back(par->GetName());
            values.push_back(par->GetVal(false));
            units.push_back(par->GetUnit(true));
            par = NULL;
         }
         n_pars += fSrcDM[i]->GetFreePars()->GetNPars();
      }
   }
   // Copy in outputs
   outputs.SetPropModPars(titles, i_start, names, values, units);
}

//______________________________________________________________________________
void TUModelBase::FillSolarSystemFluxes()
{
   //--- Allocates and fills local IS fluxes (Solar system position).

   // Free if pre-existing
   if (fSolarSystemFluxesPrim)
      delete fSolarSystemFluxesPrim;
   if (fSolarSystemFluxesSec)
      delete fSolarSystemFluxesSec;
   if (fSolarSystemFluxesTot)
      delete fSolarSystemFluxesTot;

   // Check that SolarSystem coordinates exists (if not 0D model)
   if (fAxesTXYZ && !fSolarSystemCoords) {
      string message = "SolarSystem coordinated not defined/filled for model "
                       + GetModelName();
      TUMessages::Error(stdout, "TUModelBase", "FillSolarSystemFluxes", message);
   }


   // Allocate and fill in 3 steps
   //   1. Use existing axis
   fSolarSystemFluxesPrim = new TUValsTXYZECr();
   fSolarSystemFluxesSec = new TUValsTXYZECr();
   fSolarSystemFluxesTot = new TUValsTXYZECr();
   fSolarSystemFluxesPrim->AllocateForAxesCrE(this, true);
   fSolarSystemFluxesSec->AllocateForAxesCrE(this, true);
   fSolarSystemFluxesTot->AllocateForAxesCrE(this, true);


   //   2. Create grid for each CR bin
   for (Int_t j_cr = GetNCRs() - 1; j_cr >= 0; --j_cr) {

      // Create grid of values for prim and tot fluxes for this CR
      TUValsTXYZEGrid *grid_prim = new TUValsTXYZEGrid();
      grid_prim->AllocateVals(GetNE());

      TUValsTXYZEGrid *grid_sec =  new TUValsTXYZEGrid();
      grid_sec->AllocateVals(GetNE());

      TUValsTXYZEGrid *grid_tot =  new TUValsTXYZEGrid();
      grid_tot->AllocateVals(GetNE());

      //   3. Fill with value at Solar System
      for (Int_t k_ekn = 0; k_ekn < GetNE(); ++k_ekn) {
         grid_prim->SetVal(EvalFluxPrim(j_cr, k_ekn, fSolarSystemCoords), k_ekn);
         grid_tot->SetVal(EvalFluxTot(j_cr, k_ekn, fSolarSystemCoords), k_ekn);
         grid_sec->SetVal(grid_tot->GetVal(k_ekn) - grid_prim->GetVal(k_ekn), k_ekn);
      }

      // Use this grid
      fSolarSystemFluxesPrim->SetValsE_Grid(j_cr, grid_prim, true);
      grid_prim = NULL;
      fSolarSystemFluxesSec->SetValsE_Grid(j_cr, grid_sec, true);
      grid_sec = NULL;
      fSolarSystemFluxesTot->SetValsE_Grid(j_cr, grid_tot, true);
      grid_tot = NULL;
   }
}

//______________________________________________________________________________
gENUM_BCTYPE TUModelBase::GetBCType(Int_t j_cr, Bool_t is_le_or_he) const
{
   //--- Get boundary condition type for j_cr.
   //  j_cr              CR index
   // is_le_or_he        low-energy (1) or high-energy (0) condition

   switch (TUAxesCrE::GetCREntry(j_cr).GetFamily()) {
      case kNUC: {
            if (is_le_or_he)
               return fBCTypeNucLE;
            else
               return fBCTypeNucHE;
         }
         break;
      case kANTINUC: {
            if (is_le_or_he)
               return fBCTypeAntinucLE;
            else
               return fBCTypeAntinucHE;
         }
         break;
      case kLEPTON: {
            if (is_le_or_he)
               return fBCTypeLeptonLE;
            else
               return fBCTypeLeptonHE;
         }
         break;
      default: {
            string message = "Boundary condition type not found";
            TUMessages::Error(stdout, "TUModelBase", "GetBCType", message);
         }
   }
   return fBCTypeNucLE;
}

//______________________________________________________________________________
Bool_t TUModelBase::GetFreeParsStatusHalfLives() const
{
   //--- Updates status of CR half-lives.

   if (TUAxesCrE::TUCRList::GetFreePars()) {
      TUAxesCrE::TUCRList::GetFreePars()->UpdateParListStatusFromIndividualStatus();
      return TUAxesCrE::TUCRList::GetFreePars()->GetParListStatus();
   } else
      return false;
}

//______________________________________________________________________________
Bool_t TUModelBase::GetFreeParsStatusISM() const
{
   //--- Updates status of ISM parameters.

   if (fISM && fISM->GetFreePars()) {
      fISM->GetFreePars()->UpdateParListStatusFromIndividualStatus();
      return fISM->GetFreePars()->GetParListStatus();
   } else
      return false;
}

//______________________________________________________________________________
Bool_t TUModelBase::GetFreeParsStatusSolMod() const
{
   //--- Updates status of sol.mod. parameters.

   if (fSolMod && fSolMod->GetFreePars()) {
      fSolMod->GetFreePars()->UpdateParListStatusFromIndividualStatus();
      return fSolMod->GetFreePars()->GetParListStatus();
   } else
      return false;
}

//______________________________________________________________________________
Bool_t TUModelBase::GetFreeParsStatusSrcAstro(Int_t s_src) const
{
   //--- Updates status of astro src parameters.

   if (fSrcAstro[s_src] && fSrcAstro[s_src]->GetFreePars()) {
      fSrcAstro[s_src]->GetFreePars()->UpdateParListStatusFromIndividualStatus();
      return fSrcAstro[s_src]->GetFreePars()->GetParListStatus();
   } else
      return false;
}

//______________________________________________________________________________
Bool_t TUModelBase::GetFreeParsStatusSrcDM(Int_t s_src) const
{
   //--- Updates status of DM src parameters.

   if (fSrcDM[s_src] && fSrcDM[s_src]->GetFreePars()) {
      fSrcDM[s_src]->GetFreePars()->UpdateParListStatusFromIndividualStatus();
      return fSrcDM[s_src]->GetFreePars()->GetParListStatus();
   } else
      return false;
}

//______________________________________________________________________________
Bool_t TUModelBase::GetFreeParsStatusTransport() const
{
   //--- Updates status of transport parameters.

   if (fTransport && fTransport->GetFreePars()) {
      fTransport->GetFreePars()->UpdateParListStatusFromIndividualStatus();
      return fTransport->GetFreePars()->GetParListStatus();
   } else
      return false;
}

//______________________________________________________________________________
Bool_t TUModelBase::GetFreeParsStatusXSections() const
{
   //--- Updates status of XS parameters.

   if (TUXSections::GetFreePars()) {
      TUXSections::GetFreePars()->UpdateParListStatusFromIndividualStatus();
      return TUXSections::GetFreePars()->GetParListStatus();
   } else
      return false;
}

//______________________________________________________________________________
void TUModelBase::Initialise(Bool_t is_delete)
{
   //--- Initialises class members.
   //  is_delete         Whether to delete or only initialise

   // Ensure all free parameters of class members (gathered in inherited
   // class TUFreeParList) do not longer appear in TUFreeParList (otherwise,
   // create trouble when deleted)
   TUFreeParList::Initialise(is_delete);

   if (is_delete) {
      if (fAxesTXYZ) delete fAxesTXYZ;
      fAxesTXYZ = NULL;
      if (fCoordE) delete fCoordE;
      fCoordE = NULL;
      if (fCoordsTXYZ) delete fCoordsTXYZ;
      fCoordsTXYZ = NULL;
      if (fSolarSystemCoords) delete fSolarSystemCoords;
      if (fSolarSystemFluxesPrim) delete fSolarSystemFluxesPrim;
      if (fSolarSystemFluxesSec) delete fSolarSystemFluxesSec;
      if (fSolarSystemFluxesTot) delete fSolarSystemFluxesTot;
      if (fInitPars) delete fInitPars;
      fInitPars = NULL;
      if (fISM) delete fISM;

      if (fSrcAstro) {
         for (Int_t i = 0; i < GetNSrcAstro(); ++i) {
            if (fSrcAstro[i]) delete fSrcAstro[i];
            fSrcAstro[i] = NULL;
         }
         delete[] fSrcAstro;
      }

      if (fSrcDM) {
         for (Int_t i = 0; i < GetNSrcDM(); ++i) {
            if (fSrcDM[i]) delete fSrcDM[i];
            fSrcDM[i] = NULL;
         }
         delete[] fSrcDM;
      }

      if (fIsDeleteSolMod && fSolMod) delete fSolMod;
      if (fTransport) delete fTransport;
   }

   fAxesTXYZ = NULL;
   fBCTypeAntinucLE = kUNSET;
   fBCTypeAntinucHE = kUNSET;
   fBCTypeLeptonLE = kUNSET;
   fBCTypeLeptonHE = kUNSET;
   fBCTypeNucLE = kUNSET;
   fBCTypeNucHE = kUNSET;
   fCoordE = NULL;
   fCoordsTXYZ = NULL;
   fEpsIntegr = 0.;
   fEpsIterConv = 0.;
   fEpsNormData = 0.;
   fInitPars = NULL;
   fIsDeleteSolMod = true;
   fIsPropagated = false;
   fISM = NULL;
   fModel = kBASE;
   fNSrcAstro = 0;
   fNSrcDM = 0;
   fSolarSystemCoords = NULL;
   fSolarSystemFluxesPrim = NULL;
   fSolarSystemFluxesSec = NULL;
   fSolarSystemFluxesTot = NULL;
   fSolMod = NULL;
   fSrcAstro = NULL;
   fSrcDM = NULL;
   fTransport = NULL;
}

//______________________________________________________________________________
void TUModelBase::InitialiseForCalculation(Bool_t is_init_or_update, Bool_t is_force_update, Bool_t is_verbose, FILE *f_log)
{
   //--- Initialisation for model calculations.
   //  is_init_or_update Whether initialise class or update (look for changed parameters)
   //  is_force_update   If is_init_or_update=false (update), force to to it even if no parameter changed (useful in a few occasions)
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   if (fModel == kBASE)
      return;
   else {
      string message = "You must override this virtual method in class for " + GetModelName();
      TUMessages::Error(f_log, "TUModelBase", "InitialiseForCalculation", message);
      if (is_verbose) return;
   }
}

//______________________________________________________________________________
void TUModelBase::InvertForELossGain(Int_t j_cr, TUMediumEntry *ism_entry, Double_t *alpha, Double_t *sterm_tot, Double_t *sterm_prim,
                                     gENUM_MATRIXINVERT invert_scheme, Double_t *n_tot, Double_t *n_prim)
{
   //--- Solves the second order equation (tridiagonal matrix inversion) on log-axis for
   //    various boundary conditions at low and high energy (see gENUM_BCTYPE in TUEnum.h):
   //       n + alpha * d/dLnEkn(beta*n - gamma * dn/dLnEkn) = sterm
   //    N.B.1: Two inversions are carried out for two different source terms,
   //       - Sterm=sterm_prim (primary contrib. only)
   //       - Sterm=sterm_tot (all contrib.: primary, secondary, and radioactive).
   //
   // INPUTS:
   //  j_cr              CR index
   //  ism_entry         ISM content for which to calculate
   //  alpha             Prefactor in front of energy-part terms (see for each model)
   //  sterm_tot         Total source term
   //  sterm_prim        Primary (only) source term
   //  invert_scheme     Matrix inversion scheme (see TUEnum.h)
   // OUTPUTS:
   //  n_tot             Address of solution for n_tot
   //  n_prim            Address of solution for n_prim

   // Copy sterm_tot and sterm_prim to dNdETot and dNdEPrim
   for (Int_t k_e = GetNE() - 1; k_e >= 0; --k_e) {
      n_tot[k_e] = sterm_tot[k_e];
      n_prim[k_e] = sterm_prim[k_e];
   }

   // If no E losses or gains, solution is analytical (nothing to do)!
   if (!IsELossesOrGains())
      return;

   // Update first and second order energy terms
   //   beta_startm1[-1,...,NE] goes from k_e={-1...NE}
   //   gamma_startmhalf[-1/2,...,NE-1/2] goes from k_e={0...NE}


   vector<Double_t> beta_startm1, gamma_startmhalf;
   FillEFirstSecondOrderTerms(j_cr, ism_entry, beta_startm1, gamma_startmhalf);

   // Otherwise, numerical inversion of the second order equation (log-step)
   Double_t dx = log(TUAxesCrE::GetE(j_cr)->GetStep());
   Double_t b0, c0, an, bn;
   // 1) Set LE boundary conditions for CR
   // NB: in vector passed, GetCoef2ndOrderHalfBin[k] is evaluated at k-1/2, from {-1/2,...j+1/2,...(GetNE()-1)+1/2}.
   switch (TUModelBase::GetBCTypeLE(j_cr)) {
      case kNOCHANGE: {
            b0 = 1.;
            c0 = 0.;
         }
         break;
      case kNOCURRENT: {
            // J(E0) = 0 => no energy flows in and out of the system
            //    b0 = 1 + alpha_0*beta_0/(2dx) + alpha_0*gamma_{1/2}/(dx)^2
            //    c0 = alpha_0*beta_1/(2dx) - alpha_0*gamma_{1/2}/(dx)^2
            Double_t alpha_0 = alpha[0];
            Double_t beta_0 = beta_startm1[1];
            Double_t beta_1 = beta_startm1[2];
            Double_t gamma_half = gamma_startmhalf[1];
            b0 = 1.  + alpha_0 * beta_0 / (2.*dx) + alpha_0 * gamma_half / (dx * dx);
            c0 = alpha_0 * beta_1 / (2.*dx) - alpha_0 * gamma_half / (dx * dx);
         }
         break;
      case kD2NDLNEKN2_ZERO: {
            // d2n/d[Ln(Ekn]^2(E0) = 0 => P.Salati (used in Maurin et al. and Donato et al. (2001))
            Double_t alpha_0 = alpha[0];
            Double_t beta_m1 = beta_startm1[0];
            //Double_t beta_0 = beta_startm1[1];
            Double_t beta_1 = beta_startm1[2];
            Double_t dgamma = gamma_startmhalf[1] - gamma_startmhalf[0];

            // *) Derived originally
            //    b0 = 1 - alpha_0*beta_0/(dx) + alpha_0*(gamma_{1/2} - gamma_{-1/2})/(dx)^2
            //    c0 = alpha_0*beta_1/(dx) - alpha_0*(gamma_{1/2} - gamma_{-1/2})/(dx)^2
            //b0 = 1. - alpha_0 * beta_0 / dx + alpha_0 * dgamma / (dx * dx);
            //c0 = alpha_0 * beta_1 / dx - alpha_0 * dgamma / (dx * dx);
            // *) New derivation (ensure better stability)
            //    b0 = 1 - alpha_0*beta_{-1}/(dx) + alpha_0*(gamma_{1/2} - gamma_{-1/2})/(dx)^2
            //    c0 = alpha_0*(beta_1+beta_{-1})/(2dx) - alpha_0*(gamma_{1/2} - gamma_{-1/2})/(dx)^2
            b0 = 1. - alpha_0 * beta_m1 / dx + alpha_0 * dgamma / (dx * dx);
            c0 = alpha_0 * (beta_1 + beta_m1) / (2.*dx) - alpha_0 * dgamma / (dx * dx);
         }
         break;
      case kDFDP_ZERO: {
            // df/dp(E0) = 0 => null derivative (used in DRAGON)
            //    b0 = 1 + alpha_0*delta_0*(beta_{-1} + 2gamma_{-1/2}/dx) + alpha_0*(gamma_{1/2} + gamma_{-1/2})/(dx)^2
            //    c0 = alpha_0*(beta_1-beta_{-1})/(2dx) - alpha_0*(gamma_{1/2}+gamma_{-1/2})/(dx)^2
            // with
            //    delta = (Ek/E) * (1+E^2/p^2)
            Double_t delta_0 = TUAxesCrE::GetEk(j_cr, 0) / TUAxesCrE::GetEtot(j_cr, 0)
                               * (1. + 1. / (TUAxesCrE::GetBeta(j_cr, 0) * TUAxesCrE::GetBeta(j_cr, 0)));
            Double_t alpha_0 = alpha[0];
            Double_t beta_m1 = beta_startm1[0];
            Double_t beta_1 = beta_startm1[2];
            Double_t gamma_mhalf = gamma_startmhalf[0];
            Double_t gamma_phalf = gamma_startmhalf[1];
            b0 = 1.  + alpha_0 * delta_0 * (beta_m1 + 2.*gamma_mhalf / dx) + alpha_0 * (gamma_mhalf + gamma_phalf) / (dx * dx);
            c0 = alpha_0 * (beta_1 - beta_m1) / (2.*dx) - + alpha_0 * (gamma_mhalf + gamma_phalf) / (dx * dx);
         }
         break;
      case kUNSET: {
            string message = "Boundary condition unset for model";
            TUMessages::Error(stdout, "TUModelBase", "InvertForELossGain", message);
         }
         break;
      default: {
            string message = "Condition " + TUEnum::Enum2Name(TUModelBase::GetBCTypeLE(j_cr)) + " not implemented";
            TUMessages::Error(stdout, "TUModelBase", "InvertForELossGain", message);
         }
   }

   // 2) Set HE boundary conditions for CR
   // NB: in vector passed, GetCoef2ndOrderHalfBin[k] is evaluated at k-1/2, from [0...NE]  (NE+1 elements)
   // NB: below, we denote 'n' the last index in E grid, i.e. n=NE-1
   switch (TUModelBase::GetBCTypeHE(j_cr)) {
      case kNOCHANGE: {
            an = 0.;
            bn = 1.;
         }
         break;
      case kNOCURRENT: {
            // J(En) = 0 => no energy flows in and out of the system
            //    an = -alpha_n*beta_{n-1}/(2dx) - alpha_n*gamma_{n-1/2}/(dx)^2
            //    bn = 1 - alpha_n*beta_n/(2dx) + alpha_n*gamma_{n-1/2}/(dx)^2
            // with
            //    n = NE-1;
            Int_t n = GetNE() - 1;
            Double_t alpha_n = alpha[n];
            Double_t beta_n_m1 = beta_startm1[n];
            Double_t beta_n = beta_startm1[n + 1];
            Double_t gamma_n_mhalf = gamma_startmhalf[n];
            an = -alpha_n * beta_n_m1 / (2.*dx) - alpha_n * gamma_n_mhalf / (dx * dx);
            bn = 1.  - alpha_n * beta_n / (2.*dx) + alpha_n * gamma_n_mhalf / (dx * dx);
         }
         break;
      case kD2NDLNEKN2_ZERO: {
            // d2ndEkn2(En) = 0
            // with
            //    n = NE-1;
            Int_t n = GetNE() - 1;
            Double_t alpha_n = alpha[n];
            Double_t beta_n_m1 = beta_startm1[n];
            //Double_t beta_n = beta_startm1[n + 1];
            Double_t beta_n_p1 = beta_startm1[n + 2];
            Double_t dgamma = gamma_startmhalf[n + 1] - gamma_startmhalf[n];

            // *) Derived originally
            //    an = -alpha_n*beta_{n-1}/(dx) + alpha_n*(gamma_{n+1/2} - gamma_{n-1/2})/(dx)^2
            //    bn = 1 + alpha_n*beta_n/(dx) - alpha_n*(gamma_{n+1/2} - gamma_{n-1/2})/(dx)^2
            //an = -alpha_n * beta_n_m1 / dx + alpha_n * dgamma / (dx * dx);
            //bn = 1. + alpha_n * beta_n / dx - alpha_n * dgamma / (dx * dx);
            // *) New derivation (ensure better stability)
            //    an = -alpha_n*(beta_{n-1}+beta_{n+1})/(2dx) + alpha_n*(gamma_{n+1/2} - gamma_{n-1/2})/(dx)^2
            //    bn = 1 + alpha_n*beta_{n+1}/(dx) - alpha_n*(gamma_{n+1/2} - gamma_{n-1/2})/(dx)^2
            an = -alpha_n * (beta_n_m1 + beta_n_p1) / (2.*dx) + alpha_n * dgamma / (dx * dx);
            bn = 1. + alpha_n * beta_n_p1 / dx - alpha_n * dgamma / (dx * dx);
         }
         break;
      case kDFDP_ZERO: {
            // df/dp(En) = 0 => null derivative (used in DRAGON)
            //    an = alpha_n*(beta_{n+1}-beta_{n-1})/(2dx) - alpha_n*(gamma_{n+1/2}+gamma_{n-1/2})/(dx)^2
            //    bn = 1 + alpha_n*delta_*(beta_{n+1} - 2gamma_{n+1/2}/dx) + alpha_n*(gamma_{n+1/2} + gamma_{n-1/2})/(dx)^2
            // with
            //    n = NE-1;
            //    delta = (Ek/E) * (1+E^2/p^2)
            Int_t n = GetNE() - 1;
            Double_t alpha_n = alpha[n];
            Double_t delta_n = TUAxesCrE::GetEk(j_cr, n) / TUAxesCrE::GetEtot(j_cr, n)
                               * (1. + 1. / (TUAxesCrE::GetBeta(j_cr, n) * TUAxesCrE::GetBeta(j_cr, n)));
            Double_t beta_n_m1 = beta_startm1[n];
            Double_t beta_n_p1 = beta_startm1[n + 2];
            Double_t gamma_n_mhalf = gamma_startmhalf[n];
            Double_t gamma_n_phalf = gamma_startmhalf[n + 1];
            an = alpha_n * (beta_n_p1 - beta_n_m1) / (2.*dx) - alpha_n * (gamma_n_mhalf + gamma_n_phalf) / (dx * dx);
            bn = 1.  + alpha_n * delta_n * (beta_n_p1 - 2.*gamma_n_phalf / dx) + alpha_n * (gamma_n_mhalf + gamma_n_phalf) / (dx * dx);
         }
         break;
      case kUNSET: {
            string message = "Boundary condition unset for model";
            TUMessages::Error(stdout, "TUModelBase", "InvertForELossGain", message);
         }
         break;
      default: {
            string message = "Condition " + TUEnum::Enum2Name(TUModelBase::GetBCTypeHE(j_cr)) + " not implemented";
            TUMessages::Error(stdout, "TUModelBase", "InvertForELossGain", message);
         }
   }

   // 3) Invert using appropriate boundary conditions
   TUNumMethods::SolveEq_1D2ndOrder_Explicit(GetNE(), dx, &alpha[0], &beta_startm1[1], &gamma_startmhalf[0], &n_tot[0], invert_scheme, b0, c0, an, bn);

   if (!IsPureSecondary(j_cr))
      TUNumMethods::SolveEq_1D2ndOrder_Explicit(GetNE(), dx, &alpha[0], &beta_startm1[1], &gamma_startmhalf[0], &n_prim[0], invert_scheme, b0, c0, an, bn);
}

//______________________________________________________________________________
Bool_t TUModelBase::IsExistSrc(Int_t s_src, Int_t j_cr, Bool_t is_astro_or_dm) const
{
   //--- Returns whether a DM or Astro src exist for the s_src-th source and j_cr-th CR.
   //  s_src             Index of source to consider
   //  j_cr              CR index to look for in source
   //  is_astro_or_dm    Whether to look in Astro or DM source

   if (is_astro_or_dm) {
      if (s_src < 0 || s_src > GetNSrcAstro() - 1 || fSrcAstro[s_src]->IndexCRInSrcSpecies(j_cr) < 0)
         return false;
      else
         return true;
   } else {
      if (s_src < 0 || s_src > GetNSrcDM() - 1 || fSrcAstro[s_src]->IndexCRInSrcSpecies(j_cr) < 0)
         return false;
      else
         return true;
   }
}

//______________________________________________________________________________
Double_t TUModelBase::NormaliseSrcAbundToCRDataUsingNormList(Int_t i_attempt, Bool_t is_verbose, FILE *f_log)
{
   //--- Loops on CR elements/data in inherited class TUNormList and renormalise
   //    source spectrum, in the model used, to the measured CR spectrum. The
   //    normalisation is done as follows:
   //       1. element is in the list -> scale to data+energy selected;
   //    N.B.: we use the Force-Field approximation to modulate IS fluxes to
   //    compare to data, because it is simple, fast, and good enough as long
   //    as the data points are not taken at too low energy (to be as Solar
   //    modulation/model insensitive as possible).
   //
   //  i_attempt         Attempt number (number of times this function is called)
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   if (GetNSrcAstro() != 1) {
      string message = "Source spectrum re-normalisation is possible only "
                       "if there is one and only one astro. source loaded";
      TUMessages::Error(f_log, "TUModelBase", "NormaliseSrcAbundToCRDataUsingNormList",
                        message);
   }

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   // Fill local fluxes (Solar system position) for any iteration
   // (an iteration is associated to a new calculation)
   FillSolarSystemFluxes();

   if (is_verbose && i_attempt == 1) {
      if (TUNormList::GetNZNotToNorm() != 0)
         fprintf(f_log, "%s [norm] overall scaling (special case for Z=%s) of all source abundances (inputs are relative abundances)\n",
                 indent.c_str(), TUNormList::ListZNotToNorm().c_str());
      else
         fprintf(f_log, "%s [norm] overall scaling of all source abundances (inputs are relative abundances)\n", indent.c_str());
      fprintf(f_log, "%s    -> Normalisation factors found to match selected data:\n", indent.c_str());
   }

   Double_t test_convergence = 0.;

   // Loop over quantities to calculate correction factors so
   // that the total flux matches the measured value (data)
   // N.B.: quantities to norm are sorted by growing Z
   vector<Double_t> scale_factor;
   Int_t max_znorm = -1;
   Bool_t is_all_fluxes_negative = true;
   for (Int_t i = 0; i < TUNormList::GetNNorm(); ++i) {
      // Calculate correction factor for this norm
      // data (if negative, means that we can skip it).
      if (i_attempt == 1)
         scale_factor.push_back(CalculateScaleFactor(i, true, is_verbose, f_log));
      else
         scale_factor.push_back(CalculateScaleFactor(i, false, is_verbose, f_log));

      if (scale_factor[i] < 0)
         continue;
      else
         is_all_fluxes_negative = false;

      max_znorm = TUNormList::GetNormZ(i);


      // Update test for convergence
      test_convergence = max(test_convergence, fabs(1. - scale_factor[i])
                             / scale_factor[i]);
      if (is_verbose) {
         if (i_attempt == 1)
            fprintf(f_log, "%s     Z_norm=%d  factor=%le\n", indent.c_str(), TUNormList::GetNormZ(i), scale_factor[i]);
         else if (is_verbose && fabs(1. - scale_factor[i]) > 1.e-7)
            fprintf(f_log, "%s     %s scaled by %le\n", indent.c_str(), TUNormList::GetNormName(i).c_str(), scale_factor[i]);
      }
   }
   // If nothing to rescale
   if (max_znorm == -1) {
      if (is_all_fluxes_negative) {
         string message = "All the calculated fluxes are negative so it"
                          " cannot rescale the flux to the data. \n";
         message += "Check that you do not use a too large value"
                    " for Va and Vc (a negative value in the fluxes may be"
                    " the result of an unstable numerical inversion).\n";
         message += "   => you should not trust the output of this"
                    " specific configuration for the propagation parameters!!!";
         TUMessages::Warning(f_log, "TUModelBase", "NormaliseSrcAbundToCRDataUsingNormList", message);
         // return -15 to signify that it will not loop to find the
         // good rescaling (because there is none in this case)
         //  => also to state that the propagation parameters for
         //  this run are to be output on screen
         return -15;
      } else {
         TUMessages::Warning(f_log, "TUModelBase", "NormaliseSrcAbundToCRDataUsingNormList",
                             "no flux found to be re-scaled!");
         // return a small number to ensure that it will not loop to
         // find a good rescaling (because there is none in this case)
         return 1.e-50;
      }
   }

   // Renormalise CR species: depends whether this is first iteration or not
   if (i_attempt == 1) {
      // Note that we use both the 'generic species to propagate and the
      // species present in the source (CRs may not be all present in source)
      // If first iteration, all CRs (not secondaries) are normalised,
      // otherwise non-scaled primary contributions could become dominant
      // contributors to secondary production which is wrong!
      Int_t jcr_start = 0;
      Int_t znorm_prev = -5; // to include 2H,BAR and 1H-BAR in norm!
      for (Int_t i = 0; i < TUNormList::GetNNorm(); ++i) {
         if (scale_factor[i] < 0)
            continue;

         // Index in CR list for a given Znorm -> index in source
         Int_t znorm = TUNormList::GetNormZ(i);
         Int_t jcr_stop = TUNormList::IndexNormInFilter(i, TUNormList::GetNCRIndicesInNorm(i) - 1);
         if (znorm == max_znorm)
            jcr_stop = GetNCRs() - 1;

         // Normalise with scale factor of Znorm (for Zspecies<=Znorm,
         // expect for all heavier than max_znorm)
         for (Int_t jcr = jcr_start; jcr <= jcr_stop; ++jcr) {
            // Isotopes are not ordered by growing Z (but growing mass): care
            // is required not to mess with the initial isotopic abundances!
            Int_t z_cr = GetCREntry(jcr).GetZ();
            if ((z_cr > znorm && jcr_stop < GetNCRs() - 1) || z_cr <= znorm_prev)
               continue;

            // Index of j_cr in Src: if not in source, nothing to do
            Int_t jsrc = fSrcAstro[0]->IndexCRInSrcSpecies(jcr);
            // Index of element 'not to norm' in NormList
            Int_t i_not2norm = TUNormList::IndexZInListOfNotToNorm(z_cr);
            if (IsPureSecondary(jcr) || jsrc < 0) {
               // If CR is pure secondary, no rescaling
               if (is_verbose)
                  fprintf(f_log, "%s    %-6s is a pure secondary (not rescaled, q=0)\n", indent.c_str(),
                          GetCREntry(jcr).GetName().c_str());
               continue;
            } else if (i_not2norm >= 0) {
               // Is CR is in list of rescaled CRs:
               //  - if in list, not rescale
               //  - if not, adjust relative abundance to the ones not rescaled
               Int_t i_crnot2norm = TUNormList::IndexCRInListOfNotToNorm(i_not2norm, GetCREntry(jcr).GetName());
               if (i_crnot2norm >= 0) {
                  if (is_verbose)
                     fprintf(f_log, "%s    %-6s abundance is %le (trial value in fit)\n",
                             indent.c_str(), GetCREntry(jcr).GetName().c_str(), fSrcAstro[0]->GetNormSrcSpectrum(jsrc));
                  continue;
               } else {
                  // Adjust abundance to the first isotope 'not to rescale'
                  Int_t jcr_ref = Index(TUNormList::GetCRNotToNorm(i_not2norm, 0));
                  Double_t rel_abund = GetCREntry(jcr).GetSSIsotopeAbundance()
                                       / GetCREntry(jcr_ref).GetSSIsotopeAbundance();
                  Double_t abund = rel_abund * fSrcAstro[0]->GetNormSrcSpectrum(fSrcAstro[0]->IndexCRInSrcSpecies(jcr_ref));
                  fSrcAstro[0]->SetNormSrcSpectrum(jsrc, abund);
                  if (is_verbose)
                     fprintf(f_log, "%s    %-6s is set to %le (fixed SS relative abundance %s/%s=%le)\n",
                             indent.c_str(), GetCREntry(jcr).GetName().c_str(), fSrcAstro[0]->GetNormSrcSpectrum(jsrc),
                             GetCREntry(jcr).GetName().c_str(), GetCREntry(jcr_ref).GetName().c_str(), rel_abund);
                  continue;
               }
            }
            // If reach this point, renormalise
            fSrcAstro[0]->ScaleNormSrcSpectrum(jsrc, scale_factor[i]);
            if (is_verbose)
               fprintf(f_log, "%s    %-6s [Z=%2d] is scaled by %le  (from Z_norm=%d)\n",
                       indent.c_str(),  GetCREntry(jcr).GetName().c_str(), GetCREntry(jcr).GetZ(), scale_factor[i], znorm);
         }
         // Adapt jcr_start
         jcr_start = TUNormList::IndexNormInFilter(i, 0);
         znorm_prev = znorm;
      }
      // Return 10 in order not to stop the normalisation procedure
      // if (!TUModelBase::TUPropagSwitches::IsSecondaries())
      return test_convergence;
      // else
      //    return 10.;
   } else {
      // All other iterations: only normalise elements in selection
      for (Int_t i = 0; i < TUNormList::GetNNorm(); ++i) {
         if (scale_factor[i] < 0)
            continue;

         // Loop on corresponding elements for each qty
         // (qties not secondaries are always normalised)
         for (Int_t k = 0; k < TUNormList::GetNCRIndicesInNorm(i); ++k) {
            Int_t j_cr = TUNormList::IndexNormInFilter(i, k);
            if (TUAxesCrE::IsPureSecondary(j_cr))
               continue;
            Int_t j_src = fSrcAstro[0]->IndexCRInSrcSpecies(j_cr);
            if (j_src >= 0)
               fSrcAstro[0]->ScaleNormSrcSpectrum(j_src, scale_factor[i]);
         }
      }
      // The returned value serves as a criterion for convergence
      return test_convergence;
   }
}

//______________________________________________________________________________
TUValsTXYZECr *TUModelBase::OrphanFluxes(TUCoordTXYZ *coord_txyz, Int_t tot0_prim1_sec2)
{
   //--- Returns TUValsTXYZECr object containing fluxes (all CRs and all E)
   //    calculated at position coord_txyz (which is a grid of values).
   //    N.B.: a new pointer is created/returned that must be deleted afterwards.
   //
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  tot0_prim1_sec2   Switch to get total(0), primary(1) or secondary(2) contrib.

   // Allocate fluxes (values on grid for NCRs) on CrE axes.
   // N.B.: this is for a given location, so no TXYZ axes (only E)
   TUValsTXYZECr *fluxes = new TUValsTXYZECr();
   fluxes->AllocateForAxesCrE(this, true);

   // Create grid for each CR bin
   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {

      // Create grid of values for prim and tot fluxes for this CR
      TUValsTXYZEGrid *grid = new TUValsTXYZEGrid();
      grid->AllocateVals(GetNE());

      // Fill with value at coord_txyz
      for (Int_t k_ekn = 0; k_ekn < GetNE(); ++k_ekn)
         grid->SetVal(EvalFlux(j_cr, k_ekn, coord_txyz, tot0_prim1_sec2), k_ekn);

      // Use this grid
      fluxes->SetValsE_Grid(j_cr, grid, true);
      grid = NULL;
   }
   return fluxes;
}

//______________________________________________________________________________
TH1D *TUModelBase::OrphanGCRLocalAbundances(Bool_t is_cr_or_element, Double_t &ekn,
      Int_t tot0_prim1_sec2,
      string element_onwhich2rescale,
      Double_t rescaled_value)
{
   //--- Returns histo of GCR abundances of current propagation model.
   // INPUTS:
   //  is_cr_or_element         Abundances for isotopes (true) or elements (false)
   //  ekn                      Desired energy for calculation
   //  tot0_prim1_sec2          Switch to get total(0), primary(1) or secondary(2) contrib.
   //  element_onwhich2rescale  Element against which all elements are normalised
   //  rescaled_value           Normalisation value
   // OUTPUT:
   //  ekn                      Closest (lower) energy to input


   TH1D *h = NULL;

   // Check that element to norm exists in list (or abort)
   vector<Int_t> cr_indices;
   if (!TUAxesCrE::ElementNameToCRIndices(element_onwhich2rescale, cr_indices, false)) {
      string message = "Element name " + element_onwhich2rescale
                       + " does not appear in list of CRs";
      TUMessages::Error(stdout, "TUModelBase", "OrphanGCRLocalAbundances", message);
   }

   // Look for energy closest to ekn
   Int_t k_ekn = TUAxesCrE::GetE(cr_indices[0])->IndexClosest(ekn);
   ekn = TUAxesCrE::GetE(cr_indices[0])->GetVal(k_ekn);

   // Calculate norm
   Double_t norm = rescaled_value
                   / EvalFluxCombo(element_onwhich2rescale, k_ekn, fSolarSystemCoords, tot0_prim1_sec2, false);

   const Int_t n = 3;
   string type[n] = {"tot", "prim", "sec"};
   // Create histo according to cases

   string name = GetModelName() + "_abund_gcr_" + type[tot0_prim1_sec2];
   if (is_cr_or_element) {
      name = name + "_isotopes";
      h = TUAxesCrE::OrphanEmptyHistoOfCRs(name.c_str(), "Labels from CR list");
      for (Int_t i = 0; i < h->GetNbinsX(); ++i) {
         Double_t flux = EvalFlux(i, k_ekn, fSolarSystemCoords, tot0_prim1_sec2);
         h->SetBinContent(i + 1, flux * norm);
      }
   } else {
      name = name + "_elements";
      vector<Int_t> list_z = GetAxesCrE()->ExtractAllZ(false);
      Int_t n_z = list_z.size();
      h = TUAxesCrE::OrphanEmptyHistoOfElements(name.c_str(), "Labels from elements in CR list", list_z);
      for (Int_t i = 0; i < n_z; ++i) {
         Double_t flux = EvalFluxCombo(TUAxesCrE::ZToElementName(list_z[i]), k_ekn, fSolarSystemCoords, tot0_prim1_sec2, false);
         h->SetBinContent(i + 1, flux * norm);
      }
   }
   string tmp = "";
   if (rescaled_value <= 1000. && rescaled_value >= 1.01)
      tmp = Form("%.f", rescaled_value);
   else
      tmp = Form("%.3le", rescaled_value);
   string title = "Relative abundance [" + element_onwhich2rescale
                  + " #equiv " + (string)tmp + "]";
   h->SetYTitle(title.c_str());

   return h;
}

//______________________________________________________________________________
TCanvas *TUModelBase::OrphanModelParameters(TURunOutputs const &outputs, Bool_t is_print, FILE *f_print, Bool_t is_date) const
{
   //--- Returns canvas with model parameters.
   //  outputs               use content to display
   //  is_print              Whether to print or not parameters
   //  f_print               File in which to print
   //  is_date               Whether to print or not header with date

   Int_t n_pars = outputs.GetNPropModPars();
   Int_t n_titles = outputs.GetNPropModTitles();


   //n_lines = std::accumulate(n_pars.begin(), n_pars.end(), 1);
   Double_t y_space = 15;
   Int_t n_text = n_titles + 1 + n_pars + 1;
   Double_t height =  n_text * y_space;
   Double_t text = 1. / Double_t(n_text);
   Double_t y_pos = 0.98;
   // Model ID
   TCanvas *cvs = new TCanvas("model_parameters", "Model parameters", 1000, 0, 350, height);
   string line = Form("Model ID = %s", GetModelName().c_str());
   if (is_print) {
      TUMisc::PrintUSINEHeader(f_print, is_date);
      fprintf(f_print, "# %s\n", line.c_str());
   }
   TText *t = new TText(.5, y_pos, line.c_str());
   if (fSolMod) {
      y_pos -= text;
      line = Form("Modulation ID = %s", fSolMod->GetModelName().c_str());
      if (is_print)
         fprintf(f_print, "# %s\n", line.c_str());
      t = new TText(.5, y_pos, line.c_str());
   }
   t->SetNDC(kTRUE);
   t->SetTextAlign(22);
   t->SetTextColor(kBlack);
   t->SetTextFont(23);
   t->SetTextSize(18);
   t->Draw();

   // Loop over types of parameters
   for (Int_t f = 0; f < n_titles; ++f) {
      y_pos -= text;
      if (is_print)
         fprintf(f_print, "\n# %s\n", outputs.GetPropModTitles(f).c_str());
      t = new TText(.05, y_pos, outputs.GetPropModTitles(f).c_str());
      t->SetNDC(kTRUE);
      t->SetTextAlign(12);
      t->SetTextColor(TUMisc::RootColor(f));
      t->SetTextFont(23);
      t->SetTextSize(16);
      t->Draw();

      // Loop over all possible sub-parameters for this type
      Int_t i_start = outputs.GetPropModIndexStart(f);
      Int_t i_stop = n_pars;
      if (f < n_titles - 1)
         i_stop = outputs.GetPropModIndexStart(f + 1);
      for (Int_t i = i_start; i < i_stop; ++i) {
         line = outputs.GetPropModParNames(i)
                + Form(" = %le ", outputs.GetPropModParValues(i))
                + outputs.GetPropModParUnits(i);
         if (is_print)
            fprintf(f_print, "%s\n", line.c_str());
         y_pos -= text;
         TText *tt = new TText(.1, y_pos, line.c_str());
         tt->SetNDC(kTRUE);
         tt->SetTextAlign(12);
         tt->SetTextColor(TUMisc::RootColor(f));
         tt->SetTextFont(43);
         tt->SetTextSize(14);
         tt->Draw();
      }
   }

   return cvs;
}

//______________________________________________________________________________
TGraph *TUModelBase::OrphanPrimaryContent(Int_t z_or_index_cr, TUCoordTXYZ *coord_txyz,
      Bool_t is_element_or_isotope)
{
   //--- Returns ROOT TGraph display for element or isotope  primary fraction [%]
   //    for a given element (resp. cr) at a selected position for all energies.
   //  z_or_index_cr     Z or index of CR to plot
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  is_nuc_or_element Whether histo is formed for isotope or element

   // Create and fill TGraph (for all energies)
   TGraph *gr = new TGraph(TUAxesCrE::GetNE());
   TUAxis *e = TUAxesCrE::GetEFamily(kNUC);
   string cr_name;
   string title;
   string name;
   string model = GetModelName();
   TUMisc::RemoveSpecialChars(model);

   if (is_element_or_isotope) {
      cr_name = TUAxesCrE::ZToElementName(z_or_index_cr);
      name = model + "_primfrac_" + cr_name;
      title = cr_name + " primary fraction (%)";
      for (Int_t k = 0; k < e->GetN(); ++k) {
         Double_t ekn_gevn = e->GetVal(k);
         gr->SetPoint(k, ekn_gevn, EvalFractionElement(z_or_index_cr, k, coord_txyz, 1));
      }
   } else {
      cr_name = TUAxesCrE::GetCREntry(z_or_index_cr).GetName();
      name = model + "_primfrac_" + cr_name;
      title = cr_name + "primary fraction (%)";
      for (Int_t k = 0; k < e->GetN(); ++k) {
         Double_t ekn_gevn = e->GetVal(k);
         gr->SetPoint(k, ekn_gevn, EvalFraction(z_or_index_cr, k, coord_txyz, 1));
      }
   }
   gr->SetName(name.c_str());
   gr->SetTitle(title.c_str());
   gr->GetXaxis()->SetTitle("Ekn [GeV/n]");
   gr->GetXaxis()->SetTitleOffset(1.5);
   gr->GetXaxis()->SetTitleSize(0.03);
   gr->GetYaxis()->SetTitle(title.c_str());
   gr->GetYaxis()->SetTitleOffset(2.);
   gr->GetYaxis()->SetTitleSize(0.03);

   return gr;
}

//______________________________________________________________________________
TH1D *TUModelBase::OrphanPrimaryContent(Double_t &ekn_gevn, TUCoordTXYZ *coord_txyz,
                                        string const &start, string const &stop,
                                        Bool_t is_element_or_isotope)
{
   //--- Returns histogram of primary content [%] for a CR or an element
   //    at a selected energy, and at position coord_txyz.
   //  ekn_gevn          Kinetic energy per nucleon [GeV/n]
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  start             CR/element name of first entry in histo
   //  stop              CR/element name of last entry in histo
   //  is_nuc_or_element Whether histo is formed for isotopic fraction or elemental fraction

   // Get index closest to energy asked for, then store found energy in char[]
   TUAxis *e = GetEFamily(kNUC);
   Int_t k_e = e->IndexClosest(ekn_gevn);
   string ekn_name = Form("%.2fGeVn", ekn_gevn);
   string ekn_title = Form("%.2f [GeV/n]", ekn_gevn);
   e = NULL;
   string model = GetModelName();
   TUMisc::RemoveSpecialChars(model);

   string title;
   string name;
   string cr_list = start + "_" + stop;
   TH1D *h = NULL;
   if (is_element_or_isotope) {
      name = model + "_primfrac_" + cr_list + "_" + ekn_name;
      title = "Primary content of elements @ " + ekn_title;
      Int_t z_min = GetAxesCrE()->ElementNameToZ(start, false, stdout);
      Int_t z_max = GetAxesCrE()->ElementNameToZ(stop, false, stdout);
      vector<Int_t> list_z = GetAxesCrE()->ExtractAllZ(false, z_min, z_max);
      h = OrphanEmptyHistoOfElements(name, title, list_z);
      for (Int_t i = 0; i < (Int_t)list_z.size(); ++i)
         h->SetBinContent(i + 1, EvalFractionElement(list_z[i], k_e, coord_txyz, 1));
   } else {
      name = model + "_primfracisot_" + cr_list + "_" + ekn_name;
      title = "Primary content of isotopes @ " + ekn_title;
      Int_t jcr_start = Index(start, true, stdout, true);
      Int_t jcr_stop = Index(stop, true, stdout, true);
      vector<Int_t> list_jcr = ExtractAllCRs(false, jcr_start, jcr_stop);
      h = OrphanEmptyHistoOfCRs(name, title, list_jcr);
      for (Int_t j = 0; j < (Int_t)list_jcr.size(); ++j)
         h->SetBinContent(j + 1, EvalFraction(list_jcr[j], k_e, coord_txyz, 1));
   }
   h->SetYTitle("Primary fraction (%)");
   h->SetLabelSize(0.05);
   h->SetMinimum(0.);
   h->SetMaximum(100.);

   return h;
}

//______________________________________________________________________________
void TUModelBase::PrintBoundaryConditions(FILE *f) const
{
   //--- Print in file 'f' boundary conditons for all species.
   //  f                 File in which to write

   string indent = TUMessages::Indent(true);
   fprintf(f, "%s  Boundary conditions:\n", indent.c_str());
   if (TUAxesCrE::IsAtLeastOneNucleus())
      fprintf(f, "%s    - Nuclei: LE=%s  HE=%s\n", indent.c_str(), TUEnum::Enum2Name(fBCTypeNucLE).c_str(), TUEnum::Enum2Name(fBCTypeNucHE).c_str());
   if (TUAxesCrE::IsAtLeastOneAntinuc())
      fprintf(f, "%s    - Antinuclei: LE=%s  HE=%s\n", indent.c_str(), TUEnum::Enum2Name(fBCTypeAntinucLE).c_str(), TUEnum::Enum2Name(fBCTypeAntinucHE).c_str());
   if (TUAxesCrE::IsAtLeastOneLepton())
      fprintf(f, "%s    - Lepton: LE=%s  HE=%s\n", indent.c_str(), TUEnum::Enum2Name(fBCTypeLeptonLE).c_str(), TUEnum::Enum2Name(fBCTypeLeptonHE).c_str());
   fprintf(f, "\n");
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUModelBase::PrintIsotopicFraction(FILE *f, TUCoordTXYZ *coord_txyz)
{
   //--- Print in file 'f' the isotopic fraction of all isotopes in list
   //    at position 'coord_txyz'.
   //  f                 File in which to write
   //  coord_txyz       Spacetime position of calculation

   // Find Z min and max for nuclei (not smaller than 1)
   Int_t zmin = max(1, TUAxesCrE::TUCRList::ExtractZMin());
   Int_t zmax = TUAxesCrE::TUCRList::ExtractZMax();

   // Print separator and what to be printed
   string separator = GetModelName() + ": isotopic fraction of an element vs Ekn";
   TUMessages::Separator(f, separator);
   fprintf(f, "Ekn (GeV/n)   %%I1      %%I2        ...\n");
   fprintf(f, "--------------------------------------\n");

   // Loop on elements (for which to calculate isotopic fraction content)
   for (Int_t z = zmin; z <= zmax; ++z) {

      // Find isotopes in element (if element not in list, continue)
      string element  = TUAxesCrE::TUCRList::ZToElementName(z);
      vector<Int_t> i_crs;
      if (!TUAxesCrE::TUCRList::ElementNameToCRIndices(element, i_crs, false))
         continue;

      // Print line header
      fprintf(f, "%-5s       ", element.c_str());
      for (Int_t i = 0; i < (Int_t)i_crs.size(); ++i)
         fprintf(f, "%-6s    ", (TUAxesCrE::TUCRList::GetCREntry(i_crs[i])).GetName().c_str());
      fprintf(f, "\n");

      for (Int_t kekn = 0; kekn < GetNE(); kekn += GetNE() / 5) {
         fprintf(f, "%.3le     ", TUAxesCrE::GetEkn(i_crs[0], kekn));
         for (Int_t i = 0; i < Int_t(i_crs.size()); ++i)
            fprintf(f, "%5.2lf     ", EvalFraction(i_crs[i], kekn,
                                                   coord_txyz, 0 /*isotopic0_prim1_sec2*/));

         fprintf(f, "\n");
      }
      fprintf(f, "--------------------------------------\n");
      if (z == zmax)  fprintf(f, "\n\n");
   }
   return;
}

//______________________________________________________________________________
void TUModelBase::PrintSolarSystemISFluxes(FILE *f, Int_t tot0_prim1_sec2)
{
   //--- Print in file 'f' Solar System IS fluxes (tot, prim or sec contribution).
   //  f                 File in which to write
   //  tot0_prim1_sec2   Switch to get total(0), primary(1) or secondary(2) contrib.

   FillSolarSystemFluxes();
   if (tot0_prim1_sec2 == 0)
      fSolarSystemFluxesTot->PrintVals(f);
   else if (tot0_prim1_sec2 == 1)
      fSolarSystemFluxesPrim->PrintVals(f);
   else if (tot0_prim1_sec2 == 2)
      fSolarSystemFluxesSec->PrintVals(f);
}

//______________________________________________________________________________
void TUModelBase::PrintSources(FILE *f, Bool_t is_summary) const
{
   //--- Prints sources content in file f.
   //  f                 File in which to print

   // Astro sources
   if (fSrcAstro) {
      for (Int_t i = 0; i < fNSrcAstro; ++i) {
         if (!fSrcAstro[i])
            continue;
         if (i == 0) fSrcAstro[i]->PrintSrc(f, true, is_summary);
         else fSrcAstro[i]->PrintSrc(f, false, is_summary);
      }
   }

   // DM sources
   if (fSrcDM) {
      for (Int_t i = 0; i < fNSrcDM; ++i) {
         if (!fSrcDM[i])
            continue;
         if (i == 0) fSrcDM[i]->PrintSrc(f, true, is_summary);
         else fSrcDM[i]->PrintSrc(f, false, is_summary);
      }
   }
}

//______________________________________________________________________________
void TUModelBase::PrintSummary(FILE *f) const
{
   //--- Prints summary in file f.
   //  f                 File in which to print

   PrintSummaryBase(f);
   PrintSummaryModel(f);
}

//______________________________________________________________________________
void TUModelBase::PrintSummaryBase(FILE *f) const
{
   //--- Prints summary ingredients in file f.
   //  f                 File in which to print

   TUMessages::Separator(f, "Nuclei and parents");
   TUAxesCrE::PrintParents(f);

   TUMessages::Separator(f, "Cosmic Ray energies");
   TUAxesCrE::PrintESummary(f);

   TUMessages::Separator(f, "CR Data");
   TUDataSet::PrintFileNames(f);

   TUMessages::Separator(f, "Data/energy used to normalise CR fluxes");
   TUNormList::PrintNormQuery(f);

   TUMessages::Separator(f, "Source templates");
   TUSrcTemplates::PrintSrcTemplates(f);

   TUMessages::Separator(f, "Cross sections");
   TUXSections::PrintXSecFiles(f);
   TUXSections::PrintTargetList(f);
   TUXSections::PrintTertiaryList(f);
}

//______________________________________________________________________________
void TUModelBase::PrintSummaryModel(FILE *f) const
{
   //--- Prints model summary in file f.
   //  f                 File in which to print

   TUMessages::Separator(f, "Model summary");
   if (fAxesTXYZ)
      fAxesTXYZ->PrintSummary(f);
   TUPropagSwitches::PrintPropagSwitches(f);
   if (fISM)
      fISM->PrintSummary(f);
   if (fTransport)
      fTransport->PrintSummary(f);
   PrintSources(f, true);
   PrintPars(f);
}

//______________________________________________________________________________
void TUModelBase::Propagate(Int_t jcr_start, Int_t jcr_stop, Bool_t is_norm_to_data, Bool_t is_verbose, FILE *f_log)
{
   //--- Propagate all CR species found, normalise to nuclear data if demanded.
   //  jcr_start         Index of CR for first to calculate
   //  jcr_stop          Index of CR for last to calculate
   //  is_norm_to_data   Whether to normalise fluxes to TOA CR data (useful if source norm. param. not in minimisation)
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   // Update model-specific members (if required)
   InitialiseForCalculation(/*is_init_or_update*/false, /*is_force_update*/false, is_verbose, f_log);

   // Propagate for (anti-)nuclei
   // N.B.: must be propagated before any species
   if (IsAtLeastOneNucleus() || IsAtLeastOneAntinuc()) {
      if (is_norm_to_data)
         CalculateChargedCRs_NormalisedToData(jcr_start, jcr_stop, is_verbose, f_log);
      else
         CalculateChargedCRs(jcr_start, jcr_stop, is_verbose, f_log);
   }

   // Propagation for this configuration is now done!
   fIsPropagated = true;
}

//______________________________________________________________________________
void TUModelBase::SetBCType(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Read and set boundary conditions from init. file.
   //  init_pars         USINE initialisation parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE


   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s   => Read boundary conditions for run\n", indent.c_str());

   if (TUAxesCrE::IsAtLeastOneAntinuc()) {
      string bc_le = init_pars->GetParEntry(init_pars->IndexPar("UsineRun", "Calculation", "BC_ANTINUC_LE")).GetVal();
      string bc_he = init_pars->GetParEntry(init_pars->IndexPar("UsineRun", "Calculation", "BC_ANTINUC_HE")).GetVal();
      TUEnum::Name2Enum(bc_le, fBCTypeAntinucLE);
      TUEnum::Name2Enum(bc_he, fBCTypeAntinucHE);
      if (is_verbose)
         fprintf(f_log, "%s       - Boundary Conditions (ANTINUC): %s (LE) and %s (HE)\n",
                 indent.c_str(), bc_le.c_str(), bc_he.c_str());
   }
   if (TUAxesCrE::IsAtLeastOneNucleus()) {
      string bc_le = init_pars->GetParEntry(init_pars->IndexPar("UsineRun", "Calculation", "BC_NUC_LE")).GetVal();
      string bc_he = init_pars->GetParEntry(init_pars->IndexPar("UsineRun", "Calculation", "BC_NUC_HE")).GetVal();
      TUEnum::Name2Enum(bc_le, fBCTypeNucLE);
      TUEnum::Name2Enum(bc_he, fBCTypeNucHE);
      if (is_verbose)
         fprintf(f_log, "%s       - Boundary Conditions (NUC): %s (LE) and %s (HE)\n",
                 indent.c_str(), bc_le.c_str(), bc_he.c_str());
   }
   if (TUAxesCrE::IsAtLeastOneLepton()) {
      string bc_le = init_pars->GetParEntry(init_pars->IndexPar("UsineRun", "Calculation", "BC_LEPTON_LE")).GetVal();
      string bc_he = init_pars->GetParEntry(init_pars->IndexPar("UsineRun", "Calculation", "BC_LEPTON_HE")).GetVal();
      TUEnum::Name2Enum(bc_le, fBCTypeLeptonLE);
      TUEnum::Name2Enum(bc_he, fBCTypeLeptonHE);
      if (is_verbose)
         fprintf(f_log, "%s       - Boundary Conditions (LEPTONS): %s (LE) and %s (HE)\n",
                 indent.c_str(), bc_le.c_str(), bc_he.c_str());
   }
   TUMessages::Indent(false);
}
//______________________________________________________________________________
void TUModelBase::SetClass(string const &usine_initfile, string const &propag_model, Bool_t is_verbose, FILE *f_log)
{
   //--- Sets class from USINE initialisation file.
   //  usine_initfile    USINE initialisation file
   //  propag_model      Propagation model name (as group name in initialisation file)
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   if (fInitPars)
      delete fInitPars;
   fInitPars = new TUInitParList();
   fInitPars->SetClass(usine_initfile, is_verbose, f_log);
   SetClass(fInitPars, propag_model, is_verbose, f_log);
}

//______________________________________________________________________________
void TUModelBase::SetClass(TUInitParList *init_pars, string const &propag_model, Bool_t is_verbose, FILE *f_log)
{
   //--- Sets class from TUInitParList pointer in three steps:
   //       A) Set generic model-independent ingredients (CR+E axes, XSecs, data, ...)
   //       B) Set generic model-dependent ingredients (geometry, ISM, transport...)
   //       C) Set specific model-dependent ingredients
   //       D) Update free parameters of the model
   //       E) Initialise for calculation
   //    N.B.: the second step requires a name ('propag_model'), which will be
   //    searched for as a group name in the initialisation file (all model
   //    parameters are provided under this group name).
   //  init_pars         USINE initialisation parameters
   //  propag_model      Propagation model name (as group name in initialisation file)
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUModelBase::SetClass]\n", indent.c_str());
   Initialise(true);

   // A) Set generic model-independent ingredients
   SetClass_ModelBaseIndepMembers(init_pars, is_verbose, f_log);
   if (is_verbose)
      fprintf(f_log, "\n");
   // B) Set generic model-dependent ingredients (if propag_model name found in init_par)
   //    and not "BASE" (used for test purposes)
   if (propag_model == "BASE") {
      if (is_verbose)
         fprintf(f_log, "%s   ### Load BASE ingredients (propag_model=BASE) only\n\n", indent.c_str());
   } else {
      if (is_verbose)
         fprintf(f_log, "%s   ### Load generic model-dependent ingredients (propag_model=%s)\n",
                 indent.c_str(), propag_model.c_str());
      // Search for 'propag_model' as a group name in init_pars
      Int_t i_group = init_pars->IndexGroup(propag_model);
      if (i_group < 0) {
         string message = "propag_model=" + propag_model + " not found among groups={"
                          + init_pars->ExtractNameGroups() + "} in " + init_pars->GetFileNames();
         TUMessages::Error(f_log, "TUModelBase", "SetClass", message);
      }
      SetClass_ModelBaseDepMembers(init_pars, propag_model, is_verbose, f_log);
   }

   // C) Set specific model-dependent extra parameters
   if (is_verbose)
      fprintf(f_log, "%s   ### Load specific model-dependent ingredients (propag_model=%s)\n",
              indent.c_str(), propag_model.c_str());
   SetClass_ModelSpecific(init_pars, is_verbose, f_log);

   // D) Update model parameters
   if (is_verbose)
      fprintf(f_log, "%s   ### Update parameters for %s\n", indent.c_str(), propag_model.c_str());
   FillModelFreePars(is_verbose, f_log);

   // E) Initialise and allocate propagation model class members
   if (is_verbose)
      fprintf(f_log, "%s   ### Prepare for %s flux calculation (initialise class members)\n",
              indent.c_str(), propag_model.c_str());
   SetBCType(init_pars, is_verbose, f_log);
   InitialiseForCalculation(/*is_init_or_update*/true,  /*is_force_update*/false, is_verbose, f_log);

   if (is_verbose)
      fprintf(f_log, "%s[TUModelBase::SetClass] <DONE>\n\n", indent.c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUModelBase::SetClass_ModelBaseDepMembers(TUInitParList *init_pars, string const &propag_model, Bool_t is_verbose, FILE *f_log)
{
   //--- Sets model classes from TUInitParList pointer.
   //  init_pars         USINE initialisation parameters
   //  propag_model      Model selected for propagation (see TUEnum.h)
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUModelBase::SetClass_ModelBaseDepMembers]\n", indent.c_str());

   // Initialise free parameters of the model
   TUFreeParList::Initialise(true);

   // Model should match one of the names given in TUEnum.h
   // N.B.: the model name is also the one used in the initialisation file!
   string propag_model_tmp = propag_model;
   TUEnum::Name2Enum(propag_model_tmp, fModel);

   // Set model parameters (from group=propag_model)
   string group = propag_model;


   // 1. Set axes
   if (is_verbose)
      fprintf(f_log, "%s### Set geometry for %s\n", indent.c_str(), propag_model.c_str());
   if (fAxesTXYZ) delete fAxesTXYZ;
   fAxesTXYZ = new TUAxesTXYZ();
   fAxesTXYZ->SetClass(init_pars, group, "Geometry", is_verbose, f_log);
   if (is_verbose)
      fprintf(f_log, "\n");


   // 2. Set Solar System position
   if (is_verbose)
      fprintf(f_log, "%s### Set Solar System position for %s\n", indent.c_str(), propag_model.c_str());
   fSolarSystemCoords = new TUCoordTXYZ();
   const Int_t n_coords = 3;
   string param[n_coords] = {"XSun", "YSun", "ZSun"};
   for (Int_t i = 0; i < n_coords; ++i) {
      string earth_coords =  init_pars->GetParEntry(init_pars->IndexPar(group, "Geometry", param[i])).GetVal();
      if (earth_coords != "-")
         fSolarSystemCoords->SetVal(i + 1, atoi(earth_coords.c_str()));
   }
   fSolarSystemCoords->ConvertVals2Bins(fAxesTXYZ);
   if (is_verbose) {
      fSolarSystemCoords->PrintVals(f_log);
      fSolarSystemCoords->PrintBins(f_log);
      fprintf(f_log, "\n");
   }


   // 3. Set ISM
   if (is_verbose)
      fprintf(f_log, "%s### Set ISM for %s\n", indent.c_str(), propag_model.c_str());
   if (fISM) delete fISM;
   fISM = new TUMediumTXYZ();
   fISM->SetClass(init_pars, group, "ISM", is_verbose, f_log);
   if (is_verbose)
      fprintf(f_log, "\n");

   // 4. Set Transport parameters
   if (is_verbose)
      fprintf(f_log, "%s### Set transport for %s\n", indent.c_str(), propag_model.c_str());
   if (fTransport) delete fTransport;
   fTransport = new TUTransport();
   fTransport->SetClass(init_pars, group, "Transport", is_verbose, f_log);
   if (is_verbose)
      fprintf(f_log, "\n");


   // 5. Set Sources (first delete previously existing sources)
   if (is_verbose)
      fprintf(f_log, "%s### Set sources for %s\n", indent.c_str(), propag_model.c_str());
   if (fSrcAstro) {
      for (Int_t i = 0; i < GetNSrcAstro(); ++i) {
         if (fSrcAstro[i]) delete fSrcAstro[i];
         fSrcAstro[i] = NULL;
      }
      delete[] fSrcAstro;
      fSrcAstro = NULL;
   }
   fNSrcAstro = 0;
   if (fSrcDM) {
      for (Int_t i = 0; i < GetNSrcDM(); ++i) {
         if (fSrcDM[i]) delete fSrcDM[i];
         fSrcDM[i] = NULL;
      }
      delete[] fSrcDM;
      fSrcDM = NULL;
   }
   fNSrcDM = 0;
   // Collect, for the the group (propag_model) and subgroup (SrcSteadyState
   // and SrcPointLike), a list of all parameters whose 'val' is formatted
   // to be 'XXX|YYY...'), where XXX is always the source name. Gather all
   // the names among all parameters, and then loop on all found names to
   // fill sources.
   const Int_t n = 2;
   string subgroup[n] = {"SrcSteadyState", "SrcPointLike"};
   vector<Int_t> indices;
   vector<vector<string> > names;
   Int_t n_src = 0;
   Int_t n_src_astro = 0;
   Int_t n_src_dm = 0;
   // Loop and fill two src subgroups (SrcSteadyState and SrcPointLike)
   for (Int_t i = 0; i < n; ++i) {
      if (is_verbose)
         fprintf(f_log, "%s   - Extract (from init. file) %s  sources for %s\n",
                 indent.c_str(), subgroup[i].c_str(), propag_model.c_str());
      indices = init_pars->IndicesPars(group, subgroup[i]);
      vector<string> srcnames;
      Int_t n_astro = 0;
      Int_t n_dm = 0;

      // Loop on all init parameter indices for this #group#subgroup,
      // to sort what source names must be considered (discard
      // repeated names or empty)
      for (Int_t j = 0; j < (Int_t)indices.size(); ++j) {
         string par_j = init_pars->GetParEntry(indices[j]).GetVal();
         // Skip parameter if empty
         if (par_j == "-")
            continue;
         // Extract source name (format is ASTRO_STD|PARAMS)
         vector<string> name_etc;
         TUMisc::String2List(par_j, "|", name_etc);
         // If name not in list, add it
         if (!TUMisc::IsInList(srcnames, name_etc[0], false))
            srcnames.push_back(name_etc[0]);
      }

      n_src += srcnames.size();
      names.push_back(srcnames);
      for (Int_t j = 0; j < Int_t(srcnames.size()); ++j) {
         if (srcnames[j].substr(0, 5) == "ASTRO")
            ++n_astro;
         else if (srcnames[j].substr(0, 2) == "DM")
            ++n_dm;
      }
      n_src_astro += n_astro;
      n_src_dm += n_dm;

      if (is_verbose) {
         if (n_astro + n_dm == 0)
            fprintf(f_log, " => NONE\n");
         else
            fprintf(f_log, " => %d (ASTRO=%d, DM=%d)\n",  n_astro + n_dm, n_astro, n_dm);
      }
   }

   // Loop on all names found, create source, and add in ASTRO or DM
   fNSrcAstro = n_src_astro;
   if (fNSrcAstro > 0)
      fSrcAstro = new TUSrcMultiCRs*[fNSrcAstro];
   fNSrcDM = n_src_dm;
   if (fNSrcDM > 0)
      fSrcDM = new TUSrcMultiCRs*[fNSrcDM];
   Int_t i_astro = 0;
   Int_t i_dm = 0;
   for (Int_t i = 0; i < n; ++i) {
      for (Int_t j = 0; j < Int_t(names[i].size()); ++j) {
         if (is_verbose)
            fprintf(f_log, "%s   - Set %s\n", indent.c_str(), names[i][j].c_str());
         TUSrcMultiCRs *src = new TUSrcMultiCRs();
         src->SetClass(init_pars, group, subgroup[i], names[i][j], this, this, is_verbose, f_log);
         // SetClass(TUInitParList *init_pars, string const &group,
         //          string const &subgroup, string const &src_name,
         //          TUCRList *used_crs, TUSrcTemplates *used_templates);

         // Whether this source is ASTRO or DM, add it to fSrcAstro or fSrcDM
         if (src->IsAstroOrDM()) {
            fSrcAstro[i_astro] = src;
            ++i_astro;
         } else {
            fSrcDM[i_dm] = src;
            ++i_dm;
         }
         src = NULL;
      }
   }
   if (is_verbose)
      fprintf(f_log, "\n");

   if (is_verbose)
      fprintf(f_log, "%s[TUModelBase::SetClass_ModelBaseDepMembers] <DONE>\n\n", indent.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUModelBase::SetClass_ModelBaseIndepMembers(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Sets propagation base classes from TUInitParList pointer.
   //  init_pars         USINE initialisation parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUModelBase::SetClass_ModelBaseIndepMembers]\n", indent.c_str());

   // Set inherited classes

   // 1. CR and E axes, and allocate generic coordinates
   TUAxesCrE::SetClass(init_pars, is_verbose, f_log);
   if (fCoordE)
      delete fCoordE;
   fCoordE = new TUCoordE();
   if (fCoordsTXYZ)
      delete fCoordsTXYZ;
   fCoordsTXYZ = new TUCoordTXYZ();
   if (is_verbose)
      fprintf(f_log, "\n");

   // 2. Propagation switches
   TUPropagSwitches::SetClass(init_pars, is_verbose, f_log);
   if (is_verbose)
      fprintf(f_log, "\n");

   // 3. Sets of TOA CR data
   TUDataSet::SetClass(init_pars, is_verbose, f_log);
   if (is_verbose)
      fprintf(f_log, "\n");

   // 4. List of qties (and data) to which to normalise fluxes
   TUNormList::SetClass(init_pars, this, is_verbose, f_log, this);
   if (is_verbose)
      fprintf(f_log, "\n");

   // 5. List of src templates
   TUSrcTemplates::SetClass(init_pars, is_verbose, f_log);
   if (is_verbose)
      fprintf(f_log, "\n");

   // 6. All X-sections (inelastic, production, etc.)
   TUXSections::SetClass(init_pars, is_verbose, f_log, this);
   if (is_verbose)
      fprintf(f_log, "\n");

   if (is_verbose)
      fprintf(f_log, "%s[TUModelBase::SetClass_ModelBaseIndepMembers] <DONE>\n\n", indent.c_str());


   TUMessages::Indent(false);
}

//______________________________________________________________________________
Bool_t TUModelBase::SetClass_ModelSpecific(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Set class members specific to model selected.
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   if (fModel == kBASE)
      return false;
   else {
      string message = "You must override this virtual method in class for " + GetModelName();
      TUMessages::Error(f_log, "TUModelBase", "SetClass_ModelSpecific", message);
      return is_verbose;
   }
   return false;
}


//______________________________________________________________________________
void TUModelBase::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message = "Load model base ingredients and test.";
   TUMessages::Test(f, "TUModelBase", message);

   // Set class and test methods
   fprintf(f, " * Load base and check prints\n");

   // Test SetClass()
   fprintf(f, "   > SetClass(init_pars=\"%s\", BASE, false, f_log=f);\n", init_pars->GetFileNames().c_str());
   SetClass(init_pars, "BASE", false, f);
   fprintf(f, " * Check Print() methods\n");
   fprintf(f, "   > PrintTargetList(f)\n");
   PrintTargetList(f);
   fprintf(f, "   > PrintTertiaryList(f)\n");
   PrintTertiaryList(f);
   fprintf(f, "   > PrintXSecFiles(f)\n");
   PrintXSecFiles(f);
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, "   > PrintSummaryBase(f)\n");
   PrintSummaryBase(f);
   fprintf(f, "   > PrintPars(f)\n");
   PrintPars(f);
   fprintf(f, "   > PrintSummary(f)\n");
   PrintSummary(f);
   fprintf(f, "   > PrintSummaryTransport(f)\n");
   PrintSummaryTransport(f);
   fprintf(f, "   > PrintSummaryModel(f)\n");
   PrintSummaryModel(f);
}

//______________________________________________________________________________
Bool_t TUModelBase::TUI_ModifySrcParameters()
{
   //--- Text User Interface to modify source parameters.

   // Create a temporary TUFreeParList to handle all source parameters
   TUFreeParList tmp_pars;

   // Get free pars for Astro sources
   for (Int_t k = 0; k < fNSrcAstro; ++k) {
      TUFreeParList *tmp = fSrcAstro[k]->GetFreePars();
      if (tmp) {
         for (Int_t i = 0; i < tmp->GetNPars(); ++i)
            tmp_pars.AddPar(tmp->GetParEntry(i), true);
      }
      tmp = NULL;
   }

   // Get free pars for DM sources
   for (Int_t k = 0; k < fNSrcDM; ++k) {
      TUFreeParList *tmp = fSrcDM[k]->GetFreePars();
      if (tmp) {
         for (Int_t i = 0; i < tmp->GetNPars(); ++i)
            tmp_pars.AddPar(tmp->GetParEntry(i), true);
      }
      tmp = NULL;
   }

   PrintSources(stdout, true);
   tmp_pars.TUI_Modify();

   if (tmp_pars.GetParListStatus()) {
      for (Int_t k = 0; k < fNSrcAstro; ++k)
         fSrcAstro[k]->UpdateFromFreeParsAndResetStatus();
      for (Int_t k = 0; k < fNSrcDM; ++k)
         fSrcDM[k]->UpdateFromFreeParsAndResetStatus();
      return true;
   } else
      return false;
}

//______________________________________________________________________________
void TUModelBase::TUI_SetTXYZ(TUCoordTXYZ &coords)
{
   //--- Text User Interface to set coords for this axis.
   // OUTPUT
   //  coords            Coordinates to select

   string indent = TUMessages::Indent(true);

   // Select some coordinates
   cout << indent << "   ### Select coordinates in this model geometry" << endl;

   printf("%s   Geometry: %s\n", indent.c_str(), fAxesTXYZ->GetGeomName().c_str());
   for (Int_t i = 0; i < fAxesTXYZ->GetNAxes(); ++i) {
      // If axis not used, skip
      if (!fAxesTXYZ->IsTXYZ(i))
         continue;

      TUAxis *axis = fAxesTXYZ->GetAxis(i);
      printf("%s       %s-axis in range {%.3le,%.3le} %s\n", indent.c_str(),
             axis->GetName().c_str(), axis->GetMin(), axis->GetMax(), axis->GetUnit(true).c_str());
      axis = NULL;
   }

   // Get new position
   printf("   Current position: %s\n",
          coords.FormVals(true, fAxesTXYZ->IsT(), fAxesTXYZ->GetNDim()).c_str());
   for (Int_t i = 0; i < fAxesTXYZ->GetNAxes(); ++i) {
      // If axis not used, skip
      if (!fAxesTXYZ->IsTXYZ(i))
         continue;
      TUAxis *axis = fAxesTXYZ->GetAxis(i);
      cout << indent << "       New " << axis->GetName() << " value " << axis->GetUnit(true) << ": ";
      Double_t val;
      cin >> val;
      if (val > axis->GetMax() || axis->GetMin()) {
         cout << indent << "   => Value selected out of range!" << endl;
         i -= 1;
      } else
         coords.SetVal(i, val);
      axis = NULL;
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUModelBase::UpdateCoordE(Int_t j_cr, Int_t k_ekn, Int_t halfbin_status)
{
   //--- Updates E coordinate (bin and values).
   //  j_cr              CR index
   //  k_ekn             E bin index
   //  halfbin_status    Exact E bin position used:
   //                       -1: half-lower bin k_ekn-1/2
   //                        0: k_ekn
   //                       +1: half-upper bin k_ekn+1/2

   fCoordE->SetBinE(k_ekn, halfbin_status);
   ConvertBinE2ValsE(j_cr, fCoordE, true/*is_use*/);
}

//______________________________________________________________________________
void TUModelBase::UpdateAllParListStatusFromIndividualParStatus()
{
   //--- Updates status for all families of free parameters.

   // Geometry
   if (fAxesTXYZ && fAxesTXYZ->GetFreePars())
      fAxesTXYZ->GetFreePars()->UpdateParListStatusFromIndividualStatus();

   // CR list
   if (TUAxesCrE::TUCRList::GetFreePars())
      TUAxesCrE::TUCRList::GetFreePars()->UpdateParListStatusFromIndividualStatus();

   // ISM
   if (fISM && fISM->GetFreePars())
      fISM->GetFreePars()->UpdateParListStatusFromIndividualStatus();

   // Solar Modulation
   if (fSolMod && fSolMod->GetFreePars())
      fSolMod->GetFreePars()->UpdateParListStatusFromIndividualStatus();

   // Astro. sources
   for (Int_t s = 0; s < GetNSrcAstro(); ++s) {
      if (GetSrcAstro(s)->GetFreePars())
         GetSrcAstro(s)->GetFreePars()->UpdateParListStatusFromIndividualStatus();
   }

   // DM sources
   for (Int_t s = 0; s < GetNSrcDM(); ++s) {
      if (GetSrcDM(s)->GetFreePars())
         GetSrcDM(s)->GetFreePars()->UpdateParListStatusFromIndividualStatus();
   }

   // Transport
   if (fTransport && fTransport->GetFreePars())
      fTransport->GetFreePars()->UpdateParListStatusFromIndividualStatus();

   // XS
   if (TUXSections::GetFreePars())
      TUXSections::GetFreePars()->UpdateParListStatusFromIndividualStatus();
}

//______________________________________________________________________________
void TUModelBase::UseSolMod(TUSolModVirtual *sol_mod)
{
   //--- Sets Solar modulation model to use.
   //  sol_mod           Sol.mod. model

   if (fSolMod && fIsDeleteSolMod)
      delete fSolMod;

   fSolMod = sol_mod;
   fIsDeleteSolMod = false;
}
