// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <iostream>
// ROOT include
// USINE include
#include "../include/TUMath.h"
#include "../include/TUSolMod0DFF.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUSolMod0DFF                                                         //
//                                                                      //
// Force-Field approximation of Solar modulation.                       //
//                                                                      //
// The class TUSolMod0DFF (inheriting from the abstract                 //
// class TUSolModVirtual) provides the solution of the Force-Field      //
// equation for Solar modulation (and demodulation). The latter is      //
// an approximation of the full equation for the transport of charged   //
// particles in the Solar cavity, to calculate top-of-atmosphere        //
// (TOA) fluxes from interstellar (IS) ones. For a description of the   //
// force-field approximation, see the references below.                 //
//                                                                      //
// NB: IS and TOA fluxes are J=dJ/dEkn [(m2 s sr GeV/n)^{-1}].          //
//                                                                      //
// Pros of the Force-Field approximation:                               //
//    - only one free parameter, the solar modulation level phi [GV];
//    - simple and fast (analytical solution) to implement;
//    - successful for most of the energy range;
//    - allows to demodulate (IS from TOA), which is not possible
//      for more realistic modulation model).
// Cons:                                                                //
//    - phenomenological and effective description;
//    - does not apply to r other than 1 AU in the Solar cavity;
//    - fails at low energy (see Perko).
//                                                                      //
// BEGIN_HTML
// <b>Useful references:</b> <a href="http://adsabs.harvard.edu/abs/1968ApJ...154.1011G" target="_blank">Gleeson & Axford (1968)</a>,
// <a href="http://adsabs.harvard.edu/abs/1987A%26A...184..119P" target="_blank">Perko (1987)</a> and
// <a href="http://adsabs.harvard.edu/abs/1992ApJ...397..153P" target="_blank">Perko (1992)</a>.<br><br>
// <b>Force-Field equations and parameters:</b>
// END_HTML
//BEGIN_LATEX(fontsize=14)
// In this model, the TOA and IS total energy E are related by:
//   #frac{E^{TOA}}{A} = #frac{E^{IS}}{A} - #frac{|Z|}{A} #times #phi.
// The force-field modulation parameter #phi has the dimension of a rigidity
// (or an electric potential), and its value varies according the 11-years
// solar cycle, being greater for a period of maximal Solar activity. Often
// people refer to an equivalent quantity #Phi, with
//   #Phi=#frac{|Z|}{A}#times #phi.
// The TOA and IS fluxes are then related through the simple rule
//   #frac{dJ^{TOA} (E^{TOA})/dEkn}{dJ^{IS}(E^{IS})/dEkn} = ( #frac{p^{TOA}}{p^{IS}})^{2}
// where p is the momentum of the CR.
//END_LATEX
//////////////////////////////////////////////////////////////////////////

ClassImp(TUSolMod0DFF)

//______________________________________________________________________________
TUSolMod0DFF::TUSolMod0DFF() : TUSolModVirtual()
{
   // ****** Default constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUSolMod0DFF::TUSolMod0DFF(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log) : TUSolModVirtual()
{
   // ****** Default constructor ******
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   Initialise(false);
   SetClass(init_pars, is_verbose, f_log);
}

//______________________________________________________________________________
TUSolMod0DFF::~TUSolMod0DFF()
{
   // ****** default destructor ******

   Initialise(true);
   ParsModExps_Delete();
}
//______________________________________________________________________________
void TUSolMod0DFF::DemodulateData(TUCREntry const &cr, TUDataEntry *data,
                                  Bool_t is_ratio, Bool_t is_verbose) const
{
   //--- Demodulates/modulates a CR data entry (from its phi). Is it exact
   //    only if CR is an isotope, otherwise 'cr' is a pseudo CR to provide
   //    the approximate (de)modulation.
   // INPUTS:
   //  cr                CR or 'pseudo' CR
   //  data              CR data to (de)modulate
   //  is_ratio          Whether CR is a ratio or a flux
   //  is_verbose        Print or not result of modul/demodul (check)
   // OUTPUT:
   //  data              Demodulated (IS) CR data

   // Create vector for TOA and IS quantities:
   //   - 3 values for energy (<e>, ebinl, ebinu)
   //   - 5 values for flux/ratio (y, yerrstatl,  yerrstatu, yerrsystl,  yerrsystu)
   const Int_t n_e = 3;
   const Int_t n_y = 5;
   vector<Double_t> ekn_is(n_e, 0.);
   vector<Double_t> ekn_toa(n_e, 0.);
   vector<Double_t> y_is(n_y, 0.);
   vector<Double_t> y_toa(n_y, 0.);
   y_toa[0] = data->GetY();
   y_toa[1] = data->GetY() - data->GetYErrStatL();
   y_toa[2] = data->GetY() + data->GetYErrStatU();
   y_toa[3] = data->GetY() - data->GetYErrSystL();
   y_toa[4] = data->GetY() + data->GetYErrSystU();

   // Demodulation (TOA->IS) in 3 steps:
   //    1. EType->EKN
   //    2. Modulate EKN
   //    3. EKN->EType
   // 1. Ekn->Etype
   ekn_toa[0] = data->GetEMean();
   ekn_toa[1] = data->GetEBinL();
   ekn_toa[2] = data->GetEBinU();
   for (Int_t j = 0; j < n_e; ++j)
      ekn_toa[j] = TUPhysics::ConvertE(ekn_toa[j], data->GetEType(), kEKN, cr.GetA(), cr.GetZ(), cr.GetmGeV());
   for (Int_t j = 0; j < n_y; ++j)
      y_toa[j] *= TUPhysics::dNdEin_to_dNdEout(ekn_toa[j], data->GetEType(), kEKN, cr.GetA(), cr.GetZ(), cr.GetmGeV());

   // 2. Demodulate
   //   - Y and ErrY @ <E>
   for (Int_t j = 0; j < n_y; ++j) {
      TOAtoIS_singleEkn(cr, ekn_is[0], y_is[j], ekn_toa[0], y_toa[j], data->GetExpphi(), is_ratio);
   }
   //   - E, [Emin,Emax] @ Y
   for (Int_t j = 1; j < n_e; ++j) {
      Double_t dummy = 0.;
      TOAtoIS_singleEkn(cr, ekn_is[j], dummy, ekn_toa[j], y_toa[0], data->GetExpphi(), is_ratio);
   }

   // 3. Ekn->EType
   for (Int_t j = 0; j < n_e; ++j)
      ekn_is[j] = TUPhysics::ConvertE(ekn_is[j], kEKN, data->GetEType(), cr.GetA(), cr.GetZ(), cr.GetmGeV());
   for (Int_t j = 0; j < n_y; ++j)
      y_is[j] *= TUPhysics::dNdEin_to_dNdEout(ekn_is[j], kEKN, data->GetEType(), cr.GetA(), cr.GetZ(), cr.GetmGeV());


   if (is_verbose) {
      cout << "Modulated (TOA) / Demodulated (IS) / Re-modulated (TOA):" << endl;
      data->Print(stdout, 1);
   }

   // Copy demodulated (IS) data
   data->SetE(ekn_is[0], ekn_is[1], ekn_is[2]);
   data->SetY(y_is[0]);
   data->SetYErrStat(-fabs(y_is[0] - y_is[1]), +fabs(y_is[0] - y_is[2]));
   data->SetYErrSyst(-fabs(y_is[0] - y_is[3]), +fabs(y_is[0] - y_is[4]));


   // Check (if verbose) if re-modulation returns the original TOA values
   if (!is_verbose)
      return;
   else {
      TUDataEntry data_is;
      data_is.Copy(*data);
      data_is.Print(stdout, 1);

      // Re-modulation (IS->TOA) in 3 steps:
      //    1. EType->EKN
      //    2. Modulate EKN
      //    3. EKN->EType
      // 1. EType->Ekn
      for (Int_t j = 0; j < n_e; ++j)
         ekn_toa[j] = TUPhysics::ConvertE(ekn_toa[j], data->GetEType(), kEKN, cr.GetA(), cr.GetZ(), cr.GetmGeV());
      for (Int_t j = 0; j < n_y; ++j)
         y_toa[j] *= TUPhysics::dNdEin_to_dNdEout(ekn_toa[j], data->GetEType(), kEKN, cr.GetA(), cr.GetZ(), cr.GetmGeV());
      // 2. Re-modulate
      //   - Y and ErrY @ <E>
      for (Int_t j = 0; j < n_y; ++j)
         IStoTOA_singleEkn(cr, ekn_is[0], y_is[j], ekn_toa[0], y_toa[j], data->GetExpphi(), is_ratio);
      //   - E, [Emin,Emax] @ Y
      for (Int_t j = 1; j < n_e; ++j) {
         Double_t dummy = 0.;
         IStoTOA_singleEkn(cr, ekn_is[j], y_is[0], ekn_toa[j], dummy, data->GetExpphi(), is_ratio);
      }
      // 3. EKn->Etype
      for (Int_t j = 0; j < n_e; ++j)
         ekn_toa[j] = TUPhysics::ConvertE(ekn_toa[j], kEKN, data->GetEType(), cr.GetA(), cr.GetZ(), cr.GetmGeV());
      for (Int_t j = 0; j < n_y; ++j)
         y_toa[j] *= TUPhysics::dNdEin_to_dNdEout(ekn_toa[j], kEKN, data->GetEType(), cr.GetA(), cr.GetZ(), cr.GetmGeV());

      // Copy TOA data and print
      data->SetE(ekn_toa[0], ekn_toa[1], ekn_toa[2]);
      data->SetY(y_toa[0]);
      data->SetYErrStat(-fabs(y_toa[0] - y_toa[1]), +fabs(y_toa[0] - y_toa[2]));
      data->SetYErrSyst(-fabs(y_toa[0] - y_toa[3]), +fabs(y_toa[0] - y_toa[4]));
      data->Print(stdout, 1);

      // N.B.: yet, this is IS data that we want to return!
      data->Copy(data_is);
   }
}

//______________________________________________________________________________
Double_t TUSolMod0DFF::EknIStoEknTOA(TUCREntry const &cr, Double_t const &ekn_is,
                                     Double_t const &phi_gv) const
{
   //--- Returns the modulated kinetic energy per nucleon (TOA) from the
   //    unmodulated (IS) kinetic energy per nucleon ekn_is, for the CR
   //    species "cr", and a modulation level phi_gv [GV].
   //    N.B.: this applies for any nuclear species (A,Z) and leptons.
   //    For the latter (e- and e+), we force A=1 so that Ekn is defined (=Ek).
   //  cr                CR entry to modulate
   //  ekn_is            IS (unmodulated) kinetic energy per nucleon [GeV/n]
   //  phi_gv            Force-Field modulation parameter [GV]

   // If phi small enough, does not need to modulate!
   if (phi_gv < 1.e-5) return ekn_is;

   Double_t Z = Double_t(fabs(cr.GetZ()));
   Double_t A = Double_t(cr.GetA());
   if (A == 0) A = 1;
   // mass "per nucleon" (for the lepton, A=1 so that it is the mass of the lepton)
   Double_t m_gevn = cr.GetmGeV() / A;

   // Critical momentum per nucleon...
   Double_t pnC = Z * fRig0 / A;
   // Critical Total energy per nucleon
   Double_t EtnC = sqrt(m_gevn * m_gevn + pnC * pnC);
   Double_t EtnTrans = EtnC + Z / A * phi_gv;
   Double_t EtnMin   = EtnTrans + pnC * log(m_gevn / (EtnC + pnC));

   // Total IS energy per nucleon
   Double_t EtnIS = ekn_is + m_gevn;

   if (EtnIS <= 0.) return 0.;
   else if (EtnIS <= EtnTrans) {
      Double_t EtnTOA = m_gevn * cosh((EtnIS - EtnMin) / pnC);
      Double_t ekn_toa = EtnTOA - m_gevn;
      return ekn_toa;
   } else {
      Double_t EtnTOA = EtnIS - Z / A * phi_gv;
      Double_t ekn_toa = EtnTOA - m_gevn;
      return ekn_toa;
   }
}

//______________________________________________________________________________
Double_t TUSolMod0DFF::EknTOAtoEknIS(TUCREntry const &cr, Double_t const &ekn_toa,
                                     Double_t const &phi_gv) const
{
   //--- Returns the unmodulated kinetic energy per nucleon (IS) from the
   //    modulated (TOA) kinetic energy per nucleon ekn_toa, for the CR species
   //    "cr", and a modulation level phi_gv [GV].
   //    N.B.: this applies for any nuclear species (A,Z) and leptons. For the
   //    latter (e- and e+), we force A=1 so that Ekn is defined (=Ek).
   //  cr                CR entry to demodulate
   //  ekn_toa           TOA (modulated) kinetic energy per nucleon [GeV/n]
   //  phi_gv            Force-Field modulation parameter [GV]

   // If phi_gv small enough, does not need to modulate!
   if (phi_gv < 1.e-5) return ekn_toa;

   Double_t Z = Double_t(fabs(cr.GetZ()));
   Double_t A = Double_t(cr.GetA());
   if (A == 0.) A = 1.;

   // mass "per nucleon" (for the lepton, A=1 so that it is the mass of the lepton)
   Double_t m_gevn = cr.GetmGeV() / A;
   // Critical momentum per nucleon...
   Double_t pnC = Z * fRig0 / A;
   // Critical Total energy per nucleon
   Double_t EtnC = sqrt(m_gevn * m_gevn + pnC * pnC);

   // Total energy per nucleon
   Double_t EtnTOA = ekn_toa + m_gevn;
   Double_t EtnTrans = EtnC + Z / A * phi_gv;
   Double_t EtnMin   = EtnTrans + pnC * log(m_gevn / (EtnC + pnC));

   Double_t EtnIS = EtnTOA + Z / A * phi_gv;
   if (EtnIS > EtnTrans) return (EtnIS - m_gevn);
   else return ((EtnMin + pnC * acosh(EtnTOA / m_gevn)) - m_gevn);
}

//______________________________________________________________________________
void TUSolMod0DFF::Initialise(Bool_t is_delete)
{
   //---Initialises class members (and delete if required).
   //  is_delete         Whether to delete or just initialise

   TUFreeParList::Initialise(is_delete);
   ParsModExps_Delete();

   fRig0 = -1.;
   fIndexphi = -1;
}

//______________________________________________________________________________
void TUSolMod0DFF::IStoTOA(TUCREntry const &cr, Int_t n_ekn, Double_t *vekn_is,
                           Double_t *vflux_is, Double_t *vekn_toa,
                           Double_t *vflux_toa) const
{
   //--- Modulates (TOA from IS flux) for CR species 'cr' on vekn_is_toa->GetN()
   //    energy per nucleon bins and a modulation parameter phi_gv [GV].
   //    N.B.: It is up to the user to check that all flux arrays have the correct
   //    number of bins (same as the number of energy bins)!
   // INPUTS:
   //  cr                CR entry to modulate
   //  n_ekn             Number of energy in spectra to modulate
   //  vekn_is[n_ekn]    IS (unmodulated) kinetic energies per nucleon [GeV/n]
   //  vflux_is[n_ekn]   IS (unmodulated) fluxes for cr @ vekn_is
   //  phi_gv            Force-Field modulation parameter [GV]
   // OUTPUTS:
   //  vekn_toa[n_ekn]   TOA (modulated) kinetic energy per nucleon [GeV/n]
   //  vflux_toa[n_ekn]  TOA (modulated) flux for cr @ vekn_toa


   for (Int_t i = n_ekn - 1; i >= 0; --i)
      IStoTOA_singleEkn(cr, vekn_is[i], vflux_is[i], vekn_toa[i], vflux_toa[i], EvalPhi(cr));
}

//______________________________________________________________________________
void TUSolMod0DFF::IStoTOA(TUCREntry const &cr, TUAxis *axis_ekn,
                           Double_t *vflux_is, Double_t *vflux_toa, Double_t *r)
{
   //--- Modulates (TOA from IS flux) for CR species 'cr' on axis_ekn->GetN()
   //    energy per nucleon bins and a modulation parameter phi_gv [GV].
   //    N.B.: with this method, the values of *vflux_toa are calculated on
   //    a TOA energy grid that is forced to match the input IS energy grid
   //    (ekn_is=ekn_toa='ekn_is_toa'). It is up to the user to check that
   //    all flux arrays have the correct number of bins (same as the number
   //    of energy bins)!
   // INPUTS:
   //  cr                 CR entry to modulate
   //  axis_ekn[n_ekn]    Shared IS and TOA Ekn grid [GeV/n]
   //  vflux_is[n_ekn]    IS (unmodulated) fluxes for cr @ axis_ekn
   //  r                  Radius at which to calculate modulated fluxes [au] (unused in ForceField)
   // OUTPUT:
   //  vflux_toa[n_ekn]   TOA (modulated) flux for cr @ vekn_ekn_is_toa

   // If fIndexphi not set, do it now!
   if (fIndexphi < 0)
      fIndexphi = TUFreeParList::IndexPar("phi");

   IStoTOA(cr, axis_ekn, vflux_is, vflux_toa);
}

//______________________________________________________________________________
void TUSolMod0DFF::IStoTOA(TUCREntry const &cr, TUAxis *axis_ekn, Double_t *vflux_is,
                           Double_t *vflux_toa) const
{
   //--- Modulates (TOA from IS flux) for CR species 'cr' on axis_ekn->GetN()
   //    energy per nucleon bins and a modulation parameter phi_gv [GV].
   //    N.B.: with this method, the values of *vflux_toa are calculated on
   //    a TOA energy grid that is forced to match the input IS energy grid
   //    (ekn_is=ekn_toa='ekn_is_toa'). It is up to the user to check that
   //    all flux arrays have the correct number of bins (same as the number
   //    of energy bins)!
   // INPUTS:
   //  cr                 CR entry to modulate
   //  axis_ekn           TUAxis object of kinetic energies per nucleon grid [GeV/n]
   //  vflux_is[GetN()]   IS (unmodulated) fluxes for cr @ axis_ekn_is
   // OUTPUT:
   //  vflux_toa[GetN()]  TOA (modulated) flux for cr @ axis_ekn_is

   Int_t n_ekn = axis_ekn->GetN();
   Double_t phi_gv = 0.;

   // TOA Energies rescaled to IS energies...
   for (Int_t i = 0; i < n_ekn; ++i) {
      phi_gv = EvalPhi(cr, axis_ekn->GetVal(i));
      // If phi_gv small enough, does not need to modulate!
      if (phi_gv < 1.e-5) {
         vflux_toa[i] = vflux_is[i];
         continue;
      }

      // We seek the IS energy for which the ekn_toa energy
      // would recover the value of the grid "vekn"
      Double_t tmpekn_is = EknTOAtoEknIS(cr, axis_ekn->GetVal(i), phi_gv);

      // Seek position to interpolate
      Int_t pos = TUMath::BinarySearch(n_ekn, axis_ekn->GetVals(), tmpekn_is) + 1;
      Double_t trash = 0.;
      if (pos == 0)
         IStoTOA_singleEkn(cr, axis_ekn->GetVal(0), vflux_is[0], trash, vflux_toa[i], phi_gv);
      else if (pos == n_ekn)
         IStoTOA_singleEkn(cr, axis_ekn->GetVal(n_ekn - 1), vflux_is[n_ekn - 1], trash, vflux_toa[i], phi_gv);
      else {
         Double_t tmpflux_is = TUMath::Interpolate(tmpekn_is, axis_ekn->GetVal(pos - 1), axis_ekn->GetVal(pos),
                               vflux_is[pos - 1], vflux_is[pos], kLOGLOG);
         IStoTOA_singleEkn(cr, tmpekn_is, tmpflux_is, trash, vflux_toa[i], phi_gv);
      }
   }
}

//______________________________________________________________________________
void TUSolMod0DFF::IStoTOA_singleEkn(TUCREntry const &cr, Double_t const &ekn_is,
                                     Double_t const &flux_is, Double_t &ekn_toa,
                                     Double_t &flux_toa, Double_t const &phi_gv,
                                     Bool_t is_ratio) const
{
   //--- Modulates IS flux (into TOA) at a single kinetic energy per nucleon,
   //    for the CR species "cr" and a modulation parameter phi_gv [GV].
   //    N.B.: the modulation is performed for a CR (m,A,Z), but note that the
   //    option "is_ratio" allows to modulate a ratio of two CRs, although only
   //    approximately. This is done by assuming that all species of the ratio
   //    have similar A and Z (the error due to this approximation is generally
   //    smaller than the experimental error bars if the species in the ratio
   //    are close in A and Z, but use at your own risk!).
   // INPUTS:
   //  cr                CR entry to modulate
   //  ekn_is            IS (unmodulated) kinetic energy per nucleon [GeV/n]
   //  flux_is           IS (unmodulated) flux for cr @ ekn_is
   //  phi_gv            Force-Field modulation parameter [GV]
   //  is_ratio          If true, approximate modulation (assumes all nuclei have the same A/Z)
   // OUTPUTS:
   //  ekn_toa           TOA (modulated) kinetic energy per nucleon [GeV/n]
   //  flux_toa          TOA (modulated) flux for cr @ ekn_toa

   // If phi_gv small enough, does not need to modulate!
   if (phi_gv < 1.e-5) {
      ekn_toa = ekn_is;
      flux_toa = flux_is;
      return;
   }

   ekn_toa = EknIStoEknTOA(cr, ekn_is, phi_gv);

   Double_t Z = Double_t(fabs(cr.GetZ()));
   Double_t A = Double_t(cr.GetA());
   if (A == 0.) A = 1.;

   // mass "per nucleon" (or for the lepton as A=1 in this case)
   Double_t m_gevn = cr.GetmGeV() / A;

   // Critical momentum per nucleon...
   Double_t pnC = Z * fRig0 / A;
   // Critical Total energy per nucleon
   Double_t EtnC = sqrt(m_gevn * m_gevn + pnC * pnC);
   Double_t EtnIS = ekn_is + m_gevn;
   Double_t EtnTrans = EtnC + Z / A * phi_gv;
   Double_t EtnMin   = EtnTrans + pnC * log(m_gevn / (EtnC + pnC));

   // Total energy per nucleon lower than the critical energy: does not reach Earth!
   if (EtnIS <= EtnMin) flux_toa = 0.;
   else {
      Double_t EtnTOA = ekn_toa + m_gevn;
      Double_t pnIS  = sqrt(EtnIS * EtnIS - m_gevn * m_gevn);
      Double_t pnTOA = sqrt(EtnTOA * EtnTOA - m_gevn * m_gevn);
      // if a ratio is modulated, only the energy changes, although this is valid
      // only for isotopes having Z/A not too different one from another
      if (is_ratio == false) flux_toa = TUMath::Sqr(pnTOA / pnIS) * flux_is;
      else flux_toa = flux_is;
   }
}

//______________________________________________________________________________
void TUSolMod0DFF::Print(FILE *f, Bool_t is_summary) const
{
   //--- Prints into file f.
   //  f                 File in which to print

   string indent = TUMessages::Indent(true);

   // Model name
   string model = GetModelName();

   if (is_summary)
      fprintf(f, "%s Sol.Mod=%s, phi=%lf GV\n", indent.c_str(),
              model.c_str(), TUFreeParList::GetParEntry(0)->GetVal(false));
   else {
      TUMessages::Separator(f, model);
      // Fixed parameters
      fprintf(f, "%s => Fixed parameters: Rig0=%f GV\n", indent.c_str(), fRig0);
      // Free parameters
      if (TUFreeParList::GetNPars() > 0)
         TUFreeParList::PrintPars(f);
   }

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUSolMod0DFF::SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Sets class members from initialisation parameters.
   //  init_pars         TUInitParList class of initialisation parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string group = "SolMod0DFF";
   string subgroup = "Base";

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUSolMod0DFF::SetClass]\n", indent.c_str());

   Initialise(true);

   // Allocate and add free Force-Field parameters (delete if no free param. found)
   TUFreeParList::AddPars(init_pars, group, subgroup, is_verbose, f_log);
   if (is_verbose)
      TUFreeParList::PrintPars(f_log);

   string param = "Rig0";
   if (is_verbose)
      fprintf(f_log, "%s### Set Force-Field Solar Modulation [%s#%s#%s]\n",
              indent.c_str(), group.c_str(), subgroup.c_str(), param.c_str());

   SetRig0(atof(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, param)).GetVal().c_str()));

   fIndexphi = TUFreeParList::IndexPar("phi");

   if (is_verbose)
      fprintf(f_log, "%s[TUSolMod0DFF::SetClass] <DONE>\n\n", indent.c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUSolMod0DFF::Setphi(Double_t const &small_phi_gv)
{
   //--- Sets modulation level (small phi).
   //  small_phi         Modulation level phi (Phi=|Z|/A * phi) in [GV]

   // Check whether phi is set as a free parameter: if not, add it!
   if (TUFreeParList::GetNPars() == 0) {
      AddPar("phi", "GV", 0.5);
      fIndexphi = TUFreeParList::IndexPar("phi");
   }

   TUFreeParList::SetParVal(0, small_phi_gv);
}

//______________________________________________________________________________
void TUSolMod0DFF::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message = "Force-Field Solar modulation model.";
   TUMessages::Test(f, "TUSolMod0DFF", message);

   // Set class
   fprintf(f, " * Check class methods\n");
   fprintf(f, "   > SetClass(init_pars=\"%s\", is_verbose=false, f_log=f);\n", init_pars->GetFileNames().c_str());
   SetClass(init_pars, false, f);
   fprintf(f, "   > SetClass(init_pars=\"%s\", true, f);\n", init_pars->GetFileNames().c_str());
   SetClass(init_pars, true, f);
   fprintf(f, "   > Print(f, s_summary=false)\n");
   Print(f, false);
   fprintf(f, "   > TUFreeParList::PrintPars(f);\n");
   TUFreeParList::PrintPars(f);
   fprintf(f, "   > TUFreeParList::SetParVal(TUFreeParList::IndexPar(\"phi\"), 1.);\n");
   TUFreeParList::SetParVal(TUFreeParList::IndexPar("phi"), 1.);
   fprintf(f, "   > TUFreeParList::PrintPars(f);\n");
   TUFreeParList::PrintPars(f);
   fprintf(f, "   > TUFreeParList::Initialise(true);\n");
   TUFreeParList::Initialise(true);
   fprintf(f, "   > TUFreeParList::PrintPars(f);\n");
   TUFreeParList::PrintPars(f);
   fprintf(f, "\n");
   fprintf(f, "\n");

   // Check methods
   fprintf(f, " * Create 1H and Electron CR species and check modulation\n");
   string f_charts = TUMisc::GetPath(init_pars->GetParEntry(init_pars->IndexPar("Base", "ListOfCRs", "fChartsForCRs")).GetVal());
   fprintf(f, "   > TUCREntry const &cr_1h = new TUCREntry();\n");
//   fprintf(f, "   > TUCREntry const &cr_e = new TUCREntry();\n");
   fprintf(f, "   > cr_1h.SetEntry(1H, f_charts);");
//   fprintf(f, " cr_e.SetEntry(ELECTRON, f_charts);\n");
   TUCREntry cr_1h;
//   TUCREntry cr_e;
   cr_1h.SetEntry("1H", f_charts);
//   cr_e.SetEntry("ELECTRON", f_charts);
   fprintf(f, "   > cr_1h.Print(f);\n");
   cr_1h.Print(f);
//   fprintf(f, "   > cr_e.Print(f);\n");
//   cr_e.Print(f);
   Double_t ekn_toa = 0.4;
   Double_t ekn_is = 0.4;
   Double_t flux_toa = 0.1;
   Double_t flux_is = 0.1;
   Double_t phi_gv = 0.3;
   fprintf(f, "   >  EknTOAtoEknIS(cr_1h, ekn_toa=0.4GeV/n, phi_gv=0.3GV);  =>  EknIS=%f\n",
           EknTOAtoEknIS(cr_1h, ekn_toa, phi_gv));
//   fprintf(f, "   >  EknTOAtoEknIS(cr_e, ekn_toa=0.4GeV, phi_gv=0.3GV);      =>  EkIS=%f\n",
//           EknTOAtoEknIS(cr_e, ekn_toa, phi_gv));
   fprintf(f, "   >  EknIStoEknTOA(cr_1h, ekn_is=0.4GeV/n, phi_gv=0.3GV);  =>  EknTOA=%f\n",
           EknIStoEknTOA(cr_1h, ekn_is, phi_gv));
//   fprintf(f, "   >  EknIStoEknTOA(cr_e, ekn_is=0.4GeV, phi_gv=0.3GV);      =>  EkTOA=%f\n",
//           EknIStoEknTOA(cr_e, ekn_is, phi_gv));
   fprintf(f, "\n");
   fprintf(f, "   >  IStoTOA_singleEkn(cr_1h, ekn_is, flux_is, [out]ekn_toa, [out]flux_toa, phi_gv);  => ");
   IStoTOA_singleEkn(cr_1h, ekn_is, flux_is, ekn_toa, flux_toa, phi_gv);
   fprintf(f, "  ekn_is=%.3f flux_is=%.3f  ekn_toa=%.3f flux_toa=%.3f\n", ekn_is, flux_is, ekn_toa, flux_toa);
   fprintf(f, "   >  TOAtoIS_singleEkn(cr_1h, [out]ekn_is, [out]flux_is, ekn_toa, flux_toa, phi_gv);  => ");
   TOAtoIS_singleEkn(cr_1h, ekn_is, flux_is, ekn_toa, flux_toa, phi_gv);
   fprintf(f, "  ekn_is=%.3f flux_is=%.3f\n", ekn_is, flux_is);
   fprintf(f, "\n");
//   fprintf(f, "   >  IStoTOA_singleEkn(cr_e, ek_is, flux_is, [out]ek_toa, [out]flux_toa, phi_gv);  => ");
//   IStoTOA_singleEkn(cr_e, ekn_is, flux_is, ekn_toa, flux_toa, phi_gv);
//   fprintf(f, "  ek_is=%.3f flux_is=%.3f  ek_toa=%.3f flux_toa=%.3f\n", ekn_is, flux_is, ekn_toa, flux_toa);
//   fprintf(f, "   >  TOAtoIS_singleEkn(cr_e, [out]ek_is, [out]flux_is, ek_toa, flux_toa, phi_gv);  => ");
//   TOAtoIS_singleEkn(cr_e, ekn_is, flux_is, ekn_toa, flux_toa, phi_gv);
//   fprintf(f, "  ek_is=%.3f flux_is=%.3f\n", ekn_is, flux_is);
   fprintf(f, "\n");
   fprintf(f, "\n");
   // Check creation of parameters for data
   fprintf(f, " * Check creation of free pars for each exp in dataset (subset from selection \"H,He:BESS,AMS:kEKN\")\n");
   SetClass(init_pars, false, f);
   TUDataSet data_set;
   data_set.SetClass(init_pars, false, f);
   TUDataSet *data = data_set.OrphanFormSubSet("H,He:BESS,AMS:kEKN");
   //data->PrintExps();
   fprintf(f, "   > ParsModExps_Create(data);\n");
   ParsModExps_Create(data);
   fprintf(f, "   > ParsModExps_Print(f);\n");
   ParsModExps_Print(f);
   fprintf(f, "   > this->PrintPars(f);\n");
   this->PrintPars(f);
   fprintf(f, "   > ParsModExps_Get(2)->SetParVal(0, 2.);\n");
   ParsModExps_Get(2)->SetParVal(0, 2.);
   fprintf(f, "   > ParsModExps_UpdateSolModPars(2);\n");
   ParsModExps_UpdateSolModPars(2);
   fprintf(f, "   > this->PrintPars(f);\n");
   this->PrintPars(f);
   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, "   > ParsModExps_Delete();\n");
   ParsModExps_Delete();
   fprintf(f, "   > ParsModExps_Create(data);\n");
   ParsModExps_Create(data);
   fprintf(f, "   > ParsModExps_Print(f);\n");
   ParsModExps_Print(f);
   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, "   > ParsModExps_Indices(phi_BESS93_199307_, &i_exp, &i_par)");
   Int_t i_exp, i_par;
   ParsModExps_Indices("phi_BESS93_199307_", i_exp, i_par);
   fprintf(f, " => i_exp=%d, i_par=%d\n", i_exp, i_par);
   fprintf(f, "   > ParsModExps_Get(i_exp)->GetParEntry(i_par)->Print(f);\n");
   ParsModExps_Get(i_exp)->GetParEntry(i_par)->Print(f);
   string exp = data->GetExp(1);
   Int_t i_exp_inpars =  ParsModExps_Index(exp);
   fprintf(f, "   > ParsModExps_Index(\"%s\") = %d\n", exp.c_str(), i_exp_inpars);
   string exp_trimmed = exp;
   TUMisc::RemoveSpecialChars(exp_trimmed);
   fprintf(f, "     [N.B.: TUMisc::RemoveSpecialChars(\"%s\") = %s\n", exp.c_str(), exp_trimmed.c_str());
   fprintf(f, "   > ParsModExps_Get(%d)->GetParEntry(0)->Print(f);\n", i_exp_inpars);
   ParsModExps_Get(i_exp_inpars)->GetParEntry(0)->Print(f);

   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, " N.B.: for usage on real data, type ./bin/usine -id\n");
   fprintf(f, "\n");

   delete data;
   data = NULL;
}

//______________________________________________________________________________
void TUSolMod0DFF::TOAtoIS(TUCREntry const &cr, Int_t n_ekn, Double_t *vekn_is,
                           Double_t *vflux_is, Double_t *vekn_toa,
                           Double_t *vflux_toa, Double_t const &phi_gv) const
{
   //--- Demodulates (calculates IS from TOA flux) for CR species 'cr', n_ekn energies.
   //    energy per nucleon bins and a modulation parameter phi_gv [GV].
   //    N.B.: It is up to the user to check that all flux arrays have the correct
   //    number of bins (same as the number of energy bins)!
   // INPUTS:
   //  cr                CR entry to modulate
   //  n_ekn             Number of energy in spectra to modulate
   //  vekn_is[n_ekn]    IS (unmodulated) kinetic energies per nucleon [GeV/n]
   //  vflux_is[n_ekn]   IS (unmodulated) fluxes for cr @ vekn_is
   //  phi_gv            Force-Field modulation parameter [GV]
   // OUTPUTS:
   //  vekn_toa[n_ekn]   TOA (modulated) kinetic energy per nucleon [GeV/n]
   //  vflux_toa[n_ekn]  TOA (modulated) flux for cr @ vekn_toa

   for (Int_t i = n_ekn - 1; i >= 0; --i) {
      TOAtoIS_singleEkn(cr, vekn_is[i], vflux_is[i], vekn_toa[i], vflux_toa[i], phi_gv);
   }
}

//______________________________________________________________________________
void TUSolMod0DFF::TOAtoIS_singleEkn(TUCREntry const &cr, Double_t &ekn_is, Double_t &flux_is,
                                     Double_t const &ekn_toa, Double_t const &flux_toa,
                                     Double_t const &phi_gv, Bool_t is_ratio) const
{
   //--- Demodulates TOA flux (into IS) at a single kinetic energy per nucleon, for
   //    the CR species "cr" and a modulation parameter phi_gv [GV].
   //    N.B.: the demodulation is performed for a CR (m,A,Z), but note that the
   //    option "is_ratio" allows to demodulate a ratio of two CRs, although only
   //    approximately. This is done by assuming that all species of the ratio have
   //    similar A and Z (the error due to this approximation is generally smaller
   //    than the experimental error bars if the species in the ratio are close in
   //    A and Z, but use at your own risk!).
   // INPUTS:
   //  cr                CR entry to modulate
   //  ekn_toa           TOA (modulated) kinetic energy per nucleon [GeV/n]
   //  flux_toa          TOA (modulated) flux for cr @ ekn_toa
   //  phi_gv            Force-Field modulation parameter [GV]
   //  is_ratio          If true, approximate modulation (assumes all nuclei have the same A/Z)
   // OUTPUTS:
   //  ekn_is            IS (unmodulated) kinetic energy per nucleon [GeV/n]
   //  flux_is           IS (unmodulated) flux for cr @ ekn_is


   // If phi_gv small enough, does not need to modulate!
   if (phi_gv < 1.e-5) {
      ekn_is = ekn_toa;
      flux_is = flux_toa;
      return;
   }

   ekn_is = EknTOAtoEknIS(cr, ekn_toa, phi_gv);

   Double_t Z = Double_t(fabs(cr.GetZ()));
   Double_t A = Double_t(cr.GetA());
   if (A == 0.) A = 1.;

   // mass "per nucleon" (or for the lepton as A=1 in this case)
   Double_t m_gevn = cr.GetmGeV() / A;
   // Critical momentum per nucleon...
   Double_t pnC = Z * fRig0 / A;
   // Critical Total energy per nucleon
   Double_t EtnC = sqrt(m_gevn * m_gevn + pnC * pnC);
   Double_t EtnIS = ekn_is + m_gevn;
   Double_t EtnTrans = EtnC + Z / A * phi_gv;
   Double_t EtnMin   = EtnTrans + pnC * log(m_gevn / (EtnC + pnC));

   // Total energy per nucleon lower than the critical energy: does not reach Earth !
   if (EtnIS <= EtnMin) flux_is = 0.;
   else {
      Double_t EtnTOA = ekn_toa + m_gevn;
      Double_t pnIS  = sqrt(EtnIS * EtnIS - m_gevn * m_gevn);
      Double_t pnTOA = sqrt(EtnTOA * EtnTOA - m_gevn * m_gevn);

      // if a ratio is demodulated, only the energy change, although this
      // is valid only for isotopes having similar Z/A!
      if (is_ratio == false) flux_is = TUMath::Sqr(pnIS / pnTOA) * flux_toa;
      else flux_is = flux_toa;
   }

   //cr.Print();
   //cout << " AVANT:  ekn_TOA=" << ekn_toa << "  ekn_IS=" << ekn_is
   //     << " flux_TOA=" << flux_toa << "  flux_IS=" << flux_is << endl;
}
