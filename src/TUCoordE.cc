// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUCoordE.h"
#include "../include/TUMessages.h"
#include "../include/TUMisc.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUCoordE                                                             //
//                                                                      //
// Energy coordinate index or values (E dependent variable values).     //
//                                                                      //
// The class TUCoordE provides E (energy) coordinate, as a bin or as a  //
// list of energy dependent variable values. The latter is passed an an //
// argument whenever a formula-based description of quantities is used  //
// within the USINE framework. Conversion between E bin and E values is //
// possible using ConvertBinE2ValsE(bincr, TUAxesCrE): bincr is the CR  //
// for which we wish to do the conversion, and TUAxesCrE is required    //
// to provide the 'E-dependent variable' values for this CR and E bin   //
// (the conversion from ValsE to BinE is not implemented).              //
//                                                                      //
// The energy coordinate TUCoordE is an argument for all USINE generic  //
// E-dep quantities (e.g., in TUTransport, TUSrcMultiCRs, ...).         //
// The latter rest on class members derived from TUValsTXYZEVirtual,    //
// realised either as a grid-based description (in TUValsTXYZEGrid),    //
// or as a formula-based description (in TUValsTXYZEFormula). Passing   //
// TUCoordE as an argument of methods in TUTransport, TUSrcMultiCRs,    //
// etc., ensures a simple, efficient, and generic implementation for    //
// method calls in the code (regardless of the model/geometry chosen,   //
// i.e. grid-based or formula-based). Note that this complexity is      //
// usually hidden for the base user in the USINE framework.             //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUCoordE)

//______________________________________________________________________________
TUCoordE::TUCoordE()
{
   // ****** Default constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUCoordE::TUCoordE(TUCoordE const &coords)
{
   // ****** Copy constructor ******
   //  coords               Object to copy

   Initialise(false);
   Copy(coords);
}

//______________________________________________________________________________
TUCoordE::~TUCoordE()
{
   // ****** Default destructor ******
   //

   Initialise(true);
}

//______________________________________________________________________________
void TUCoordE::Copy(TUCoordE const &coords)
{
   //--- Copies coordinates (bins and values) in class.
   //  coords            E-coordinate to copy from

   Initialise(true);

   fBinE = coords.GetBinE();
   fHalfBinStatus = coords.GetHalfBinStatus();
   fIsDeleteValsE = true;
   fNValsE = coords.GetNValsE();

   // Depending whether fValsE (of the object to copy) point
   // to or are values to delete
   fValsE = new Double_t[fNValsE];
   for (Int_t i = 0; i < coords.GetNValsE(); ++i)
      fValsE[i] = coords.GetValE(i);
}

//______________________________________________________________________________
string TUCoordE::FormValsE() const
{
   //--- Returns string of fValsE values.

   vector<string> vec;
   for (Int_t i = 0; i < fNValsE; ++i) {
      string tmp = Form("%.3le", fValsE[i]);
      vec.push_back(tmp);
   }

   return string("(" + TUMisc::List2String(vec, ',') + ")");
}

//______________________________________________________________________________
void TUCoordE::Initialise(Bool_t is_delete)
{
   //--- Initialises all class members.
   //  is_delete         Whether to delete or just initialise

   // Free memory if necessary
   if (is_delete) {
      if (fValsE && fIsDeleteValsE) {
         delete[] fValsE;
      }
      fValsE = NULL;
   }

   fBinE = -1;
   fHalfBinStatus = 0;
   fIsDeleteValsE = true;
   fNValsE = 0;
   fValsE = NULL;
}

//______________________________________________________________________________
void TUCoordE::PrintVals(FILE *f) const
{
   //--- Prints in f coordinate values.
   //  f                    Output file

   string indent = TUMessages::Indent(true);
   string valse = FormValsE();
   if (valse == "()")
      fprintf(f, "%s vals_e is EMPTY!\n", indent.c_str());
   else
      fprintf(f, "%s vals_e=%s (halfbin_status=%d)\n",
              indent.c_str(), valse.c_str(), fHalfBinStatus);
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCoordE::SetValsE(Double_t *valse, Int_t n_valse, Int_t halfbin_status,
                        Bool_t is_use_or_copy)
{
   //--- Fills data member fValsE.
   //  valse             E-dependent variable values to copy
   //  n_valse           Number of variables in valse
   //  halfbin_status    Evaluated at bine (0), bine-1/2 (-1), or bine+1/2 (+1)
   //  is_use_or_copy    Whether ValsE points to existing value or is copied

   // Free memory
   Initialise(true);

   fNValsE = n_valse;
   fHalfBinStatus = halfbin_status;

   // Points to value or copy
   if (is_use_or_copy) {
      fIsDeleteValsE = false;
      fValsE = valse;
   } else {
      fIsDeleteValsE = true;
      fValsE = new Double_t[fNValsE];
      for (Int_t i = 0; i < fNValsE; ++i)
         fValsE[i] = valse[i];
   }
}

//______________________________________________________________________________
void TUCoordE::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "Container for binE index and associated E-dep variable values\n";
   TUMessages::Test(f, "TUCoordE", message);


   // Set class and test methods
   fprintf(f, " * Set method\n");
   fprintf(f, "   > SetValsE(valse={1.1,-2,4.5e-3}, n_valse=3, halfbin_status=1, is_use_or_copy=true);\n");
   vector<Double_t> valse;
   valse.push_back(1.1);
   valse.push_back(-2.);
   valse.push_back(4.5e-3);
   SetValsE(&valse[0], (Int_t)valse.size(), 1, false);
   fprintf(f, "   >PrintVals(f);\n");
   PrintVals(f);
   fprintf(f, "\n");

   fprintf(f, " * Test copy\n");
   fprintf(f, "   > TUCoordE *coords = new TUCoordE();\n");
   TUCoordE *coords = new TUCoordE();
   fprintf(f, "   > coords->Copy(*this);\n");
   coords->Copy(*this);
   fprintf(f, "   > coords->GetBinE()          => %d;\n", coords->GetBinE());
   fprintf(f, "   > coords->GetHalfBinStatus() => %d;\n", coords->GetHalfBinStatus());
   fprintf(f, "   > coords->FormValsE()        => %s;\n", coords->FormValsE().c_str());
   fprintf(f, "   > SetBinE(bin=2, halfbin_status=0);\n");
   SetBinE(2, 0);
   fprintf(f, "   > coords->Copy(*this);\n");
   coords->Copy(*this);
   fprintf(f, "   > coords->GetBinE()          => %d;\n", coords->GetBinE());
   fprintf(f, "   > coords->GetHalfBinStatus() => %d;\n", coords->GetHalfBinStatus());
   fprintf(f, "   > coords->FormValsE()        => %s;\n", coords->FormValsE().c_str());
   fprintf(f, "   > delete coords;\n");
   delete coords;

   fprintf(f, "\n");
   fprintf(f, "N.B.: look in TUAxesCrE for check of ConvertBinE2ValsE(j_cr, "
           "TUAxesCrE *axes_cre,is_use_or_copy) method.\n");
}
