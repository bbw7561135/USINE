// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUCoordTXYZ.h"
#include "../include/TUMessages.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUCoordTXYZ                                                          //
//                                                                      //
// Space-time (up to 3D+1) indices/vals TXYZ (unused indices set to -1).//
//                                                                      //
// The class TUCoords provides TXYZ (space-time) coordinates, either    //
// as bins or values for each coordinate. Conversion between bins and   //
// values (and vice-versa) is possible using the function members       //
// ConvertBins2Vals(TUAxesTXYZ) and ConvertVals2Bins(TUAxesTXYZ).       //
// Note that this conversion requires a TUAxesTXYZ class, i.e., a       //
// geometry definition to map TXYZ bins to TXYZ values.                 //
//                                                                      //
// The coordinates TUCoordTXYZ are an argument for all USINE generic    //
// space-time quantities (e.g., in TUMediumTXYZ, TUSrcPointLike, ...).  //
// The latter rest on class members derived from TUValsTXYZEVirtual,    //
// realised either as a grid-based description (in TUValsTXYZEGrid),    //
// or as a formula-based description (in TUValsTXYZEFormula). Passing   //
// TUCoordTXYZ as an argument of methods in TUTransport, TUMediumTXYZ,  //
// etc., ensure a simple, efficient, and generic implementation for     //
// method calls in the code (regardless of the model/geometry chosen,   //
// i.e. grid-based or formula-based).                                   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUCoordTXYZ)

//______________________________________________________________________________
TUCoordTXYZ::TUCoordTXYZ()
{
   // ****** Default constructor ******

   Initialise();
}

//______________________________________________________________________________
TUCoordTXYZ::TUCoordTXYZ(TUCoordTXYZ const &coords)
{
   // ****** Copy constructor ******
   //  coords            Object to copy

   Copy(coords);
}

//______________________________________________________________________________
TUCoordTXYZ::~TUCoordTXYZ()
{
   // ****** Default destructor ******
   //
}

//______________________________________________________________________________
void TUCoordTXYZ::ConvertBins2Vals(TUAxesTXYZ *axes_txyz)
{
   //--- Calculates 'fVals' from 'fBins' using 'axes_txyz'.
   //  axes_txyz         Space-time axes

   // Loop on space-time axes
   for (Int_t i = 0; i < axes_txyz->GetNAxes(); ++i) {
      if (axes_txyz->IsTXYZ(i))
         fVals[i] = axes_txyz->GetVal(i, fBins[i]);
      else
         fVals[i] = 0.;

   }
}

//______________________________________________________________________________
void TUCoordTXYZ::ConvertVals2Bins(TUAxesTXYZ *axes_txyz)
{
   //--- Calculates 'fBins' from 'fVals' using 'axes_txyz'.
   //  axes_txyz         Space-time axes

   // Loop on space-time axes
   for (Int_t i = 0; i < axes_txyz->GetNAxes(); ++i) {
      if (axes_txyz->IsTXYZ(i))
         fBins[i] = axes_txyz->IndexClosest(i, fVals[i]);
      else
         fBins[i] = 0.;
   }
}

//______________________________________________________________________________
void TUCoordTXYZ::Copy(TUCoordTXYZ const &coords)
{
   //--- Copies coordinates (bins and values) in class.
   //  coords            TXYZ-coordinates to copy from

   for (Int_t i = 0; i < fN; ++i) {
      fBins[i] = coords.GetBin(i);
      fVals[i] = coords.GetVal(i);
   }
}

//______________________________________________________________________________
string TUCoordTXYZ::FormBins(Bool_t is_extra) const
{
   //--- Returns string of bins (for positive indices only).
   //  is_extra          If true, add '(t,x,y,z)='

   string tmp_txyz = "";
   if (is_extra) {
      if (fBins[0] >= 0) {
         if (fBins[1] >= 0 && fBins[2] >= 0 && fBins[3] >= 0)
            tmp_txyz = Form("(t,x,y,z)_bin=(%d,%d,%d,%d)", fBins[0], fBins[1], fBins[2], fBins[3]);
         else if (fBins[1] >= 0 && fBins[2] >= 0)
            tmp_txyz = Form("(t,x,y)_bin=(%d,%d,%d)", fBins[0], fBins[1], fBins[2]);
         else if (fBins[1] >= 0)
            tmp_txyz = Form("(t,x)_bin=(%d,%d)", fBins[0], fBins[1]);
         else
            tmp_txyz = Form("(t)_bin=(%d)", fBins[0]);
      } else {
         if (fBins[1] >= 0 && fBins[2] >= 0 && fBins[3] >= 0)
            tmp_txyz = Form("(x,y,z)_bin=(%d,%d,%d)", fBins[1], fBins[2], fBins[3]);
         else if (fBins[1] >= 0 && fBins[2] >= 0)
            tmp_txyz = Form("(x,y)_bin=(%d,%d)", fBins[1], fBins[2]);
         else if (fBins[1] >= 0)
            tmp_txyz = Form("(x)_bin=(%d)", fBins[1]);
      }
   } else {
      if (fBins[0] >= 0) {
         if (fBins[1] >= 0 && fBins[2] >= 0 && fBins[3] >= 0)
            tmp_txyz = Form("%d,%d,%d,%d", fBins[0], fBins[1], fBins[2], fBins[3]);
         else if (fBins[1] >= 0 && fBins[2] >= 0)
            tmp_txyz = Form("%d,%d,%d", fBins[0], fBins[1], fBins[2]);
         else if (fBins[1] >= 0)
            tmp_txyz = Form("%d,%d", fBins[0], fBins[1]);
         else
            tmp_txyz = Form("%d", fBins[0]);
      } else {
         if (fBins[1] >= 0 && fBins[2] >= 0 && fBins[3] >= 0)
            tmp_txyz = Form("%d,%d,%d", fBins[1], fBins[2], fBins[3]);
         else if (fBins[1] >= 0 && fBins[2] >= 0)
            tmp_txyz = Form("%d,%d", fBins[1], fBins[2]);
         else if (fBins[1] >= 0)
            tmp_txyz = Form("%d", fBins[1]);
      }
   }
   return (string)tmp_txyz;
}

//______________________________________________________________________________
string TUCoordTXYZ::FormVals(Bool_t is_extra, Bool_t is_t, Int_t ndim) const
{
   //--- Returns string of vals.
   //  is_extra          If true, add '(t,x,y,z)='
   //  is_t              Whether print val for t axis
   //  ndim              Print only values for ndim axes

   string tmp_txyz = "";

   if (is_t) {
      if (ndim == 0) {
         if (is_extra) tmp_txyz = Form("t=(%.3le)", fVals[0]);
         else tmp_txyz = Form("(%.3le)", fVals[0]);
      } else if (ndim == 1) {
         if (is_extra) tmp_txyz = Form("(t,x)=(%.3le,%.3le)", fVals[0], fVals[1]);
         else tmp_txyz = Form("(%.3le,%.3le)", fVals[0], fVals[1]);
      } else if (ndim == 2) {
         if (is_extra) tmp_txyz = Form("(t,x,y)=(%.3le,%.3le,%.3le)", fVals[0], fVals[1], fVals[2]);
         else tmp_txyz = Form("(%.3le,%.3le,%.3le)", fVals[0], fVals[1], fVals[2]);
      } else if (ndim >= 3 || ndim < 0) {
         if (is_extra) tmp_txyz = Form("(t,x,y,z)=(%.3le,%.3le,%2le,%.3le)", fVals[0], fVals[1], fVals[2], fVals[3]);
         else tmp_txyz = Form("(%.3le,%.3le,%2le,%.3le)", fVals[0], fVals[1], fVals[2], fVals[3]);
      }
   } else {
      if (ndim == 0)
         tmp_txyz = Form("()");
      else if (ndim == 1) {
         if (is_extra) tmp_txyz = Form("x=(%.3le)", fVals[1]);
         else tmp_txyz = Form("(%.3le)", fVals[1]);
      } else if (ndim == 2) {
         if (is_extra) tmp_txyz = Form("(x,y)=(%.3le,%.3le)", fVals[1], fVals[2]);
         else tmp_txyz = Form("(%.3le,%.3le)", fVals[1], fVals[2]);
      } else if (ndim >= 3 || ndim <= 0) {
         if (is_extra) tmp_txyz = Form("(x,y,z)=(%.3le,%2le,%.3le)", fVals[1], fVals[2], fVals[3]);
         else tmp_txyz = Form("(%.3le,%2le,%.3le)", fVals[1], fVals[2], fVals[3]);
      }
   }

   return (string)tmp_txyz;
}

//______________________________________________________________________________
void TUCoordTXYZ::Initialise()
{
   //--- Initialises coordinates.

   for (Int_t i = 0; i < fN; ++i) {
      fBins[i] = -1;
      fVals[i] = 0.;
   }
}

//______________________________________________________________________________
void TUCoordTXYZ::PrintBins(FILE *f) const
{
   //--- Prints in f coordinate bins.
   //  f                    Output file

   string indent = TUMessages::Indent(true);
   fprintf(f, "%s bin_txyz=%s\n", indent.c_str(), FormBins(false).c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCoordTXYZ::PrintVals(FILE *f) const
{
   //--- Prints in f coordinate values.
   //  f                    Output file

   string indent = TUMessages::Indent(true);
   fprintf(f, "%s vals_txyz=%s\n", indent.c_str(), FormVals(false).c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCoordTXYZ::SetBins(Int_t bint, Int_t binx, Int_t biny, Int_t binz)
{
   //--- Sets class bins content.
   // bint                  T axis bin to set
   // binx                  X axis bin to set
   // biny                  Y axis bin to set
   // binz                  Z axis bin to set

   fBins[0] = bint;
   fBins[1] = binx;
   fBins[2] = biny;
   fBins[3] = binz;
}

//______________________________________________________________________________
void TUCoordTXYZ::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "Container for bin indices up to 3D+1 geometries (t,x,y,z).\n";
   TUMessages::Test(f, "TUCoordTXYZ", message);

   // Set class and test methods
   fprintf(f, " * Test class methods\n");
   fprintf(f, "   > SetBinX(2);\n");
   SetBinX(2);
   fprintf(f, "   > SetBinY(0);\n");
   SetBinY(0);
   fprintf(f, "   > FormBins(true);  => %s\n", FormBins(true).c_str());
   fprintf(f, "   > FormBins(false);  => %s\n", FormBins(false).c_str());
   fprintf(f, "   > SetBinT(6);\n");
   SetBinT(6);
   fprintf(f, "   > FormBins(true);  => %s\n", FormBins(true).c_str());
   fprintf(f, "   > SetBinTXYZ(1,3,2,1);\n");
   SetBinTXYZ(1, 3, 2, 1);
   fprintf(f, "   > FormBins(true);  => %s\n", FormBins(true).c_str());
   fprintf(f, "\n");
   fprintf(f, "\n");


   fprintf(f, " * Create TUAxesTXYZ and check conversion Bins<->Vals\n");
   fprintf(f, "   > TUAxesTXYZ axes_txyz;\n");
   fprintf(f, "   > axes_txyz.SetTAxis(0., 2., 5, \"t\", \"Myr\", false);\n");
   fprintf(f, "   > axes_txyz.SetXAxis(0., 10., 10, \"x\", \"kpc\", false);\n");
   fprintf(f, "   > axes_txyz.SetYAxis(0., 5., 10, \"y\", \"kpc\", false);\n");
   fprintf(f, "   > axes_txyz.SetZAxis(-5., 5., 6, \"z\", \"kpc\", false);\n");
   TUAxesTXYZ axes_txyz;
   axes_txyz.SetTAxis(0., 2., 5, "t", "Myr", kLIN);
   axes_txyz.SetXAxis(0., 10., 11, "x", "kpc", kLIN);
   axes_txyz.SetYAxis(0., 5., 11, "y", "kpc", kLIN);
   axes_txyz.SetZAxis(-5., 5., 6, "z", "kpc", kLIN);
   fprintf(f, "   > axes_txyz.PrintSummary(f);\n");
   axes_txyz.PrintSummary(f);
   fprintf(f, "\n");
   fprintf(f, "   > ConvertBins2Vals(&axes_txyz);\n");
   ConvertBins2Vals(&axes_txyz);
   fprintf(f, "   > PrintBins(f);\n");
   PrintBins(f);
   fprintf(f, "   > PrintVals(f);\n");
   PrintVals(f);
   fprintf(f, "\n");
   fprintf(f, "   > SetValTXYZ(0.5,2.,3.,-1);\n");
   SetValTXYZ(0.5, 2., 3., -1);
   fprintf(f, "   > ConvertVals2Bins(&axes_txyz);\n");
   ConvertVals2Bins(&axes_txyz);
   fprintf(f, "   > PrintVals(f);\n");
   PrintVals(f);
   fprintf(f, "   > PrintBins(f);\n");
   PrintBins(f);
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, " * Test copy\n");
   fprintf(f, "   > TUCoordTXYZ coords;\n");
   TUCoordTXYZ coords;
   fprintf(f, "   > coords.Copy(*this);\n");
   coords.Copy(*this);
   fprintf(f, "   > PrintBins(f);\n");
   PrintBins(f);
   fprintf(f, "   > coords.PrintBins(f);\n");
   coords.PrintBins(f);
   fprintf(f, "   > PrintVals(f);\n");
   PrintVals(f);
   fprintf(f, "   > coords.PrintVals(f);\n");
   coords.PrintVals(f);
}
