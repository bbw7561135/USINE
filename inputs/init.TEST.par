#______________________________________________________________________________
# INITIALISATION FILE (read/stored by TUInitParList::SetClass())
# N.B.: line starting with # is a comment
#
# Description:
# -----------
# List of parameters and their values/dependences loaded before any USINE run
# (CRs to propagate, XS files, ISM, transport, and source description...).
# Fit parameters are also selected in this file: no need to recompile!
#
# Format:   'group @ subgroup @ parameter @ M=? @ value'
# ------
#   - 'group', 'subgroup', and 'parameter': predefined keywords (do not edit!)
#   - 'M=?': predefined status of parameter (do not edit!)
#       → M=1 for multi-valued parameter (e.g., allows several XS files)
#       → M=0 for single-valued parameter
#   - value: value of the parameters (user-editable)
#
# Looking for a parameter syntax? The meaning of a group/subgroup?
#   → search the doc online or at $USINE/doc/_build/html/syntax.html
#______________________________________________________________________________

Base  @ CRData      @ fCRData           @ M=1 @ $USINE/inputs/crdata_crdb20170523.dat
Base  @ CRData      @ fCRData           @ M=1 @ $USINE/inputs/crdata_dummy.dat
Base  @ CRData      @ NormList          @ M=0 @ H,He:PAMELA|20.|kEkn;C,N,O,F,Ne,Na,Mg,Al,Si,P,S,Cl,Ar,K,Ca,Sc,Ti,V,Cr,Mn,Fe,Ni:HEAO|10.6|kEkn
Base  @ EnergyGrid  @ NBins             @ M=0 @ 33
Base  @ EnergyGrid  @ NUC_EknRange      @ M=0 @ [5.e-2,5.e3]
Base  @ EnergyGrid  @ ANTINUC_EknRange  @ M=0 @ [5e-2,1.e2]
Base  @ EnergyGrid  @ LEPTONS_EkRange   @ M=0 @ [5e-2,1.e4]
Base  @ ListOfCRs   @ fAtomicProperties @ M=0 @ $USINE/inputs/atomic_properties.dat
Base  @ ListOfCRs   @ fChartsForCRs     @ M=0 @ $USINE/inputs/crcharts_Zmax30_ghost97.dat
Base  @ ListOfCRs   @ IsLoadGhosts      @ M=0 @ false
###Base @ ListOfCRs   @ ListOfCRs         @ M=0 @ [1H,58Fe]
Base  @ ListOfCRs   @ ListOfCRs         @ M=0 @ [2H-BAR,30Si]
Base  @ ListOfCRs   @ ListOfParents     @ M=0 @ 2H-bar:1H-bar,1H,4He;1H-bar:1H,4He
Base  @ ListOfCRs   @ PureSecondaries   @ M=0 @ Li,Be,B
Base  @ ListOfCRs   @ SSRelativeAbund   @ M=0 @ $USINE/inputs/solarsystem_abundances2003.dat
Base  @ MediumCompo @ Targets           @ M=0 @ H,He
Base  @ XSections   @ Tertiaries        @ M=0 @ 1H-bar,2H-bar
Base  @ XSections   @ fProd             @ M=1 @ $USINE/inputs/XS_NUCLEI/sigProdGALPROP17_OPT12.dat
Base  @ XSections   @ fProd             @ M=1 @ $USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_Donato01.dat
Base  @ XSections   @ fProd             @ M=1 @ $USINE/inputs/XS_ANTINUC/dSdEProd_dbar_1H4He+HHe_Duperray05_Coal79MeV.dat
Base  @ XSections   @ fProd             @ M=1 @ $USINE/inputs/XS_ANTINUC/dSdEProd_dbar_pbar+HHe_Duperray05_Coal79MeV.dat
Base  @ XSections   @ fTotInelAnn       @ M=1 @ $USINE/inputs/XS_NUCLEI/sigInelTripathi99+Coste12.dat
Base  @ XSections   @ fTotInelAnn       @ M=1 @ $USINE/inputs/XS_ANTINUC/sigInelANN_pbar+HHe_Donato01.dat
Base  @ XSections   @ fTotInelAnn       @ M=1 @ $USINE/inputs/XS_ANTINUC/sigInelANN_dbar+HHe_Duperray05.dat
Base  @ XSections   @ fTotInelNonAnn    @ M=1 @ $USINE/inputs/XS_ANTINUC/sigInelNONANN_pbar+HHe_Donato01.dat
Base  @ XSections   @ fTotInelNonAnn    @ M=1 @ $USINE/inputs/XS_ANTINUC/sigInelNONANN_dbar+HHe_Duperray05.dat
Base  @ XSections   @ fdSigdENAR        @ M=1 @ $USINE/inputs/XS_ANTINUC/dSdENAR_pbar+HHe_Duperray05_Anderson.dat
Base  @ XSections   @ fdSigdENAR        @ M=1 @ $USINE/inputs/XS_ANTINUC/dSdENAR_dbar+HHe_Duperray05_Anderson.dat


Model0DLeakyBox @ Geometry       @ ParNames         @ M=0 @ -
Model0DLeakyBox @ Geometry       @ ParUnits         @ M=0 @ -
Model0DLeakyBox @ Geometry       @ ParVals          @ M=0 @ -
Model0DLeakyBox @ Geometry       @ TAxis            @ M=0 @ -
Model0DLeakyBox @ Geometry       @ XAxis            @ M=0 @ -
Model0DLeakyBox @ Geometry       @ YAxis            @ M=0 @ -
Model0DLeakyBox @ Geometry       @ ZAxis            @ M=0 @ -
Model0DLeakyBox @ Geometry       @ XSun             @ M=0 @ -
Model0DLeakyBox @ Geometry       @ YSun             @ M=0 @ -
Model0DLeakyBox @ Geometry       @ ZSun             @ M=0 @ -
Model0DLeakyBox @ ISM            @ ParNames         @ M=0 @ -
Model0DLeakyBox @ ISM            @ ParUnits         @ M=0 @ -
Model0DLeakyBox @ ISM            @ ParVals          @ M=0 @ -
Model0DLeakyBox @ ISM            @ Density          @ M=1 @ HI:FORMULA|0.867
Model0DLeakyBox @ ISM            @ Density          @ M=1 @ HII:FORMULA|0.033
Model0DLeakyBox @ ISM            @ Density          @ M=1 @ H2:FORMULA|0.
Model0DLeakyBox @ ISM            @ Density          @ M=1 @ He:FORMULA|0.1
Model0DLeakyBox @ ISM            @ Te               @ M=0 @ FORMULA|1.e4
Model0DLeakyBox @ SrcPointLike   @ Species          @ M=1 @ -
Model0DLeakyBox @ SrcPointLike   @ SpectAbundInit   @ M=1 @ -
Model0DLeakyBox @ SrcPointLike   @ SpectTempl       @ M=1 @ -
Model0DLeakyBox @ SrcPointLike   @ SpectValsPerCR   @ M=1 @ -
Model0DLeakyBox @ SrcPointLike   @ SrcXPosition     @ M=1 @ -
Model0DLeakyBox @ SrcPointLike   @ SrcYPosition     @ M=1 @ -
Model0DLeakyBox @ SrcPointLike   @ SrcZPosition     @ M=1 @ -
Model0DLeakyBox @ SrcPointLike   @ TStart           @ M=1 @ -
Model0DLeakyBox @ SrcPointLike   @ TStop            @ M=1 @ -
Model0DLeakyBox @ SrcSteadyState @ Species          @ M=1 @ ASTRO_STD|ALL
Model0DLeakyBox @ SrcSteadyState @ SpectAbundInit   @ M=1 @ ASTRO_STD|kSSISOTFRAC,kSSISOTABUND,kFIPBIAS
Model0DLeakyBox @ SrcSteadyState @ SpectTempl       @ M=1 @ ASTRO_STD|POWERLAW|q
Model0DLeakyBox @ SrcSteadyState @ SpectValsPerCR   @ M=1 @ ASTRO_STD|q[PERCR:DEFAULT=1.e-5];alpha[PERCR:DEFAULT=2.1,1H=2.1,4He=2.1];eta_s[SHARED:-1.]
Model0DLeakyBox @ SrcSteadyState @ SpatialTempl     @ M=1 @ ASTRO_STD|-
Model0DLeakyBox @ SrcSteadyState @ SpatialValsPerCR @ M=1 @ ASTRO_STD|-
Model0DLeakyBox @ Transport      @ ParNames         @ M=0 @ Va,lambda0,delta,eta_t
Model0DLeakyBox @ Transport      @ ParUnits         @ M=0 @ km/s/kpc,g/cm2,-,-
Model0DLeakyBox @ Transport      @ ParVals          @ M=0 @ 88.,26.,0.52,1.
Model0DLeakyBox @ Transport      @ Wind             @ M=1 @ -
Model0DLeakyBox @ Transport      @ VA               @ M=0 @ FORMULA|Va
Model0DLeakyBox @ Transport      @ K                @ M=1 @ K00:FORMULA|lambda0*Rig^(-delta)*beta^eta_t
Model0DLeakyBox @ Transport      @ Kpp              @ M=0 @ FORMULA|(4./3.)*(Va*1.022712e-3*beta*Etot)^2/(delta*(4-delta^2)*(4-delta))


Model1DKisoVc  @ Geometry       @ ParNames         @ M=0 @ L,h,rhole
Model1DKisoVc  @ Geometry       @ ParUnits         @ M=0 @ kpc,kpc,kpc
Model1DKisoVc  @ Geometry       @ ParVals          @ M=0 @ 8.,0.1,2.038018e-01
Model1DKisoVc  @ Geometry       @ TAxis            @ M=0 @ -
Model1DKisoVc  @ Geometry       @ XAxis            @ M=0 @ z:[0,L],10,LIN
Model1DKisoVc  @ Geometry       @ YAxis            @ M=0 @ -
Model1DKisoVc  @ Geometry       @ ZAxis            @ M=0 @ -
Model1DKisoVc  @ Geometry       @ XSun             @ M=0 @ -
Model1DKisoVc  @ Geometry       @ YSun             @ M=0 @ -
Model1DKisoVc  @ Geometry       @ ZSun             @ M=0 @ -
Model1DKisoVc  @ ISM            @ ParNames         @ M=0 @ -
Model1DKisoVc  @ ISM            @ ParUnits         @ M=0 @ -
Model1DKisoVc  @ ISM            @ ParVals          @ M=0 @ -
Model1DKisoVc  @ ISM            @ Density          @ M=1 @ HI:FORMULA|0.867
Model1DKisoVc  @ ISM            @ Density          @ M=1 @ HII:FORMULA|0.033
Model1DKisoVc  @ ISM            @ Density          @ M=1 @ H2:FORMULA|0.
Model1DKisoVc  @ ISM            @ Density          @ M=1 @ He:FORMULA|0.1
Model1DKisoVc  @ ISM            @ Te               @ M=0 @ FORMULA|1.e4
Model1DKisoVc  @ SrcPointLike   @ Species          @ M=1 @ -
Model1DKisoVc  @ SrcPointLike   @ SpectAbundInit   @ M=1 @ -
Model1DKisoVc  @ SrcPointLike   @ SpectTempl       @ M=1 @ -
Model1DKisoVc  @ SrcPointLike   @ SpectValsPerCR   @ M=1 @ -
Model1DKisoVc  @ SrcPointLike   @ SrcXPosition     @ M=1 @ -
Model1DKisoVc  @ SrcPointLike   @ SrcYPosition     @ M=1 @ -
Model1DKisoVc  @ SrcPointLike   @ SrcZPosition     @ M=1 @ -
Model1DKisoVc  @ SrcPointLike   @ TStart           @ M=1 @ -
Model1DKisoVc  @ SrcPointLike   @ TStop            @ M=1 @ -
Model1DKisoVc  @ SrcSteadyState @ Species          @ M=1 @ ASTRO_STD|ALL
Model1DKisoVc  @ SrcSteadyState @ SpectAbundInit   @ M=1 @ ASTRO_STD|kSSISOTFRAC,kSSISOTABUND,kFIPBIAS
Model1DKisoVc  @ SrcSteadyState @ SpectTempl       @ M=1 @ ASTRO_STD|POWERLAW|q
Model1DKisoVc  @ SrcSteadyState @ SpectValsPerCR   @ M=1 @ ASTRO_STD|q[PERCR:DEFAULT=1.e-5];alpha[PERZ:DEFAULT=2.268922e+00,H=2.3];eta_s[SHARED:-1.]
Model1DKisoVc  @ SrcSteadyState @ SpatialTempl     @ M=1 @ ASTRO_STD|-
Model1DKisoVc  @ SrcSteadyState @ SpatialValsPerCR @ M=1 @ ASTRO_STD|-
Model1DKisoVc  @ Transport      @ ParNames         @ M=0 @ Va,Vc,K0,delta,eta_t,Rbreak,Deltabreak,sbreak
Model1DKisoVc  @ Transport      @ ParUnits         @ M=0 @ km/s,km/s,kpc^2/Myr,-,-,GV,-,-
Model1DKisoVc  @ Transport      @ ParVals          @ M=0 @ 8.548451e+01,1.954918e+01,0.035859151,6.141924e-01,-5.972996e-02,125.89254,2.281522e-01,0.010023191
Model1DKisoVc  @ Transport      @ Wind             @ M=1 @ W0:FORMULA|Vc
Model1DKisoVc  @ Transport      @ VA               @ M=0 @ FORMULA|Va
Model1DKisoVc  @ Transport      @ K                @ M=1 @ K00:FORMULA|beta^eta_t*K0*Rig^delta*(1+(Rig/Rbreak)^(Deltabreak/sbreak))^(-sbreak)
Model1DKisoVc  @ Transport      @ Kpp              @ M=0 @ FORMULA|(4./3.)*(Va*1.022712e-3*beta*Etot)^2/(delta*(4-delta^2)*(4-delta)*K00)


Model2DKisoVc  @ Base           @ NBessel          @ M=0 @ 9
Model2DKisoVc  @ Base           @ NIntegrR         @ M=0 @ 1000
Model2DKisoVc  @ Base           @ NIntegrZ         @ M=0 @ 200
Model2DKisoVc  @ Geometry       @ ParNames         @ M=0 @ L,h,R,rhole
Model2DKisoVc  @ Geometry       @ ParUnits         @ M=0 @ kpc,kpc,kpc,kpc
Model2DKisoVc  @ Geometry       @ ParVals          @ M=0 @ 4.,0.1,20.,0.12
Model2DKisoVc  @ Geometry       @ TAxis            @ M=0 @ -
Model2DKisoVc  @ Geometry       @ XAxis            @ M=0 @ r:[0,R],10,LIN
Model2DKisoVc  @ Geometry       @ YAxis            @ M=0 @ z:[0,L],10,LIN
Model2DKisoVc  @ Geometry       @ ZAxis            @ M=0 @ -
Model2DKisoVc  @ Geometry       @ XSun             @ M=0 @ 8.5
Model2DKisoVc  @ Geometry       @ YSun             @ M=0 @ 0.
Model2DKisoVc  @ Geometry       @ ZSun             @ M=0 @ -
Model2DKisoVc  @ ISM            @ ParNames         @ M=0 @ -
Model2DKisoVc  @ ISM            @ ParUnits         @ M=0 @ -
Model2DKisoVc  @ ISM            @ ParVals          @ M=0 @ -
Model2DKisoVc  @ ISM            @ Density          @ M=1 @ HI:FORMULA|0.867
Model2DKisoVc  @ ISM            @ Density          @ M=1 @ HII:FORMULA|0.033
Model2DKisoVc  @ ISM            @ Density          @ M=1 @ H2:FORMULA|0.
Model2DKisoVc  @ ISM            @ Density          @ M=1 @ He:FORMULA|0.1
Model2DKisoVc  @ ISM            @ Te               @ M=0 @ FORMULA|1.e4
Model2DKisoVc  @ SrcPointLike   @ Species          @ M=1 @ -
Model2DKisoVc  @ SrcPointLike   @ SpectAbundInit   @ M=1 @ -
Model2DKisoVc  @ SrcPointLike   @ SpectTempl       @ M=1 @ -
Model2DKisoVc  @ SrcPointLike   @ SpectValsPerCR   @ M=1 @ -
Model2DKisoVc  @ SrcPointLike   @ SrcXPosition     @ M=1 @ -
Model2DKisoVc  @ SrcPointLike   @ SrcYPosition     @ M=1 @ -
Model2DKisoVc  @ SrcPointLike   @ SrcZPosition     @ M=1 @ -
Model2DKisoVc  @ SrcPointLike   @ TStart           @ M=1 @ -
Model2DKisoVc  @ SrcPointLike   @ TStop            @ M=1 @ -
Model2DKisoVc  @ SrcSteadyState @ Species          @ M=1 @ ASTRO_STD|ALL
Model2DKisoVc  @ SrcSteadyState @ SpectAbundInit   @ M=1 @ ASTRO_STD|kSSISOTFRAC,kSSISOTABUND,kFIPBIAS
Model2DKisoVc  @ SrcSteadyState @ SpectTempl       @ M=1 @ ASTRO_STD|POWERLAW|q
Model2DKisoVc  @ SrcSteadyState @ SpectValsPerCR   @ M=1 @ ASTRO_STD|q[PERCR:DEFAULT=1.e-5];alpha[PERCR:DEFAULT=2.1,1H=2.1,4He=2.2];eta_s[SHARED:-1.]
Model2DKisoVc  @ SrcSteadyState @ SpatialTempl     @ M=1 @ ASTRO_STD|CASEBHATTA96
Model2DKisoVc  @ SrcSteadyState @ SpatialValsPerCR @ M=1 @ ASTRO_STD|-
###Model2DKisoVc @ SrcSteadyState @ SpatialTempl     @ M=1 @ ASTRO_STD|SNRMODIFIED
###Model2DKisoVc @ SrcSteadyState @ SpatialValsPerCR @ M=1 @ ASTRO_STD||rsol[SHARED:8.5];a[SHARED:1.];b[SHARED:1.];dex[SHARED:1.]
Model2DKisoVc  @ Transport      @ ParNames         @ M=0 @ Va,Vc,K0,delta,eta_t
Model2DKisoVc  @ Transport      @ ParUnits         @ M=0 @ km/s,km/s,kpc^2/Myr,-,-
Model2DKisoVc  @ Transport      @ ParVals          @ M=0 @ 48.9,12.,0.0112,0.7,1.
Model2DKisoVc  @ Transport      @ Wind             @ M=1 @ W0:FORMULA|0
Model2DKisoVc  @ Transport      @ Wind             @ M=1 @ W1:FORMULA|Vc
Model2DKisoVc  @ Transport      @ VA               @ M=0 @ FORMULA|Va
Model2DKisoVc  @ Transport      @ K                @ M=1 @ K00:FORMULA|beta^eta_t*K0*Rig^delta
Model2DKisoVc  @ Transport      @ Kpp              @ M=0 @ FORMULA|(4./3.)*(Va*1.022712e-3*beta*Etot)^2/(delta*(4-delta^2)*(4-delta)*K00)


ModelTEST @ Geometry       @ ParNames         @ M=0 @ L,R
ModelTEST @ Geometry       @ ParUnits         @ M=0 @ kpc,kpc
ModelTEST @ Geometry       @ ParVals          @ M=0 @ 10,20.
ModelTEST @ Geometry       @ TAxis            @ M=0 @ -
ModelTEST @ Geometry       @ XAxis            @ M=0 @ r:[0,R],50,LIN
ModelTEST @ Geometry       @ YAxis            @ M=0 @ z:[-L,L],10,LIN
ModelTEST @ Geometry       @ ZAxis            @ M=0 @ -
ModelTEST @ Geometry       @ XSun             @ M=0 @ -
ModelTEST @ Geometry       @ YSun             @ M=0 @ -
ModelTEST @ Geometry       @ ZSun             @ M=0 @ -
ModelTEST @ ISM            @ ParNames         @ M=0 @ n0,n1
ModelTEST @ ISM            @ ParUnits         @ M=0 @ cm3,cm3
ModelTEST @ ISM            @ ParVals          @ M=0 @ 1,0.1
ModelTEST @ ISM            @ Density          @ M=1 @ HI:FORMULA|n0*(x+z)
ModelTEST @ ISM            @ Density          @ M=1 @ HII:FORMULA|n0*(x*x+z)
ModelTEST @ ISM            @ Density          @ M=1 @ H2:FORMULA|BOB:=x+z;TUT:=x*x;n0*(BOB+TUT)
ModelTEST @ ISM            @ Density          @ M=1 @ He:FORMULA|n1*(x+z)
ModelTEST @ ISM            @ Te               @ M=0 @ FORMULA|1.e4
ModelTEST @ SrcPointLike   @ Species          @ M=1 @ -
ModelTEST @ SrcPointLike   @ SpectAbundInit   @ M=1 @ -
ModelTEST @ SrcPointLike   @ SpectTempl       @ M=1 @ -
ModelTEST @ SrcPointLike   @ SpectValsPerCR   @ M=1 @ -
ModelTEST @ SrcPointLike   @ SrcXPosition     @ M=1 @ -
ModelTEST @ SrcPointLike   @ SrcYPosition     @ M=1 @ -
ModelTEST @ SrcPointLike   @ SrcZPosition     @ M=1 @ -
ModelTEST @ SrcPointLike   @ TStart           @ M=1 @ -
ModelTEST @ SrcPointLike   @ TStop            @ M=1 @ -
ModelTEST @ SrcSteadyState @ Species          @ M=1 @ ASTRO_STD|ALL
ModelTEST @ SrcSteadyState @ SpectAbundInit   @ M=1 @ ASTRO_STD|kSSISOTFRAC,kSSISOTABUND,kFIPBIAS
ModelTEST @ SrcSteadyState @ SpectTempl       @ M=1 @ ASTRO_STD|POWERLAW|q
ModelTEST @ SrcSteadyState @ SpectValsPerCR   @ M=1 @ ASTRO_STD|q[PERCR:DEFAULT=1e-4];alpha[PERZ:DEFAULT=2.1,H=2.1,He=2.2];eta_s[SHARED:-1.]
ModelTEST @ SrcSteadyState @ SpatialTempl     @ M=1 @ ASTRO_STD|CASEBHATTA96
ModelTEST @ SrcSteadyState @ SpatialValsPerCR @ M=1 @ ASTRO_STD|-
ModelTEST @ Transport      @ ParNames         @ M=0 @ Va,Vc,K0,delta,eta_t
ModelTEST @ Transport      @ ParUnits         @ M=0 @ km/s,km/s,kpc^2/Myr,-,-
ModelTEST @ Transport      @ ParVals          @ M=0 @ 70.,15.,0.0112,0.7,1.
ModelTEST @ Transport      @ Wind             @ M=1 @ W0:FORMULA|Vc
ModelTEST @ Transport      @ Wind             @ M=1 @ W1:FORMULA|Vc+Va
ModelTEST @ Transport      @ Wind             @ M=1 @ W2:FORMULA|2*Vc
ModelTEST @ Transport      @ VA               @ M=0 @ FORMULA|2*Va
ModelTEST @ Transport      @ K                @ M=1 @ K00:FORMULA|(x+y)*beta^eta_t*K0*Rig^delta
ModelTEST @ Transport      @ K                @ M=1 @ K11:FORMULA|K0*Rig^delta
ModelTEST @ Transport      @ K                @ M=1 @ K22:FORMULA|K0*Etot^delta
ModelTEST @ Transport      @ K                @ M=1 @ K12:FORMULA|K0*beta^eta_t
ModelTEST @ Transport      @ Kpp              @ M=0 @ FORMULA|(4./3.)*(Va*beta*Etot)^2/(delta*(4-delta^2)*(4-delta)*K00)


SolMod0DFF @ Base @ ParNames @ M=0 @ phi
SolMod0DFF @ Base @ ParUnits @ M=0 @ GV
SolMod0DFF @ Base @ ParVals  @ M=0 @ 0.5
SolMod0DFF @ Base @ Rig0     @ M=0 @ 0.2


TemplSpectrum @ POWERLAW        @ ParNames   @ M=0 @ q,alpha,eta_s
TemplSpectrum @ POWERLAW        @ ParUnits   @ M=0 @ (/GeV/n/m3/Myr),-,-
TemplSpectrum @ POWERLAW        @ Definition @ M=0 @ FORMULA|q*beta^(eta_s)*Rig^(-alpha)
TemplSpectrum @ TEST            @ ParNames   @ M=0 @ q,alpha,eta_s
TemplSpectrum @ TEST            @ ParUnits   @ M=0 @ (/GeV/n/m3/Myr),-,-
TemplSpectrum @ TEST            @ Definition @ M=0 @ FORMULA|q*beta^(eta_s)*Rig^(-alpha)


TemplSpatialDist @ CST            @ ParNames   @ M=0 @ -
TemplSpatialDist @ CST            @ ParUnits   @ M=0 @ -
TemplSpatialDist @ CST            @ Definition @ M=0 @ FORMULA|1
TemplSpatialDist @ DOOR           @ ParNames   @ M=0 @ rlo, rup
TemplSpatialDist @ DOOR           @ ParUnits   @ M=0 @ kpc, kpc
TemplSpatialDist @ DOOR           @ Definition @ M=0 @ FORMULA|if((x>rup),0,if((x<rlo),0,1))
TemplSpatialDist @ STEPUP         @ ParNames   @ M=0 @ rup
TemplSpatialDist @ STEPUP         @ ParUnits   @ M=0 @ kpc
TemplSpatialDist @ STEPUP         @ Definition @ M=0 @ FORMULA|if((x>rup),1,0)
TemplSpatialDist @ STEPDOWN       @ ParNames   @ M=0 @ rlo
TemplSpatialDist @ STEPDOWN       @ ParUnits   @ M=0 @ kpc
TemplSpatialDist @ STEPDOWN       @ Definition @ M=0 @ FORMULA|if((x<rlo),1,0)
TemplSpatialDist @ CASEBHATTA96   @ ParNames   @ M=0 @ -
TemplSpatialDist @ CASEBHATTA96   @ ParUnits   @ M=0 @ -
TemplSpatialDist @ CASEBHATTA96   @ Definition @ M=0 @ FORMULA|pow(x/8.5,2.)*exp(-3.53*(x-8.5)/8.5)
TemplSpatialDist @ SNRMODIFIED    @ ParNames   @ M=0 @ rsol,a,b,dex
TemplSpatialDist @ SNRMODIFIED    @ ParUnits   @ M=0 @ kpc,-,-,dex/kpc
TemplSpatialDist @ SNRMODIFIED    @ Definition @ M=0 @ FORMULA|pow(10.,dex*(x-rsol))*pow((x/rsol),a)*exp(b*(x-rsol)/rsol)
TemplSpatialDist @ STECKERJONES77 @ ParNames   @ M=0 @ -
TemplSpatialDist @ STECKERJONES77 @ ParUnits   @ M=0 @ -
TemplSpatialDist @ STECKERJONES77 @ Definition @ M=0 @ FORMULA|pow(x/10.,0.6)*exp(-3.*(x-10.)/10.)
TemplSpatialDist @ STRONGMOSKA99  @ ParNames   @ M=0 @ -
TemplSpatialDist @ STRONGMOSKA99  @ ParUnits   @ M=0 @ -
TemplSpatialDist @ STRONGMOSKA99  @ Definition @ M=0 @ FORMULA|pow(x/8.5,0.5)*exp(-1.*(x-8.5)/8.5)
TemplSpatialDist @ EINASTO_CYL    @ ParNames   @ M=0 @ rhos,rs,alpha
TemplSpatialDist @ EINASTO_CYL    @ ParUnits   @ M=0 @ Msol/kpc3,kpc,-
TemplSpatialDist @ EINASTO_CYL    @ Definition @ M=0 @ FORMULA|r_sp:=x*x+y*y;rhos*exp((-2/alpha)*(pow((r_sp/rs),alpha)-1))
TemplSpatialDist @ DUMMY          @ ParNames   @ M=0 @ -
TemplSpatialDist @ DUMMY          @ ParUnits   @ M=0 @ -
TemplSpatialDist @ DUMMY          @ Definition @ M=0 @ FORMULA|exp(-(x*x+y*y+z*z/8.))

UsineRun  @ Calculation @ BC_ANTINUC_LE      @ M=0 @ kD2NDLNEKN2_ZERO
UsineRun  @ Calculation @ BC_ANTINUC_HE      @ M=0 @ kNOCHANGE
UsineRun  @ Calculation @ BC_LEPTON_LE       @ M=0 @ kNOCURRENT
UsineRun  @ Calculation @ BC_LEPTON_HE       @ M=0 @ kNOCURRENT
UsineRun  @ Calculation @ BC_NUC_LE          @ M=0 @ kD2NDLNEKN2_ZERO
UsineRun  @ Calculation @ BC_NUC_HE          @ M=0 @ kNOCHANGE
UsineRun  @ Calculation @ EPS_ITERCONV       @ M=0 @ 1.e-6
UsineRun  @ Calculation @ EPS_INTEGR         @ M=0 @ 1.e-4
UsineRun  @ Calculation @ EPS_NORMDATA       @ M=0 @ 1.e-10
UsineRun  @ Calculation @ IsUseNormList      @ M=0 @ true
UsineRun  @ Display     @ QtiesExpsEType     @ M=0 @ ALL
###UsineRun @ Display     @ QtiesExpsEType     @ M=0 @ He:AMS,BESS:kEKN;B/C:AMS:kR
UsineRun  @ Display     @ ErrType            @ M=0 @ kERRSTAT
UsineRun  @ Display     @ FluxPowIndex       @ M=0 @ 2.8
UsineRun  @ Models      @ Propagation        @ M=1 @ Model1DKisoVc
UsineRun  @ Models      @ SolarModulation    @ M=1 @ SolMod0DFF
UsineRun  @ OnOff       @ IsDecayBETA        @ M=0 @ true
UsineRun  @ OnOff       @ IsDecayFedBETA     @ M=0 @ true
UsineRun  @ OnOff       @ IsDecayEC          @ M=0 @ true
UsineRun  @ OnOff       @ IsDecayFedEC       @ M=0 @ true
UsineRun  @ OnOff       @ IsDestruction      @ M=0 @ true
UsineRun  @ OnOff       @ IsELossAdiabatic   @ M=0 @ true
UsineRun  @ OnOff       @ IsELossBremss      @ M=0 @ false
UsineRun  @ OnOff       @ IsELossCoulomb     @ M=0 @ true
UsineRun  @ OnOff       @ IsELossIC          @ M=0 @ false
UsineRun  @ OnOff       @ IsELossIon         @ M=0 @ true
UsineRun  @ OnOff       @ IsELossSynchrotron @ M=0 @ false
UsineRun  @ OnOff       @ IsEReacc           @ M=0 @ true
UsineRun  @ OnOff       @ IsPrimExotic       @ M=0 @ false
UsineRun  @ OnOff       @ IsPrimStandard     @ M=0 @ true
UsineRun  @ OnOff       @ IsSecondaries      @ M=0 @ true
UsineRun  @ OnOff       @ IsTertiaries       @ M=0 @ true
UsineRun  @ OnOff       @ IsWind             @ M=0 @ true


UsineFit  @ Config   @ Minimiser        @ M=0 @ Minuit2
UsineFit  @ Config   @ Algorithm        @ M=0 @ combined
UsineFit  @ Config   @ NMaxCall         @ M=0 @ 100000
UsineFit  @ Config   @ Strategy         @ M=0 @ 1
UsineFit  @ Config   @ Tolerance        @ M=0 @ 1.e-2
UsineFit  @ Config   @ Precision        @ M=0 @ 1.e-10
UsineFit  @ Config   @ PrintLevel       @ M=0 @ 2
UsineFit  @ Config   @ IsMINOS          @ M=0 @ false
UsineFit  @ Config   @ IsUseBinRange    @ M=0 @ true
UsineFit  @ Config   @ NExtraInBinRange @ M=0 @ 0

UsineFit  @ TOAData  @ QtiesExpsEType   @ M=0 @ B/C:AMS,PAMELA:KR;O:HEAO:kEKN
###UsineFit @ TOAData  @ QtiesExpsEType   @ M=0 @ He:AMS,BESS:kEKN;B/C:AMS:KR
UsineFit  @ TOAData  @ ErrType          @ M=0 @ kERRCOV:$USINE/inputs/CRDATA_COVARIANCE/
UsineFit  @ TOAData  @ IsModelOrDataForRelCov @ M=0 @ true
###UsineFit @ TOAData  @ ErrType          @ M=0 @ kERRTOT
###UsineFit @ TOAData  @ ErrType          @ M=0 @ kERRSTAT
UsineFit  @ TOAData  @ EminData         @ M=0 @ 1.e-5
UsineFit  @ TOAData  @ EmaxData         @ M=0 @ 200.
UsineFit  @ TOAData  @ TStartData       @ M=0 @ 1950-01-01_00:00:00
UsineFit  @ TOAData  @ TStopData        @ M=0 @ 2100-01-01_00:00:00
UsineFit  @ FreePars @ CRs              @ M=1 @ HalfLifeBETA_10Be:NUISANCE,LIN,[1.3,1.5],1.387,0.012
UsineFit  @ FreePars @ DataErr          @ M=1 @ SCALE_AMS02_201105201605__BC:NUISANCE,LIN,[-10,10],0.,1.
UsineFit  @ FreePars @ DataErr          @ M=1 @ UNF_AMS02_201105201605__BC:NUISANCE,LIN,[-10,10],0.,1.
UsineFit  @ FreePars @ DataErr          @ M=1 @ BACK_AMS02_201105201605__BC:NUISANCE,LIN,[-10,10],0.,1.
UsineFit  @ FreePars @ DataErr          @ M=1 @ ACC_AMS02_201105201605__BC:NUISANCE,LIN,[-10,10],0.,1.
UsineFit  @ FreePars @ Geometry         @ M=1 @ -
UsineFit  @ FreePars @ ISM              @ M=1 @ -
UsineFit  @ FreePars @ Modulation       @ M=1 @ phi_AMS02_201105201605_:NUISANCE,LIN,[0.3,,1.1],0.73,0.2
UsineFit  @ FreePars @ SrcPointLike     @ M=1 @ -
UsineFit  @ FreePars @ SrcSteadyState   @ M=1 @ alpha_HE:NUISANCE,LIN,[1.7.,2.5],2.3,0.1
UsineFit  @ FreePars @ Transport        @ M=1 @ eta_t:FIT,LIN,[-3,3],-0.456,0.1
UsineFit  @ FreePars @ Transport        @ M=1 @ delta:FIT,LIN,[0.2,0.9],0.684,0.02
UsineFit  @ FreePars @ Transport        @ M=1 @ Va:FIT,LIN,[1.,120],74.5,0.1
UsineFit  @ FreePars @ Transport        @ M=1 @ Vc:FIT,LIN,[0.,70],18.6,0.1
UsineFit  @ FreePars @ Transport        @ M=1 @ K0:FIT,LOG,[-2.5,-0.5],-1.668,0.1
###UsineFit @ FreePars @ SrcSteadyState   @ M=1 @ q_4HE:FIT,LOG,[-5.,-3],-4.,0.1
###UsineFit @ FreePars @ Transport        @ M=1 @ L:FIT,LOG,[0.5,2],1.,0.1
###UsineFit @ FreePars @ Transport        @ M=1 @ rhole:FIT,LIN,[0.,0.3],0.1,0.05
###UsineFit @ FreePars @ Transport        @ M=1 @ Rbreak:NUISANCE,LOG,[2.1,2.8],2.3,0.2
###UsineFit @ FreePars @ Transport        @ M=1 @ Deltabreak:NUISANCE,LIN,[0,0.5],0.2,0.05
###UsineFit @ FreePars @ Transport        @ M=1 @ sbreak:NUISANCE,LOG,[-3.,0.],-2.,0.05
UsineFit  @ FreePars @ XSection         @ M=1 @ EnhancePowHE_ALL:NUISANCE,LIN,[0.,2.],1.,1.
UsineFit  @ FreePars @ XSection         @ M=1 @ Norm_16O+H:NUISANCE,LIN,[0.7,1.3],1.,0.05
UsineFit  @ FreePars @ XSection         @ M=1 @ Norm_12C+H:NUISANCE,LIN,[0.7,1.3],1.,0.05
UsineFit  @ FreePars @ XSection         @ M=1 @ Norm_11B+H:NUISANCE,LIN,[0.7,1.3],1.,0.05
UsineFit  @ FreePars @ XSection         @ M=1 @ Norm_10B+H:NUISANCE,LIN,[0.7,1.3],1.,0.05
UsineFit  @ FreePars @ XSection         @ M=1 @ Norm_16O+H->11B:NUISANCE,LIN,[0.5,2.],1.,0.2
UsineFit  @ FreePars @ XSection         @ M=1 @ EAxisScale_16O+H->11B:NUISANCE,LIN,[0.5,1.5],1.,0.5
UsineFit  @ FreePars @ XSection         @ M=1 @ EThresh_16O+H->11B:NUISANCE,LIN,[0.5,2.],1.,0.5
UsineFit  @ FreePars @ XSection         @ M=1 @ SlopeLE_16O+H->11B:NUISANCE,LIN,[-1,1.],0.,0.5
UsineFit  @ FreePars @ XSection         @ M=1 @ Norm_12C+H->11B:NUISANCE,LIN,[0.5,2.],1.,0.2
UsineFit  @ FreePars @ XSection         @ M=1 @ EAxisScale_12C+H->11B:NUISANCE,LIN,[0.5,1.5],1.,0.5
UsineFit  @ FreePars @ XSection         @ M=1 @ EThresh_12C+H->11B:NUISANCE,LIN,[0.5,2.],1.,0.5
UsineFit  @ FreePars @ XSection         @ M=1 @ SlopeLE_12C+H->11B:NUISANCE,LIN,[-1,1.],0.,0.5
###UsineFit  @ FreePars @ XSection         @ M=1 @ Norm_14N+ANY:NUISANCE,LIN,[0.7,1.3],1.,0.05
###UsineFit  @ FreePars @ XSection         @ M=1 @ Norm_ANY+H:NUISANCE,LIN,[0.7,1.3],1.,0.05
###UsineFit  @ FreePars @ XSection         @ M=1 @ Norm_16O+He->ANY:NUISANCE,LIN,[0.7,1.3],1.,0.05
###UsineFit  @ FreePars @ XSection         @ M=1 @ EAxisScale_ALL:NUISANCE,LIN,[0.5,1.5],1.,0.5
#
### Alternatively, can use linear combination of XS
###UsineFit @ FreePars @ XSection         @ M=1 @ LCInelBar94_12C+H:NUISANCE,LIN,[0.,1.5],0.25,0.05
###UsineFit @ FreePars @ XSection         @ M=1 @ LCInelTri99_12C+H:NUISANCE,LIN,[0.,1.5],0.25,0.05
###UsineFit @ FreePars @ XSection         @ M=1 @ LCInelWeb03_12C+H:NUISANCE,LIN,[0.,1.5],0.25,0.05
###UsineFit @ FreePars @ XSection         @ M=1 @ LCInelWel97_12C+H:NUISANCE,LIN,[0.,1.5],0.25,0.05
###UsineFit @ FreePars @ XSection         @ M=1 @ LCProdGal17_16O+H->11B:NUISANCE,LIN,[0.,1.5],0.25,0.2
###UsineFit @ FreePars @ XSection         @ M=1 @ LCProdSou01_16O+H->11B:NUISANCE,LIN,[0.,1.5],0.25,0.2
###UsineFit @ FreePars @ XSection         @ M=1 @ LCProdWeb03_16O+H->11B:NUISANCE,LIN,[0.,1.5],0.25,0.2
###UsineFit @ FreePars @ XSection         @ M=1 @ LCProdWKS98_16O+H->11B:NUISANCE,LIN,[0.,1.5],0.25,0.2

UsineFit  @ Outputs  @ IsPrintCovMatrix     @ M=0 @ true
UsineFit  @ Outputs  @ IsPrintHessianMatrix @ M=0 @ true
### 2D contours and profile likelihood (can take some time...)
###UsineFit @ Outputs  @ Contours             @ M=1 @ K0,Vc:5:{1}
###UsineFit @ Outputs  @ Contours             @ M=1 @ ALL:10:{1,2}
###UsineFit @ Outputs  @ Profiles             @ M=1 @ delta:10
###UsineFit @ Outputs  @ Profiles             @ M=1 @ Vc:5
###UsineFit @ Outputs  @ Profiles             @ M=1 @ ALL:7
###UsineFit @ Outputs  @ Scans                @ M=1 @ ALL:25
UsineFit @ Outputs  @ Scans                @ M=1 @ eta_t:15
UsineFit @ Outputs  @ Scans                @ M=1 @ delta:15
UsineFit @ Outputs  @ Scans                @ M=1 @ Va:10
UsineFit @ Outputs  @ Scans                @ M=1 @ Norm_16O+H:25