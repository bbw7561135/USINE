# For detailed information, see the [online documentation](http://lpsc.in2p3.fr/usine/) & [USINE publications](http://adsabs.harvard.edu/abs/)

##  I. WHAT IS USINE?

USINE is a library for Galactic cosmic-ray (GCR) propagation models, valid in the energy range ~[10^(-2) - 10^(6)] GeV/n. The library contains several semi-analytical propagation models previously used in the literature ([Leaky-Box](http://adsabs.harvard.edu/abs/2009A%26A...497..991P), disc+halo [1D](http://adsabs.harvard.edu/abs/2010A%26A...516A..66P) and [2D](http://adsabs.harvard.edu/abs/2001ApJ...555..585M) diffusion models) as well as several solar modulation models (e.g., [Force-Field](http://adsabs.harvard.edu/abs/1968ApJ...154.1011G,1987A%26A...184..119P,1992ApJ...397..153P)). Even though USINE is meant to evolve to describe all CR species, models in this version are restricted to nuclei and anti-nuclei.

Galactic cosmic-ray propagation requires many inputs, which are handled by as many classes in USINE:

   - *CR charts*: element properties and nuclear charts specific to CRs (i.e. stable or half-life of the order of the propagation time)
   - *CR data*: list of data points (energy, value, date, name of experiment, etc.)
   - *Cross sections*: energy-dependent inelastic and production (straight-ahead or differential) cross sections on ISM elements
   - *CR sources*: source spectra and spatial distribution of CR sources  
   - *ISM*: composition of the InterStellar Medium (elements, density, temperature, ionization fraction)  
   - *CR transport*: coefficients for momentum and spatial diffusion, convection, reacceleration, energy losses
   - *Models*: CR propagation models to go from sources to interstellar fluxes and Solar modulation models to propagate interstellar (IS) to top-of-atmosphere (TOA) fluxes

## II. USINE version and contact

The first USINE version dates back to the last millennium and remained a private code for a long time. The first public release is version **v3.4**:
   - V1.0 [1999/10] -- C version *(D. Maurin; contributions from F. Donato, P. Salati, R. Taillet)*
   - V2.0 [2004/10] -- C++/ROOT version on svn *(D. Maurin; contrib. from A. Putze & R. Taillet)*
   - V3.0 [2013/12] -- Better C++/ROOT with FunctionParser & documentation *(D. Maurin)*
   - V3.4 [2018/07] -- First public release *(D. Maurin)*
   - V3.5 [2019/07] -- Release used to analyse B/C and antiproton AMS-02 data *(D. Maurin)*

For any question, or if you want to do some development with USINE,  [contact me](mailto:clumpy@lpsc.in2p3.fr).

## III. INSTALLATION AND COMPILATION

The USINE code is developed in C++ to benefit from object-oriented capabilities (overload, inheritance, etc.). It is interfaced with the [ROOT CERN library](https://root.cern.ch/) for displays and minimization routines, and with [fparser](http://warp.povusers.org/FunctionParser/fparser.html) to implement formulae with constants and parameters as simple text format.

**Third-party softwares**  

   - [fparser](http://warp.povusers.org/FunctionParser/fparser.html) is shipped with the code (no need to install!)
   - [ROOT CERN library](https://root.cern.ch/): The easiest way to install it on your system is via your package manager (apt-get, dnf, or brew, etc.), and do not forget the development (``-devel``) packages. For MacOS, we recommend to install gsl and optionally ROOT via homebrew. To define the environment variable ``ROOTSYS``, you need to source, e.g., in your configuration file ``~/.bashrc``,  ``thisroot.sh``/``thisroot.csh``. Alternatively, you may want to compile locally ROOT. To do so, [download a version](https://root.cern.ch/releases) and follow the configuration/compilation instructions to [build ROOT](https://root.cern.ch/building-root). In addition, in the latter case, do not forget to define the ``ROOTSYS`` environment variable in your ``~/.bashrc``). 

**Install USINE**   
1. *Clone from git repository* (if you seek for an old code release, see Release history)
   ```
   > git clone git@gitlab.com:dmaurin/USINE.git
   ```
2. The compilation relies on cmake \u2265 2.8 (file CMakeLists.txt)
   ```
   > cd USINE
   > mkdir build; cd build
   > cmake ../
   > make -jN   *[using N=2, 3,... cores]*
   ```

3. *Define the USINE environment variables* (e.g., in your ~/.bashrc)
   ```
   > export USINE=absolute_path_to_local_installation
   > export PATH=$USINE/bin:$PATH
   > export LD_LIBRARY_PATH=$USINE/lib:$LD_LIBRARY_PATH
   > export DYLD_LIBRARY_PATH=$USINE/lib:$DYLD_LIBRARY_PATH  *[Mac OS only]*
   ```
    
   *Do not forget to ``> source ~/.bashrc`` (or whereever your variables are defined) to ensure that the environment variables are set in your current xterm (``> echo $USINE`` should point to the directory where you installed USINE).*

4. *Test installation*
To ensure that `USINE` is ready to use, type
   ```
   > cd $USINE
   > ./bin/usine -tUSINE
   ```
   If all tests pass, then you are good to go! If not, check that some tests are indeed performed (i.e. there is some chatter when you run the test), and then: (i) if no chatter, it probably has to do with your installation (missing environment variable, missing ROOT package\u2026); (ii) if you are told that some checks failed, it may be related to your machine architecture/compiler, so contact the USINE developers (send your `.fail` test files) who will evaluate the severity of the problem.

5. *USINE execution*
   ```
   > ./bin/usine              [to access all USINE options]
   ```
   *Run examples are provided in the documentation (see http://lpsc.in2p3.fr/usine/) so that you can check the various options available and the expected outputs.*
