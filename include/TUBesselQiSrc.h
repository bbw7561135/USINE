// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUBesselQiSrc
#define USINE_TUBesselQiSrc

// C/C++ include
// ROOT include
// USINE include
#include "TUBesselJ0.h"
#include "TUValsTXYZEVirtual.h"

class TUBesselQiSrc {

private:
   TUAxis             *fAxisr;        // Grid for radial coordinates used to calculate fQi and fQiz
   TUAxis             *fAxisz;        // Grid for z-dependent coordinates used to calculate fQiz
   string              fFileName;     // File name from which (where) fQi are read (stored)
   Bool_t              fIsUseAxisr;   // Whether to delete or not fAxisr in destructor
   Bool_t              fIsUseAxisz;   // Whether to delete or not fAxisz in destructor
   Bool_t              fIsUseQi;      // Whether to delete or not fQi in destructor
   Bool_t              fIsUseQiz;     // Whether to delete or not fQiz in destructor
   Int_t               fNi;           // Number of F-B coefficients
   Double_t           *fQi;           //[Ni] F-B coefficients for source in disc [-]
   Double_t           *fQiz;          //[fNz*Ni] F-B coefficients for sources in halo [-]
   string              fTemplateName; // Template distribution from which fQi (or fQiz) is calculated
   string              fUID;          // Unique ID of fQi (or fQiz) associated to template

   string              FormFileNameBase(Bool_t is_qi_or_qiz) const;
   inline Bool_t       IsUseQi() const                             {return fIsUseQi;}
   inline Bool_t       IsUseQiz() const                            {return fIsUseQiz;}
   void                ResetAxisr();
   void                ResetAxisz();
   void                ResetQi();
   void                ResetQiz();

public:
   TUBesselQiSrc();
   virtual ~TUBesselQiSrc();

   void                CalculateAndStoreQi(TUBesselJ0 *bessel, TUValsTXYZEVirtual *spat_dist, Bool_t is_verbose, FILE *f_log);
   void                CalculateAndStoreQiz(TUBesselJ0 *bessel,  TUValsTXYZEVirtual *spat_dist, Bool_t is_verbose, FILE *f_log);
   inline string       GetFileName() const                         {return fFileName;}
   inline Int_t        GetNi() const                               {return fNi;}
   inline Int_t        GetNr() const                               {return fAxisr->GetN();}
   inline Int_t        GetNz() const                               {return fAxisz->GetN();}
   inline Double_t     GetQi(Int_t i_bess) const                   {return fQi[i_bess];}
   inline Double_t    *GetQi() const                               {return &fQi[0];}
   inline Double_t     GetQiz(Int_t i_bess, Int_t i_z) const       {return fQiz[i_z*fNi+i_bess];}
   inline Double_t    *GetQiz(Int_t i_z) const                     {return &fQiz[i_z*fNi+0];}
   inline string       GetTemplateName() const                     {return fTemplateName;}
   inline Double_t     Getz(Int_t i_z) const                       {return fAxisz->GetVal(i_z);}
   Int_t               IndexClostestz(Double_t z, Bool_t is_warning=true) const;
   void                Initialise(Bool_t is_delete);
   void                PrintQi(FILE *f=stdout) const;
   void                PrintQiz(FILE *f=stdout) const;
   void                ReadQi(string const &f_name, Int_t n_qi, Bool_t is_verbose, FILE *f_log);
   void                ReadQiz(string const &f_name, Int_t n_qi, Bool_t is_verbose, FILE *f_log);
   inline void         SetAxisr(Double_t const &rmax_kpc, Int_t n) {ResetAxisr(); fAxisr = new TUAxis(0., rmax_kpc, n, "AxisrIntegr", "kpc", kLIN); fIsUseAxisr = false;}
   inline void         SetAxisz(Double_t const &zmax_kpc, Int_t n) {ResetAxisz(); fAxisz = new TUAxis(0., zmax_kpc, n, "AxiszIntegr", "kpc", kLIN); fIsUseAxisz = false;}
   void                TEST(TUInitParList *init_pars, FILE *f=stdout);
   inline string       UID() const                                 {return fUID;}
   inline void         UseAxisr(TUAxis *axis_r)                    {ResetAxisr(); fAxisr = axis_r; fIsUseAxisr = true;}
   inline void         UseAxisz(TUAxis *axis_z)                    {ResetAxisz(); fAxisz = axis_z; fIsUseAxisz = true;}
   inline void         UseQi(Double_t *qi, Int_t ni)               {ResetQi(); fQi = qi; fIsUseQi = true; fNi = ni;}
   inline void         UseQiz(Double_t *qiz, Int_t ni)             {ResetQiz(); fQiz = qiz; fIsUseQiz = true; fNi = ni;}

   ClassDef(TUBesselQiSrc, 1)  // Fourier-Bessel coefficient expansion for astrophysical and DM sources
};

#endif
