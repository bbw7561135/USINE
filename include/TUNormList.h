// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUNormList
#define USINE_TUNormList

// C/C++ include
// ROOT include
// USINE include
#include "TUCRList.h"
#include "TUDataSet.h"
#include "TUNormEntry.h"

class TUNormList {

private:
   vector<vector<string> > fCRsNotToNorm;        //![NZNotToNorm][NCRs] For each Z not to norm, isotopes not to fit
   vector<TUNormEntry>     fQuery;               //![NQuery] List of queried elements/exp/data
   string                  fListNameFilter;      // CR list name used to filter out fQueries and fNormData
   vector<Int_t>           fMapNormDataToQuery;  //![NNorm] Each qty in fNormData has an index in fQuery
   vector<TUDataEntry>     fNormData;            //![NNorm] Data found (best match fQuery) sorted by growing mass
   vector<vector<Int_t> >  fNormIndicesInFilter; //![NNorm][NCRs] Indices in CR list used to filter out Norm quantities
   vector<Int_t>           fZNotToNorm;          //![NZNotToNorm] Keep track of Z removed from list

   inline void             AddQuery(TUNormEntry& query)                           {fQuery.push_back(query);}
   void                    FillDataFromQuery(TUDataSet* exp_data, Bool_t is_verbose, FILE *f_log, TUCRList *crs=NULL);
   inline Int_t            GetNCRsNotToNorm(Int_t i_not2norm) const               {return (Int_t)fCRsNotToNorm[i_not2norm].size();}
   inline Bool_t           IsZInListOfNotToNorm(Int_t z) const                    {return TUMisc::IsInList(fZNotToNorm, z);}
   string                  ListZNotToNormAndCRs() const;

public:
   TUNormList();
   TUNormList(TUNormList const &norm_list);
   TUNormList(TUInitParList *init_pars, TUDataSet *data, Bool_t is_verbose, FILE *f_log, TUCRList *crs=NULL);
   virtual ~TUNormList();

   inline TUNormList      *Clone()                                                {return new TUNormList(*this);}
   void                    Copy(TUNormList const &norm_list);
   inline TUNormList      *Create()                                               {return new TUNormList;}
   inline string           GetCRNotToNorm(Int_t i_not2norm, Int_t j_cr) const     {return fCRsNotToNorm[i_not2norm][j_cr];}
   inline string           GetListNameFilter() const                              {return fListNameFilter;}
   inline Int_t            GetNCRIndicesInNorm(Int_t i_norm) const                {return (Int_t)fNormIndicesInFilter[i_norm].size();}
   inline Int_t            GetNNorm() const                                       {return (Int_t)fNormData.size();}
   inline TUDataEntry      GetNormData(Int_t i_norm) const                        {return fNormData[i_norm];}
   inline string           GetNormName(Int_t i_norm) const                        {return fQuery[IndexNormInQueryList(i_norm)].GetName();}
   inline Int_t            GetNormZ(Int_t i_norm) const                           {return fQuery[IndexNormInQueryList(i_norm)].GetZ();}
   inline Int_t            GetNZNotToNorm() const                                 {return (Int_t)fZNotToNorm.size();}
   inline Int_t            GetZNotToNorm(Int_t i_not2norm) const                  {return fZNotToNorm[i_not2norm];}
   inline Int_t            GetNQuery() const                                      {return (Int_t)fQuery.size();}
   inline TUNormEntry      GetQuery(Int_t i_query) const                          {return fQuery[i_query];}
   inline Int_t            IndexCRInListOfNotToNorm(Int_t i_not2norm, string cr)  {return TUMisc::IndexInList(fCRsNotToNorm[i_not2norm], cr, true);}
   Int_t                   IndexInNormData(string const &qty) const;
   inline Int_t            IndexNormInFilter(Int_t i_norm, Int_t jcr) const       {return fNormIndicesInFilter[i_norm][jcr];}
   inline Int_t            IndexNormInQueryList(Int_t i_norm) const               {return fMapNormDataToQuery[i_norm];}
   inline Int_t            IndexZInListOfNotToNorm(Int_t z) const                 {return TUMisc::IndexInList(fZNotToNorm, z);}
   void                    Initialise();
   inline Bool_t           IsElement(Int_t i_norm) const                          {return fQuery[IndexNormInQueryList(i_norm)].IsElement();}
   inline string           ListZNotToNorm() const                                 {return (GetNZNotToNorm()==0) ? "-" : TUMisc::List2String(fZNotToNorm, ',');}
   void                    PrintNormData(FILE *f=stdout) const;
   void                    PrintNormQuery(FILE *f=stdout) const;
   void                    RemoveNorm(Int_t i_norm);
   void                    SetClass(TUInitParList *init_pars, TUDataSet *data, Bool_t is_verbose, FILE *f_log, TUCRList *crs=NULL);
   void                    TEST(TUInitParList *init_pars, FILE *f=stdout);
   void                    UpdateAndFillNotToNorm(vector<Int_t> const &z_not_to_norm, vector<vector<string> > const &crs_not_to_norm, Bool_t is_verbose, FILE *f_log);

   ClassDef(TUNormList, 1)  // Reference CR data query list and best-matching data found
};

#endif
