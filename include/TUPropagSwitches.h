// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUPropagSwitches
#define USINE_TUPropagSwitches

// C/C++ include
// ROOT include
// USINE include
#include "TUInitParList.h"

class TUPropagSwitches {

private:

   vector<string>           fNameSwitch;       //![NModes] Name associated to propagation effect
   vector<string>           fNameSwitchRoot;   //![NModes] Root name (useful for display)
   vector<string>           fNameSwitchShort;  //![NModes] Short name (useful for Root object names)
   vector<Bool_t>           fIsOn;             //![NModes] Propagation switch state (on or off)
   Bool_t                   fStatus;           // Set to 1 whenever (at least) one fIsOn is modified (0 otherwise)

   inline Bool_t            CheckIndex(Int_t index_switch) const        {return ((index_switch>=0 && index_switch<GetNSwitches()) ? true : false);}
   void                     Initialise();


public:
   TUPropagSwitches();
   TUPropagSwitches(TUPropagSwitches const &prop_switch);
   TUPropagSwitches(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   virtual ~TUPropagSwitches();

   inline TUPropagSwitches *Clone()                                      {return new TUPropagSwitches(*this);}
   void                     Copy(TUPropagSwitches const &prop_switch);
   inline TUPropagSwitches *Create()                                     {return new TUPropagSwitches;}
   void                     ExtractConfigurations(TUPropagSwitches const &on_off, vector<TUPropagSwitches> &list) const;
   string                   ExtractNameSwitch(Int_t index_switch, Int_t d0_root1_short2) const;
   string                   ExtractNameSwitches(Int_t d0_root1_short2, TUPropagSwitches *ref_config=NULL) const;
   inline Int_t             GetNSwitches() const                         {return (Int_t)fIsOn.size();}
   inline Bool_t            GetStatus() const                            {return fStatus;}
   inline Bool_t            IsDecayBETA() const                          {return fIsOn[10];}
   inline Bool_t            IsDecayEC() const                            {return fIsOn[11];}
   inline Bool_t            IsDecayFedBETA() const                       {return fIsOn[12];}
   inline Bool_t            IsDecayFedEC() const                         {return fIsOn[13];}
   inline Bool_t            IsDestruction() const                        {return fIsOn[2];}
   inline Bool_t            IsELossAdiabatic() const                     {return fIsOn[8];}
   inline Bool_t            IsELossBremsstrahlung() const                {return fIsOn[3];}
   inline Bool_t            IsELossCoulomb() const                       {return fIsOn[4];}
   inline Bool_t            IsELossIC() const                            {return fIsOn[5];}
   inline Bool_t            IsELossIon() const                           {return fIsOn[6];}
   inline Bool_t            IsELossSynchrotron() const                   {return fIsOn[7];}
   inline Bool_t            IsEReacceleration() const                    {return fIsOn[9];}
   inline Bool_t            IsELossesOrGains() const                     {return (IsELossAdiabatic() || IsELossBremsstrahlung() || IsELossCoulomb() || IsELossIC() || IsELossIon() || IsELossSynchrotron() || IsEReacceleration() ? true : false);}
   inline Bool_t            IsOn(Int_t index_switch) const               {if (CheckIndex(index_switch)) return fIsOn[index_switch]; else return false;}
   inline Bool_t            IsPrimExotic() const                         {return fIsOn[15];}
   inline Bool_t            IsPrimStandard() const                       {return fIsOn[0];}
   inline Bool_t            IsSecondaries() const                        {return fIsOn[1];}
   inline Bool_t            IsTertiaries() const                         {return fIsOn[14];}
   inline Bool_t            IsWind() const                               {return fIsOn[16];}
   void                     PrintPropagSwitches(FILE *f=stdout, Bool_t is_comparison=false) const;
   void                     SetAllSwitches(Bool_t true_or_false);
   void                     SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   inline void              SetIsDecayBETA(Bool_t is)                    {SetSwitch(10, is);}
   inline void              SetIsDecayEC(Bool_t is)                      {SetSwitch(11, is);}
   inline void              SetIsDecayFedBETA(Bool_t is)                 {SetSwitch(12, is);}
   inline void              SetIsDecayFedEC(Bool_t is)                   {SetSwitch(13, is);}
   inline void              SetIsDestruction(Bool_t is)                  {SetSwitch(2, is);}
   inline void              SetIsELossAdiabatic(Bool_t is)               {SetSwitch(8, is);}
   inline void              SetIsELossBremsstrahlung(Bool_t is)          {SetSwitch(3, is);}
   inline void              SetIsELossCoulomb(Bool_t is)                 {SetSwitch(4, is);}
   inline void              SetIsELossIC(Bool_t is)                      {SetSwitch(5, is);}
   inline void              SetIsELossIon(Bool_t is)                     {SetSwitch(6, is);}
   inline void              SetIsELossSynchrotron(Bool_t is)             {SetSwitch(7, is);}
   inline void              SetIsEReacceleration(Bool_t is)              {SetSwitch(9, is);}
   inline void              SetIsPrimExotic(Bool_t is)                   {SetSwitch(15, is);}
   inline void              SetIsPrimStandard(Bool_t is)                 {SetSwitch(0, is);}
   inline void              SetIsSecondaries(Bool_t is)                  {SetSwitch(1, is);}
   inline void              SetIsTertiaries(Bool_t is)                   {SetSwitch(14, is);}
   inline void              SetIsWind(Bool_t is)                         {SetSwitch(16, is);}
   inline void              SetStatus(Bool_t status)                     {fStatus = status;}
   void                     SetSwitch(Int_t index_switch, Bool_t is);
   void                     TEST(TUInitParList *init_pars, FILE *f=stdout);
   void                     TUI_ModifyPropagSwitches();

   ClassDef(TUPropagSwitches, 1)  // Enable/disable propagation options: decay, E losses,...
};

#endif

