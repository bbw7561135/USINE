// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUMessages
#define USINE_TUMessages

// C/C++ include
#include <string>
using namespace std;
// ROOT include
#include <TRint.h>
// USINE include

namespace TUMessages {

   void    Error(FILE *f, string const &class_name, string const &method, string const &message);
   string  Indent(Bool_t is_add_or_subtract);
   void    Processing(FILE *f, string const &class_name, string const &method, string const &message);
   void    Separator(FILE *f, string const &title, Int_t choice = 0);
   void    Test(FILE *f, string const &class_name, string const &message);
   void    Warning(FILE *f, string const &class_name, string const &method, string const &message);
}

#endif
