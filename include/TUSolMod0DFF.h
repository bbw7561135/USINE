// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUSolMod0DFF
#define USINE_TUSolMod0DFF

// C/C++ include
// ROOT include
// USINE include
#include "TUDataEntry.h"
#include "TUSolModVirtual.h"

class TUSolMod0DFF : public TUSolModVirtual {

protected:
   Int_t          fIndexphi;  // Index phi sol.mod. (free params. inherited from TUSolModVirtual)
   Double_t       fRig0;      // Critical rigidity in Rig0=0.2 GV (see, e.g., Perko ApJ 397, 1992)

   void           Initialise(Bool_t is_delete = true);

public:
   TUSolMod0DFF();
   TUSolMod0DFF(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   virtual ~TUSolMod0DFF();

   void                      DemodulateData(TUCREntry const &cr, TUDataEntry *data, Bool_t is_ratio, Bool_t is_verbose=false) const;
   Double_t                  EknTOAtoEknIS(TUCREntry const &cr, Double_t const &ekn_toa, Double_t const &phi_gv) const;
   Double_t                  EknIStoEknTOA(TUCREntry const &cr, Double_t const &ekn_is, Double_t const &phi_gv) const;
   virtual inline Double_t   EvalPhi(TUCREntry const &cr, Double_t const &ekn = 0) const {return TUFreeParList::GetParEntry(fIndexphi)->GetVal(false);}
   virtual inline gENUM_SOLMOD_MODEL GetModel() const      {return kSOLMOD0DFF;}
   inline Double_t           GetRig0(void) const           {return fRig0;}
   virtual void              IStoTOA(TUCREntry const &cr, TUAxis *axis_ekn, Double_t *vflux_is, Double_t *vflux_toa, Double_t *r);
   void                      IStoTOA(TUCREntry const &cr, TUAxis *axis_ekn, Double_t *vflux_is, Double_t *vflux_toa) const;
   void                      IStoTOA(TUCREntry const &cr, Int_t n_ekn, Double_t *vekn_is, Double_t *vflux_is, Double_t *vekn_toa, Double_t *vflux_toa) const;
   void                      IStoTOA_singleEkn(TUCREntry const &cr, Double_t const &ekn_is, Double_t const &flux_is, Double_t &ekn_toa, Double_t &flux_toa, Double_t const &phi_gv, Bool_t is_ratio = false) const;
   virtual void              Print(FILE *f, Bool_t is_summary) const;
   virtual void              SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   void                      Setphi(Double_t const &small_phi_gv);
   inline void               SetRig0(Double_t const &rc)   {fRig0 = rc;}
   virtual void              TEST(TUInitParList *init_pars, FILE *f=stdout);
   void                      TOAtoIS(TUCREntry const &cr, Int_t n_ekn, Double_t *vekn_is, Double_t *vflux_is, Double_t *vekn_toa, Double_t *vflux_toa, Double_t const &phi_gv) const;
   void                      TOAtoIS_singleEkn(TUCREntry const &cr, Double_t &ekn_is, Double_t &flux_is, Double_t const &ekn_toa, Double_t const &flux_toa, Double_t const &phi_gv, Bool_t is_ratio = false) const;

   ClassDef(TUSolMod0DFF, 1)  // Solar modulation 0D: force-field approx. [inherits from TUSolModVirtual]
};
#endif
