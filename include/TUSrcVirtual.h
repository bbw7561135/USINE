// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUSrcVirtual
#define USINE_TUSrcVirtual

// C/C++ include
// ROOT include
// USINE include
#include "TUSrcTemplates.h"

class TUSrcVirtual {

public:
   TUSrcVirtual();
   virtual ~TUSrcVirtual() = 0;

   virtual TUSrcVirtual              *Clone() = 0;
   virtual void                       CopySpectrumOrSpatDist(TUValsTXYZEVirtual *spect_or_spatdist, Bool_t is_spect_or_spatdist) = 0;
   virtual TUSrcVirtual              *Create() = 0;
   virtual void                       FillSpectrumOrSpatialDist(string const &formula, Bool_t is_verbose, FILE *f_log, string const &commasep_vars, TUFreeParList* free_pars, Bool_t is_use_or_copy_freepars, Bool_t is_spect_or_spatdist) = 0;
   virtual void                       FillSpectrumOrSpatialDistFromTemplate(TUSrcTemplates *templates, string const &templ_name, Bool_t is_spect_or_spatdist, FILE *f_log) = 0;
   virtual TUFreeParList             *GetSrcFreePars(Bool_t is_spect_or_spatdist) = 0;
   virtual string                     GetSrcName() const = 0;
   inline virtual TUValsTXYZEVirtual *GetSrcSpatialDist() const                                       {TUValsTXYZEVirtual *dummy = NULL; return dummy;}
   virtual TUValsTXYZEVirtual        *GetSrcSpectrum() const = 0;
   inline virtual TUValsTXYZEFormula *GetTDepPosition(Int_t i_coord) const                            {TUValsTXYZEFormula *dummy = NULL; return dummy;}
   virtual void                       Initialise(Bool_t is_delete) = 0;
   virtual void                       PrintSrc(FILE *f=stdout, Bool_t is_header=true) const = 0;
   virtual void                       SetSrcName(string const &name) = 0;
   virtual void                       TEST(TUInitParList *init_pars, FILE *f=stdout) = 0;
   virtual Bool_t                     TUI_ModifyFreeParameters() = 0;
   inline virtual string              UIDSpatialDist() const                                          {return "";}
   inline virtual string              UIDSpectrum() const                                             {return "";}
   virtual void                       UpdateFromFreeParsAndResetStatus() = 0;
   virtual void                       UseSpectrumOrSpatDist(TUValsTXYZEVirtual *spect_or_spatdist, Bool_t is_spect_or_spatdist) = 0;
   inline virtual Double_t            ValueSrcPosition(Int_t i_coord/*0,1,2*/, Double_t const &t_myr) {return 0.;}
   inline virtual Double_t            ValueSrcSpatialDistrib(TUCoordTXYZ *coord_txyz = NULL)          {return 0.;}
   virtual Double_t                   ValueSrcSpectrum(TUCoordE *coord_e, TUCoordTXYZ *coord_txyz = NULL) = 0;

   ClassDef(TUSrcVirtual, 1)  // Abstract class for source container
};

#endif

