// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUValsTXYZEFormula
#define USINE_TUValsTXYZEFormula

#ifdef __MAKECINT__
class FunctionParser{};
#else
#include "../FunctionParser/fparser.hh"
#endif
// C/C++ include
// ROOT include
// USINE include
#include "TUAxesCrE.h"
#include "TUValsTXYZEVirtual.h"

class TUValsTXYZEFormula : public TUValsTXYZEVirtual {
private:
   FunctionParser                     fFormula;          // Formula (http://warp.povusers.org/FunctionParser)
   string                             fFormulaString;    // Formula description (FunctionParser and C-ANSI math)
   string                             fFormulaVars;      // Comma-separated list of variables (case sensitive)
   TUFreeParList                     *fFreePars;         // Free parameters for formula [aka formula 'constants']
   Bool_t                             fIsDeleteFreePars; // Whether to delete or not fFreePars in destructor
   string                             fName;             // Name of this formula (optional)
   Int_t                              fNVar;             // Number of variables in fFormula
   void                               SetOrUpdateFormula(string const &fparser_formula, string const &commasep_vars, TUFreeParList *formula_constants, Bool_t is_use_or_copy_freepars, Bool_t is_update_formula);

public:
   TUValsTXYZEFormula();
   TUValsTXYZEFormula(TUValsTXYZEFormula const &vals);
   virtual ~TUValsTXYZEFormula();


   inline virtual TUValsTXYZEFormula *Clone()                                                        {return new TUValsTXYZEFormula(*this);}
   void                               Copy(TUValsTXYZEFormula const &vals);
   inline virtual TUValsTXYZEFormula *Create()                                                       {return new TUValsTXYZEFormula;}
   Double_t                           EvalFormula(Double_t *vals_txyz, Double_t *edep_vals);
   inline Double_t                    EvalFormulaE(Double_t *edep_vals)                              {UpdateFromFreeParsAndResetStatus(false); return fFormula.Eval(edep_vals);}
   inline Double_t                    EvalFormulaTXYZ(Double_t *vals_txyz=NULL)                      {UpdateFromFreeParsAndResetStatus(false); return fFormula.Eval(vals_txyz);}
   inline FunctionParser             *GetFormula()                                                   {return &fFormula;}
   inline virtual string              GetFormulaString() const                                       {return fFormulaString;}
   inline virtual string              GetFormulaVars() const                                         {return fFormulaVars;}
   inline virtual TUFreeParList      *GetFreePars() const                                            {return fFreePars;}
   inline virtual string              GetName() const                                                {return fName;}
   inline Int_t                       GetNVar() const                                                {return fNVar;}
   inline Bool_t                      IsDeleteFreePars() const                                       {return fIsDeleteFreePars;}
   virtual void                       Initialise(Bool_t is_delete=false);
   void                               PrintFormula(FILE *f=stdout) const;
   virtual void                       PrintSummary(FILE *f=stdout, string const &description = "") const;
   virtual void                       SetClass(string const &formula, Bool_t is_verbose, FILE *f_log, string const &commasep_vars="", TUFreeParList *free_pars = NULL, Bool_t is_use_or_copy_freepars=true);
   inline void                        SetFormula(string const &fparser_formula, string const &commasep_vars, TUFreeParList *formula_constants=NULL, Bool_t is_use_or_copy_freepars=true) {SetOrUpdateFormula(fparser_formula,commasep_vars,formula_constants,is_use_or_copy_freepars,false);}
   inline virtual void                SetName(string const &name)                                    {fName = name;}
   void                               TEST(FILE *f=stdout);
   inline virtual string              UID() const                                                    {return ((fFreePars) ? (fName + "_" + fFreePars->UID()) : fName);}
   virtual void                       UpdateFromFreeParsAndResetStatus(Bool_t is_force_update);
   inline virtual Double_t            Value()                                                        {return EvalFormulaTXYZ();}
   inline virtual Double_t            ValueE(TUCoordE *coord_e)                                      {return EvalFormulaE(coord_e->GetValsE());}
   inline virtual Double_t            ValueETXYZ(TUCoordE *coord_e, TUCoordTXYZ *coord_txyz)         {if (coord_txyz) return EvalFormula(coord_txyz->GetVals(), coord_e->GetValsE()); else {vector<Double_t> dummy_txyz(4,0.); return EvalFormula(&dummy_txyz[0], coord_e->GetValsE());}}
   inline virtual Double_t            ValueTXYZ(TUCoordTXYZ *coord_txyz)                             {return ((coord_txyz) ? EvalFormulaTXYZ(coord_txyz->GetVals()) : Value());}
   inline virtual Int_t               ValsTXYZEFormat() const                                        {return 0; /*fparser formula*/}

  ClassDef(TUValsTXYZEFormula, 1)  // Space-time and e-dependent formula values [inherits from TUValsTXYZ]
};

#endif
