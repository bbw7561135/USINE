// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUValsTXYZECr
#define USINE_TUValsTXYZECr

// C/C++ include
// ROOT include
#include <TGraph.h>
#include <TH1D.h>
// USINE include
#include "TUAxesCrE.h"
#include "TUMisc.h"
#include "TUSolModVirtual.h"
#include "TUValsTXYZEFormula.h"
#include "TUValsTXYZEGrid.h"
#include "TUValsTXYZEVirtual.h"

class TUValsTXYZECr : public TUFreeParList {

private:
   TUAxesCrE                   *fAxesCrE;               // CRs (charts+parents) and E axes (one per CR family)
   TUAxesTXYZ                  *fAxesTXYZ;              // T,X,Y,Z axes
   Bool_t                       fIsDeleteCrE;           // Whether destructor deletes fAxesCrE or not
   Bool_t                       fIsDeleteTXYZ;          // Whether destructor deletes fAxesTXYZ or not
   TUValsTXYZEVirtual         **fValsTXYZE;             //![NCRs] Formulae or values on TXYZE grid
   gENUM_FCNTYPE               *fValsTXYZEFormat;       //![NCRs] kFORMULA, kSPLINE, kGRID (see TUEnum)

   void                         AllocateValsNCRs(Int_t ncrs);
   //inline TGraph              *OrphanElementFraction(Int_t z_element, TUCoordTXYZ *coord_txyz=NULL)      {return OrphanNuclearFraction(z_element, coord_txyz, true);}
   //inline TH1D                *OrphanElementFraction(Double_t ekn_gevn, TUCoordTXYZ *coord_txyz=NULL, string const &start="DEFAULT", string const &stop="DEFAULT")  {return OrphanNuclearFraction(ekn_gevn, coord_txyz, start, stop, true);}
   //inline TGraph              *OrphanIsotopicFraction(Int_t j_cr, TUCoordTXYZ *coord_txyz=NULL)          {return OrphanNuclearFraction(j_cr, coord_txyz, false);}
   //inline TH1D                *OrphanIsotopicFraction(Double_t ekn_gevn, TUCoordTXYZ *coord_txyz=NULL, string const &start="DEFAULT", string const &stop="DEFAULT") {return OrphanNuclearFraction(ekn_gevn, coord_txyz, start, stop, false);}
   //TH1D                       *OrphanNuclearFraction(Double_t ekn_gevn, TUCoordTXYZ *coord_txyz, string start, string stop, Bool_t is_element_or_isotope);
   //TGraph                     *OrphanNuclearFraction(Int_t z_or_index_cr, TUCoordTXYZ *coord_txyz, Bool_t is_element_or_isotope);
   Double_t                     *OrphanValsCR(Int_t bincr, TUAxis *e_grid, gENUM_ETYPE e_type, Double_t const &e_power, TUCoordTXYZ *coord_txyz, TUSolModVirtual *model_solmod=NULL);

public:
   TUValsTXYZECr();
   TUValsTXYZECr(TUValsTXYZECr const &vals_txyzcre);
   virtual ~TUValsTXYZECr();

   void                          AllocateVals(Int_t bincr, gENUM_FCNTYPE fcn_type);
   inline void                   AllocateForAxesCrE(TUAxesCrE *axes_cre, Bool_t is_use_or_copy)            {TUAxesTXYZ *axes_txyz=NULL; AllocateForAxesTXYZCrE(axes_txyz, axes_cre, is_use_or_copy);}
   void                          AllocateForAxesTXYZCrE(TUAxesTXYZ *axes_txyz, TUAxesCrE *axes_cre, Bool_t is_use_or_copy);
   void                          AllocateForAxesTXYZCrE(TUInitParList *init_pars, string const &group, FILE *f_log);
   inline virtual TUValsTXYZECr *Clone()                                                                   {return new TUValsTXYZECr(*this);}
   void                          Copy(TUValsTXYZECr const &vals_txyzcre);
   inline virtual TUValsTXYZECr *Create()                                                                  {return new TUValsTXYZECr;}
   inline gENUM_FCNTYPE          ExtractFormat(Int_t bincr) const                                          {return fValsTXYZEFormat[bincr];}
   inline TUAxesCrE             *GetAxesCrE() const                                                        {return fAxesCrE;}
   inline TUAxesTXYZ            *GetAxesTXYZ() const                                                       {return fAxesTXYZ;}
   inline TUCREntry              GetCREntry(Int_t bincr) const                                             {return fAxesCrE->GetCREntry(bincr);}
   inline TUAxis                *GetE(Int_t bincr) const                                                   {return fAxesCrE->GetE(bincr);}
   inline Double_t               GetE(Int_t bincr, Int_t bine, gENUM_ETYPE e_type) const                   {return fAxesCrE->GetE(bincr, bine, e_type);}
   inline TUAxis                *GetEFamily(gENUM_CRFAMILY cr_family) const                                {return fAxesCrE->GetEFamily(cr_family);}
   inline gENUM_ETYPE            GetEType(Int_t bincr) const                                               {return fAxesCrE->GetEType(bincr);}
   inline TUFreeParList         *GetFreePars()                                                             {return this;}
   inline Int_t                  GetNCRs() const                                                           {return ((fAxesCrE) ? fAxesCrE->GetNCRs() : 0);}
   inline Int_t                  GetNE() const                                                             {return ((fAxesCrE) ? fAxesCrE->GetNE() : 0);}
   inline TUAxis                *GetT() const                                                              {return fAxesTXYZ->GetT();}
   inline TUValsTXYZEVirtual    *GetValsTXYZE(Int_t bincr) const                                           {return fValsTXYZE[bincr];}
   inline TUAxis                *GetX() const                                                              {return fAxesTXYZ->GetX();}
   inline TUAxis                *GetY() const                                                              {return fAxesTXYZ->GetY();}
   inline TUAxis                *GetZ() const                                                              {return fAxesTXYZ->GetZ();}
   void                          Initialise(Bool_t is_delete=false);
   Double_t                      IntegratedValOnERange(string &combo, gENUM_ETYPE e_type, Int_t n_extrapts4integ, Double_t &e_min, Double_t &e_max, Double_t const &e_power, TUCoordTXYZ *coord_txyz = NULL, TUSolModVirtual *model_solmod = NULL);
   vector<Double_t>              IntegratedValOnERange(string &combo, gENUM_ETYPE e_type, Int_t n_extrapts4integ, vector<Double_t> &l_emin, vector<Double_t> &l_emax, Double_t const &e_power, TUCoordTXYZ *coord_txyz = NULL, TUSolModVirtual *model_solmod = NULL);
   inline Bool_t                 IsDeleteCrE() const                                                       {return fIsDeleteCrE;}
   inline Bool_t                 IsDeleteTXYZ() const                                                      {return fIsDeleteTXYZ;}
   inline TUAxis                *OrphanGetECombo(string const &combo, gENUM_ETYPE e_type, FILE *f=stdout) const {return fAxesCrE->OrphanGetECombo(combo,e_type, f);}
   inline TUAxis                *OrphanGetECRs(vector<Int_t> const &cr_indices, gENUM_ETYPE e_type) const  {return fAxesCrE->OrphanGetECRs(cr_indices,e_type);}
   Double_t                     *OrphanVals(Int_t bincr, TUAxis *e_grid, gENUM_ETYPE e_type, Double_t const &e_power = 0., TUCoordTXYZ *coord_txyz = NULL);
   Double_t                     *OrphanVals(string combo, TUAxis *e_grid, gENUM_ETYPE e_type, Double_t const &e_power, TUCoordTXYZ *coord_txyz = NULL, TUSolModVirtual *model_solmod = NULL);
   inline void                   PrintSummary(FILE *f, Int_t bincr) const                                  {fValsTXYZE[bincr]->PrintSummary(f);}
   void                          PrintVals(FILE *f, Int_t bincr, TUAxis *e_grid, gENUM_ETYPE e_type, Double_t const &e_power = 0., TUCoordTXYZ *coord_txyz = NULL);
   void                          PrintVals(FILE *f, TUCoordTXYZ *coord_txyz = NULL);
   void                          PrintVals(FILE *f, string const &combo, TUAxis *e_grid, gENUM_ETYPE e_type, Double_t const &e_power, TUCoordTXYZ *coord_txyz = NULL, TUSolModVirtual* model_solmod=NULL);
   void                          PrintVals(FILE *f, string const &combo, TUAxis *e_grid, gENUM_ETYPE e_type, Double_t const &e_power, TUCoordTXYZ *coord_txyz, vector<TUSolModVirtual*> &models_solmod);
   void                          SetValsE_Formula(Int_t bincr, string const &fparser_formula, Bool_t is_verbose, FILE *f_log, string const &commasep_vars = "", TUFreeParList* free_pars = NULL, Bool_t is_use_or_copy_freepars = true);
   void                          SetValsE_Grid(Int_t bincr, string const &file, Bool_t is_verbose, FILE *f_log);
   void                          SetValsE_Grid(Int_t bincr, TUValsTXYZEGrid *vals_grid, Bool_t is_use_or_copy);
   void                          SetValsE_Grid(Int_t bincr, vector<Double_t> const &vals);
   void                          SetValsTXYZ_Grid(Int_t bincr, string const &root_tformula_formatted);
   void                          Spectrum(string combo, TUAxis *e_grid, gENUM_ETYPE e_type, Double_t const &e_power, vector<Double_t> &x, vector<Double_t> &y, TUCoordTXYZ *coord_txyz=NULL, TUSolModVirtual *model_solmod=NULL);
   void                          TEST(TUInitParList *init_pars, FILE *f=stdout);
   void                          UpdateFromFreeParsAndResetStatus();
   inline Double_t               Value(Int_t bincr)                                                        {UpdateFromFreeParsAndResetStatus(); return fValsTXYZE[bincr]->Value();}
   inline Double_t               ValueE(Int_t bincr, TUCoordE *coord_e)                                    {UpdateFromFreeParsAndResetStatus(); return fValsTXYZE[bincr]->ValueE(coord_e);}
   inline Double_t               ValueETXYZ(Int_t bincr, TUCoordE *coord_e, TUCoordTXYZ *coord_txyz)       {UpdateFromFreeParsAndResetStatus(); return fValsTXYZE[bincr]->ValueETXYZ(coord_e, coord_txyz);}
   Double_t                      ValueElementFraction(Int_t z_element, Double_t const &ekn_gevn, TUCoordTXYZ *coord_txyz);
   Double_t                      ValueIsotopicFraction(Int_t j_cr, Double_t const &ekn_gevn, TUCoordTXYZ *coord_txyz);
   inline Double_t               ValueTXYZ(Int_t bincr, TUCoordTXYZ *coord_txyz)                           {UpdateFromFreeParsAndResetStatus(); return fValsTXYZE[bincr]->ValueTXYZ(coord_txyz);}

   ClassDef(TUValsTXYZECr, 1)  // T,X,Y,Z,E containers for CR list + extract CR combos [grid or formula]
};

#endif
