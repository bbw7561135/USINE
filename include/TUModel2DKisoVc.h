// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUModel2DKisoVc
#define USINE_TUModel2DKisoVc

// C/C++ include
// ROOT include
// USINE include
#include "TUBesselQiSrc.h"
#include "TUModelBase.h"

class TUModel2DKisoVc : public TUModelBase, public TUBesselJ0 {

private:
   Double_t               *fAiTerm;              //![NBess*NCRs*NE]
   TUAxis                  fAxisRIntegr;         // Grid for radial coordinate (r), used for FB coeffs.
   TUAxis                  fAxisZIntegr;         // Grid for z-dependent coordinate, used for fQizOnGrid
   Double_t               *fCoefPreFactor;       //![NBess*NCRs*NE] Term 2h/(Ai*Ek) in Eq.(1)  [Myr/GeV]
   Int_t                   fIndexPar_h;          // Index h (disc half-thickness) in TUFreeParList [kpc]
   Int_t                   fIndexPars_L;         // Index L (halo half-thickness) in TUFreeParList [kpc]
   Int_t                   fIndexPars_R;         // Index R (Galaxy radius) in TUFreeParList [kpc]
   Int_t                   fIndexPars_rh;        // Index rh (local bubble radius) in TUFreeParList [kpc]
   gENUM_MATRIXINVERT      fInvertScheme;        // Scheme chosen for matrix inversion (kTRID or kGAUSSJORDAN)
   TUMediumEntry          *fISMEntry;            // 2D model constant interstellar medium
   Double_t               *fNiPrim;              //![NBess*NCRs*NE] FB coefs. Ni (primary only) [(GeV/n)^{-1}.m^{-3}]
   Double_t               *fNiTot;               //![NBess*NCRs*NE] FB coefs. Ni [(GeV/n)^{-1}.m^{-3}]
   TUBesselQiSrc         **fQiAstro;             //![NSrcAstro][NSpecies(src)] FB coefs. for astro sources (disc)
   TUBesselQiSrc         **fQizDM;               //![NSrcDM][NSpecies(src)]] FB coefs. for DM sources (halo)


   Double_t                Ai(Int_t i_bess, Int_t j_cr, Int_t k_ekn);
   Double_t                Ai(Int_t i_bess, Int_t j_cr, Int_t k_ekn, Double_t const &kdiff);
   Double_t                Ai(Double_t const &si, Int_t j_cr, Int_t k_ekn, Double_t const &kdiff);
   virtual Bool_t          CalculateChargedCRs(Int_t jcr_start, Int_t jcr_stop, Bool_t is_verbose, FILE *f_log);
   void                    FillCoefPreFactorAndAiTerm();
   inline Double_t        *GetAiTerm(Int_t i_bess, Int_t j_cr) const                               {return &fAiTerm[i_bess*GetNE()*GetNCRs() + j_cr*GetNE() + 0];}
   inline Double_t         GetAiTerm(Int_t i_bess, Int_t j_cr, Int_t k_ekn) const                  {return fAiTerm[i_bess*GetNE()*GetNCRs() + j_cr*GetNE() + k_ekn];}
   inline Double_t        *GetCoefPreFactor(Int_t i_bess, Int_t j_cr) const                        {return &(fCoefPreFactor[i_bess*GetNE()*GetNCRs() + j_cr*GetNE() + 0]);}
   inline Double_t         GetCoefPreFactor(Int_t i_bess, Int_t j_cr, Int_t k_ekn) const           {return fCoefPreFactor[i_bess*GetNE()*GetNCRs() + j_cr*GetNE() + k_ekn];}
   inline Double_t        *GetNiPrim() const                                                       {return fNiPrim;}
   inline Double_t        *GetNiPrim(Int_t i_bess, Int_t j_cr) const                               {return &(fNiPrim[i_bess * (GetNE() * GetNCRs()) + j_cr * GetNE() + 0]);}
   inline Double_t         GetNiPrim(Int_t i_bess, Int_t j_cr, Int_t k_ekn) const                  {return fNiPrim[i_bess * (GetNE() * GetNCRs()) + j_cr * GetNE() + k_ekn];}
   inline Double_t        *GetNiTot() const                                                        {return fNiTot;}
   inline Double_t        *GetNiTot(Int_t i_bess, Int_t j_cr) const                                {return &(fNiTot[i_bess * (GetNE() * GetNCRs()) + j_cr * GetNE() + 0]);}
   inline Double_t         GetNiTot(Int_t i_bess, Int_t j_cr, Int_t k_ekn) const                   {return fNiTot[i_bess * (GetNE() * GetNCRs()) + j_cr * GetNE() + k_ekn];}
   Int_t                   GetNR() const                                                           {return fAxisRIntegr.GetN();}
   Int_t                   GetNZ() const                                                           {return fAxisZIntegr.GetN();}
   inline Double_t         GetQiAstro(Int_t i_bess, Int_t j_species, Int_t s_src) const            {return fQiAstro[s_src][j_species].GetQi(i_bess);}
   inline Double_t         GetQizDM(Int_t i_bess, Int_t j_species, Int_t s_src, Int_t i_z) const   {return fQizDM[s_src][j_species].GetQiz(i_bess, i_z);}
   inline Double_t        *GetQizDM(Int_t j_species, Int_t s_src, Int_t i_z) const                 {return fQizDM[s_src][j_species].GetQiz(i_z);}
   void                    Initialise(Bool_t is_delete);
   void                    InitialiseSrc(Bool_t is_delete);
   void                    IterateTertiariesNi(Int_t i_bess, Int_t j_cr, Double_t* sterm_tot,  Double_t* sterm_prim, Double_t const &eps, Bool_t is_verbose, FILE *f_log);
   Double_t                Niz_WithParentBETA(Int_t j_parent, Int_t i_bess, Int_t j_cr, Int_t k_ekn, Double_t z);
   Double_t                Niz_WithoutParentBETA(Double_t const &ni0, Int_t i_bess, Int_t j_cr, Int_t k_ekn, Double_t z);
   Double_t                Ni_PrimContribInHalo_Yiz(Int_t i_bess, Int_t j_cr, Int_t k_ekn, Int_t i_z, Double_t const &kdiff, Double_t const &si);
   Double_t                Ni_PrimContribInHalo_Yiz(Int_t i_bess, Int_t j_cr, Int_t k_ekn, Double_t z, Double_t const &kdiff, Double_t const &si);
   Double_t                Ni_PrimContribInHalo_z0(Int_t i_bess, Int_t j_cr, Int_t k_ekn);
   inline void             SetAiTerm(Int_t i_bess, Int_t j_cr, Int_t k_ekn, Double_t const &val)   {fAiTerm[i_bess * (GetNE()*GetNCRs()) + j_cr * GetNE() + k_ekn] = val;}
   virtual inline Bool_t   SetClass_ModelSpecific(TUInitParList* init_pars, Bool_t is_verbose, FILE *f_log);
   inline void             SetCoefPreFactor(Int_t i_bess, Int_t j_cr, Int_t k_ekn, Double_t const &val) {fCoefPreFactor[i_bess*GetNE()*GetNCRs() + j_cr*GetNE() + k_ekn] = val;}
   inline void             SetNiPrim(Int_t i_bess, Int_t j_cr, Int_t k_ekn, Double_t const &val)   {fNiPrim[i_bess * (GetNE()*GetNCRs()) + j_cr * GetNE() + k_ekn] = val;}
   inline void             SetNiTot(Int_t i_bess, Int_t j_cr, Int_t k_ekn, Double_t const &val)    {fNiTot[i_bess * (GetNE()*GetNCRs()) + j_cr * GetNE() + k_ekn] = val;}
   Double_t                Si(Int_t i_bess, Int_t j_cr, Int_t k_ekn, Double_t const &kdiff);
   Double_t                Si(Int_t i_bess, Int_t j_cr, Int_t k_ekn);

public:
   TUModel2DKisoVc();
   virtual ~TUModel2DKisoVc();
   inline virtual Double_t AdiabaticLosses(Int_t j_cr, Double_t const &ek, TUCoordTXYZ *coord_txyz=NULL) {return (-ek*(2.*GetCREntry(j_cr).GetmGeV()+ek)/(GetCREntry(j_cr).GetmGeV()+ek)*ValueVc_kpcperMyr()/(3.*GetParVal_h()));}
   virtual Double_t        CoeffReac1stOrder(Int_t j_cr, Int_t k_ekn);
   virtual Double_t        CoeffReac2ndOrderLowerHalfBin(Int_t j_cr, Int_t k_ekn);
   Double_t                EvalFlux(Int_t j_cr, Int_t k_ekn, Double_t r, Double_t z, Int_t tot0_prim1_sec2=0);
   inline virtual Double_t EvalFluxPrim(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz=NULL)     {return EvalFlux(j_cr, k_ekn, coord_txyz->GetValX(), coord_txyz->GetValY(), 1);}
   inline virtual Double_t EvalFluxTot(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz=NULL)      {return EvalFlux(j_cr, k_ekn, coord_txyz->GetValX(), coord_txyz->GetValY(), 0);}
   inline TUFreeParEntry  *GetPar_h()                                                              {return TUModelBase::TUFreeParList::GetParEntry(fIndexPar_h);}
   inline TUFreeParEntry  *GetPar_L()                                                              {return TUModelBase::TUFreeParList::GetParEntry(fIndexPars_L);}
   inline TUFreeParEntry  *GetPar_R()                                                              {return TUModelBase::TUFreeParList::GetParEntry(fIndexPars_R);}
   inline TUFreeParEntry  *GetPar_rh()                                                             {return TUModelBase::TUFreeParList::GetParEntry(fIndexPars_rh);}
   inline Double_t         GetParVal_h() const                                                     {return TUModelBase::TUFreeParList::GetParEntry(fIndexPar_h)->GetVal(false);}
   inline Double_t         GetParVal_L() const                                                     {return TUModelBase::TUFreeParList::GetParEntry(fIndexPars_L)->GetVal(false);}
   inline Double_t         GetParVal_R() const                                                     {return TUModelBase::TUFreeParList::GetParEntry(fIndexPars_R)->GetVal(false);}
   inline Double_t         GetParVal_rh() const                                                    {return TUModelBase::TUFreeParList::GetParEntry(fIndexPars_rh)->GetVal(false);}
   inline Double_t         GetRSol() const                                                         {return TUModelBase::GetSolarSystemCoords()->GetValX();}
   virtual void            InitialiseForCalculation(Bool_t is_init_or_update, Bool_t is_force_update, Bool_t is_verbose, FILE *f_log);
   void                    TEST(TUInitParList *init_pars, FILE *f=stdout);
   Double_t                ValueK(Int_t j_cr, Int_t k_ekn, Int_t halfbin_status=0);
   Double_t                ValueKpp(Int_t j_cr, Int_t k_ekn, Int_t halfbin_status=0);
   inline Double_t         ValueVa_kmpers()                                                        {return TUModelBase::ValueVA();}
   inline Double_t         ValueVa_kpcperMyr()                                                     {return (ValueVa_kmpers()*TUPhysics::Convert_kmpers_to_kpcperMyr());}
   inline Double_t         ValueVc_kmpers()                                                        {TUCoordTXYZ *coord_txyz=NULL; return TUModelBase::ValueWind(coord_txyz, 1);}
   inline Double_t         ValueVc_kpcperMyr()                                                     {return (ValueVc_kmpers()*TUPhysics::Convert_kmpers_to_kpcperMyr());}

   ClassDef(TUModel2DKisoVc, 1)  // Propagation model 2D (thin-disc, thick-halo): K_iso(E)+Vc
};

#endif
