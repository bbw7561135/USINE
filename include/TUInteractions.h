// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUInteractions
#define USINE_TUInteractions

// C/C++ include
// ROOT include
#include <TGraph.h>
// USINE include
#include "TUAtomProperties.h"
#include "TUCREntry.h"
#include "TUMediumEntry.h"

namespace TUInteractions {

   Double_t DEdtCoulomb(TUCREntry const &cr, TUMediumEntry *medium, Double_t const &ekn);
   Double_t DEdtIonisation(TUCREntry const &cr, TUMediumEntry *medium, TUAtomProperties *atom, Double_t const &ekn);
   Double_t DEdtIonCoulomb(TUCREntry const &cr, TUMediumEntry *medium, TUAtomProperties *atom, Double_t const &ekn);
   Double_t DEdtIonCoulomb(TUCREntry const &cr, TUMediumEntry *medium, TUAtomProperties *atom, Double_t const &beta, Double_t const &gamma);

   TGraph  *OrphanGetGraphELoss(TUCREntry const &cr, TUMediumEntry *medium, TUAtomProperties *atom, TUAxis *ekn, Bool_t is_ion_or_coulomb, Bool_t is_rate_or_time);
   void     PlotEC(TApplication *my_app, Bool_t is_test=false, FILE *f_test=stdout);
   void     PlotELosses(vector<TUCREntry> &crs, TUMediumEntry *medium, TUAtomProperties *atom, TUAxis *ekn, TApplication *my_app, Bool_t is_test=false, FILE *f_test=stdout);

   // Electron capture (EC) for K-shell = REC(radiative) + NREC (non-radiative) + ECPP
   Double_t SigEC_OBK(TUCREntry const &cr, Int_t zt, Double_t const &ekn);
   Double_t SigEC_OBK2(TUCREntry const &cr, Int_t zt, Double_t const &ekn);
   Double_t SigEC_Letaw(TUCREntry const &cr, Int_t zt, Double_t const &ekn);
   Double_t SigNREC_Letaw(TUCREntry const &cr, Int_t zt, Double_t const &ekn);
   Double_t SigNREC_Eichler(TUCREntry const &cr, Int_t zt, Double_t const &ekn);
   Double_t SigREC_Letaw(TUCREntry const &cr, Int_t zt, Double_t const &ekn);
   Double_t SigREC_Anholt(TUCREntry const &cr, Int_t zt, Double_t const &ekn, TUAtomProperties *atom);
   // Electron stripping (ES) for K-shell
   Double_t SigES_Letaw(TUCREntry const &cr, Int_t zt, Double_t const &ekn);
   Double_t SigES_Bohr(TUCREntry const &cr, Int_t zt, Double_t const &ekn);
   Double_t SigES_Gillespie(TUCREntry const &cr, Int_t zt, Double_t const &ekn);
   Double_t SigES_Baltz(TUCREntry const &cr, Int_t zt, Double_t const &ekn);
   Double_t SigES_Meyerhof(TUCREntry const &cr, Int_t zt, Double_t const &ekn);

   Double_t Sigpp(Double_t const &etot, Int_t el0_inel1_tot2);

   void     TEST(FILE *f=stdout);
}
#endif
