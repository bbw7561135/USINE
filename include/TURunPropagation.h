// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TURunPropagation
#define USINE_TURunPropagation

// C/C++ include
#include <vector>
#include <utility>
// ROOT include
#include <Math/Minimizer.h>
#include <Math/Factory.h>
#include <Math/Functor.h>
#include <TFile.h>
#include <TGraph.h>
#include <TGraphAsymmErrors.h>
#include <TLegend.h>
#include <TMultiGraph.h>
// USINE include
#include "TUModelBase.h"
#include "TURunOutputs.h"


class TURunPropagation {

public:

   struct Contour_t {
      Int_t            i_par1; // Index of first parameter
      Int_t            i_par2; // Index of second parameter
      Int_t            n_pts;  // Number of points to use for contour
      vector<Double_t> cls;    // List of confidence levels (in sigma unit)
      TMultiGraph     *mg;     // MultiGraph for contours (one graph per confidence level)
      TLegend         *leg;    // Legend for contours (one per CL)
   }; // Structure to store contour properties (if fit)

   struct Scan_t {
      Int_t   i_par; // Index of scanned parameter
      Int_t   n_pts; // Number of points for scan
      TGraph *gr;    // Graph for this contour
   }; // Structure to store contour properties (if fit)


private:
   Double_t                  fChi2Min;             // Minimum of chi2 stored after minimisation
   vector<Contour_t>         fContours;            // List of selected pairs of parameters and output contour
   TUDataSet                *fDisplayedData;       // CR TOA data used for display
   gENUM_ERRTYPE             fDisplayedErrType;    // Error type in display (kERRSTAT, kERRSYST, kERRTOT)
   Double_t                  fDiplayedFluxEPower;  // Flux multiplied by E-type^fDiplayedFluxEPower
   Double_t                 *fFitCovMatrix;        // [NPars*NPars] Covariance matrix of fFitPars parameters (all types)
   TUDataSet                *fFitData;             // CR TOA data used for fit
   gENUM_ERRTYPE             fFitDataErrType;      // Error type used for fit (kERRSTAT, kERRSYST, kERRTOT, kERRCOV)
   string                    fFitERange;           // E-range for fit (for print/display)
   TUFreeParList            *fFitPars;             // Free parameters of the analysis
   Double_t                 *fHessianMatrix;       // [NPars*NPars] Hessian matrix of fFitPars parameters (all types)
   Int_t                     fIndexSample;         // If run in CL mode, index of sample ran for save (-1 = no save)
   string                    fInitFileName;        // Name of the USINE initialisation file
   Bool_t                    fIsCLMode;            // True if usine run with -u option
   Bool_t                    fIsMINOS;             // Whether MINOS is on (1) or off (0) to get proper fit uncertainties
   Bool_t                    fIsModelOrDataForRelCov; // USINE uses data cov.mat. of rel.errors: model or data value to get cov.?
   Bool_t                    fIsPrintHessianMatrix;// Whether to print the Hessian matrix (CovMatrix)^{-1}
   Bool_t                    fIsPrintCovMatrix;    // Whether to print the covariance matrix elements (Hessian)^{-1}
   Bool_t                    fIsPrintBestFit;      // Whether to print or not best-fit values
   Bool_t                    fIsSaveInFiles;       // Whether to save results in files (ASCII, ROOT, etc.) or not
   Bool_t                    fIsShowPlots;         // Whether to show plots or not (batch mode)
   Bool_t                    fIsUseNormList;       // Whether in run we normalise source abundances to data or not
   Bool_t                    fIsUseBinRange;       // Use Emean or integrated value in bin (assuming power-law) for fit
   ROOT::Math::Minimizer    *fMinimizer;           // Root minimiser based on Chi2_TOAFluxes() function
   string                    fMinimizerAlgo;       // Algorithm (numerical-minimization in ROOT)
   string                    fMinimizerName;       // Minimiser (Minuit2, Fumili, GSLMultiMin, ...)
   Int_t                     fMinimizerNMaxCall;   // Max calls ~ 100000 (minuit) or iteration (GSL)
   Int_t                     fMinimizerStrategy;   // Strategy level of minimizer (1=standard, 2 to improve minimum)
   Double_t                  fMinuitPrecision;     // Precision ~ 1e-6 (see Minuit manual)
   Int_t                     fMinuitPrintLevel;    // Print level (e.g. 1, see Minuit manual)
   Double_t                  fMinuitTolerance;     // Tolerance ~ 1e-3 (see Minuit manual)
   Int_t                     fNExtraInBinRange;    // If fIsUseBinRange=1, additional power-law in subranges (for accuracy)
   Int_t                     fNPropagModels;       // Numbers of models in fPropagModels
   Int_t                     fNSamples;            // Total number of samples (if CL mode)
   Int_t                     fNSolModModels;       // Numbers of models in fSolModModels
   string                    fOutputDir;           // Output directory (all files stored there)
   FILE                     *fOutputLog;           // Log name (stdout or file in fOutputDir/)
   vector<Scan_t>            fProfiles;            // List of selected parameters to profile (likelihood) and output graph
   TUModelBase             **fPropagModels;        // List of propagation models
   vector<Scan_t>            fScans;               // List of selected parameters to scan and output graph
   TUSolModVirtual         **fSolModModels;        // List of solar modulation models

   Double_t                  Chi2Cov(vector<Double_t> &model, vector<Double_t> &data, TMatrixTSym <Double_t> &cov) const;
   Double_t                  Chi2FromNuisance(Double_t const &xs_lc_constraint = 1.) const;
   inline void               ClearContours()                           {for (Int_t i=0; i<GetNContours(); ++i) {fContours[i].cls.clear(); if (fContours[i].mg) delete fContours[i].mg; fContours[i].mg=NULL; delete fContours[i].leg; fContours[i].leg=NULL; } fContours.clear();}
   void                      ClearScansOrProfiles(Bool_t is_scan_or_prof);
   void                      ExtractFromFitData(Int_t i_solmod, TURunOutputs &outputs, Bool_t is_verbose, FILE *f_out) const;
   void                      ExtractIndicesContours(TUInitParList* init_pars, Int_t i_initpar);
   void                      ExtractIndicesScansOrProfiles(TUInitParList* init_pars, Int_t i_initpar, Bool_t is_scan_or_prof);
   Bool_t                    FillGraphsContours(Int_t i_contour);
   void                      FillGraphsScansOrProfiles(Int_t i_scan, Bool_t is_scan_or_prof);
   Int_t                     GetNdofForXS() const;
   inline Int_t              GetNScansOrProfiles(Bool_t is_scan_or_prof){return (is_scan_or_prof ? GetNScans() : GetNProfiles());}
   Bool_t                    ModelBiasAtFitData(Int_t i_qty, Int_t i_exp, gENUM_ETYPE etype, vector<Double_t> &x_bias, vector<Double_t> &y_bias);
   void                      PlotContoursScansOrProfiles(TCanvas **cvs, Int_t scan0_prof1_cont2, TText *usine_txt, Bool_t is_print, FILE * f_print, TFile *root_file);
   void                      PlotSpectraISFractions(TURunOutputs const &results, Bool_t is_prim_or_isot, TCanvas *c_frac, TMultiGraph *mg_frac, TLegend *leg_frac, TText *usine_txt, Bool_t is_print, FILE * f_print, TFile *root_file);
   inline void               PrintLog(string const &str, Bool_t is_logandstdout) const {fprintf(fOutputLog, "%s\n", str.c_str()); if (fOutputLog!=stdout && is_logandstdout) fprintf(stdout, "%s\n", str.c_str());}
   void                      SumCiAndSigmaForXSLC(vector<Double_t> sum_lc_pars[], vector<Double_t> sigma_lc_pars[], vector<string> reac_lc_pars[]) const;

public:
   TURunPropagation();
   virtual ~TURunPropagation();

   inline Double_t           Chi2_TOAFluxes_py(vector<Double_t> const pars) {return Chi2_TOAFluxes(&pars[0]);}
   Double_t                  Chi2_TOAFluxes(const Double_t *pars);
   void                      CloseOutputLog()                           {if (fOutputLog != stdout) fclose(fOutputLog);}
   void                      FillSpectra(TURunOutputs &results, string const &combos_etypes_phiff="", Double_t e_power=0., TUCoordTXYZ *coord_txyz = NULL, Bool_t is_print=false, FILE *f_print=stdout);
   inline TMultiGraph       *GetContours(Int_t i_cont) const            {return fContours[i_cont].mg;}
   inline Double_t           GetDiplayedFluxEPower() const              {return fDiplayedFluxEPower;}
   inline Double_t          *GetFitCovMatrix() const                    {return fFitCovMatrix;}
   inline TUDataSet         *GetFitData() const                         {return fFitData;}
   inline gENUM_ERRTYPE      GetFitDataErrType() const                  {return fFitDataErrType;}
   inline TUFreeParList     *GetFitPars() const                         {return fFitPars;}
   inline Double_t          *GetHessianMatrix() const                   {return fHessianMatrix;}
   inline string             GetInitFileName() const                    {return fInitFileName;}
   inline Int_t              GetIndexSample() const                     {return fIndexSample;}
   inline ROOT::Math::Minimizer *GetMinimizer() const                   {return fMinimizer;}
   inline Int_t              GetNContours() const                       {return fContours.size();}
   inline Int_t              GetNdof() const                            {return fFitData->GetNData() - fFitPars->GetNPars(kFIT) - GetNdofForXS();}
   inline Int_t              GetNProfiles() const                       {return fProfiles.size();}
   inline Int_t              GetNPropagModels() const                   {return fNPropagModels;}
   inline Int_t              GetNSamples() const                        {return fNSamples;}
   inline Int_t              GetNScans() const                          {return fScans.size();}
   inline Int_t              GetNSolModModels() const                   {return fNSolModModels;}
   inline string             GetOutputDir() const                       {return fOutputDir;}
   inline FILE              *GetOutputLog() const                       {return fOutputLog;}
   inline TGraph            *GetProfile(Int_t i_prof) const             {return fProfiles[i_prof].gr;}
   inline TUModelBase       *GetPropagModel(Int_t i_model)              {return fPropagModels[i_model];}
   inline TGraph            *GetScan(Int_t i_scan) const                {return fScans[i_scan].gr;}
   inline TUSolModVirtual   *GetSolModModel(Int_t i_model)              {return fSolModModels[i_model];}
   void                      Initialise(Bool_t is_delete);
   inline Bool_t             IsModelOrDataForRelCov() const             {return fIsModelOrDataForRelCov;}
   inline Bool_t             IsSaveInFiles() const                      {return fIsSaveInFiles;}
   inline Bool_t             IsUseNormList() const                      {return fIsUseNormList;}
   void                      LoopDisplaySpectra(TApplication *app, Bool_t is_sep_isot, TUCoordTXYZ *coord_txyz = NULL, Bool_t is_print=false, FILE *f_print=stdout);
   void                      MinimiseTOAFluxes(TUInitParList* init_pars, Bool_t is_verbose, Bool_t is_logandstdout);
   TGraphAsymmErrors        *OrphanResiduals(vector<Double_t> const &e_model, vector<Double_t> &y_model, TGraphAsymmErrors *gr_data, Int_t switch_res);
   void                      PlotSpectra(TApplication *app, TURunOutputs const &results, Bool_t is_sep_isot, Bool_t is_print = false, FILE *f_print = stdout);
   void                      Plots_AntinucExtra(TApplication *app, Int_t switch_plot, Bool_t is_noina, string const &option, gENUM_ETYPE e_type = kEKN, Bool_t is_test=false, FILE *f_test=stdout);
   void                      Plots_DecayOnOff(TApplication *app, Bool_t is_beta_or_ec, Bool_t is_test_exactmatch, string const &option, gENUM_ETYPE e_type = kEKN, Bool_t is_test=false, FILE *f_test=stdout);
   void                      Plots_GCRvsSSAbund(TApplication *app, Double_t ekn, Bool_t is_print=false, FILE *f_print=stdout);
   void                      Plots_ImpactBoundaryConditions(TApplication *app, Bool_t is_normdata4allconfigs, string const &option, Bool_t is_test=false, FILE *f_test=stdout);
   void                      Plots_ImpactPropagSwitches(TApplication *app, Bool_t is_normdata4allconfigs, string const &option, Bool_t is_test=false, FILE *f_test=stdout);
   void                      Plots_ImpactXSOnCombos(TApplication *app, Int_t switch_xs, Bool_t is_normdata4allconfigs, string const &option, Bool_t is_test=false, FILE *f_test=stdout);
   void                      Plots_XSProdRanking(TApplication *app, string option, gENUM_ETYPE e_type = kEKN, Bool_t is_test=false, FILE *f_test=stdout);
   void                      PrintFitCovMatrix(FILE *f, vector<Int_t> const &indices_infitpars, Double_t *fitcovmatrix, Bool_t is_allfit) const;
   void                      PrintFitResult(FILE *f=stdout, Bool_t is_date = true) const;
   void                      PrintListOfModels(FILE *f=stdout) const;
   inline void               PrintPropagationMode(FILE *f=stdout) const {if (fNPropagModels>0) fPropagModels[0]->PrintPropagSwitches(f);}
   void                      PrintListOfModelParameters(FILE *f=stdout) const;
   void                      Propagate(Bool_t is_norm_to_data, Bool_t is_verbose = true, Int_t jcr_start=0, Int_t jcr_stop=-1/*to have NCRs-1*/);
   void                      SetClass(string const &usine_initfile, Bool_t is_verbose, string const &output_dir, FILE *f_log);
   void                      SetClass(TUInitParList* init_pars, Bool_t is_verbose, string const &output_dir, FILE *f_log);
   inline void               SetIndexSample(Int_t i_sample)             {fIndexSample = i_sample;}
   inline void               SetIsPrintBestFit(Bool_t is_print)         {fIsPrintBestFit = is_print;}
   inline void               SetIsPrintCovMatrix(Bool_t is_print)       {fIsPrintCovMatrix = is_print;}
   inline void               SetIsPrintHessianMatrix(Bool_t is_print)   {fIsPrintHessianMatrix = is_print;}
   inline void               SetIsSaveInFiles(Bool_t is_save)           {fIsSaveInFiles = is_save;}
   inline void               SetIsShowPlots(Bool_t is_show)             {fIsSaveInFiles = is_show;}
   inline void               SetIsNormlList(Bool_t is_normlist)         {fIsUseNormList = is_normlist;}
   inline void               SetIsCLMode(Bool_t is_clmode)              {fIsCLMode = is_clmode;}
   inline void               SetMinuitPrintLevel(Int_t level)           {fMinuitPrintLevel = level;}
   inline void               SetNSamples(Int_t n_samples)               {fNSamples = n_samples;}
   inline void               SetOutputDir(string const &dir)            {fOutputDir = dir;}
   void                      TUI_ModifyPropagSwitches() const;
   static void               TUI_SelectListOfPhi(vector<Double_t> &list_phi);
   string                    UID(string const &combo, gENUM_ETYPE e_type, Double_t const &e_power, string const& model_propag="", string const& uid_solmod="");
   void                      UpdateFitParsAndFitData(string const &usine_initfile, Bool_t is_verbose, Bool_t is_logandstdout);
   void                      UpdateFitParsAndFitData(TUInitParList* init_pars, Bool_t is_verbose, Bool_t is_logandstdout);

   ClassDef(TURunPropagation, 1)  // Run propagation (output plots and files), comparisons, and checks
};

#endif
