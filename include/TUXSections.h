// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUXSections
#define USINE_TUXSections

// C/C++ include
// ROOT include
#include <TH2D.h>
// USINE include
#include "TUAxesCrE.h"
#include "TUMediumTXYZ.h"
#include "TUMessages.h"


class TUXSections {

private:

   vector<string> parsxs_keys =
      {"Norm", "EaxisScale", "EnhancePowHE", "EThresh", "SlopeLE",
       "LCInelBar94", "LCInelTri99", "LCInelWeb03", "LCInelWel97",
       "LCProdGal17", "LCProdSou01", "LCProdWeb03", "LCProdWKS98"
      };

   struct XSReacPars_t {
      Int_t  i_frag;            // Index of CR frag in fCrE  (-1 if inelastic XS)
      Int_t  i_proj;            // Index of CR proj in fCrE (list of CRs)
      Int_t  i_targ;            // Index of target in fTargets
      Int_t  i_tert;            // Index of tertiary in fCrE (for non-annihilating rescattering)
      Int_t  ipar_xsnorm;       // Index in fFreePars (-1 if no norm par for reaction)
      Int_t  ipar_eaxisscale;   // Index in fFreePars (-1 if no rescaling of E axis for reaction)
      Int_t  ipar_enhancepowhe; // Index in fFreePars (-1 if no [XS_EnhancementHE()]^(enhancepowhe))
      Int_t  ipar_ethresh;      // Index in fFreePars (-1 if no energy threshold for reaction)
      Int_t  ipar_slopele;      // Index in fFreePars (-1 if no low-E factor (Ekn/Ekn_thresh)^slope)
      vector<Int_t> ipar_lcinel;// Indices in fFreePars (-1 if no LC) for linear combin. for inelastic XS
      vector<Int_t> ipar_lcprod;// Indices in fFreePars (-1 if no LC) for linear combin. for production XS
      Bool_t is_all;            // If true, applies to all reactions (except if linear combination used)
      Bool_t is_all_lcinel;     // If true, applies to all inelastic reactions (for linear combination)
      Bool_t is_all_lcprod;     // If true, applies to all production reactions (for linear combination)
      Bool_t is_all_frag;       // If true, applies to all fragments of reaction
      Bool_t is_all_proj;       // If true, applies to all projectiles of reaction
      Bool_t is_all_targ;       // If true, applies to all targets of reaction
      Bool_t is_xs_ia;          // True if reaction is inelastic (or if is_all is true)
      Bool_t is_xs_ina;         // True if reaction is rescattering (or if is_all is true)
      Bool_t is_xs_prod;        // True if reaction is production (or if is_all is true)
      string name;              // Name of the reaction
   }; // Structure to list XS reactions (e.g. 10BE+H or 12C+H->10BE) used as fitting parameters

   TUAxesCrE            *fCrE;                  // 'CR' and 'E' axes (see description in TUAxesCrE.h)
   Double_t             *fDSigDEkINA;           //![NTarg*NTert*NE(out)*NE(in)] Diff. INA X-sec [mb/GeV]
   vector<string>        fFilesXDiffINA;        // Files used to fill fDSigDEkINA (differential INA X-sec)
   vector<string>        fFilesXProd;           // Files used to fill fProdSig and/or fProdDSigDEk
   vector<string>        fFilesXTotIA;          // Files used to fill fTotIA (total IA X-Sec)
   vector<string>        fFilesXTotINA;         // Files used to fill fTotINA (total INA X-Sec)
   TUFreeParList        *fFreePars;             // Free parameters for XS
   Double_t           ***fGhostsProdDSigDEk;    //![NTarg*NCRs][NParents(cr)*NGhosts(cr)][NE*NE] Diff.prod. for ghosts [mb/GeV]
   Double_t           ***fGhostsProdSig;        //![NTarg*NCRs][NParents(cr)*NGhosts(cr)][NE] SAA for ghost [mb]
   Int_t               **fGhostsProdStatus;     //![NCRs][NParents(cr)*NGhosts(cr)] unfilled=-1, SAA=0(10 if sig=0), diff=1(11 if sig=0)
   Bool_t                fIsAtLeast1DiffProd;   // Whether in loaded prod. Xsec, at least one is differential
   Bool_t                fIsDeleteCrE;          // Whether to delete or not fCrE in the destructor
   Bool_t                fIsDeleteFreePars;     // Delete fFreePars or not (in destructor)
   Bool_t               *fIsDSigDEkINAFilled;   //![NTert] Whether fDSigDEkINA is filled [true] or not
   Bool_t               *fIsTotIAFilled;        //![NCRs] Whether fTotIA is filled [true] or not
   Bool_t               *fIsTotINAFilled;       //![NTert] Whether fTotINA is filled [true] or not
   Int_t                *fMapCRIndex2INAIndex;  //![NCRs] CR index to NAR index (-1 if CR has no NAR X-sec)
   Int_t                *fMapINAIndex2CRIndex;  //![NTert] NAR index to CR index
   TUMediumEntry        *fMedium_ISM0D;         // Storage for ISM0D (optimisation for constant density models)
   Double_t             *fnvDSigDEkINA_ISM0D;   //![NTert*NE(out)*NE(in)] ISM weighted n.v.dsig [/(GeV Myr)]
   Double_t           ***fnvDSigDEkProd_ISM0D;  //![NCRs][NParents(cr)][NE*NE] n.v.dsig [/(GeV Myr)]
   Double_t           ***fnvSigProd_ISM0D;      //![NCRs][NParents(cr)][NE] Idem n.v.sig [/Myr]
   Double_t             *fnvSigTotIA_ISM0D;     //![NCRs*NE] ISM weighted n.v.sig [/Myr]
   Double_t             *fnvSigTotINA_ISM0D;    //![NTert*NE] ISM weighted n.v.sig [/Myr]
   vector<XSReacPars_t>  fParsXS_ReacIndices;   //![NReac] Indices of CRs (in fCrE and fFreePars) for all relevant XS
   TUXSections          *fParsXS_RefXS;         // Copy of initial XS (used as reference)
   Double_t           ***fProdDSigDEk;          //![NTarg*NCRs][NParents(cr)][NE*NE] Diff.prod.[mb/GeV]
   Double_t           ***fProdSig;              //![NTarg*NCRs][NParents(cr)][NE] SAA X-sec prod. [mb]
   Int_t               **fProdStatus;           //![NCRs][NParents(cr)] unfilled=-1, SAA=0(10 if sig=0), diff=1(11 if sig=0)
   vector<string>        fTargets;              //![NTarg] Case-insensitive ISM target elements (e.g., H, HE...)
   vector<string>        fTertiaries;           //![NTert] Case-insensitive CRs with INA (1H-BAR, 2H-BAR)
   Double_t             *fTotIA;                //![NTarg*NCRs*NE] Tot.Inel.Ann. X-sec [mb]
   Double_t             *fTotINA;               //![NTarg*NTert*NE] Tot.Inel.Non-Ann. X-sec [mb]
   vector<TUXSections>   fXSLinComb;            // Vector of TUXSections to form linear combinations of XS
   vector<string>        fXSLinComb_InelReacs;  // Vector of names of inelastic XS used as linear combination
   vector<string>        fXSLinComb_ProdReacs;  // Vector of names of production XS used as linear combination
   Int_t                 fXSLinComb_NInelXS;    // Number of inelastic XS in linear combination
   Int_t                 fXSLinComb_NProdXS;    // Number of production XS in linear combination

   void                  AllocateProdSigAllTargets(Int_t j_parent, Int_t j_cr, Int_t g_ghost);
   void                  AllocateProdDSigAllTargets(Int_t j_parent, Int_t j_cr, Int_t g_ghost);
   void                  AllocateAndInitialiseXSec();
   void                  DeleteProdRate_ISM0D();
   void                  DeleteProdSigAllTargets(Int_t j_parent, Int_t j_cr, Int_t g_ghost);
   void                  DeleteProdDSigAllTargets(Int_t j_parent, Int_t j_cr, Int_t g_ghost);
   void                  DeleteXSec();
   void                  ExtractIndicesForXSToRead(string const &parent, string const &frag, Bool_t &is_skip_xs, Bool_t &is_ghost_xs, vector<Int_t> &cr_indices, vector<Int_t> &parent_indices, vector<Int_t> &ghost_indices, vector<string> &reactions) const;
   inline string         ExtractTargets() const                                                                   {return TUMisc::List2String(fTargets, ',');}
   inline string         ExtractTertiaries() const                                                                {return TUMisc::List2String(fTertiaries, ',');}
   void                  ExtractXSGraphsList(string list_proj, string list_targ, string list_frag, TMultiGraph *mg, TLegend *leg_mg, vector<TH2D*> &v_hist, FILE *f_out=stdout) const;
   inline void           Set_nvDSigDEkINA_ISM0D(Int_t j_tertiary, Int_t k_in, Int_t k_out, Double_t const &val)   {fnvDSigDEkINA_ISM0D[j_tertiary*GetNE()*GetNE()+k_out*GetNE()+k_in] = val;}
   void                  FillInterRateDSigINA_ISM0D(Int_t j_tert, TUMediumEntry *medium);
   void                  FillInterRateProd_ISM0D(Int_t j_parent, Int_t j_cr, TUMediumEntry *medium);
   void                  FillInterRateTotIA_ISM0D(Int_t j_cr, TUMediumEntry *medium);
   void                  FillInterRateTotINA_ISM0D(Int_t j_tert, TUMediumEntry *medium);
   string                FormReactionNameProd(Int_t j_parent, Int_t j_cr, Int_t g_ghost, Int_t t_targ=-1) const;
   inline Double_t       GetDSigDEkINA(Int_t j_tertiary, Int_t t_targ, Int_t k_in, Int_t k_out) const             {return fDSigDEkINA[t_targ*GetNTertiaries()*GetNE()*GetNE()+j_tertiary*GetNE()*GetNE()+k_out*GetNE()+k_in];}
   inline TUMediumEntry *GetMedium_ISM0D() const                                                                  {return fMedium_ISM0D;}
   inline Double_t       GetProdDSigDEk(Int_t j_parent, Int_t t_targ, Int_t j_cr, Int_t k_in, Int_t k_out, Int_t g_ghost = -1) const  {if (g_ghost==-1) {return fProdDSigDEk[t_targ*GetNCRs()+j_cr][j_parent][k_out*GetNE()+k_in];} else {return fGhostsProdDSigDEk[t_targ*GetNCRs()+j_cr][j_parent*fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost][k_out*GetNE()+k_in];}}
   inline Int_t          GetProdStatus(Int_t j_parent, Int_t j_cr, Int_t g_ghost) const                           {if (g_ghost==-1) return fProdStatus[j_cr][j_parent]; else return fGhostsProdStatus[j_cr][j_parent*fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost];}
   inline Double_t       Get_nvDSigDEkINA_ISM0D(Int_t j_tertiary, Int_t k_in, Int_t k_out) const                  {return fnvDSigDEkINA_ISM0D[j_tertiary*GetNE()*GetNE()+k_out*GetNE()+k_in];}
   inline Double_t       Get_nvSigTotINA_ISM0D(Int_t j_tertiary, Int_t k_ekn) const                               {return fnvSigTotINA_ISM0D[j_tertiary*GetNE()+k_ekn];}
   inline Double_t      *Get_nvDSigDEkINA_ISM0D(Int_t j_tertiary, Int_t k_out) const                              {return &fnvDSigDEkINA_ISM0D[j_tertiary*GetNE()*GetNE()+k_out*GetNE()+ 0];}
   inline Double_t       Get_nvDSigDEkProd_ISM0D(Int_t j_parent, Int_t j_cr, Int_t k_in, Int_t k_out) const       {return fnvDSigDEkProd_ISM0D[j_cr][j_parent][k_out*GetNE()+k_in];}
   inline Double_t      *Get_nvDSigDEkProd_ISM0D(Int_t j_parent, Int_t j_cr, Int_t k_out) const                   {return &fnvDSigDEkProd_ISM0D[j_cr][j_parent][k_out*GetNE()+ 0];}
   inline Double_t       Get_nvSigProd_ISM0D(Int_t j_parent, Int_t j_cr, Int_t k_ekn) const                       {return fnvSigProd_ISM0D[j_cr][j_parent][k_ekn];}
   vector<Int_t>         IndicesTargetsInFile(string const &commasep_targets) const;
   void                  Initialise(Bool_t is_delete=false);
   Bool_t                IsAllDSigDEkINAFilled() const;
   Bool_t                IsAllProdFilled();
   Bool_t                IsAllTotIAFilled() const;
   Bool_t                IsAllTotINAFilled() const;
   inline Bool_t         IsDeleteCrE() const                                                                      {return fIsDeleteCrE;}
   inline Bool_t         IsDeleteFreePars() const                                                                 {return fIsDeleteFreePars;}
   inline Bool_t         IsDSigDEkINAFilled(Int_t j_tertiary) const                                               {return fIsDSigDEkINAFilled[j_tertiary];}
   Bool_t                IsFileAlreadyLoaded(string const &f_xsec, Bool_t is_verbose, FILE *f_log) const;
   inline Bool_t         IsProdSigmaOrdSigma(Int_t j_parent, Int_t j_cr, Int_t g_ghost = -1) const                {return !(Bool_t)(GetProdStatus(j_parent, j_cr, g_ghost)%10);}
   Bool_t                IsTargetOrCROrEknEmpty(FILE *f_log) const;
   inline Bool_t         IsTotIAFilled(Int_t j_cr) const                                                          {return fIsTotIAFilled[j_cr];}
   inline Bool_t         IsTotINAFilled(Int_t j_tertiary) const                                                   {return fIsTotINAFilled[j_tertiary];}
   Double_t              ISMWeightedProd_nvDSigDEk(Int_t j_parent, Int_t j_cr, Int_t k_in, Int_t k_out, TUMediumEntry *medium) const;
   Double_t              ISMWeightedProd_nvSig(Int_t j_parent, Int_t j_cr, Int_t k_out, TUMediumEntry *medium) const;
   Double_t              ISMWeightedProdDSig(Int_t j_parent, Int_t j_cr, Int_t k_in, Int_t k_out, TUMediumEntry *medium) const;
   Double_t              ISMWeightedProdSig(Int_t j_parent, Int_t j_cr, Int_t k_out, TUMediumEntry *medium) const;
   Double_t              ISMWeightedTertINA_nvDSigDEk(Int_t j_tertiary, Int_t k_in, Int_t k_out, TUMediumEntry *medium) const;
   Double_t              ISMWeightedTertINADSig(Int_t j_tertiary, Int_t k_in, Int_t k_out, TUMediumEntry *medium) const;
   Double_t              ISMWeightedTotIA_nvSig(Int_t j_cr_or_tertiary, Int_t k_ekn, TUMediumEntry *medium) const;
   Double_t              ISMWeightedTotIASig(Int_t j_cr_or_tertiary, Int_t k_ekn, TUMediumEntry *medium) const;
   Double_t              ISMWeightedTotINA_nvSig(Int_t j_cr_or_tertiary, Int_t k_ekn, TUMediumEntry *medium) const;
   Double_t              ISMWeightedTotINASig(Int_t j_cr_or_tertiary, Int_t k_ekn, TUMediumEntry *medium) const;
   inline Bool_t         IsProdFilled(Int_t j_parent, Int_t j_cr, Int_t g_ghost = -1) const                       {return (GetProdStatus(j_parent, j_cr, g_ghost)>=0) ? true : false;}
   void                  LoadOrUpdateProdDSig(ifstream &f_read, string const &f_name, string const &f_unit, string const &f_targets, TUAxis &axisekn_proj, TUAxis &axisekn_frag, Int_t f_repeatedloop, Bool_t is_verbose, FILE *f_log);
   void                  LoadOrUpdateProdSig(ifstream &f_read, string const &f_name, string const &f_unit, string const &f_targets, TUAxis &axisekn, Bool_t is_verbose, FILE *f_log);
   void                  ParsXS_AddPar(TUFreeParList *pars, Int_t i_par, Bool_t is_use_or_copy, FILE *f_log);
   void                  ParsXS_ExtractExample(XSReacPars_t &reac) const;
   string                ParsXS_ExtractKeywords(XSReacPars_t const &reac) const;
   void                  ParsXS_Initialise(Bool_t is_delete=false);
   void                  ParsXS_InitReac(XSReacPars_t &reac) const;
   Bool_t                ParsXS_IsMatchingKey(XSReacPars_t const &reac1, XSReacPars_t const &reac2) const;
   Bool_t                ParsXS_IsOverlappingReacs(string const& key, XSReacPars_t const &reac1, XSReacPars_t const &reac2, FILE *f_log, Bool_t is_abort=true) const;
   Bool_t                ParsXS_IsSame(XSReacPars_t const &reac1, XSReacPars_t const &reac2) const;
   TGraph               *OrphanGetTGraphSigInel(Int_t j_cr_or_tertiary, Int_t t_targ, Bool_t is_ia_or_ina) const;
   TGraph               *OrphanGetTGraphSigProd(Int_t j_parent, Int_t t_targ, Int_t j_frag, Int_t g_ghost=-1) const;
   TH2D                 *OrphanGetTH2DSigINA(Int_t j_tertiary, Int_t t_targ) const;
   TH2D                 *OrphanGetTH2DSigProd(Int_t j_parent, Int_t t_targ, Int_t j_frag, Int_t g_ghost=-1) const;
   static void           ReadFileHeader(ifstream &f_read, string const &f_name, string& f_type, string& f_unit, string& f_targets, TUAxis &axisekn_in, TUAxis &axisekn_out, Int_t &f_repeatedloop, Bool_t is_verbose, FILE *f_log);
   inline void           Set_nvDSigDEkINA_ISM0D(Int_t j_tertiary, Int_t k_in, Int_t k_out, Double_t const &val) const  {fnvDSigDEkINA_ISM0D[j_tertiary*GetNE()*GetNE()+k_out*GetNE()+k_in] = val;}
   inline void           Set_nvDSigDEkProd_ISM0D(Int_t j_parent, Int_t j_cr, Int_t k_in, Int_t k_out, Double_t const &val) {fnvDSigDEkProd_ISM0D[j_cr][j_parent][k_out*GetNE()+k_in] = val;}
   inline void           Set_nvSigProd_ISM0D(Int_t j_parent, Int_t j_cr, Int_t k_ekn, Double_t const &val)        {fnvSigProd_ISM0D[j_cr][j_parent][k_ekn] = val;}
   inline void           Set_nvSigTotIA_ISM0D(Int_t j_cr, Int_t k_ekn, Double_t const &val)                       {fnvSigTotIA_ISM0D[j_cr*GetNE()+k_ekn] = val;}
   inline void           Set_nvSigTotINA_ISM0D(Int_t j_tertiary, Int_t k_ekn, Double_t const &val)                {fnvSigTotINA_ISM0D[j_tertiary*GetNE()+k_ekn] = val;}
   inline void           SetProdStatus(Int_t j_parent, Int_t j_cr, Int_t status, Int_t g_ghost)                   {if (g_ghost==-1) fProdStatus[j_cr][j_parent] = status; else fGhostsProdStatus[j_cr][j_parent*fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost] = status;}
   void                  UpdateTertiaryList(string commasep_tertiaries, FILE *f_log);
   void                  XSLinComb_Load(vector<string> files, Bool_t is_inel_or_prod, Bool_t is_verbose, FILE *f_log);
   void                  XSLinComb_UpdateXSInel(vector <Double_t> &coeffs, Int_t j_cr, Int_t t_targ);
   void                  XSLinComb_UpdateXSProd(vector <Double_t> &coeffs, Int_t j_parent, Int_t t_targ, Int_t j_cr);



public:
   TUXSections();
   TUXSections(TUXSections const &xsec);
   TUXSections(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log, TUAxesCrE *use_axescre=NULL);
   virtual ~TUXSections();

   inline void           ClearFilesXDiffINA()                                                                     {return fFilesXDiffINA.clear();}
   inline void           ClearFilesXProd()                                                                        {return fFilesXProd.clear();}
   inline void           ClearFilesXTotIA()                                                                       {return fFilesXTotIA.clear();}
   inline void           ClearFilesXTotINA()                                                                      {return fFilesXTotINA.clear();}
   inline TUXSections   *Clone()                                                                                  {return new TUXSections(*this);}
   void                  Copy(TUXSections const &xsec);
   inline TUXSections   *Create()                                                                                 {return new TUXSections;}
   void                  ExtractDiffProdSpecies(vector<Int_t> &indices_crs, vector<vector<Int_t> > &indices_parents, gENUM_CRFAMILY switch_family) const;
   void                  ExtractXSGraphs(Int_t j_parent, Int_t t_targ, Int_t j_frag, TMultiGraph *mg, TLegend *leg_mg, vector<TH2D*> &v_hist, Int_t calledfrom = 0, Int_t g_ghost=-1, FILE *f_out=stdout) const;
   void                  FillInterRate_ISM0D(TUMediumEntry *medium, Bool_t is_force_refill);
   inline TUAxesCrE     *GetAxesCrE() const                                                                       {return fCrE;}
   inline TUAxis        *GetEkn(Int_t j_cr) const                                                                 {return fCrE->GetEFamily(fCrE->GetCREntry(j_cr).GetFamily());}
   inline string         GetENativeNameUnit(Int_t j_cr) const                                                     {return TUEnum::Enum2NameUnit(fCrE->GetCREntry(j_cr).GetFamily());}
   inline string         GetFileXDiffINA(Int_t i_file) const                                                      {return fFilesXDiffINA[i_file];}
   inline string         GetFileXProd(Int_t i_file) const                                                         {return fFilesXProd[i_file];}
   inline string         GetFileXTotIA(Int_t i_file) const                                                        {return fFilesXTotIA[i_file];}
   inline string         GetFileXTotINA(Int_t i_file) const                                                       {return fFilesXTotINA[i_file];}
   inline TUFreeParList *GetFreePars() const                                                                      {return fFreePars;}
   inline string         GetNameTarget(Int_t t_targ) const                                                        {return fTargets[t_targ];}
   inline string         GetNameTertiary(Int_t j_ter) const                                                       {return fTertiaries[j_ter];}
   inline Int_t          GetNCRs() const                                                                          {return ((fCrE) ? fCrE->GetNCRs() : 0);}
   inline Int_t          GetNE() const                                                                            {return ((fCrE) ? fCrE->GetNE() : 0);}
   inline Int_t          GetNFreePars() const                                                                     {return ((fFreePars) ? fFreePars->GetNPars() : 0);}
   inline Int_t          GetNTargets() const                                                                      {return (Int_t)fTargets.size();}
   inline Int_t          GetNTertiaries() const                                                                   {return (Int_t)fTertiaries.size();}
   inline Int_t          GetNFilesXDiffINA() const                                                                {return fFilesXDiffINA.size();}
   inline Int_t          GetNFilesXProd() const                                                                   {return fFilesXProd.size();}
   inline Int_t          GetNFilesXTotIA() const                                                                  {return fFilesXTotIA.size();}
   inline Int_t          GetNFilesXTotINA() const                                                                 {return fFilesXTotINA.size();}
   inline Double_t       Get_nvSigTotIA_ISM0D(Int_t j_cr, Int_t k_ekn) const                                      {return fnvSigTotIA_ISM0D[j_cr*GetNE()+k_ekn];}
   inline Double_t       GetProdSig(Int_t j_parent, Int_t t_targ, Int_t j_cr, Int_t k_ekn, Int_t g_ghost = -1) const {if (g_ghost==-1) {return fProdSig[t_targ*GetNCRs()+j_cr][j_parent][k_ekn];} else {return fGhostsProdSig[t_targ*GetNCRs() + j_cr][j_parent*fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost][k_ekn];}}
   inline Double_t      *GetProdSig(Int_t j_parent, Int_t t_targ, Int_t j_cr) const                               {return &fProdSig[t_targ*GetNCRs()+j_cr][j_parent][0];}
   inline Double_t       GetTotIA(Int_t j_cr, Int_t t_targ, Int_t k_ekn) const                                    {return fTotIA[t_targ*GetNCRs()*GetNE()+j_cr*GetNE()+k_ekn];}
   inline Double_t      *GetTotIA(Int_t j_cr, Int_t t_targ) const                                                 {return &fTotIA[t_targ*GetNCRs()*GetNE()+j_cr*GetNE()+0];}
   inline Double_t       GetTotINA(Int_t j_tertiary, Int_t t_targ, Int_t k_ekn) const                             {return fTotINA[t_targ*GetNTertiaries()*GetNE()+j_tertiary*GetNE()+k_ekn];}
   inline Double_t      *GetTotINA(Int_t j_tertiary, Int_t t_targ) const                                          {return &fTotINA[t_targ*GetNTertiaries()*GetNE()+j_tertiary*GetNE()+0];}
   inline Bool_t         IsAtLeast1DiffProd() const                                                               {return fIsAtLeast1DiffProd;}
   inline Bool_t         IsProdNull(Int_t j_parent, Int_t j_cr, Int_t g_ghost = -1) const                         {return (GetProdStatus(j_parent, j_cr, g_ghost)>=10) ? true : false;}
   inline Bool_t         IsTertiary(Int_t j_cr) const                                                             {return (fMapCRIndex2INAIndex[j_cr]>=0) ? true : false;}
   void                  LoadOrUpdateDSigDEkINA(string const &f_xsec, Bool_t is_verbose, FILE *f_log);
   void                  LoadOrUpdateProd(string const &f_xsec, Bool_t is_verbose, FILE *f_log);
   void                  LoadOrUpdateTotInel(string const &f_xsec, Bool_t is_ia_or_ina, Bool_t is_verbose, FILE *f_log);
   inline Int_t          MapCRIndex2INAIndex(Int_t j_cr) const                                                    {return fMapCRIndex2INAIndex[j_cr];}
   inline Int_t          MapINAIndex2CRIndex(Int_t j_tertiary) const                                              {return fMapINAIndex2CRIndex[j_tertiary];}
   inline TUXSections   *ParsXS_GetRefXS() const                                                                  {return fParsXS_RefXS;}
   inline Int_t          ParsXS_GetNReac() const                                                                  {return (Int_t)fParsXS_ReacIndices.size();}
   inline XSReacPars_t   ParsXS_GetReac(Int_t r_reac) const                                                       {return fParsXS_ReacIndices[r_reac];}
   inline Int_t          ParsXS_GetReacIndexProjLop(Int_t r_reac) const                                           {return ((fParsXS_ReacIndices[r_reac].i_frag>=0 && fParsXS_ReacIndices[r_reac].i_proj>=0) ? fCrE->IndexInParentList(fParsXS_ReacIndices[r_reac].i_frag, fCrE->GetCREntry(fParsXS_ReacIndices[r_reac].i_proj).GetName()) : -1);}
   inline string         ParsXS_LinComb_GetNameReac(Int_t i_r, Bool_t is_inel_or_prod) const                      {return ((is_inel_or_prod) ? fXSLinComb_InelReacs[i_r] : fXSLinComb_ProdReacs[i_r]);}
   inline Int_t          ParsXS_LinComb_GetNReac(Bool_t is_inel_or_prod)  const                                   {return ((is_inel_or_prod) ? (Int_t)fXSLinComb_InelReacs.size() : (Int_t)fXSLinComb_ProdReacs.size());}
   static void           ParsXS_Print(FILE *f, XSReacPars_t const &reac);
   void                  ParsXS_PrintXSRefvsCurrent(FILE *f, Int_t r_reac) const;
   void                  ParsXS_SetPars(TUFreeParList *pars, Bool_t is_use_or_copy, Bool_t is_verbose, FILE *f_log);
   void                  ParsXS_UpdateFromFreeParsAndResetStatus(TUMediumEntry *medium=NULL, Bool_t is_verbose = false, Bool_t is_update_xs = true, FILE *f_log = stdout);
   void                  Plots(string const &list_proj, string const &list_targ, string const &list_frag, TApplication *my_app, Bool_t is_test=false, FILE *f_test=stdout) const;
   inline void           PrintESummary(FILE *f=stdout) const                                                      {fCrE->PrintESummary(f);}
   void                  PrintListOfXSecProdEqualZero(FILE *f=stdout) const;
   void                  PrintTargetList(FILE *f=stdout) const;
   void                  PrintTertiaryList(FILE *f=stdout) const;
   void                  PrintXSecFiles(FILE *f=stdout) const;
   void                  ResetProdZeroFor1StepReactionNotCRToX(Int_t j_cr);
   void                  ResetProdZeroFor2StepReactionNotCRToInterToX(Int_t j_cr, Int_t j_inter);
   void                  SecondaryProduction(Double_t* dndekn_proj, Int_t l_proj, Int_t j_cr, TUMediumEntry *medium, Double_t *output, Double_t const &eps, Bool_t is_ism0D, FILE *f_log);
   void                  SecondaryProduction(Double_t* dndekn_proj, Int_t l_proj, Int_t j_cr, TUMediumTXYZ *medium_txyz, TUCoordTXYZ *coord_txyz, Double_t *output, Double_t const &eps, FILE *f_log);
   void                  SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log, TUAxesCrE *use_axescre=NULL);
   inline void           SetDSigDEkINA(Int_t j_tertiary, Int_t t_targ, Int_t k_in, Int_t k_out, Double_t const &val) {fDSigDEkINA[t_targ*GetNTertiaries()*GetNE()*GetNE()+j_tertiary*GetNE()*GetNE()+k_out*GetNE()+k_in] = val;}
   inline void           SetProdDSigDEk(Int_t j_parent, Int_t t_targ, Int_t j_cr, Int_t k_in, Int_t k_out, Double_t const &val, Int_t g_ghost=-1) {if (g_ghost==-1) fProdDSigDEk[t_targ*GetNCRs()+j_cr][j_parent][k_out*GetNE()+k_in] = val; else fGhostsProdDSigDEk[t_targ*GetNCRs()+j_cr][j_parent*fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost][k_out*GetNE()+k_in] = val;}
   inline void           SetProdSig(Int_t j_parent, Int_t t_targ, Int_t j_cr, Int_t k_ekn, Double_t const &val, Int_t g_ghost=-1) {if (g_ghost==-1) fProdSig[t_targ*GetNCRs()+j_cr][j_parent][k_ekn] = val; else fGhostsProdSig[t_targ*GetNCRs()+j_cr][j_parent*fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost][k_ekn] = val;}
   inline void           SetTotIA(Int_t j_cr, Int_t t_targ, Int_t k_ekn, Double_t const &val)                      {fTotIA[t_targ*GetNCRs()*GetNE()+j_cr*GetNE()+k_ekn] = val;}
   inline void           SetTotINA(Int_t j_tertiary, Int_t t_targ, Int_t k_ekn, Double_t const &val)               {fTotINA[t_targ*GetNTertiaries()*GetNE()+j_tertiary*GetNE()+k_ekn] = val;}
   Double_t              TertiaryProduction(Double_t* dndekn, Int_t j_cr, Int_t k_ekn, TUMediumEntry *medium, Double_t const &eps, Bool_t is_ism0D, Bool_t &is_converged);
   Double_t              TertiaryProduction(Double_t* dndekn, Int_t j_cr, Int_t k_ekn, TUMediumTXYZ *medium_txyz, TUCoordTXYZ *coord_txyz, Double_t const &eps, Bool_t &is_converged);
   void                  TEST(TUInitParList *init_pars, FILE *f=stdout);
   Double_t              XS_EnhancementHE(Int_t j_cr, Int_t k_ekn) const;


   ClassDef(TUXSections, 1)  // Nuclear X-sections (total, diff., inel.) for CRs on E grids [loaded from file]
};

#endif
