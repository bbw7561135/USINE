// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUSrcSteadyState
#define USINE_TUSrcSteadyState

// C/C++ include
// ROOT include
// USINE include
#include "TUSrcVirtual.h"

class TUSrcSteadyState : public TUSrcVirtual {

private:
   Bool_t                             fIsDeleteSrcSpectrum;    // Whether to delete or not fSrcSpectrum in destructor
   Bool_t                             fIsDeleteSrcSpatialDist; // Whether to delete or not fsrcSpatialDist in destructor
   string                             fSrcName;                // Source name (for a single CR species)
   TUValsTXYZEVirtual                *fSrcSpatialDist;         // Spatial distribution of the source [-]
   TUValsTXYZEVirtual                *fSrcSpectrum;            // Source spectrum dn/dEkn [#part/(m^3.s.GeV/n)]

public:
   TUSrcSteadyState();
   TUSrcSteadyState(TUSrcSteadyState const &src_steadystate);
   virtual ~TUSrcSteadyState();

   inline virtual TUSrcSteadyState   *Clone()                                               {return new TUSrcSteadyState(*this);}
   virtual void                       CopySpectrumOrSpatDist(TUValsTXYZEVirtual *spect_or_spatdist, Bool_t is_spect_or_spatdist);
   void                               Copy(TUSrcSteadyState const &src_steadystate);
   inline virtual TUSrcSteadyState   *Create()                                              {return new TUSrcSteadyState;}
   void                               FillSpatialDistFromTemplate(TUSrcTemplates *templ, string const &templ_name, FILE *f_log);
   void                               FillSpectrumFromTemplate(TUSrcTemplates *templ, string const &templ_name, FILE *f_log);
   virtual void                       FillSpectrumOrSpatialDist(string const &formula, Bool_t is_verbose, FILE *f_log, string const &commasep_vars, TUFreeParList* free_pars, Bool_t is_use_or_copy_freepars, Bool_t is_spect_or_spatdist);
   virtual void                       FillSpectrumOrSpatialDistFromTemplate(TUSrcTemplates *templ, string const &templ_name, Bool_t is_spect_or_spatdist, FILE *f_log);
   inline virtual TUFreeParList      *GetSrcFreePars(Bool_t is_spect_or_spatdist)           {return ((is_spect_or_spatdist) ? fSrcSpectrum->GetFreePars() : fSrcSpatialDist->GetFreePars());}
   inline virtual string              GetSrcName() const                                    {return fSrcName;}
   inline virtual TUValsTXYZEVirtual *GetSrcSpatialDist() const                             {return fSrcSpatialDist;}
   inline virtual TUValsTXYZEVirtual *GetSrcSpectrum() const                                {return fSrcSpectrum;}
   virtual void                       Initialise(Bool_t is_delete);
   inline Bool_t                      IsDeleteSrcSpectrum() const                           {return fIsDeleteSrcSpectrum;}
   inline Bool_t                      IsDeleteSrcSpatialDist() const                        {return fIsDeleteSrcSpatialDist;}
   virtual void                       PrintSrc(FILE *f=stdout, Bool_t is_header=true) const;
   inline virtual void                SetSrcName(string const &name)                        {fSrcName = name;}
   virtual void                       TEST(TUInitParList *init_pars, FILE *f=stdout);
   virtual Bool_t                     TUI_ModifyFreeParameters();
   inline virtual string              UIDSpatialDist() const                                {return string(fSrcName + "_" + fSrcSpatialDist->UID());}
   inline virtual string              UIDSpectrum() const                                   {return string(fSrcName + "_" + fSrcSpectrum->UID());}
   virtual void                       UpdateFromFreeParsAndResetStatus();
   virtual void                       UseSpectrumOrSpatDist(TUValsTXYZEVirtual *spect_or_spatdist, Bool_t is_spect_or_spatdist);
   inline virtual Double_t            ValueSrcSpatialDistrib(TUCoordTXYZ *coord_txyz = NULL)              {UpdateFromFreeParsAndResetStatus(); return fSrcSpatialDist->ValueTXYZ(coord_txyz);}
   inline virtual Double_t            ValueSrcSpectrum(TUCoordE *coord_e, TUCoordTXYZ *coord_txyz = NULL) {UpdateFromFreeParsAndResetStatus(); return fSrcSpectrum->ValueETXYZ(coord_e, coord_txyz);}

   ClassDef(TUSrcSteadyState,1)  // Steady-state source: name, spectrum, spatial dist. [inherits from TUSrcVirtual]
};

#endif
