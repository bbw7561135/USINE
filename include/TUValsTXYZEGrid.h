// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUValsTXYZEGrid
#define USINE_TUValsTXYZEGrid

// C/C++ include
// ROOT include
// USINE include
#include "TUValsTXYZEVirtual.h"

class TUValsTXYZEGrid : public TUValsTXYZEVirtual {
private:
   string                          fFileName;       // Name of the file read to fill fVals
   vector<Int_t>                   fN;              //![0<N<6] Number of bins {NT,NX,NY,NZ,NE} in each dimension
   string                          fName;           // Additional name associated to file (optional)
   Int_t                           fNCells;         // Total number of cells (time/space/E grid)
   Int_t                           fNCellsTXYZ;     // Number of time/space cells
   Int_t                           fNDimXYZ;        // Number ND of spatial dimensions (0, 1->X, 2->XY, 3->XYZ)
   Double_t                       *fVals;           //![NT*NX*NY*NZ*NE] Values on space-T-E grid

   void                            AddOrSubtractVals(TUValsTXYZEGrid *array, Bool_t is_add_or_sub);
   void                            AddOrSubtractVals(TUValsTXYZEGrid *array, Int_t *bins_txyz, Bool_t is_add_or_sub);
   Int_t                           GetBinGlobal(Int_t bine, Int_t *bins_txyz=NULL) const;
   void                            GetBinETXYZ(Int_t global_bin, Int_t &bine, Int_t *bins_txyz) const;
   void                            GetBinETXYZ(Int_t global_bin, Int_t &bine, TUCoordTXYZ *coord_txyz) const;
   inline Bool_t                   IsTXYZ(Int_t i) const                                    {return (fN[i]>1) ? true : false;}

public:
   TUValsTXYZEGrid();
   TUValsTXYZEGrid(TUValsTXYZEGrid const &vals);
   virtual ~TUValsTXYZEGrid();

   inline void                     AddVals(TUValsTXYZEGrid *array)                          {AddOrSubtractVals(array, true);}
   inline void                     AddVals(TUValsTXYZEGrid *array, Int_t *bins_txyz)        {AddOrSubtractVals(array, bins_txyz, true);}
   void                            AllocateVals(TUAxesTXYZ *axes_txyz, Int_t ne=0);
   void                            AllocateVals(Int_t ne, Int_t nt, Int_t nx, Int_t ny, Int_t nz);
   inline void                     AllocateVals(Int_t nt, Int_t nx, Int_t ny, Int_t nz)     {AllocateVals(0, nt, nx, ny, nz);}
   inline void                     AllocateVals(Int_t ne)                                   {AllocateVals(ne, 0, 0, 0, 0);}
   inline virtual TUValsTXYZEGrid *Clone()                                                  {return new TUValsTXYZEGrid(*this);}
   virtual void                    Copy(TUValsTXYZEGrid const &vals);
   inline virtual TUValsTXYZEGrid *Create()                                                 {return new TUValsTXYZEGrid;}
    inline Double_t                *GetAddressVals(Int_t *bins_txyz)                        {return &fVals[GetBinGlobal(0, bins_txyz)];}
   inline string                   GetFileName() const                                      {return fFileName;}
   inline Int_t                    GetN(Int_t i) const                                      {return fN[i];}
   inline virtual string           GetName() const                                          {return fName;}
   inline Int_t                    GetNCells() const                                        {return fNCells;}
   inline Int_t                    GetNCellsTXYZ() const                                    {return fNCellsTXYZ;}
   inline Int_t                    GetNDimXYZ() const                                       {return fNDimXYZ;}
   inline Int_t                    GetNE() const                                            {return fN[4];}
   inline Int_t                    GetNT() const                                            {return fN[0];}
   inline Int_t                    GetNX() const                                            {return fN[1];}
   inline Int_t                    GetNY() const                                            {return fN[2];}
   inline Int_t                    GetNZ() const                                            {return fN[3];}
   inline Double_t                 GetVal(Int_t global_bin) const                           {return fVals[global_bin];}
   inline Double_t                 GetVal(Int_t bine, Int_t *bins_txyz) const               {return fVals[GetBinGlobal(bine, bins_txyz)];}
   inline Double_t                 GetValE(Int_t bine) const                                {return fVals[GetBinGlobal(bine)];}
   inline Double_t                 GetValTXYZ(Int_t *bins_txyz) const                       {return GetVal(0, bins_txyz);}
   virtual void                    Initialise(Bool_t is_delete=false);
   inline Bool_t                   IsE() const                                              {return ((GetNE()>0) ? true : false);}
   Bool_t                          IsSameFormat(TUValsTXYZEGrid *vals) const;
   Bool_t                          IsSameFormat(Int_t ne, TUAxesTXYZ *axes_txyz) const;
   Bool_t                          IsSameFormat(TUAxesTXYZ *axes_txyz) const;
   inline Bool_t                   IsT() const                                              {return IsTXYZ(0);}
   inline Bool_t                   IsX() const                                              {return IsTXYZ(1);}
   inline Bool_t                   IsY() const                                              {return IsTXYZ(2);}
   inline Bool_t                   IsZ() const                                              {return IsTXYZ(3);}
   void                            KeepLowerVals(TUValsTXYZEGrid *array);
   void                            KeepUpperVals(TUValsTXYZEGrid *array);
   void                            MultiplyVals(Double_t const &val);
   virtual void                    PrintSummary(FILE *f=stdout, string const &description = "") const;
   void                            PrintVals(FILE *f=stdout) const;
   void                            PrintVals(FILE *f, Int_t bine) const;
   void                            PrintVals(FILE *f, Int_t *bins_txyz) const;
   void                            PrintValsTXYZ(FILE *f=stdout) const;
   virtual void                    SetClass(string const &file, Bool_t is_verbose, FILE *f_log, string const &commasep_vars="", TUFreeParList *free_pars = NULL, Bool_t is_use_or_copy_freepars=true);
   inline virtual void             SetName(string const &name)                              {fName = name;}
   inline void                     SetVal(Double_t const &val, Int_t bine, Int_t *bins_txyz=NULL){fVals[GetBinGlobal(bine, bins_txyz)]=val;}
   inline void                     SetValTXYZ(Double_t const &val, Int_t *bins_txyz)        {SetVal(val, 0, bins_txyz);}
   void                            SetVals(Double_t const &val);
   void                            SetVals(TUValsTXYZEGrid *array);
   void                            SetValsE(TUAxesCrE *axes_cre, Int_t bincr, vector<Double_t> const &vals);
   inline void                     SubtractVals(TUValsTXYZEGrid *array)                     {AddOrSubtractVals(array, false);}
   inline void                     SubtractVals(TUValsTXYZEGrid *array, Int_t *bins_txyz)   {AddOrSubtractVals(array, bins_txyz, false);}
   void                            TEST(TUInitParList *init_pars, FILE *f=stdout);
   inline virtual string           UID() const                                              {return fFileName;}
   inline virtual Double_t         ValueE(TUCoordE *coord_e)                                {return GetValE(coord_e->GetBinE());}
   inline virtual Double_t         ValueETXYZ(TUCoordE *coord_e, TUCoordTXYZ *coord_txyz)   {return ((coord_txyz) ? GetVal(coord_e->GetBinE(), coord_txyz->GetBins()) : GetValE(coord_e->GetBinE()));}
   inline virtual Double_t         ValueTXYZ(TUCoordTXYZ *coord_txyz)                       {return ((coord_txyz) ? GetValTXYZ(coord_txyz->GetBins()) : Value());}
   inline virtual Int_t            ValsTXYZEFormat() const                                  {return 1 /*Grid of values from file*/;}

   ClassDef(TUValsTXYZEGrid, 1)  // Grid of spacetime- and e-dependent values [inherits from TUValsTXYZ]
};

#endif
