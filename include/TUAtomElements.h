// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUAtomElements
#define USINE_TUAtomElements

// C/C++ include
#include <string>
#include <vector>
#include <stdlib.h>
using namespace std;
// ROOT include
// USINE include
#include "TUMisc.h"

class TUAtomElements {

private:
   vector<string>         fName;      //![109] Names for atomic element (upper insensitive)

public:
   TUAtomElements();
   virtual ~TUAtomElements();

   static void            CheckZ(Int_t z);
   static inline Int_t    CRNameToA(string const &cr)                                 {return atoi(cr.substr(0, cr.find_first_not_of("0123456789", 0)).c_str());}
   static inline string   CRNameToCRElement(string const &cr)                         {return cr.substr(cr.find_first_not_of("0123456789", 0), cr.size());}
   inline Int_t           CRNameToZ(string const &cr, Bool_t is_verbose = true) const {return ElementNameToZ(CRNameToCRElement(cr), is_verbose);}
   Int_t                  ElementNameToZ(string const &element, Bool_t is_verbose = true, FILE *f_log = stdout) const;
   inline Int_t           GetNElements() const                                        {return (Int_t)fName.size();}
   Bool_t                 IsContainElementName(string const &qty) const;
   Bool_t                 IsElement(string const &qty) const;
   string                 MostAbundantIsotope(string const &element, FILE *f_log = stdout) const;
   Int_t                  NameToZ(string const &qty, Bool_t is_verbose = true) const  {return (IsElement(qty) ? ElementNameToZ(qty, is_verbose) : CRNameToZ(qty, is_verbose));}
   void                   TEST(FILE *f = stdout) const;
   string                 ZToElementName(Int_t z) const;
   inline string          ZToElementNameROOT(Int_t z) const                           {string name = ZToElementName(z); return TUMisc::FormatCRQty(name,2);}

   ClassDef(TUAtomElements, 1)  // Atomic (anti-)element names: Z=[1-109]
};

#endif
