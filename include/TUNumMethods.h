// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUNumMethods
#define USINE_TUNumMethods

// C/C++ include
#include <cmath>
#include <iostream>
// ROOT include
// USINE include
#include "TUAxis.h"

namespace TUNumMethods {

   Bool_t      IntegrationSimpsonAdapt(TUAxis *axis, Double_t *integ1, Double_t *integ2, Int_t &kmin, Int_t &kmax, Double_t &s, Double_t const &eps, string const &classmethod, Bool_t is_verbose = false, Int_t j_min_dubling=3);
   Bool_t      IntegrationSimpsonAdapt(TUAxis *axis, Double_t *integrand, Int_t &kmin, Int_t &kmax, Double_t &s, Double_t const &eps, string const &classmethod, Bool_t is_verbose = false, Int_t j_min_dubling=3);
   void        IntegrationSimpsonAdapt(void (*fn)(Double_t&, Double_t*, Double_t&), Double_t &xmin, Double_t &xmax, Double_t par[], Double_t &s, Double_t const &eps, Bool_t is_log, Bool_t is_verbose = false);
   void        IntegrationSimpsonLin(TUAxis *axis, Double_t *integrand, Int_t kmin, Int_t kmax, Double_t &s);
   void        IntegrationSimpsonLog(TUAxis *axis, Double_t *integ1, Double_t *integ2, Int_t kmin, Int_t kmax, Double_t &s);
   void        IntegrationSimpsonLog(void (*fn)(Double_t&, Double_t*, Double_t&), Double_t &xmin, Double_t &xmax, Double_t par[], Double_t &s, Int_t nstep);
   void        IntegrationTrapezeRefine(TUAxis *axis, Double_t *integrand, Int_t &kmin, Int_t &kmax, Double_t &s, Int_t n);
   void        IntegrationTrapezeRefine(TUAxis *axis, Double_t *integ1, Double_t *integ2, Int_t &kmin, Int_t &kmax, Double_t &s, Int_t n);
   void        IntegrationTrapezeRefine(void (*fn)(Double_t&, Double_t*, Double_t&), Double_t &xmin, Double_t &xmax, Double_t par[], Double_t &s, Bool_t is_log, Int_t n);
   template<class F>
   inline void InlineIntegrationSimpsonLogAdapt(const F &f, Double_t &xmin, Double_t &xmax, Double_t par[], Double_t &s, Double_t const &eps, Bool_t is_verbose = false) {
      //--- Simpson's rule log-step integration (adapted from numerical recipes)
      // Input:
      //  fn         Prototype of the function to integrate
      //  xmin       Lower boundary integration
      //  xmax       Upper boundary integration
      //  par[]      Parameters of fn
      //  eps        Precision sought for integration
      //  is_verbose Chatter on or off
      // Output:
      //  s          Integration results

      Double_t ost = -1.e-40;
      Double_t os = -1.e-40;
      Int_t jmax = 25;
      Double_t st = 0.;
      if (xmin >= xmax) {
         s = 0.;
         return;
      }

      for (Int_t j = 1; j <= jmax; ++j) {
         InlineIntegrationTrapezeLogRefine(f, xmin, xmax, par, st, j);
         s = (4.*st - ost) / 3.;
         if ((fabs(s - os) < eps * fabs(os) || s < 1.e-30) && j >= 3) {
            if (is_verbose) cout << ">>>>> IntegrationSimpsonLogAdapt: convergence Dres/res<"
                                    << eps << " reached after "
                                    << j << " refinement (i.e. "
                                    << pow(2., j) << " steps)" << endl;
            return;
         }
         os = s;
         ost = st;
      }
      cout << ">>>>> Too many steps!" << endl;
      abort();
   }
   template<class F>
   inline void InlineIntegrationTrapezeLogRefine(const F &f, Double_t &xmin, Double_t &xmax, Double_t par[], Double_t &s, Int_t n) {
      //--- IntegrationTrapeze refinement log-step (called by Simpson, do not call it directly!)
      // Input:
      //  fn         Prototype of the function to integrate
      //  xmin       Lower boundary integration
      //  xmax       Upper boundary integration
      //  par[]      Parameters of fn
      //  n          Number of doubling steps
      // Output:
      //  s          Integration results

      Int_t tnm = 0;
      Double_t del = 0., sum = 0., r = 0.;
      if (xmin <= 0.) {
         cout << ">>>>> log step with xmin=0!!!! Abort!" << endl;
         abort();
      }

      if (n == 1) {
         Double_t fn_res = 0.;
         f(xmin, par, fn_res);
         s += fn_res * xmin;
         f(xmax, par, fn_res);
         s += fn_res * xmax;
         s *= 0.5 * log(xmax / xmin);
      } else {
         tnm = (Int_t)pow(2., n - 2);
         del =  pow(xmax / xmin, 1. / (Double_t)tnm);
         r = xmin * sqrt(del);
         sum = 0.0;
         for (Int_t j = 1; j <= tnm; ++j) {
            Double_t fn_res;
            f(r, par, fn_res);
            sum += fn_res * r;
            r *= del;
         }
         s = 0.5 * (s +  log(xmax / xmin) * sum / (Double_t)tnm);
      }
      return;
   }

   void       MatrixInversionGaussJordan(Double_t*a[], Int_t n, Double_t*adiag[], Int_t m);
   void       MatrixInversionTriDiagonal(Double_t*ainf, Double_t*adiag, Double_t*asup, Double_t*r, Double_t*u, Int_t n);
   void       MatrixInversionTriDiagonal(Double_t*a, Double_t*b, Double_t*c, Double_t*u, Int_t n);
   void       SolveEq_1D2ndOrder_Explicit(Int_t nx, Double_t const &dx, Double_t *a_term, Double_t* b_term, Double_t* c_term, Double_t* s_term, gENUM_MATRIXINVERT scheme, Double_t const &b0, Double_t const &c0, Double_t const &an, Double_t const &bn);
   void       SolveEq_T1D2ndOrder_CranckNicholson(Int_t nt, Int_t nx, Double_t const &dt, Double_t const &dx, Double_t **fn2_t1_2, Double_t **fn1_t1_2, Double_t **fn0_t1_2, Double_t **v_w, gENUM_MATRIXINVERT scheme=kTRID);

   void       TEST(FILE *f=stdout);
}

#endif
