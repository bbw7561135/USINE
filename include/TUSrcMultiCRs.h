// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUSrcMultiCRs
#define USINE_TUSrcMultiCRs

// C/C++ include
// ROOT include
// USINE include
#include "TUSrcVirtual.h"

class TUSrcMultiCRs {

private:
   TUCRList                  *fCRs;                      // List of CRs from which to pick fSpecies for src
   TUFreeParList             *fFreePars;                 // Free parameters of the src
   TUFreeParList            **fFreeParsPerSpecies;       //![NSpecies] Free parameters per species (among fFreePars)
   vector<Int_t>              fIndexSrcNormInFreePar;    //![NSpecies] If spec formula, index of norm par in fFreePars
   Int_t                      fIndexTemplSpatialDist;    // Index of template spatial dist used for all species
   Int_t                      fIndexTemplSpectrum;       // Index of template spectrum used for all species
   Bool_t                     fIsAstroOrDM;              // Whether this is an 'astro' or 'dark matter' src
   Bool_t                     fIsDeleteCRs;              // Whether to delete or not fCRList in destructor
   Bool_t                     fIsDeleteTemplates;        // Whether to delete or not fTemplates in destructor
   Bool_t                     fIsSteadyStateOrPointLike; // Whether this is a steady-state or point-like src
   vector<Int_t>              fMapCR2SrcIndices;         //![NCRs] Mapping of fCRs into fSpecies (src) indices
   vector<Int_t>              fMapSrc2CRIndices;         //![NSpecies] Mapping of fSpecies (src) into fCRs indices
   string                     fNameSrcNormPar;           // Name of parameter used as normalisation for spectrum
   TUCRList                  *fSpecies;                  // Species present in this src
   TUSrcVirtual             **fSrcPerSpecies;            //![NSpecies] Src (steady-state/point-like) per fSpecies
   string                     fSrcName;                  // Source name
   TUSrcTemplates            *fTemplates;                // Source templates from which fSrcPerSpecies are chosen

   void                       AllocateAndFillSrcFromTemplate(string templ_name, string user_pars, Bool_t is_verbose, FILE *f_log, Bool_t is_spectrum_or_spatdist, string name_spectrum_normparam="");
   void                       InitialiseSpectrumNorm(vector<gENUM_SRCABUND_INIT> const &src_init, Bool_t is_verbose, FILE *f_log);
   void                       SetClassGen(TUInitParList *init_pars, string group, string subgroup, string src_name, Bool_t is_verbose, FILE *f_log);

public:
   TUSrcMultiCRs();
   TUSrcMultiCRs(TUSrcMultiCRs const &src_multicrs);
   virtual ~TUSrcMultiCRs();

   void                       AddPlotRatioSrcToSSAbund(TMultiGraph *mg, TLegend *leg, string norm_elem, string const &model_name, Bool_t is_ssfip_or_tc) const;
   inline TUSrcMultiCRs      *Clone()                                                         {return new TUSrcMultiCRs(*this);}
   void                       Copy(TUSrcMultiCRs const &src_multicrs);
   inline TUSrcMultiCRs      *Create()                                                        {return new TUSrcMultiCRs;}
   void                       ExtractIsotopeswhoseNormIsAFreePar(TUFreeParList const *pars, vector<Int_t> &i_norm_species) const;
   inline TUFreeParList      *GetFreePars() const                                             {return fFreePars;}
   inline Int_t               GetNFreePars() const                                            {return ((fFreePars) ? fFreePars->GetNPars() : 0);}
   inline Int_t               GetNIndexSrcNormInFreePar() const                               {return fIndexSrcNormInFreePar.size();}
   inline string              GetNameSrcNormPar() const                                       {return fNameSrcNormPar;}
   inline Int_t               GetNSpecies() const                                             {return ((fSpecies) ? fSpecies->GetNCRs() : 0);}
   inline TUCRList           *GetCRs() const                                                  {return fCRs;}
   inline Double_t            GetNormSrcSpectrum(Int_t j_species) const                       {return fFreePars->GetParEntry(fIndexSrcNormInFreePar[j_species])->GetVal(false);}
   Double_t                   GetNormSrcSpectrum_Element(string const &element, bool is_verbose=true) const;
   inline Double_t            GetNormSrcSpectrum_Element(Int_t z_charge, bool is_verbose=true) const {return GetNormSrcSpectrum_Element(fSpecies->ZToElementName(z_charge), is_verbose);}
   inline TUCRList           *GetSpecies() const                                              {return fSpecies;}
   inline TUSrcVirtual       *GetSrc(Int_t j_species) const                                   {return fSrcPerSpecies[j_species];}
   inline string              GetSrcName() const                                              {return fSrcName;}
   inline TUValsTXYZEVirtual *GetSrcSpatialDist(Int_t j_species) const                        {return fSrcPerSpecies[j_species]->GetSrcSpatialDist();}
   inline TUValsTXYZEVirtual *GetSrcSpectrum(Int_t j_species) const                           {return fSrcPerSpecies[j_species]->GetSrcSpectrum();}
   inline TUValsTXYZEFormula *GetTDepPosition(Int_t j_species, Int_t i_coord) const           {return fSrcPerSpecies[j_species]->GetTDepPosition(i_coord);}
   inline TUSrcTemplates     *GetTemplates() const                                            {return fTemplates;}
   inline Int_t               IndexCRInSrcSpecies(Int_t j_cr) const                           {return fMapCR2SrcIndices[j_cr];}
   inline Int_t               IndexSrcSpeciesInCRList(Int_t j_species) const                  {return fMapSrc2CRIndices[j_species];}
   inline Int_t               IndexSrcNormInFreePar(Int_t i_norm) const                       {return fIndexSrcNormInFreePar[i_norm];}
   inline Int_t               IndexTemplSpatialDist() const                                   {return fIndexTemplSpatialDist;}
   inline Int_t               IndexTemplSpectrum()  const                                     {return fIndexTemplSpectrum;}
   void                       Initialise(Bool_t is_delete, Bool_t all_but_crs=false);
   inline Bool_t              IsAstroOrDM() const                                             {return fIsAstroOrDM;}
   inline Bool_t              IsDeleteCRs() const                                             {return fIsDeleteCRs;}
   inline Bool_t              IsDeleteTemplates() const                                       {return fIsDeleteTemplates;}
   inline Bool_t              IsSteadyStateOrPointLike() const                                {return fIsSteadyStateOrPointLike;}
   TH1D                      *OrphanSrcAbundances(Bool_t is_cr_or_element, Bool_t is_src_or_ss, string const &model_name, string element_onwhich2rescale="Si", Double_t rescaled_value=1.) const;
   inline void                PrintSrc(FILE* f, Int_t j_species, Bool_t is_header=true) const      {fSrcPerSpecies[j_species]->PrintSrc(f, is_header);}
   void                       PrintSrc(FILE* f=stdout, Bool_t is_header=true, Bool_t is_summary=false) const;
   inline void                ScaleNormSrcSpectrum(Int_t j_species, Double_t const &scale_factor){SetNormSrcSpectrum(j_species, GetNormSrcSpectrum(j_species)*scale_factor);}
   void                       SetClass(TUInitParList *init_pars, string const &group, string const &subgroup, string const &src_name, Bool_t is_verbose, FILE *f_log);
   void                       SetClass(TUInitParList *init_pars, string const &group, string const &subgroup, string const &src_name, TUCRList *used_crs, TUSrcTemplates *used_templates, Bool_t is_verbose, FILE *f_log);
   inline void                SetNormSrcSpectrum(Int_t j_species, Double_t const &norm)       {fFreePars->GetParEntry(fIndexSrcNormInFreePar[j_species])->SetVal(norm, false);}
   void                       SetNormSrcSpectrum_FixedIsotopicRelAbund(Double_t const &abund_element, string const &element);
   inline void                SetSrcName(string const &name)                                  {fSrcName = name;}
   void                       TEST(TUInitParList *init_pars, FILE *f=stdout);
   inline Bool_t              TUI_ModifyFreeParameters()                                      {return fFreePars->TUI_Modify();}
   inline string              UIDSpatialDist(Int_t j_species) const                           {return string(fSrcName + "_" + fSrcPerSpecies[j_species]->UIDSpatialDist());}
   inline string              UIDSpectrum(Int_t j_species) const                              {return string(fSrcName + "_" + fSrcPerSpecies[j_species]->UIDSpectrum());}
   void                       UpdateFromFreeParsAndResetStatus();
   inline Double_t            ValueSrcPosition(Int_t j_species, Int_t i_coord/*0,1,2*/, Double_t const &t_myr)     {UpdateFromFreeParsAndResetStatus(); return fSrcPerSpecies[j_species]->ValueSrcPosition(i_coord, t_myr);}
   inline Double_t            ValueSrcSpatialDistrib(Int_t j_species, TUCoordTXYZ *coord_txyz = NULL)              {UpdateFromFreeParsAndResetStatus(); return fSrcPerSpecies[j_species]->ValueSrcSpatialDistrib(coord_txyz);}
   inline Double_t            ValueSrcSpectrum(Int_t j_species, TUCoordE *coord_e, TUCoordTXYZ *coord_txyz = NULL) {UpdateFromFreeParsAndResetStatus(); return fSrcPerSpecies[j_species]->ValueSrcSpectrum(coord_e, coord_txyz);}

//   TGraphAsymmErrors         *GetSrcIsotopicAbundances(usineSrcSteadyStateSpectra *spec_best, usineSrcSteadyStateSpectra *spec_min, usineSrcSteadyStateSpectra *spec_max);  //!< For a previously calculated min, max and best spectra values, give back a graph where each "x_i" is an isotope "i" and "y_i" is qi, i.e. the source isotopic abundance with its minimum and maximum range
//   TGraphAsymmErrors         *GetSrcElementalAbundances(usineSrcSteadyStateSpectra *spec_best, usineSrcSteadyStateSpectra *spec_min, usineSrcSteadyStateSpectra *spec_max); //!< Same but for elemental abundances!

   ClassDef(TUSrcMultiCRs,1)  // Generic source description (point-like or steady-state) for a list of CRs
};

#endif
