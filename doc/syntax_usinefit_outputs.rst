.. _rst_syntax_usinefit_outputs:

UsineFit\@Outputs
~~~~~~~~~~~~~~~~~

USINE outputs enabled after minimization runs.

.. note:: The best-fit parameters are always stored in the file ``fit_result.out`` (in the user-chosen directory for the run).


.. tabularcolumns:: |p{7.cm}|p{8.cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*UsineFit\@Outputs\@IsPrintCovMatrix* (M=0)", "If ``true``, save the covariance matrix of the best-fit parameters (FIT and NUISANCE) obtained by the minimiser in ``fit_covmatrix.out`` (in the user-chosen directory for the run)."
   "*UsineFit\@Outputs\@IsPrintHessianMatrix* (M=0)", "Same, but for Hessian matrix (inverse of covariance matrix), saved in ``fit_hessianmatrix.out``."
   "*UsineFit\@Outputs\@Contours* (M=1)", "Syntax in initialisation file is ``X1,X2:N:{CL1,CL2...}``, with ``X1`` and ``X2`` the parameter names (use keyword ``ALL`` to get all contour combinations of fit parameters), ``N`` the number of points to use in the contour, ``CLi`` the contours to draw in unit of :math:`\sigma`. The output is a plot of ``X1`` vs ``X2``, also saved in the file ``fit_contours_X1_vs_X2.out`` (in the user-chosen directory for the run). Note that extracting contours can be very slow and sometimes it fails to get (and thus plot) the sought contours. Only use at the last steps of your analysis, and for well-behaved minima (i.e., :math:`\chi^2_{\rm min}/{\rm dof}\sim 1`)."
   "*UsineFit\@Outputs\@Profile* (M=1)", "Syntax in initialisation file is ``X:N``, with ``X`` the parameter name (use keyword ``ALL`` to scan all fit parameters), and ``N`` the number of points to use in the scan. The output is a plot of :math:`\chi_{\rm min}^2(X)`, also saved in the file ``fit_profilelikelihood_X.out`` (in the user-chosen directory for the run). This can take some time (do not confuse it with the *Scan* keyword above) for the following reason: imagine that your initial fit had :math:`n_{\rm fit}` parameters, now you need to find the new minimum (for each parameter) at ``N`` positions, minimising on the :math:`n_{\rm fit}-1` other parameters. Note that if your initial minimisation (:math:`n_{\rm fit}` parameters) converged well, this is guaranteed to work well as well."
   "*UsineFit\@Outputs\@Scan* (M=1)", "Syntax in initialisation file is ``X:N``, with ``X`` the parameter name (use keyword ``ALL`` to scan all fit parameters), and ``N`` the number of points to use in the scan. The output is a plot of :math:`\chi^2(X)`, also saved in the file ``fit_parameterscan_X.out`` (in the user-chosen directory for the run). This takes no time at all, as there is no further minimisation (do not confuse it with the *Profile* keyword above), only ``N`` calls to evaluate :math:`\chi^2` at various positions of the parameter."
