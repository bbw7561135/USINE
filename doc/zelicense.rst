********
Licenses
********
:USINE: The USINE library makes use of the `ROOT system <https://root.cern.ch/license>`__ and the `fparser library <http://warp.povusers.org/FunctionParser/fparser.html>`__. Both being distributed under the Lesser General Public Licence (`LGPL 3 <http://www.gnu.org/licenses/lgpl-3.0.en.html>`__), this probably makes USINE available under the same licence --- Copyright © David Maurin (1999-2018)

:USINE logo: Used with the author permission --- Copyright © Francis Tousignant (2003)


:ROOT CERN licence: The `ROOT system <https://root.cern.ch/license>`__ is being made available under the `LGPL 3 <http://www.gnu.org/licenses/lgpl-3.0.en.html>`__, which allows ROOT to be used in a wide range of open and closed environments. The optional `MathMore library <https://root.cern.ch/mathmore-library>`__ uses the GSL library which is licensed under the `GPL <http://www.gnu.org/licenses/gpl-3.0.en.html>`__ and hence the `MathMore library <https://root.cern.ch/mathmore-library>`__ is only available under the `GPL <http://www.gnu.org/licenses/gpl-3.0.en.html>`__.


:fparser license: The `fparser library <http://warp.povusers.org/FunctionParser/fparser.html>`__ is distributed under the `LGPL 3 <http://www.gnu.org/licenses/lgpl-3.0.en.html>`__ --- Copyright © 2003-2011 Juha Nieminen, Joel Yliluoma