.. _rst_keywords_group_usinefit:

Group = ``UsineFit``
^^^^^^^^^^^^^^^^^^^^

The syntax of the associated parameters is detailed in :numref:`rst_syntax_usinefit`.

.. _tab_usinefit:

.. table:: Subgroups for minimization runs (fit params and data).

   =====================  ======================================
   Group\@subgroup        Description
   =====================  ======================================
   *UsineFit\@Config*     Minimizer setup (precision, algorithms...)
   *UsineFit\@FreePars*   Free (or nuisance) parameters of the fit
   *UsineFit\@TOAData*    TOA CR quantity/exp/Etype for the fit
   =====================  ======================================