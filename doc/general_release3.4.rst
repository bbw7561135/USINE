.. _rst_general_release3.4:

Notes for V3.4 (June 2018)
~~~~~~~~~~~~~~~~~~~~~~~~~~


.. note:: V3.4 was the first public version of USINE released in June 2018. Minor corrections were implemented in July 2018.

Tag V3.4 (2018/06)
------------------

**Bug fixes (in this release)**

2018/07/19 - corr#1
   - Minor bug fixed in TUXSections (to skip XS reaction not present in list when loading file)
   - Minor bug fixed in E6 (bug related to XS file renaming done just before the release)
   - Better conversion for quantities on different E-type (updated .ref files)
   - 'IsELossCoulombIon' split in 'IsELossCoulomnb' and 'IsELossIon' in init.TEST.par and int.Model1D.par (to fix minor bug in E2)
   - Better instructions for user for option E2
   - Added section "Models and equations" in documentation
   - Updated .gitlab-ci.yml to generate properly usine.pdf and access it from online documentation


**Prior to release**

2018/06
   - Added IsWind() in list of switches (for comparison plots)
   - Small bug fixes in comparison plot axes name
   - Consistent renaming of XS files (and header description)
   - Bug fixed for rhole calculation
   - CMakeList.txt modified to also compile on mac OS

2018/05
   - Prepared version for AMS collaboration (internal use)
   - Added init.Model1D.par initialisation file for model 1D
   - Merged master into v4-dev branch
   - Removed TU\*Spline\*, SolMod1D, TUCRFluxes... (in v4-dev)

2018/04
   - Removed display of model\*bias when no nuisance parameter
   - Information on errors for displayed data and fit data added on graphs
   - Read Tc 50% (instead of Tc) in TUAtomProperties
   - removed files usine.tutorial.odp and usine.tutorial.pdf (now in this doc)
   - removed file usine.v3.3.log (now in this doc -this file!)
   - Minor bug fixed in minimisation status print
   - Sphinx documentation (git added) available at  $USINE/doc/_build/html/index.html
   - Important change for init.TEST.par (and accordingly in TUInitParList.cc), see the documentation: new format ``group @ subgroup @ parameter @ M=?  @ value``, a few subgroups reassigned to different group, a few subgroup names modified, a few parameters renamed, and hopefully better syntax to select template for source spectrum / spatial distribution.
   - Sort correctly by growing Z in option D0 (and bug fixed) + added fluxes
   - Default values for initialisation file modified to 'better match' B/C data and fluxes
   - More message for MINUIT/MINOS status
   - Graph for model with data nuisance parameters added
   - Clean-up unused/dummy methods in all classes also to avoid redundancy (TU\*\*\*List vs TU\*\*\*Entry methods)
   - Significant modifications in TUDataSet to handle possibility of data with same exp/qty but different etype
     (e.g., AMS-02 data for B/C are in kEKN and kR)
   - Added parameter in parfile to select 'a' '(fluxes shown times E^a) for displays for fits
   - Added residuals (data-model)/model in fit results
   - Bugs fixed (valgrind analysis): radioactive decay equilibrium comparison (option -ED3+), CoeffReac1stOrder (was using index out of range), a few comparisons to indices (no impact on runs and test functions), TUMisc:RemoveDuplicates(), number of free pars displayed (with -m2)


2018/03
   - combos to display: B/C,O → B/C,O:kEKN;B/C:kR to enable fit of a mix of KEKN ad KR data (syntax of -l option changed accordingly)
   - Paths with env variable (e.g., $USINE) replaced in prints
   - Path correctly stripped in some tests (to be user location independent)
   - Reference files added for -i and -e options in integration tests
   - Minor bug fixed to avoid duplicated in TUInitParList.cc
   - Minor bug fixed for multiple XS as free parameters
   - Added USINE advertisement on plots
   - More inelastic XS added (thanks Yoann!) + removed dummy name in format
   - Moved obsolete ``BEGIN_MACROS...END_MACROS`` statements in src/TU\*.cc files (THtml root documentation no longer used) into usine.MACROS/ as .C ROOT macros
   - Removed ``#if !defined(R__ALPHA) && !defined(R__SOLARIS) && !defined(R__ACC) && !defined(R__FBSD)`` and ``#endif`` enclosing ``NamespaceImp(TU...)`` in src/\*.cc for namespaces
   - Added usine -i and -e options in integration tests
   - Option A1 split in A1 & A2 (for GCR/SS plots)
   - Renamed TUPhysicsProcesses → TUInteractions, SigTot\*.dat → SigInel\*.dat
   - Enabled more free parameters for XS (EAxisScale, EnhancePowHE, EThresh, SlopeLE in addition to Norm): either for explicit reactions or all reactions
   - Renamed usine.version.txt → usine.v3.3.log, crprop_charts\* → crcharts\*, crprop_abundances2003.dat → solarsystem_abundances2003.dat, usine_run.cc → usine.cc
   - Single executable usine for all options (removed all src/usine\_\*.cc!)
   - ./bin/usine -tUSINE to test USINE installation/stability (usine.test file removed, new usine.tests/\*.ref -- one file per class)
   - FILE \*f as argument added in TEST() methods in all classes
   - Removed directory inputs/OBSOLETE/

2018/01
   - Bug fixed with source parameters in minimisation
   - Test of minimisation added (in usine.test)
   - Fitable/nuisance parameter for 'half-life' (BETA or EC) added

2017/12
   - Added user output directory in 'usine_run -t' command line
   - Several mini-bug fixed in D1, D2A, and D2B run options
   - Precision issue fixed in normalisation to data (during propagation)
   - More parameters to control precision (in initialisation file and code): EPS_INTEGR (integrations), EPS_ITER (iterations), EPS_NORMDATA (normalise data)
   - USINE 80's logo added and simplification of usine_run.cc

2017/10
   - Minor fix in parameter file
   - replace all char[]/sprintf by string/Form() because of random crashes with long names...

2017/08
   - Contribution to :math:`\chi^2` of covariance matrix re-implemented (to enable item below)
   - Any error systematics on data enabled as a nuisance parameter (model bias)

2017/07
   - More options for XS ranking D0, D1, D2A, and D2B (multi-file enabled, outputs plots
   - Annoying memory leak fixed (especially for D options)
   - git name USINE-DEV changed to USINE
   - Initialisation file format: new syntax '#'→'@' and '//'→'#' (to be consistent with other input files, i.e. '#' indicates a comment)
   - Renamed inputs/XSEC\_\* into inputs/XS\_\*, and added inputs/XS_NUCLEI/GHOST_SEPARATELY (XS files added by Y. Génolini from GALPROP)
   - Memory leak in TUXSections fixed
   - Re-factoring/modifications of option D0 and D1

2017/05
   - XS parameters added as NUISANCE for list of reactions (norm)
   - New parameters in initialisation file: 'IsMINOS' (to evaluate correctly errors after minimisation), UsineRun@Display@QtiesExpsEType and ErrType (kERRSYST, kERRSTAT, kERRTOT, kERRCOV:DIR) for display. For option kERRCOV:DIR, seek for covariance matrix in DIR and add in chi2

2017/03
   - Extra plots (for dominant production reactions) with "./bin/usine_run -t" (options E1 to E9)
   - TUQueries renamed to TUMisc, TUGlobalEnum to TUEnum
   - New code parameters IsDecayFedBETA and IsDecayFedEC (useful to run comparisons plots)
   - In Bessel re-summation (2D model), Bringmann's trick to accelerate convergence implemented
   - TUPhysics: physics and astrophysics values/conversion updated to PDG 2016
   - Documentation: typos in formulas corrected (missing 2H factors, etc.)
   - Now use user-range information for NUISANCE parameters (if parameter out of this range, chi2 returns a large number)
   - Model calculation on data E bin instead of Emean (parameter 'IsUseBinRange' and 'NExtraInBinRange' with power-law on each sub-bin assumed)
   - New parameter for boundary conditions on energy numerical inversion, separately for NUC, ANTINUC, and LEPTON
   - Bug fixed in in chi2 for multiple exp/qty fits (printing error)
   - Unused inline functions removed, code cleaned with 'cppcheck --enable=all include/TU*.h src/TU*.cc', updated FindROOT.cmake and CMakeLists.txt updated


2017/02
   - Source parameters enabled as free parameters
   - Fit parameter 'X' enabled as 'X' or 'log10(X)' with the keywords LIN and LOG
   - Enabled fit parameters either as FIT or NUISANCE
   - Take correctly into account asymmetric error bars
   - Fit-able parameters listed with option -m1
   - Normalisation of source abundances to data on/off enabled
   - Bug fixed related to source abundances