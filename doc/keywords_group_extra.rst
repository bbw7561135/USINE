.. _rst_keywords_group_extra:

Group = ``Extra``
^^^^^^^^^^^^^^^^^

The syntax of the associated parameters is detailed in :numref:`rst_syntax_extra`.

.. _tab_extra:

.. table:: Extra (not required in propagation run).

   ===========================  ======================================
   Group\@subgroup              Description
   ===========================  ======================================
   *Extra\@ISFlux*              Interstellar flux description
   ===========================  ======================================