.. _rst_syntax_base_mediumcompo:

Base\@MediumCompo
~~~~~~~~~~~~~~~~~

Choice of the ISM target elements assumed for the propagation.

=====================================  ============================
Group\@subgroup\@parameter             Description
=====================================  ============================
*Base\@MediumCompo\@Targets* (M=0)     Comma-separated target elements in ISM, e.g. ``H,He``
=====================================  ============================

.. note:: Any element list is possible, but keep in mind that the production cross sections on these elements must then be provided (more on the format in :ref:`rst_input_xs_data`).