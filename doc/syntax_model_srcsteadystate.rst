.. _rst_syntax_model_srcsteadystate:

Model...\@SrcSteadyState
~~~~~~~~~~~~~~~~~~~~~~~~

Steady-state source description (CR content, spectra, and spatial distributions) based on templates for the spectra and spatial distributions (see :ref:`rst_keywords_group_templates`).

.. note:: All parameters of the templates used for the source spectrum and spatial distribution are enabled as free parameters (see :ref:`rst_tutorial_update_fitpars`), with one free parameter per CR or a single universal free parameter for all CRs (depending on *SpectValsPerCR* and *SpatialValsPerCR* values).

.. tabularcolumns:: |p{7.5cm}|p{8.1cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*Model...\@SrcSteadyState\@Species* (M=1)", "**SrcID|List** provides list of CR species present in steady-state source **SrcID**, with  SrcID=ASTRO_xxx or DM_xxx (xxx is any string), e.g. ``ASTRO_STD|ALL``. Allowed options for **List** are ``ALL`` (use all CRs from parameter *ListOfCRs*, but discard pure secondaries) or comma-separated-list (e.g., ``1H,4He,12C,14N,16O``)"
   "*Model...\@SrcSteadyState\@SpectTempl* (M=1)", "**SrcID|X|Y** with X chosen among available spectrum templates (see :ref:`rst_keywords_group_templates`), and Y the parameter name (in the template) corresponding to the normalisation variable in the template source spectrum **X**"
   "*Model...\@SrcSteadyState\@SpectValsPerCR*  (M=1)", "**SrcID|X** with X = ``-`` (if template is a file) or  **par1[Y1];par2[Y2];par3[Y2]** (if template is a formula [1]_) with as many semi-column separated entries as the number of parameters in the template, e.g. ``ASTRO_STD|q[PERCR:DEFAULT=1.e-5];`` ``alpha[PERZ:DEFAULT=2.3e,H=2.1,He=2.2];`` ``eta_s[SHARED:-1.]``. For the latter, the following keywords and formats can be used:

     | **Y=SHARED:val** → universal parameter for all CRs
     | **Y=PERCR:DEFAULT=val0,CR1=val1,CR2=val2...** → one parameter per CR
     | **Y=PERZ:DEFAULT=val0,Z1=val1,Z2=val2...** → one parameter per element
     | **Y=LIST:DEFAULT=val0,1H_H_HE=val1,C_N_O=val2...** → one parameter per list (*DEFAULT* is taken to be the list of all remaining CRs not in user-defined lists)"
   "*Model...\@SrcSteadyState\@SpatialTempl* (M=1)", "**SrcID|X** with X chosen among available spatial distribution templates (see :ref:`rst_keywords_group_templates`)."
   "*Model...\@SrcSteadyState\@SpatialValsPerCR* (M=1)", "**SrcID|X** with X =  **par1[Y1];par2[Y2];par3[Y2]** if template is a formula [1]_ (or ``-`` if template is a file)), with as many semi-column separated entries as the number of parameters in the template, e.g. ``ASTRO_STD|rsol[SHARED:8.5];a[SHARED:1.];b[SHARED:1.];dex[SHARED:1.]``. For the latter, the following keywords and formats can be used:

     | **Y=SHARED:val** → universal parameter for all CRs
     | **Y=PERCR:DEFAULT=val0,CR1=val1,CR2=val2...** → one parameter per CR
     | **Y=PERZ:DEFAULT=val0,Z1=val1,Z2=val2...** → one parameter per element
     | **Y=LIST:DEFAULT=val0,1H_H_HE=val1,C_N_O=val2...** → one parameter per list (*DEFAULT* is taken to be the list of all remaining CRs not in user-defined lists)"

**Footnote**

.. [1] The syntaxt for formula is based on `fparser library <http://warp.povusers.org/FunctionParser>`_, see the documentation for the `formulae syntax <http://warp.povusers.org/FunctionParser/fparser.html>`_