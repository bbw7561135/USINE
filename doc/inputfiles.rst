.. _rst_inputfiles:

USINE input files
=================

`USINE` input files all are *ASCII* files located in ``$USINE/inputs/``. Their format is described in their header. More information on the different types of input files are given below.

.. toctree::
   :maxdepth: 1

   input_initfile
   input_atomprop
   input_cr_charts
   input_cr_data
   input_cr_data_cov
   input_xs_data
   input_src_abund

.. note::
   Users can feed to `USINE` their own files provided they are correctly formatted. These files can be anywhere, as long as their path (absolute or relative to ``$USINE``) is provided in the initialisation file (see :ref:`rst_initfile`)