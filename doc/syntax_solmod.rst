.. _rst_syntax_solmod:

*SolMod...* params
--------------------------------------------------------------------------

The group *SolMod...* and its subgroups are described in :numref:`tab_solmod`. In this release, only the Force-Field (FF) approximation is provided.

.. tabularcolumns:: |p{6.cm}|p{9.cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*SolMod0DFF\@Base\@ParNames (M=0)*", "Comma-separated list of parameters for model, for instance ``phi`` for FF. Fit-able parameters are internally built from these names and CR experiment data taking period (see :ref:`rst_tutorial_update_fitpars`)."
   "*SolMod0DFF\@Base\@ParUnits (M=0)*", "Associated list of units, e.g. ``GV`` for FF."
   "*SolMod0DFF\@Base\@ParVals (M=0)*", "Default value for parameter."