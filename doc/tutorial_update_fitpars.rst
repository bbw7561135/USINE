.. _rst_tutorial_update_fitpars:

Fit parameters and minimizer
----------------------------

- **Change minimiser setup**: The default minimiser used is minuit2. See :numref:`rst_syntax_usinefit_config` for details::

   UsineFit  @ Config   @ Minimiser        @ M=0 @ Minuit2
   UsineFit  @ Config   @ Algorithm        @ M=0 @ combined
   UsineFit  @ Config   @ NMaxCall         @ M=0 @ 100000
   UsineFit  @ Config   @ Tolerance        @ M=0 @ 1.e-2
   ...


- **TOA CR data to use**: data to minimise are picked from the list of data loaded (see :numref:`rst_syntax_base_crdata`). Their selection (and the errors to use) is detailed in :numref:`rst_syntax_usinefit_toadata`::

   UsineFit  @ TOAData  @ QtiesExpsEType   @ M=0 @ B/C:AMS:KR
   UsineFit  @ TOAData  @ ErrType          @ M=0 @ kERRCOV:$USINE/inputs/CRDATA_COVARIANCE/
   UsineFit  @ TOAData  @ EminData         @ M=0 @ 1.e-5
   UsineFit  @ TOAData  @ EmaxData         @ M=0 @ 200.
   ...


- **Free and nuisance parameters**: The whole business of nuisance and free parameters, as well as the list and syntax of these parameters are given in :numref:`rst_syntax_usinefit_freepars`::

   UsineFit  @ FreePars @ CRs              @ M=1 @ HalfLifeBETA_10Be:NUISANCE,LIN,[1.3,1.5],1.387,0.012
   UsineFit  @ FreePars @ DataErr          @ M=1 @ SCALE_AMS02_201105201605__BC:NUISANCE,LIN,[-10,10],0.,1.
   UsineFit  @ FreePars @ Geometry         @ M=1 @ -
   UsineFit  @ FreePars @ ISM              @ M=1 @ -
   UsineFit  @ FreePars @ Modulation       @ M=1 @ phi_AMS02_201105201605_:NUISANCE,LIN,[0.3,,1.1],0.73,0.2
   UsineFit  @ FreePars @ SrcPointLike     @ M=1 @ -
   UsineFit  @ FreePars @ SrcSteadyState   @ M=1 @ alpha:NUISANCE,LIN,[1.7.,2.5],2.3,0.1
   UsineFit  @ FreePars @ Transport        @ M=1 @ delta:FIT,LIN,[0.2,0.9],0.6,0.02
   UsineFit  @ FreePars @ Transport        @ M=1 @ Va:FIT,LIN,[1.,120],70.,0.1
   UsineFit  @ FreePars @ XSection         @ M=1 @ Norm_16O+H:NUISANCE,LIN,[0.7,1.3],1.,0.05
   UsineFit  @ FreePars @ XSection         @ M=1 @ EAxisScale_16O+H->11B:NUISANCE,LIN,[0.5,1.5],1.,0.5
   UsineFit  @ FreePars @ XSection         @ M=1 @ EThresh_16O+H->11B:NUISANCE,LIN,[0.5,2.],1.,0.5
   UsineFit  @ FreePars @ XSection         @ M=1 @ SlopeLE_16O+H->11B:NUISANCE,LIN,[-1,1.],0.,0.5
   UsineFit  @ FreePars @ XSection         @ M=1 @ Norm_12C+H->11B:NUISANCE,LIN,[0.5,2.],1.,0.2
   UsineFit  @ FreePars @ XSection         @ M=1 @ EAxisScale_12C+H->11B:NUISANCE,LIN,[0.5,1.5],1.,0.5
   UsineFit  @ FreePars @ XSection         @ M=1 @ EThresh_12C+H->11B:NUISANCE,LIN,[0.5,2.],1.,0.5
   UsineFit  @ FreePars @ XSection         @ M=1 @ SlopeLE_12C+H->11B:NUISANCE,LIN,[-1,1.],0.,0.5
   ...
