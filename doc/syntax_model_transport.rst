.. _rst_syntax_model_transport:

Model...\@Transport
~~~~~~~~~~~~~~~~~~~

Generic description of the transport parameters: diffusion tensor, wind vector, reacceleration.

.. note:: All parameters in *ParNames* can be involved in any transport parameters and are enabled as free parameters (see :ref:`rst_tutorial_update_fitpars`).

.. tabularcolumns:: |p{7.cm}|p{8.5cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*Model...\@Transport\@ParNames* (M=0)", "See :numref:`tab_parnames` (e.g. ``Va,Vc,K0,delta,eta_t``)"
   "*Model...\@Transport\@ParUnits* (M=0)", "See :numref:`tab_parnames` (e.g., ``km/s,km/s,kpc^2/Myr,-,-``)"
   "*Model...\@Transport\@ParVals* (M=0)", "See :numref:`tab_parnames` (e.g., ``48.9,12.,0.0112,0.7,1.``)"
   "*Model...\@Transport\@Wind* (M=1)", "**Wi:FNCTYPE|X** with i=0,1,2 and X=f(t,x,y,z; ParNames) if FNCTYPE=FORMULA [1]_, or X=file_name if FNCTYPE=GRID. Wind velocity [km/s], up to 3 components (along x, y, and z axes of the model geometry), e.g. ``W1:FORMULA|Vc``"
   "*Model...\@Transport\@VA* (M=0)", "**FNCTYPE|X** with X=f(t,x,y,z; ParNames) if FNCTYPE=FORMULA [1]_, or X=file_name if FNCTYPE=GRID. Alfvén velocity [km/s] for reacceleration, e.g. ``FORMULA|Va``"
   "*Model...\@Transport\@K* (M=1)", "**Kij:FNCTYPE|X** with i=0,1,2 and X=f(beta,gamma,p,Rig,Ek,Ekn,Etot; t,x,y,z; ParNames; ParNames) if FNCTYPE=FORMULA [1]_, or X=file_name if FNCTYPE=GRID. Spatial diffusion tensor [kpc^2/Myr], up to 9 components (along axes of the model geometry), e.g. ``K00:FORMULA|beta^eta_t*K0*Rig^delta``"
   "*Model...\@Transport\@Kpp* (M=0)", "**FNCTYPE|X** with X=f(t,x,y,z; ParNames) if FNCTYPE=FORMULA [1]_, or X=file_name if FNCTYPE=GRID. Momentum diffusion scalar [GeV^2/Myr] for reacceleration, e.g. ``FORMULA|(4./3.)*(Va*1.022712e-3*beta*Etot)^2/`` ``(delta*(4-delta^2)*(4-delta)*K00)``. Note that any component of the spatial diffusion tensor (e.g., K00) can be called in Kpp."


... and specific to ``Model2DKisoVc``

.. tabularcolumns:: |p{6.2cm}|p{9.4cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*Model2DKisoVc\@Base\@NBessel* (M=0)", "# of Bessel coeffs to expand/sum over Bessel functions (e.g. ``9``)"
   "*Model2DKisoVc\@Base\@NIntegrR* (M=0)", "# of steps (radial grid) to integrate Bessel coeffs (e.g. ``1000``)"
   "*Model2DKisoVc\@Base\@NIntegrZ* (M=0)", "# of steps (z-grid) integrations if exotic source in halo (e.g. ``200``)"

**Footnote**

.. [1] The syntaxt for formula is based on `fparser library <http://warp.povusers.org/FunctionParser>`_, see the documentation for the `formulae syntax <http://warp.povusers.org/FunctionParser/fparser.html>`_