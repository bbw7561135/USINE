.. _rst_input_atomprop:

Atomic properties
--------------------------------------------------------------------------

The *ASCII* file `$USINE/inputs/atomic_properties.dat <../../../inputs/atomic_properties.dat>`_ gathers atomic properties for elements Z=1-109 (encompassing all CR elements). GCR propagation may depend on these properties for:

   - **acceleration bias:** starting from Solar System abundances, a bias in source abundances could be related to a First Ionization Potential (FIP) bias or more likely to a volatility bias;
   - **Electronic-Capture (EC) decay during propagation:** decay via EC (for EC-unstable CRs) depends on the electron attachment and stripping cross sections, which depend on K-sell ionisation energies.

   .. note::
      If you ever need to add another *atomic property*, add a new column in the file and modify ``TUAtomProperties.h`` and ``TUAtomProperties.cc`` accordingly to read the new format and handle the new variables.


The file is formatted as shown below:

.. literalinclude:: ../inputs/atomic_properties.dat
   :language: c
   :lines: 1-25