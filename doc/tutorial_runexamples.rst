.. _rst_tutorial_runexamples:

Run examples
--------------------------------------------------------------------------

.. toctree::
   :maxdepth: 1

   tutorial_runexamples_l
   tutorial_runexamples_e
   tutorial_runexamples_m
   tutorial_runexamples_u

.. warning::
   Never modify `init.TEST.par <../../../inputs/init.TEST.par>`_, work on a copy instead! Any name and directory to put this copy file is fine. Note that For the 1D model, you can directly use the file `init.Model1D.par <../../../inputs/init.Model1D.par>`_.