.. _rst_tutorial_runexamples_l:

Simple run: ``./bin/usine -l``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Runs and calculates local fluxes/ratios/etc. for several modulation levels, with results stored in the user-selected output directory. The command

   ``./bin/usine -l inputs/init.TEST.par $USINE/output "B/C,B,C,O:KEKN:0.,1.;B/C:kR:0.79" 2.8 1 1 1``

produces many ASCII files:

   - output log file (details of run steps): *$USINE/output/usine.last_run.log*,
   - initialisation file for last run: *$USINE/output/usine.last_run.init.par*,
   - propagated fluxes/ratios: *$USINE/output/local_fluxes???.out* (??? model/qty specific),
   - plots and ROOT macros (.C), pdf and png for these plots.


.. figure:: DocImages/option_l/local_fluxes_BC_R.png
   :width: 65%
   :align: center

   B/C as a function of R at selected :math:`\phi_{\rm FF}`, along with selected CR data.

.. figure:: DocImages/option_l/local_fluxes_C_Ekn.png
   :width: 65%
   :align: center

   C multiplied by :math:`E_{k/n}^{2.8}` as a function of Ekn at selected :math:`\phi_{\rm FF}`, along with selected CR data.

.. figure:: DocImages/option_l/local_fluxes_isotopicfraction.png
   :width: 65%
   :align: center

   Isotopic fractions (form IS fluxes) as a function of Ekn for all isotopes involved in the selection.

.. figure:: DocImages/option_l/local_fluxes_primarycontent.png
   :width: 65%
   :align: center

   Primary (left axis) or secondary (right axis = 100 - primary) content of IS fluxes as a function of Ekn for all quantities involved in the selection.