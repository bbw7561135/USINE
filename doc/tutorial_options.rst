.. _rst_tutorial_options:

Options and arguments
--------------------------------------------------------------------------

If you want to know the available options and arguments, just type ``./bin/usine``, whose output is self-explanatory::

                    (V3.x, git_ID=...)
   USAGE:
     ./bin/usine -l         (L)ocal flux calculation, show only selected quantities
     ./bin/usine -m         (M)inimisation-related
                     -m1       [List of fit-able parameters]
                     -m2       [Chi2 minimisation on TOA fluxes (best chi2)]
    ./bin/usine -u         (U)ncertainty propagation (generates multiple fluxes and ratios)
                     -u1       [Display sample of parameters drawn best-fit/covariance matrix]
                     -u2       [CLs from USINE-formatted best-fit and covariance matrix files]


     ./bin/usine -e         (E)xtras and comparisons (interactive run)
                     -e        [List/detail of all options]
                     -ea       [Local fluxes and spatial dependences]
                     -eb       [Text-interface to modify model params (re-run any -e option)]
                     -ec       [Infos on current models and their parameters]
                     -ed       [Extra calculations/checks: XS-related, decay-related, diff. prod.]
                     -ee       [Comparison plots varying boundary conditions, params, XS files...
     ./bin/usine -i         (I)nput files/properties
                     -i        [List all options]
                     -id       [List options related to CR data input files]
                     -ie       [List options related to interaction]
                     -ix       [List options related to cross sections input files]
     ./bin/usine -t         (T)est functions for USINE ('-t' to see all test options)
                     -t        [List all test functions available]
                     -tALL     [TEST all USINE classes, propagation, and minimisation]
                     -tUSINE   [RUN -tALL against reference test files (in usine.tests/)]


.. note:: Each option comes with a mandatory list of arguments and their explanation: **USAGE** gives the arguments; **EXAMPLE** gives default values for all arguments (you can run this line directly).