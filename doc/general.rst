.. _rst_general:

General information
===================

Physics goal and ingredients
--------------------------------------------------------------------------

USINE is a library for Galactic cosmic-ray (GCR) propagation models, valid in the energy range :math:`\sim[10^{-2}-10^6]` GeV/n. The library contains several semi-analytical propagation models previously used in the literature (`Leaky-Box <http://adsabs.harvard.edu/abs/2009A%26A...497..991P>`_ model, disc+halo `1D <http://adsabs.harvard.edu/abs/2010A%26A...516A..66P>`_ and `2D <http://adsabs.harvard.edu/abs/2001ApJ...555..585M>`_ diffusion models) as well as several solar modulation models (e.g., `Force-Field <http://adsabs.harvard.edu/abs/1968ApJ...154.1011G,1987A%26A...184..119P,1992ApJ...397..153P>`_). Even though USINE is meant to evolve to describe all CR species, models in this version are restricted to nuclei and anti-nuclei.

Galactic cosmic-ray propagation requires many inputs, which are handled by as many classes in USINE, see :ref:`rst_code`.

:CR charts: Element properties and nuclear charts specific to CRs (i.e. stable or half-life of the order of the propagation time)
:CR data: List of data points (energy, value, date, name of experiment, etc.)
:Cross sections: Energy-dependent inelastic and production (straight-ahead or differential) cross sections on ISM elements
:CR sources: Source spectra and spatial distribution of CR sources
:ISM: Composition of the InterStellar Medium (elements, density, temperature, ionisation fraction)
:CR transport: Coefficients for momentum and spatial diffusion, convection, reacceleration, energy losses
:Models: CR propagation models to go from sources to interstellar fluxes
:Modulation: Solar modulation models to propagate interstellar (IS) to top-of-atmosphere (TOA) fluxes


USINE versions & release notes
--------------------------------------------------------------------------

The USINE code is developed in C++ to benefit from object-oriented capabilities (overload, inheritance, etc.). It is interfaced with the `ROOT CERN library <https://root.cern.ch/>`_ for displays and minimisation routines, and with `fparser <http://warp.povusers.org/FunctionParser/fparser.html>`_ to implement formulae with constants and parameters as simple text format.

   .. note:: The last public release is v3.5 -- see :ref:`rst_general_release3.5`. The instructions to retrieve and install USINE are in :ref:`rst_retrieve_usine`.


**Public (released) versions**
   - :ref:`rst_general_release3.5` -- Version for B/C & pbar analyses *(D. Maurin)*
   - :ref:`rst_general_release3.4` -- First public release *(D. Maurin)*


**Future release**
   - :ref:`rst_general_release4.0` -- ... *(M. Boudaud,, D. Maurin, N. Weinrich)*

**Previous (un-released) versions**
   - v3.3 [2017/03] -- On gitlab.in2p3, tutorial, more fit params *(D. Maurin; contrib. from CRAC)*
   - v3.2 [2017/01] -- I/O (.root, .png, .txt) simplified, simple code testing *(D. Maurin)*
   - v3.1 [2015/12] -- Spline enabled *(implemented by A. Ghelfi, L. Derome)*
   - v3.0 [2013/12] -- Better C++/ROOT with FunctionParser and documentation *(D. Maurin)*
   - v2.0 [2004/10] -- C++/ROOT version on svn *(D. Maurin; contrib. from A. Putze & R. Taillet)*
   - v1.0 [1999/10] -- C version *(D. Maurin; contributions from F. Donato, P. Salati, R. Taillet)*


.. _rst_publications:

Publications and files
--------------------------------------------------------------------------

**With v3.5**
   - Boudaud, Génolini et al., `submitted to PRL <https://ui.adsabs.harvard.edu/abs/2019arXiv190607119B>`__
   - Génolini, Boudaud et al., `to appear in PRD <https://ui.adsabs.harvard.edu/abs/2019arXiv190408917G>`__  → *initialisation files, best-fit, and covariance matrix of best-fit parameters (from AMS-02 B/C analysis) for models BIG, SLIM, and QUAINT available* `here! <DocFiles/2019-Génolini-benchmarks-BC.tar.gz>`__
   - Derome, Maurin, Salati, Boudaud, Génolini, and Kunzé, `to appear in A\&A <https://ui.adsabs.harvard.edu/abs/2019arXiv190408210D>`__

**With v3.4**
   - Génolini, Maurin, Moskalenko, and Unger, `PRC 98, 4611 (2018) <https://ui.adsabs.harvard.edu/abs/2018PhRvC..98c4611G>`__
   - Génolini et al., `PRL 119, 241101 (2017) <https://ui.adsabs.harvard.edu/abs/2017PhRvL.119x1101G/abstract>`__


**With v2.0**
   - Coste, Derome, Maurin, and Putze, `A&A 539, 88 (2012) <http://adsabs.harvard.edu/abs/2012A&A...539A..88C>`__
   - Putze, Maurin, and Donato, `A&A 526, 101 (2011) <http://adsabs.harvard.edu/abs/2011A&A...526A.101P>`__
   - Maurin, Putze, and Derome, `A&A 516, 67 (2010) <http://adsabs.harvard.edu/abs/2010A&A...516A..67M>`__
   - Putze, Derome, Maurin, Perotto, and Taillet, `A&A 497, 991 (2009) <http://adsabs.harvard.edu/abs/2009A&A...497..991P>`__
   - Putze, Derome, and Maurin, `A&A 516, 66 (2010) <http://adsabs.harvard.edu/abs/2010A&A...516A..66P>`__
   - Donato, Maurin, Brun, Delahaye, and Salati, `PRL 102, 071301 (2009) <http://adsabs.harvard.edu/abs/2009PhRvL.102g1301D>`__
   - Donato, Fornengo, and Maurin, `PRD 78, 043506 (2008) <http://adsabs.harvard.edu/abs/2008PhRvD..78d3506D>`__

**With v1.0**
   - Maurin, Cassé, and Vangioni-Flam, `APh 18, 471 (2003) <http://adsabs.harvard.edu/abs/2003APh....18..471M>`__
   - Maurin, Taillet, and Donato, `A&A 394, 1039 (2002) <http://adsabs.harvard.edu/abs/2002A&A...394.1039M>`__
   - Donato, Maurin, and Taillet, `A&A 381, 539 (2002) <http://adsabs.harvard.edu/abs/2002A&A...381..539D>`__
   - Maurin, Donato, Taillet, and Salati, `ApJ 555, 585 (2001) <http://adsabs.harvard.edu/abs/2001ApJ...555..585M>`__





Contact
--------------------------------------------------------------------------

For any question, please contact `David Maurin <dmaurin@lpsc.in2p3.fr>`__ (LPSC).
