.. _rst_tutorial_update:

Update parameters
--------------------------------------------------------------------------

   .. warning::
      Never modify `init.TEST.par <../../../inputs/init.TEST.par>`_, work on a copy instead! Any name and directory to put this copy file is fine. Note that For the 1D model, you can directly use the file `init.Model1D.par <../../../inputs/init.Model1D.par>`_.

.. toctree::
   :maxdepth: 1

   tutorial_update_config
   tutorial_update_models
   tutorial_update_runpars
   tutorial_update_fitpars