.. _rst_syntax_usinerun_models:

UsineRun\@Models
~~~~~~~~~~~~~~~~

Propagation and Solar modulation models to use in run.

.. tabularcolumns:: |p{7.cm}|p{8.cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*UsineRun\@Models\@Propagation* (M=1)", "Propagation model chosen for run (see enabled models in :ref:`rst_syntax_model`). Several models can be selected in standard runs (not extensively tested), but not in minimisations."
   "*UsineRun\@Models\@SolarModulation* (M=1)", "Solar modulation model chosen for run (see enabled models in :ref:`rst_syntax_solmod`). Several models can be selected in standard runs (not extensively tested), but not in minimisations."