.. _rst_install:

Installation and tests
======================

Mandatory packages
--------------------------------------------------------------------------

**ROOT/CERN package** (requires :math:`\geq 6.00`)

The easiest way, if available, is to install directly all relevant ROOT packages (using *apt-get*, *dnf*, or *brew*, etc.); do not forget the development (``-devel``) packages and specific ROOT libraries (``MathMore``, ``Minuit2``). Alternatively, you may want to compile locally ROOT. To do so, `download a version <https://root.cern.ch/releases>`_ and follows the configuration/compilation instructions to `build ROOT <https://root.cern.ch/building-root>`_. In addition, in the latter case:

         | **Please define (e.g., in your *~/.bashrc*) the ROOT environment variables**
         |    ``export ROOTSYS=`` path_to_local_installation
         |    ``export ROOTLIBS=$ROOTSYS/lib``
         |    ``export PATH=$PATH:$ROOTSYS/bin``
         |    ``export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib``
         |    ``export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$ROOTSYS/lib`` *[Mac OS only]*
         |    ``export MANPATH=$MANPATH:$ROOTSYS/man``

   .. note:: For Mac OSX, we recommend to install ROOT via `homebrew <http://brew.sh/>`_. To define the environment variable ``ROOTSYS``,  you need to source, e.g., in your configuration file ``~/.bashrc``, ``thisroot.sh``/``thisroot.csh``. To know where it is, type, e.g. for ROOT 6, ``brew info root6``.


.. _rst_retrieve_usine:

USINE download/install
--------------------------------------------------------------------------


   1. Clone from git repository:

      |    ``> git clone https://gitlab.com/dmaurin/USINE.git``

      or see the `GitLab repository <https://gitlab.com/dmaurin/USINE>`__ for alternative download forms. Once USINE is cloned, you can always choose a specific version (e.g. ``git checkout tags/V3.4``), but we recommend that you always use the latest (which is the one cloned).

   2. The compilation relies on `cmake <https://cmake.org/>`_ (file `CmakeLists.txt <../../../CMakeLists.txt>`_):

      |    ``> cd USINE``
      |    ``> mkdir build; cd build; cmake ../`` *(to have build files in separate directory)*
      |    ``> make -jN``  *[using N=2, 3,... cores]*

   3. Define the USINE environment variables (e.g., in your *~/.bashrc*):

      |    ``export USINE=`` absolute_path_to_local_installation
      |    ``export PATH=$PATH:"$USINE/bin"``
      |    ``export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$USINE/lib``
      |    ``export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$USINE/lib``  *# [Mac OS only]*

.. note:: Do not forget to ```> source ~/.bashrc``` to ensure that the environment variables are set in your current xterm (```$echo $USINE``` should point to the directory where you installed USINE).

      .. note:: This version was successfully installed and tested on:

            - Ubuntu 17.10 (gcc 7.2.0)
            - MacOS X High Sierra 10.13.4 (clang-900.0.39.2)
            - Fedora 25 (gcc 6.4.1)


Tests in USINE
--------------------------------------------------------------------------

Testing is one of the most important step of code development, to ensure that

   - the production version (i.e. user compiled version) behaves as expected,
   - modifications of the code by developers do not break any functionality.


**Is my installation successful?**

To ensure that `USINE` is ready to use, type

   | ``> cd $USINE``
   | ``> ./bin/usine -tUSINE``

If all tests pass, then you are good to go (jump to :ref:`rst_tutorial`)! If not, check that some tests are indeed performed (i.e. there is some chatter when you run the test), and then: (i) if no chatter, it probably has to do with your installation (missing environment variable, missing ROOT package…); (ii) if you are told that some checks failed, it may be related to your machine architecture/compiler, so contact the USINE developers (send your `.fail` test files) who will evaluate the severity of the problem.

   .. note:: On Mac OS systems, we observe that the last digit differs from the one in the reference file ((in many test files). This is likely due to the different architecture of systems but you can still consider that the tests passed!


**What tests are performed?**

The realm of testing is large. The tests implemented in USINE are a mixture of *unit tests* and *integration tests*. We also performed *static* and *dynamic* code analysis.

:Unit tests from ``usine -tRef`` command line:  *Unit tests* are narrow in scope and allow to check that pieces of code are doing what they are intended to. To build *unit tests*, each USINE class and namespace (see :ref:`rst_code`) contains a ``TEST()`` method calling all other methods (strictly speaking, these are not unit tests). Its output is validated during the code development and saved in a reference test file (`$USINE/usine.test/ <../../../usine.tests/>`_). New installations and/or code modifications are checked by comparing the new output against the reference (using ```kdiff```). Note that these tests can also be useful to see how the various methods are called and what they do.

:Integration tests from ``usine -tRef`` command line:  The purpose of *integration tests* is to demonstrate that different pieces of the system work together. The *system* here is the list of USINE classes performing Galactic propagation. USINE integration tests cover all run options. Analogous to the implementation of *unit tests*, the expected output of propagation runs are stored in reference files (`$USINE/usine.test/ <../../../usine.tests/>`_) and compared against the output of modified code versions. These tests encompass outputs of propagated flux calculation for all USINE models, minimisation runs, etc.

.. note:: Git repositories usually come with `Continuous Integration <https://en.wikipedia.org/wiki/Continuous_integration>`_ (CI) tools. We may rely on CI for the code in the future (the code documentation already does, to ensure recompilation for any edit).

:Static and dynamic code analysis: The difference between the two analyses is that one does not need to run the code, while the other does. *USINE* was analysed with the *static analysis* tool `cppcheck <http://cppcheck.sourceforge.net/>`_ . We also used `valgrind <http://valgrind.org/>`_ to perform *dynamic analyses*, in particular to track and fix memory leaks (run with ``$ROOTSYS/etc/valgrind-root.supp`` to track and hide false positive in ROOT!).

:Profiling and optimisation: So far, we have not used any profiler to check which pieces of code should deserve extra scrutiny for speed. This will be examined for the second release.


USINE files and directories
--------------------------------------------------------------------------

   - **Files in** ``$USINE/``

+----------------------------------------------+--------------------------------------------------------------------------------------------------------+
| `CmakeLists.txt <../../../CMakeLists.txt>`_  | File used by cmake for compilation                                                                     |
+----------------------------------------------+--------------------------------------------------------------------------------------------------------+
| FindROOT.cmake                               | Called by CmakeLists.txt to find path to ROOT installation                                             |
+----------------------------------------------+--------------------------------------------------------------------------------------------------------+
| `README.md <../../../README.md>`_            | README file (for gitlab website)                                                                       |
+----------------------------------------------+--------------------------------------------------------------------------------------------------------+

   - **Directories in** ``$USINE/``

.. <http://warp.povusers.org/FunctionParser/fparser.html>`__

+------------------+---------------------------------------------------------+
| doc/             | This documentation, see `(Re)generate the doc`_         |
+------------------+---------------------------------------------------------+
| usine.MACROS/    | Example of ROOT macros with USINE                       |
+------------------+---------------------------------------------------------+
| usine.tests/     | Reference test files, see `Tests in USINE`_             |
+------------------+---------------------------------------------------------+
| include/TU*.h    | USINE header and source files, see :ref:`rst_code`      |
| src/TU*.cc       |                                                         |
+------------------+---------------------------------------------------------+
| FunctionParser/  | Third-party library to handle generic formulae in USINE |
+------------------+---------------------------------------------------------+
| inputs/          | For more details, see :ref:`rst_inputfiles`             |
|                  |                                                         |
| - atomic_*       |  - Atomic properties (e.g., FIP, K-ion shell…)          |
| - crcharts_*     |  - Nuclear charts for propagated CRs                    |
| - crdata_*       |  - CR data (energy, flux/ratio, uncertainties…)         |
| - init.*.par     |  - Initialisation file (for transport, sources, ISM…)   |
| - qi_*           |  - Generated/used for 2D model (Bessel coeff.)          |
| - src_abund*     |  - Solar System isotopic and elemental) abundances      |
| - XS_*/          |  - Cross sections for (anti-)nuclei and leptons         |
+------------------+---------------------------------------------------------+
|                  | **…and created at compilation time**                    |
+------------------+---------------------------------------------------------+
| bin/usine        | USINE executable, see :ref:`rst_tutorial`               |
+------------------+---------------------------------------------------------+
| lib/lib*         | USINE and function parser libraries                     |
+------------------+---------------------------------------------------------+



.. _rst_generatedoc:

(Re)generate the doc
--------------------------------------------------------------------------

:General documentation:
   | The documentation you are reading is built with `Sphinx <http://sphinx-doc.org/>`_ (and `readthedoc <https://readthedocs.org/>`_ theme). If you wish to generate it locally, install the relevant `python` packages, and type
   |     ``> cd $USINE/doc``
   |     ``> make html``
   |     ``> firefox _build/html/index.html``
   |
   | You can create also a pdf (or ps) document with the commands
   |     ``> cd $USINE/doc``
   |     ``> make latex``
   |     ``> cd _build/latex``
   |     ``> make all-pdf``
   |     ``> evince _build/latex/usine.pdf``


:Doxygen documentation:
   | The code documentation can be generated based on the `Doxygen <http://www.stack.nl/~dimitri/doxygen/>`_ package and a configuration file (`Doxyfile`) provided. Just type
   |
   |     ``> cd $USINE``
   |     ``> doxygen doc//DoxyStyle/Doxyfile``
   |     ``> firefox doc/doxygen/doxygen/index.html``