.. USINE documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. role::  raw-html(raw)
    :format: html

.. image  DocImages/logo-usine.png

      ██╗   ██╗███████╗██╗███╗   ██╗███████╗
      ██║   ██║██╔════╝██║████╗  ██║██╔════╝
      ██║   ██║███████╗██║██╔██╗ ██║█████╗
      ██║   ██║╚════██║██║██║╚██╗██║██╔══╝
      ╚██████╔╝███████║██║██║ ╚████║███████╗
       ╚═════╝ ╚══════╝╚═╝╚═╝  ╚═══╝╚══════╝

:raw-html:`<hr>`


USINE documentation
===================


Welcome to USINE, a library with several semi-analytical Galactic cosmic-ray (GCR) propagation models (PDF version of documentation :download:`here <_build/latex/usine.pdf>`).

We hope you will enjoy using USINE whether you want to:

* **learn and know more about CR propagation phenomenology**, taking advantage of the simple command-line interface and graphical pop-ups to quickly see and compare the importance of various ingredients on the resulting fluxes;
* **perform state-of-the art analyses of new CR data**, taking advantage of the very flexible ASCII parameter file to select your model, configuration, etc., to fit your data with any number of free parameter (transport, source, geometry...) and nuisance parameters (cross sections, data systematic uncertainties...);
* **develop and use you own semi-analytical model** without having to spend years setting all inputs and outputs right, taking advantage of the modularity and flexibility of the USINE C++ library.


If you use USINE, please cite `Maurin (2018) <http://arxiv.org/abs/1807.02968>`_

For any question, contact `D. Maurin <dmaurin@lpsc.in2p3.fr>`__ (LPSC).


.. toctree::
   :maxdepth: 1
   :hidden:
   :numbered:

   general
   install
   models
   code
   inputfiles
   syntax
   tutorial
   zelicense
   Doxygen (for developers) <doxygen/index.html#http://>
