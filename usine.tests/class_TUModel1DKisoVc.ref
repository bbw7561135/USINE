
            -------------------------
              TUModel1DKisoVc::TEST
            -------------------------

- Class content: Load 1DKisoVc Model and test.

 * Load class
   > TUModelBase::SetClass(init_pars="inputs/init.TEST.par", Model1DKisoVc, is_verbose=false, f_log=f);
 * Basic prints
   > PrintSummaryBase(f)

             ----------------------
               Nuclei and parents
             ----------------------

     --   0:    2H-BAR (parents:   1H-BAR,1H,4HE)
     --   1:    1H-BAR (parents:       1H,4HE)
     --   2:        1H (parents:      2H -> 30SI)
     --   3:        2H (parents:     3HE -> 30SI)
     --   4:       3HE (parents:     4HE -> 30SI)
     --   5:       4HE (parents:     6LI -> 30SI)
     --   6:       6LI (parents:     7LI -> 30SI)
     --   7:       7LI (parents:     7BE -> 30SI)
     --   8:       7BE (parents:     9BE -> 30SI)
     --   9:       9BE (parents:     10B -> 30SI)
     --  10:       10B (parents:    10BE -> 30SI)
     --  11:      10BE (parents:     11B -> 30SI)
     --  12:       11B (parents:     12C -> 30SI)
     --  13:       12C (parents:     13C -> 30SI)
     --  14:       13C (parents:     14N -> 30SI)
     --  15:       14N (parents:     14C -> 30SI)
     --  16:       14C (parents:     15N -> 30SI)
     --  17:       15N (parents:     16O -> 30SI)
     --  18:       16O (parents:     17O -> 30SI)
     --  19:       17O (parents:     18O -> 30SI)
     --  20:       18O (parents:     19F -> 30SI)
     --  21:       19F (parents:    20NE -> 30SI)
     --  22:      20NE (parents:    21NE -> 30SI)
     --  23:      21NE (parents:    22NE -> 30SI)
     --  24:      22NE (parents:    23NA -> 30SI)
     --  25:      23NA (parents:    24MG -> 30SI)
     --  26:      24MG (parents:    25MG -> 30SI)
     --  27:      25MG (parents:    26MG -> 30SI)
     --  28:      26MG (parents:    26AL -> 30SI)
     --  29:      26AL (parents:    27AL -> 30SI)
     --  30:      27AL (parents:    28SI -> 30SI)
     --  31:      28SI (parents:    29SI -> 30SI)
     --  32:      29SI (only one parent:    30SI)
     --  33:      30SI (primary - no parents)

             -----------------------
               Cosmic Ray energies
             -----------------------

      => Nuclei                  Ekn   [GeV/n]   : 33 bins in [5.000e-02,5.000e+03] (log-step=1.4330e+00)
      => Anti-nuclei             Ekn   [GeV/n]   : 33 bins in [5.000e-02,1.000e+02] (log-step=1.2681e+00)
      => E-dependent variables: beta,gamma,p,Rig,Ek,Ekn,Etot  (7 E-variables)

                   -----------
                     CR Data
                   -----------

   Data read from:
    - $USINE/inputs/crdata_crdb20170523.dat
    - $USINE/inputs/crdata_dummy.dat

   -------------------------------------------
     Data/energy used to normalise CR fluxes
   -------------------------------------------

       Qty       Sub-exps      <Ekn>
         H        PAMELA          2.000000e+01 [GeV/n]
         HE       PAMELA          2.000000e+01 [GeV/n]
         C        HEAO            1.060000e+01 [GeV/n]
         N        HEAO            1.060000e+01 [GeV/n]
         O        HEAO            1.060000e+01 [GeV/n]
         F        HEAO            1.060000e+01 [GeV/n]
         NE       HEAO            1.060000e+01 [GeV/n]
         NA       HEAO            1.060000e+01 [GeV/n]
         MG       HEAO            1.060000e+01 [GeV/n]
         AL       HEAO            1.060000e+01 [GeV/n]
         SI       HEAO            1.060000e+01 [GeV/n]
         P        HEAO            1.060000e+01 [GeV/n]
         S        HEAO            1.060000e+01 [GeV/n]
         CL       HEAO            1.060000e+01 [GeV/n]
         AR       HEAO            1.060000e+01 [GeV/n]
         K        HEAO            1.060000e+01 [GeV/n]
         CA       HEAO            1.060000e+01 [GeV/n]
         SC       HEAO            1.060000e+01 [GeV/n]
         TI       HEAO            1.060000e+01 [GeV/n]
         V        HEAO            1.060000e+01 [GeV/n]
         CR       HEAO            1.060000e+01 [GeV/n]
         MN       HEAO            1.060000e+01 [GeV/n]
         FE       HEAO            1.060000e+01 [GeV/n]
         NI       HEAO            1.060000e+01 [GeV/n]

              --------------------
                Source templates
              --------------------

       - Energy spectrum template: POWERLAW
          [formula] = q*beta^(eta_s)*Rig^(-alpha)
          - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
          - user-defined variables: q,alpha,eta_s
       - Energy spectrum template: TEST
          [formula] = q*beta^(eta_s)*Rig^(-alpha)
          - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
          - user-defined variables: q,alpha,eta_s
       - Spatial distribution template: CST
          [formula] = 1
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: DOOR
          [formula] = if((x>rup),0,if((x<rlo),0,1))
          - base variables: t,x,y,z
          - user-defined variables: rlo,rup
       - Spatial distribution template: STEPUP
          [formula] = if((x>rup),1,0)
          - base variables: t,x,y,z
          - user-defined variables: rup
       - Spatial distribution template: STEPDOWN
          [formula] = if((x<rlo),1,0)
          - base variables: t,x,y,z
          - user-defined variables: rlo
       - Spatial distribution template: CASEBHATTA96
          [formula] = pow(x/8.5,2.)*exp(-3.53*(x-8.5)/8.5)
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: SNRMODIFIED
          [formula] = pow(10.,dex*(x-rsol))*pow((x/rsol),a)*exp(b*(x-rsol)/rsol)
          - base variables: t,x,y,z
          - user-defined variables: rsol,a,b,dex
       - Spatial distribution template: STECKERJONES77
          [formula] = pow(x/10.,0.6)*exp(-3.*(x-10.)/10.)
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: STRONGMOSKA99
          [formula] = pow(x/8.5,0.5)*exp(-1.*(x-8.5)/8.5)
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: EINASTO_CYL
          [formula] = r_sp:=x*x+y*y;rhos*exp((-2/alpha)*(pow((r_sp/rs),alpha)-1))
          - base variables: t,x,y,z
          - user-defined variables: rhos,rs,alpha
       - Spatial distribution template: DUMMY
          [formula] = exp(-(x*x+y*y+z*z/8.))
          - base variables: t,x,y,z
          - user-defined variables: NONE

               ------------------
                 Cross sections
               ------------------

     -- Cross section files used:
        - $USINE/inputs/XS_NUCLEI/sigInelTripathi99+Coste12.dat
        - $USINE/inputs/XS_ANTINUC/sigInelANN_pbar+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/sigInelANN_dbar+HHe_Duperray05.dat
        - $USINE/inputs/XS_NUCLEI/sigProdGALPROP17_OPT12.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_dbar_1H4He+HHe_Duperray05_Coal79MeV.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_dbar_pbar+HHe_Duperray05_Coal79MeV.dat
        - $USINE/inputs/XS_ANTINUC/sigInelNONANN_pbar+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/sigInelNONANN_dbar+HHe_Duperray05.dat
        - $USINE/inputs/XS_ANTINUC/dSdENAR_pbar+HHe_Duperray05_Anderson.dat
        - $USINE/inputs/XS_ANTINUC/dSdENAR_dbar+HHe_Duperray05_Anderson.dat
     -- Targets loaded: H,HE
     -- Tertiaries loaded: 1H-BAR,2H-BAR
   > PrintPars(f)
      => 51 free parameters:
                                                     L = +8.0000e+00 [kpc]           
                                                     h = +1.0000e-01 [kpc]           
                                                 rhole = +2.0380e-01 [kpc]           
                                                    Va = +8.5485e+01 [km/s]          
                                                    Vc = +1.9549e+01 [km/s]          
                                                    K0 = +3.5859e-02 [kpc^2/Myr]     
                                                 delta = +6.1419e-01 [-]             
                                                 eta_t = -5.9730e-02 [-]             
                                                Rbreak = +1.2589e+02 [GV]            
                                            Deltabreak = +2.2815e-01 [-]             
                                                sbreak = +1.0023e-02 [-]             
                                               q_2HBAR = +0.0000e+00 [(/GeV/n/m3/Myr)]
                                               q_1HBAR = +0.0000e+00 [(/GeV/n/m3/Myr)]
                                                  q_1H = +4.0932e+09 [(/GeV/n/m3/Myr)]
                                                  q_2H = +7.9405e+04 [(/GeV/n/m3/Myr)]
                                                 q_3HE = +6.5335e+04 [(/GeV/n/m3/Myr)]
                                                 q_4HE = +3.9362e+08 [(/GeV/n/m3/Myr)]
                                                 q_12C = +2.2162e+06 [(/GeV/n/m3/Myr)]
                                                 q_13C = +2.4824e+04 [(/GeV/n/m3/Myr)]
                                                 q_14N = +3.2642e+05 [(/GeV/n/m3/Myr)]
                                                 q_14C = +0.0000e+00 [(/GeV/n/m3/Myr)]
                                                 q_15N = +1.2000e+03 [(/GeV/n/m3/Myr)]
                                                 q_16O = +2.3688e+06 [(/GeV/n/m3/Myr)]
                                                 q_17O = +8.8368e+02 [(/GeV/n/m3/Myr)]
                                                 q_18O = +4.7494e+03 [(/GeV/n/m3/Myr)]
                                                 q_19F = +1.4130e+02 [(/GeV/n/m3/Myr)]
                                                q_20NE = +3.3533e+05 [(/GeV/n/m3/Myr)]
                                                q_21NE = +8.0405e+02 [(/GeV/n/m3/Myr)]
                                                q_22NE = +2.4662e+04 [(/GeV/n/m3/Myr)]
                                                q_23NA = +5.7510e+04 [(/GeV/n/m3/Myr)]
                                                q_24MG = +6.7671e+05 [(/GeV/n/m3/Myr)]
                                                q_25MG = +8.5670e+04 [(/GeV/n/m3/Myr)]
                                                q_26MG = +9.4321e+04 [(/GeV/n/m3/Myr)]
                                                q_26AL = +0.0000e+00 [(/GeV/n/m3/Myr)]
                                                q_27AL = +8.4100e+04 [(/GeV/n/m3/Myr)]
                                                q_28SI = +6.7581e+05 [(/GeV/n/m3/Myr)]
                                                q_29SI = +3.4314e+04 [(/GeV/n/m3/Myr)]
                                                q_30SI = +2.2620e+04 [(/GeV/n/m3/Myr)]
                                            alpha_HBAR = +2.2689e+00 [-]             
                                               alpha_H = +2.3000e+00 [-]             
                                              alpha_HE = +2.2689e+00 [-]             
                                               alpha_C = +2.2689e+00 [-]             
                                               alpha_N = +2.2689e+00 [-]             
                                               alpha_O = +2.2689e+00 [-]             
                                               alpha_F = +2.2689e+00 [-]             
                                              alpha_NE = +2.2689e+00 [-]             
                                              alpha_NA = +2.2689e+00 [-]             
                                              alpha_MG = +2.2689e+00 [-]             
                                              alpha_AL = +2.2689e+00 [-]             
                                              alpha_SI = +2.2689e+00 [-]             
                                                 eta_s = -1.0000e+00 [-]             
   > PrintSummaryModel(f)

                -----------------
                  Model summary
                -----------------

      Geometry: 1D (Steady-State) => X axis (=z)
         z     [kpc]     : 10 bins in [0.000e+00,8.000e+00] (lin-step=8.8889e-01)
      --------------------------------
       1. Primary (standard) contrib.  ON
       2. Secondary contribution       ON
       3. Inelastic inter. (destruct.) ON
       4. Bremsstrahlung losses        OFF
       5. Coulomb losses               ON
       6. Inverse Compton losses       OFF
       7. Ionisation losses            ON
       8. Synchrotron losses           OFF
       9. Adiabatic losses (if Vgal)   ON
      10. Reacceleration               ON
      11. BETA-decay                   ON
      12. EC-decay                     ON
      13. BETA-fed                     ON
      14. EC-fed                       ON
      15. Tertiary contribution        ON
      16. Exotic contribution          OFF
      17. Wind                         ON
      --------------------------------
       - HE
          [formula] = 0.1
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - HI
          [formula] = 0.867
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - HII
          [formula] = 0.033
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - H2
          [formula] = 0.
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Plasma T
          [formula] = 1.e4
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Wind0
          [formula] = Vc
          - base variables: t,x,y,z
          - user-defined variables: Va,Vc,K0,delta,eta_t,Rbreak,Deltabreak,sbreak
       - VA
          [formula] = Va
          - base variables: t,x,y,z
          - user-defined variables: Va,Vc,K0,delta,eta_t,Rbreak,Deltabreak,sbreak
       - K00
          [formula] = beta^eta_t*K0*Rig^delta*(1+(Rig/Rbreak)^(Deltabreak/sbreak))^(-sbreak)
          - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
          - user-defined variables: Va,Vc,K0,delta,eta_t,Rbreak,Deltabreak,sbreak
       - Kpp
          [formula] = K00:=beta^eta_t*K0*Rig^delta*(1+(Rig/Rbreak)^(Deltabreak/sbreak))^(-sbreak);(4./3.)*(Va*1.022712e-3*beta*Etot)^2/(delta*(4-delta^2)*(4-delta)*K00)
          - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
          - user-defined variables: Va,Vc,K0,delta,eta_t,Rbreak,Deltabreak,sbreak
    Multi-CR source: ASTRO_STD (norm. parameter name 'q')
    --------------- 
       Loaded from $USINE/inputs/crcharts_Zmax30_ghost97.dat
        -> 27 CRs in ASTRO_STD
        2H-BAR,1H-BAR,1H,2H,3HE,4HE,12C,13C,14N,14C,15N,16O,17O,18O,19F,20NE,21NE,22NE,23NA,24MG,25MG,26MG,26AL,27AL,28SI,29SI,30SI
          Steady-state source = 2H-BAR ASTRO_STD
                - Spectrum
                   [formula] = q_2HBAR*beta^(eta_s)*Rig^(-alpha_HBAR)
                   - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
                   - user-defined variables: q_2HBAR,alpha_HBAR,eta_s

      => 51 free parameters:
                                                     L = +8.0000e+00 [kpc]           
                                                     h = +1.0000e-01 [kpc]           
                                                 rhole = +2.0380e-01 [kpc]           
                                                    Va = +8.5485e+01 [km/s]          
                                                    Vc = +1.9549e+01 [km/s]          
                                                    K0 = +3.5859e-02 [kpc^2/Myr]     
                                                 delta = +6.1419e-01 [-]             
                                                 eta_t = -5.9730e-02 [-]             
                                                Rbreak = +1.2589e+02 [GV]            
                                            Deltabreak = +2.2815e-01 [-]             
                                                sbreak = +1.0023e-02 [-]             
                                               q_2HBAR = +0.0000e+00 [(/GeV/n/m3/Myr)]
                                               q_1HBAR = +0.0000e+00 [(/GeV/n/m3/Myr)]
                                                  q_1H = +4.0932e+09 [(/GeV/n/m3/Myr)]
                                                  q_2H = +7.9405e+04 [(/GeV/n/m3/Myr)]
                                                 q_3HE = +6.5335e+04 [(/GeV/n/m3/Myr)]
                                                 q_4HE = +3.9362e+08 [(/GeV/n/m3/Myr)]
                                                 q_12C = +2.2162e+06 [(/GeV/n/m3/Myr)]
                                                 q_13C = +2.4824e+04 [(/GeV/n/m3/Myr)]
                                                 q_14N = +3.2642e+05 [(/GeV/n/m3/Myr)]
                                                 q_14C = +0.0000e+00 [(/GeV/n/m3/Myr)]
                                                 q_15N = +1.2000e+03 [(/GeV/n/m3/Myr)]
                                                 q_16O = +2.3688e+06 [(/GeV/n/m3/Myr)]
                                                 q_17O = +8.8368e+02 [(/GeV/n/m3/Myr)]
                                                 q_18O = +4.7494e+03 [(/GeV/n/m3/Myr)]
                                                 q_19F = +1.4130e+02 [(/GeV/n/m3/Myr)]
                                                q_20NE = +3.3533e+05 [(/GeV/n/m3/Myr)]
                                                q_21NE = +8.0405e+02 [(/GeV/n/m3/Myr)]
                                                q_22NE = +2.4662e+04 [(/GeV/n/m3/Myr)]
                                                q_23NA = +5.7510e+04 [(/GeV/n/m3/Myr)]
                                                q_24MG = +6.7671e+05 [(/GeV/n/m3/Myr)]
                                                q_25MG = +8.5670e+04 [(/GeV/n/m3/Myr)]
                                                q_26MG = +9.4321e+04 [(/GeV/n/m3/Myr)]
                                                q_26AL = +0.0000e+00 [(/GeV/n/m3/Myr)]
                                                q_27AL = +8.4100e+04 [(/GeV/n/m3/Myr)]
                                                q_28SI = +6.7581e+05 [(/GeV/n/m3/Myr)]
                                                q_29SI = +3.4314e+04 [(/GeV/n/m3/Myr)]
                                                q_30SI = +2.2620e+04 [(/GeV/n/m3/Myr)]
                                            alpha_HBAR = +2.2689e+00 [-]             
                                               alpha_H = +2.3000e+00 [-]             
                                              alpha_HE = +2.2689e+00 [-]             
                                               alpha_C = +2.2689e+00 [-]             
                                               alpha_N = +2.2689e+00 [-]             
                                               alpha_O = +2.2689e+00 [-]             
                                               alpha_F = +2.2689e+00 [-]             
                                              alpha_NE = +2.2689e+00 [-]             
                                              alpha_NA = +2.2689e+00 [-]             
                                              alpha_MG = +2.2689e+00 [-]             
                                              alpha_AL = +2.2689e+00 [-]             
                                              alpha_SI = +2.2689e+00 [-]             
                                                 eta_s = -1.0000e+00 [-]             
   > PrintSummaryTransport(f)
       - Wind0
          [formula] = Vc
          - base variables: t,x,y,z
          - user-defined variables: Va,Vc,K0,delta,eta_t,Rbreak,Deltabreak,sbreak
       - VA
          [formula] = Va
          - base variables: t,x,y,z
          - user-defined variables: Va,Vc,K0,delta,eta_t,Rbreak,Deltabreak,sbreak
       - K00
          [formula] = beta^eta_t*K0*Rig^delta*(1+(Rig/Rbreak)^(Deltabreak/sbreak))^(-sbreak)
          - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
          - user-defined variables: Va,Vc,K0,delta,eta_t,Rbreak,Deltabreak,sbreak
       - Kpp
          [formula] = K00:=beta^eta_t*K0*Rig^delta*(1+(Rig/Rbreak)^(Deltabreak/sbreak))^(-sbreak);(4./3.)*(Va*1.022712e-3*beta*Etot)^2/(delta*(4-delta^2)*(4-delta)*K00)
          - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
          - user-defined variables: Va,Vc,K0,delta,eta_t,Rbreak,Deltabreak,sbreak
   > PrintSources(f, true)
    Multi-CR source: ASTRO_STD (norm. parameter name 'q')
    --------------- 
       Loaded from $USINE/inputs/crcharts_Zmax30_ghost97.dat
        -> 27 CRs in ASTRO_STD
        2H-BAR,1H-BAR,1H,2H,3HE,4HE,12C,13C,14N,14C,15N,16O,17O,18O,19F,20NE,21NE,22NE,23NA,24MG,25MG,26MG,26AL,27AL,28SI,29SI,30SI
          Steady-state source = 2H-BAR ASTRO_STD
                - Spectrum
                   [formula] = q_2HBAR*beta^(eta_s)*Rig^(-alpha_HBAR)
                   - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
                   - user-defined variables: q_2HBAR,alpha_HBAR,eta_s


   > PrintSummary(f)

             ----------------------
               Nuclei and parents
             ----------------------

     --   0:    2H-BAR (parents:   1H-BAR,1H,4HE)
     --   1:    1H-BAR (parents:       1H,4HE)
     --   2:        1H (parents:      2H -> 30SI)
     --   3:        2H (parents:     3HE -> 30SI)
     --   4:       3HE (parents:     4HE -> 30SI)
     --   5:       4HE (parents:     6LI -> 30SI)
     --   6:       6LI (parents:     7LI -> 30SI)
     --   7:       7LI (parents:     7BE -> 30SI)
     --   8:       7BE (parents:     9BE -> 30SI)
     --   9:       9BE (parents:     10B -> 30SI)
     --  10:       10B (parents:    10BE -> 30SI)
     --  11:      10BE (parents:     11B -> 30SI)
     --  12:       11B (parents:     12C -> 30SI)
     --  13:       12C (parents:     13C -> 30SI)
     --  14:       13C (parents:     14N -> 30SI)
     --  15:       14N (parents:     14C -> 30SI)
     --  16:       14C (parents:     15N -> 30SI)
     --  17:       15N (parents:     16O -> 30SI)
     --  18:       16O (parents:     17O -> 30SI)
     --  19:       17O (parents:     18O -> 30SI)
     --  20:       18O (parents:     19F -> 30SI)
     --  21:       19F (parents:    20NE -> 30SI)
     --  22:      20NE (parents:    21NE -> 30SI)
     --  23:      21NE (parents:    22NE -> 30SI)
     --  24:      22NE (parents:    23NA -> 30SI)
     --  25:      23NA (parents:    24MG -> 30SI)
     --  26:      24MG (parents:    25MG -> 30SI)
     --  27:      25MG (parents:    26MG -> 30SI)
     --  28:      26MG (parents:    26AL -> 30SI)
     --  29:      26AL (parents:    27AL -> 30SI)
     --  30:      27AL (parents:    28SI -> 30SI)
     --  31:      28SI (parents:    29SI -> 30SI)
     --  32:      29SI (only one parent:    30SI)
     --  33:      30SI (primary - no parents)

             -----------------------
               Cosmic Ray energies
             -----------------------

      => Nuclei                  Ekn   [GeV/n]   : 33 bins in [5.000e-02,5.000e+03] (log-step=1.4330e+00)
      => Anti-nuclei             Ekn   [GeV/n]   : 33 bins in [5.000e-02,1.000e+02] (log-step=1.2681e+00)
      => E-dependent variables: beta,gamma,p,Rig,Ek,Ekn,Etot  (7 E-variables)

                   -----------
                     CR Data
                   -----------

   Data read from:
    - $USINE/inputs/crdata_crdb20170523.dat
    - $USINE/inputs/crdata_dummy.dat

   -------------------------------------------
     Data/energy used to normalise CR fluxes
   -------------------------------------------

       Qty       Sub-exps      <Ekn>
         H        PAMELA          2.000000e+01 [GeV/n]
         HE       PAMELA          2.000000e+01 [GeV/n]
         C        HEAO            1.060000e+01 [GeV/n]
         N        HEAO            1.060000e+01 [GeV/n]
         O        HEAO            1.060000e+01 [GeV/n]
         F        HEAO            1.060000e+01 [GeV/n]
         NE       HEAO            1.060000e+01 [GeV/n]
         NA       HEAO            1.060000e+01 [GeV/n]
         MG       HEAO            1.060000e+01 [GeV/n]
         AL       HEAO            1.060000e+01 [GeV/n]
         SI       HEAO            1.060000e+01 [GeV/n]
         P        HEAO            1.060000e+01 [GeV/n]
         S        HEAO            1.060000e+01 [GeV/n]
         CL       HEAO            1.060000e+01 [GeV/n]
         AR       HEAO            1.060000e+01 [GeV/n]
         K        HEAO            1.060000e+01 [GeV/n]
         CA       HEAO            1.060000e+01 [GeV/n]
         SC       HEAO            1.060000e+01 [GeV/n]
         TI       HEAO            1.060000e+01 [GeV/n]
         V        HEAO            1.060000e+01 [GeV/n]
         CR       HEAO            1.060000e+01 [GeV/n]
         MN       HEAO            1.060000e+01 [GeV/n]
         FE       HEAO            1.060000e+01 [GeV/n]
         NI       HEAO            1.060000e+01 [GeV/n]

              --------------------
                Source templates
              --------------------

       - Energy spectrum template: POWERLAW
          [formula] = q*beta^(eta_s)*Rig^(-alpha)
          - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
          - user-defined variables: q,alpha,eta_s
       - Energy spectrum template: TEST
          [formula] = q*beta^(eta_s)*Rig^(-alpha)
          - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
          - user-defined variables: q,alpha,eta_s
       - Spatial distribution template: CST
          [formula] = 1
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: DOOR
          [formula] = if((x>rup),0,if((x<rlo),0,1))
          - base variables: t,x,y,z
          - user-defined variables: rlo,rup
       - Spatial distribution template: STEPUP
          [formula] = if((x>rup),1,0)
          - base variables: t,x,y,z
          - user-defined variables: rup
       - Spatial distribution template: STEPDOWN
          [formula] = if((x<rlo),1,0)
          - base variables: t,x,y,z
          - user-defined variables: rlo
       - Spatial distribution template: CASEBHATTA96
          [formula] = pow(x/8.5,2.)*exp(-3.53*(x-8.5)/8.5)
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: SNRMODIFIED
          [formula] = pow(10.,dex*(x-rsol))*pow((x/rsol),a)*exp(b*(x-rsol)/rsol)
          - base variables: t,x,y,z
          - user-defined variables: rsol,a,b,dex
       - Spatial distribution template: STECKERJONES77
          [formula] = pow(x/10.,0.6)*exp(-3.*(x-10.)/10.)
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: STRONGMOSKA99
          [formula] = pow(x/8.5,0.5)*exp(-1.*(x-8.5)/8.5)
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: EINASTO_CYL
          [formula] = r_sp:=x*x+y*y;rhos*exp((-2/alpha)*(pow((r_sp/rs),alpha)-1))
          - base variables: t,x,y,z
          - user-defined variables: rhos,rs,alpha
       - Spatial distribution template: DUMMY
          [formula] = exp(-(x*x+y*y+z*z/8.))
          - base variables: t,x,y,z
          - user-defined variables: NONE

               ------------------
                 Cross sections
               ------------------

     -- Cross section files used:
        - $USINE/inputs/XS_NUCLEI/sigInelTripathi99+Coste12.dat
        - $USINE/inputs/XS_ANTINUC/sigInelANN_pbar+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/sigInelANN_dbar+HHe_Duperray05.dat
        - $USINE/inputs/XS_NUCLEI/sigProdGALPROP17_OPT12.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_dbar_1H4He+HHe_Duperray05_Coal79MeV.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_dbar_pbar+HHe_Duperray05_Coal79MeV.dat
        - $USINE/inputs/XS_ANTINUC/sigInelNONANN_pbar+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/sigInelNONANN_dbar+HHe_Duperray05.dat
        - $USINE/inputs/XS_ANTINUC/dSdENAR_pbar+HHe_Duperray05_Anderson.dat
        - $USINE/inputs/XS_ANTINUC/dSdENAR_dbar+HHe_Duperray05_Anderson.dat
     -- Targets loaded: H,HE
     -- Tertiaries loaded: 1H-BAR,2H-BAR

                -----------------
                  Model summary
                -----------------

      Geometry: 1D (Steady-State) => X axis (=z)
         z     [kpc]     : 10 bins in [0.000e+00,8.000e+00] (lin-step=8.8889e-01)
      --------------------------------
       1. Primary (standard) contrib.  ON
       2. Secondary contribution       ON
       3. Inelastic inter. (destruct.) ON
       4. Bremsstrahlung losses        OFF
       5. Coulomb losses               ON
       6. Inverse Compton losses       OFF
       7. Ionisation losses            ON
       8. Synchrotron losses           OFF
       9. Adiabatic losses (if Vgal)   ON
      10. Reacceleration               ON
      11. BETA-decay                   ON
      12. EC-decay                     ON
      13. BETA-fed                     ON
      14. EC-fed                       ON
      15. Tertiary contribution        ON
      16. Exotic contribution          OFF
      17. Wind                         ON
      --------------------------------
       - HE
          [formula] = 0.1
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - HI
          [formula] = 0.867
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - HII
          [formula] = 0.033
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - H2
          [formula] = 0.
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Plasma T
          [formula] = 1.e4
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Wind0
          [formula] = Vc
          - base variables: t,x,y,z
          - user-defined variables: Va,Vc,K0,delta,eta_t,Rbreak,Deltabreak,sbreak
       - VA
          [formula] = Va
          - base variables: t,x,y,z
          - user-defined variables: Va,Vc,K0,delta,eta_t,Rbreak,Deltabreak,sbreak
       - K00
          [formula] = beta^eta_t*K0*Rig^delta*(1+(Rig/Rbreak)^(Deltabreak/sbreak))^(-sbreak)
          - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
          - user-defined variables: Va,Vc,K0,delta,eta_t,Rbreak,Deltabreak,sbreak
       - Kpp
          [formula] = K00:=beta^eta_t*K0*Rig^delta*(1+(Rig/Rbreak)^(Deltabreak/sbreak))^(-sbreak);(4./3.)*(Va*1.022712e-3*beta*Etot)^2/(delta*(4-delta^2)*(4-delta)*K00)
          - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
          - user-defined variables: Va,Vc,K0,delta,eta_t,Rbreak,Deltabreak,sbreak
    Multi-CR source: ASTRO_STD (norm. parameter name 'q')
    --------------- 
       Loaded from $USINE/inputs/crcharts_Zmax30_ghost97.dat
        -> 27 CRs in ASTRO_STD
        2H-BAR,1H-BAR,1H,2H,3HE,4HE,12C,13C,14N,14C,15N,16O,17O,18O,19F,20NE,21NE,22NE,23NA,24MG,25MG,26MG,26AL,27AL,28SI,29SI,30SI
          Steady-state source = 2H-BAR ASTRO_STD
                - Spectrum
                   [formula] = q_2HBAR*beta^(eta_s)*Rig^(-alpha_HBAR)
                   - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
                   - user-defined variables: q_2HBAR,alpha_HBAR,eta_s

      => 51 free parameters:
                                                     L = +8.0000e+00 [kpc]           
                                                     h = +1.0000e-01 [kpc]           
                                                 rhole = +2.0380e-01 [kpc]           
                                                    Va = +8.5485e+01 [km/s]          
                                                    Vc = +1.9549e+01 [km/s]          
                                                    K0 = +3.5859e-02 [kpc^2/Myr]     
                                                 delta = +6.1419e-01 [-]             
                                                 eta_t = -5.9730e-02 [-]             
                                                Rbreak = +1.2589e+02 [GV]            
                                            Deltabreak = +2.2815e-01 [-]             
                                                sbreak = +1.0023e-02 [-]             
                                               q_2HBAR = +0.0000e+00 [(/GeV/n/m3/Myr)]
                                               q_1HBAR = +0.0000e+00 [(/GeV/n/m3/Myr)]
                                                  q_1H = +4.0932e+09 [(/GeV/n/m3/Myr)]
                                                  q_2H = +7.9405e+04 [(/GeV/n/m3/Myr)]
                                                 q_3HE = +6.5335e+04 [(/GeV/n/m3/Myr)]
                                                 q_4HE = +3.9362e+08 [(/GeV/n/m3/Myr)]
                                                 q_12C = +2.2162e+06 [(/GeV/n/m3/Myr)]
                                                 q_13C = +2.4824e+04 [(/GeV/n/m3/Myr)]
                                                 q_14N = +3.2642e+05 [(/GeV/n/m3/Myr)]
                                                 q_14C = +0.0000e+00 [(/GeV/n/m3/Myr)]
                                                 q_15N = +1.2000e+03 [(/GeV/n/m3/Myr)]
                                                 q_16O = +2.3688e+06 [(/GeV/n/m3/Myr)]
                                                 q_17O = +8.8368e+02 [(/GeV/n/m3/Myr)]
                                                 q_18O = +4.7494e+03 [(/GeV/n/m3/Myr)]
                                                 q_19F = +1.4130e+02 [(/GeV/n/m3/Myr)]
                                                q_20NE = +3.3533e+05 [(/GeV/n/m3/Myr)]
                                                q_21NE = +8.0405e+02 [(/GeV/n/m3/Myr)]
                                                q_22NE = +2.4662e+04 [(/GeV/n/m3/Myr)]
                                                q_23NA = +5.7510e+04 [(/GeV/n/m3/Myr)]
                                                q_24MG = +6.7671e+05 [(/GeV/n/m3/Myr)]
                                                q_25MG = +8.5670e+04 [(/GeV/n/m3/Myr)]
                                                q_26MG = +9.4321e+04 [(/GeV/n/m3/Myr)]
                                                q_26AL = +0.0000e+00 [(/GeV/n/m3/Myr)]
                                                q_27AL = +8.4100e+04 [(/GeV/n/m3/Myr)]
                                                q_28SI = +6.7581e+05 [(/GeV/n/m3/Myr)]
                                                q_29SI = +3.4314e+04 [(/GeV/n/m3/Myr)]
                                                q_30SI = +2.2620e+04 [(/GeV/n/m3/Myr)]
                                            alpha_HBAR = +2.2689e+00 [-]             
                                               alpha_H = +2.3000e+00 [-]             
                                              alpha_HE = +2.2689e+00 [-]             
                                               alpha_C = +2.2689e+00 [-]             
                                               alpha_N = +2.2689e+00 [-]             
                                               alpha_O = +2.2689e+00 [-]             
                                               alpha_F = +2.2689e+00 [-]             
                                              alpha_NE = +2.2689e+00 [-]             
                                              alpha_NA = +2.2689e+00 [-]             
                                              alpha_MG = +2.2689e+00 [-]             
                                              alpha_AL = +2.2689e+00 [-]             
                                              alpha_SI = +2.2689e+00 [-]             
                                                 eta_s = -1.0000e+00 [-]             

N.B.: run ./bin/usine to see the model in action!
