
                ----------------
                  TUMath::TEST
                ----------------

- Class content: The namespace TUMath contains some
useful mathematical functions. Only a few of them are
tested below.

 * Binary search (examples using TMath from ROOT):
   Consider the list[n=6]={-3, 2, 4, 10, 20, 25}.
   The algorithm TUMath::BinarySearch returns the index
   i for which val<list[i] (and val>list[i-1]).
   > TUMath::BinarySearch(n=6, list, sought=3.000000);  =>  index=1
   > TUMath::BinarySearch(n=6, list, sought=15.000000);  =>  index=3
   > TUMath::BinarySearch(n=6, list, sought=-1.000000);  =>  index=0
   > TUMath::BinarySearch(n=6, list, sought=-7.000000);  =>  index=-1
   > TUMath::BinarySearch(n=6, list, sought=40.000000);  =>  index=5

 * Interpolation:
   Consider (x,y)[4] = { (1.,1.), (10.,2.), (100.,3.), (1000.,3.) }
   > Interpolate(x, n, y, x_new, n_new, y_interp, kLOGLIN_INFNULL_SUPCST, is_verbose=true, f_out=f);

   ------- WARNING in TUMath::Interpolate -------
       Message: Extrapolation required for k=0
   ------- WARNING in TUMath::Interpolate -------
x_new[0]=1.000000e-01   x[0]=1.000000e+00   x[n-1]=1.000000e+03

   ------- WARNING in TUMath::Interpolate -------
       Message: Extrapolation required for k=4
   ------- WARNING in TUMath::Interpolate -------
x_new[4]=2.000000e+03   x[0]=1.000000e+00   x[n-1]=1.000000e+03
     -> for x = 1.000000e-01, y_interp = 0.000000e+00
     -> for x = 3.162277e+00, y_interp = 1.500000e+00
     -> for x = 9.900000e+00, y_interp = 1.995635e+00
     -> for x = 1.010000e+02, y_interp = 3.004321e+00
     -> for x = 2.000000e+03, y_interp = 4.000000e+00
   > Interpolate(5, x[0], x[1], y[0], y[1], kLINLIN);  => 1.444444e+00
   > Interpolate(5, x[0], x[1], y[0], y[1], kLINLOG);  => 1.360790e+00
   > Interpolate(5, x[0], x[1], y[0], y[1], kLOGLIN);  => 1.698970e+00
   > Interpolate(5, x[0], x[1], y[0], y[1], kLOGLOG);  => 1.623345e+00

 * Test quantiles (10,50,75) from 100 Gaussian distributed values (mean=3., sigma=1.)
  > Quantiles(values, quantiles, v_q);
    - quantile=10.000000  =>  val_quantile=2.208811e+00
    - quantile=50.000000  =>  val_quantile=3.250111e+00
    - quantile=75.000000  =>  val_quantile=3.863008e+00


 * Draw from covariance matrix (and mean values):
   Consider 5 parameters with mean=1 and diagonal of cov_ii = 2*(i+1)
      - draw 10000 realizations using DrawFromMultiGauss()
      - calculate the sample mean and variance
     => check that equal to input values

    - i=0: mu_i=1, sqrt(cov_ii)=2  =>  <mu_i>=9.918908e-01, <sigma_i>=2.008631e+00 (10000 realisation)
    - i=1: mu_i=1, sqrt(cov_ii)=4  =>  <mu_i>=1.023559e+00, <sigma_i>=3.967883e+00 (10000 realisation)
    - i=2: mu_i=1, sqrt(cov_ii)=6  =>  <mu_i>=1.023838e+00, <sigma_i>=6.029785e+00 (10000 realisation)
    - i=3: mu_i=1, sqrt(cov_ii)=8  =>  <mu_i>=1.031938e+00, <sigma_i>=7.941161e+00 (10000 realisation)
    - i=4: mu_i=1, sqrt(cov_ii)=10  =>  <mu_i>=9.232108e-01, <sigma_i>=9.983807e+00 (10000 realisation)

 * Bessel functions (several representations):
   => 1st-zero of J0=2.404826e+00, J0(3.)=-2.600520e-01/-2.600520e-01, J1(3.)=3.390590e-01

 * Comparison of bessel calculations using various approximations
   (from numerical recipes and Gradshteyn & Ryzhik).
                   --- J0(x) ---

        x                 Num.Rec.          Integral repr.        Gamma repr.
          small x  =>      (OK)                   (OK)              (fails)
          for x>>1 =>      (OK)        (time-consuming, fails) (the most accurate)
        x=1.000e-01    9.9750156477e-01    9.9750156207e-01          -
        x=2.500e-01    9.8443593136e-01    9.8443592930e-01          -
        x=6.250e-01    9.0470222213e-01    9.0470222300e-01          -
        x=1.562e+00    4.7669990972e-01    4.7669990967e-01          -
        x=3.906e+00   -4.0164803065e-01   -4.0164803309e-01   -3.9864457212e-01
        x=9.766e+00   -2.2894228794e-01   -2.2894228804e-01   -2.2894330166e-01
        x=2.441e+01    9.9404944230e-03    9.9404945230e-03    9.9405914838e-03
        x=6.104e+01   -8.6666110999e-02   -8.6666110986e-02   -8.6666108892e-02
        x=1.526e+02    3.4614701523e-02    3.4614701500e-02    3.4614701365e-02
        x=3.815e+02   -3.4799486028e-02   -3.4799486025e-02   -3.4799486022e-02
        x=9.537e+02   -1.4258127099e-02   -1.4259155369e-02   -1.4258127090e-02
        x=2.384e+03   -7.8672806652e-03   -7.8680318918e-03   -7.8672806761e-03
        x=5.960e+03   -1.0303493648e-02   -1.0303573779e-02   -1.0303493651e-02


                   --- J1(x) ---

        x                 Num.Rec.          Integral repr.        Gamma repr.        Series repr.
          small x  =>      (OK)                   (OK)              (fails)        (only for x<<1)
          for x>>1 =>      (OK)        (time-consuming, fails) (the most accurate)     (fails)
        x=1.000e-03    4.9999993758e-04    4.9999993750e-04          -             4.9999993750e-04
        x=2.500e-03    1.2499990236e-03    1.2499990234e-03          -             1.2499990234e-03
        x=6.250e-03    3.1249847417e-03    3.1249847412e-03          -             3.1249847412e-03
        x=1.562e-02    7.8122615850e-03    7.8122615838e-03          -             7.8122615790e-03
        x=3.906e-02    1.9527524949e-02    1.9527524947e-02          -             1.9527524473e-02
        x=9.766e-02    4.8769940471e-02    4.8769940464e-02          -             4.8769894205e-02
        x=2.441e-01    1.2116307376e-01    1.2116307375e-01          -             1.2115855624e-01
        x=6.104e-01    2.9118380403e-01    2.9118380404e-01          -                   -
        x=1.526e+00    5.6142095429e-01    5.6142095430e-01          -                   -
        x=3.815e+00    6.8653566363e-03    6.8653566246e-03          -                   -
        x=9.537e+00    1.5342341840e-01    1.5342431578e-01    1.5341424490e-01          -
        x=2.384e+01   -1.4424442406e-01   -1.4424442380e-01   -1.4424467970e-01          -
        x=5.960e+01    7.8642884569e-02    7.8642884544e-02    7.8642870737e-02          -
        x=1.490e+02   -3.5485113096e-02   -3.5485113151e-02   -3.5485113881e-02          -
        x=3.725e+02    3.5581836480e-02    3.5581836507e-02    3.5581836525e-02          -
        x=9.313e+02    1.5321869200e-02    1.5321869356e-02    1.5321869223e-02          -
        x=2.328e+03    6.4233760509e-03    6.4227045487e-03    6.4233760404e-03          -
        x=5.821e+03    1.0288381873e-02    1.0288054005e-02    1.0288381874e-02          -

                   --- J1(zeroJ0_i) ---

          i       zeroJ0_i          J1_zeroJ0 (for x>>1)  Num.Rec.
            0  2.40482556e+00    5.1914749738e-01    5.1914749738e-01
            2  8.65372791e+00    2.7145226289e-01    2.7145226289e-01
            6  2.12116366e+01    1.7326589415e-01    1.7326589422e-01
           14  4.63411884e+01    1.1721119885e-01    1.1721119889e-01
           30  9.66052680e+01    8.1178788294e-02    8.1178788320e-02
           62  1.97135573e+02    5.6827461958e-02    5.6827461975e-02
          126  3.98197183e+02    3.9984451334e-02    3.9984451345e-02
          254  8.00320885e+02    2.8203826129e-02    2.8203826137e-02
          510  1.60456853e+03    1.9918697556e-02    1.9918697562e-02
         1022  3.21306393e+03    1.4076036390e-02    1.4076036394e-02
         2046  6.43005478e+03    9.9502209703e-03    9.9502209732e-03
         4094  1.28640365e+04    7.0347947243e-03    7.0347947264e-03
         8190  2.57320000e+04    4.9739714686e-03    4.9739714700e-03
        16382  5.14679270e+04    3.5169947742e-03    3.5169947753e-03
        32766  1.02939781e+05    2.4868434183e-03    2.4868434190e-03
        65534  2.05883489e+05    1.7584470744e-03    1.7584470749e-03
